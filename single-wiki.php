<?php

get_header();

    /* Start the Loop */
    while ( have_posts() ) :
        the_post(); ?>

        <?php $post_id = get_the_ID(); ?>
        <?php $fields = get_fields($post_id) ?>

        <div class="mainContainer by-padding">
            <div class="game-info-content">
                <div class="brief-information">
                    <?php
                    foreach ($fields['fields'] as $field) {
                        echo '
                             <div class="info-row">
                                 <span class="property-name"> '.$field['title'].' </span>
                                 <span class="property-value"> '.$field['value'].' </span>
                             </div>
                        ';
                    }
                    ?>
                </div>
                <div class="text-information">
                    <?php echo the_content() ?>
                </div>
            </div>








            <?php
            $downloads = $fields['downloads'];
            $playStation = $downloads['play_station'];
            $xbox = $downloads['xbox'];
            $pc = $downloads['pc'];
            ?>

            <div class="description-wrap" id="description-wrap">
                <ul class="tab-holder">
                    <?php
                    if( $playStation['us'] || $playStation['europe'] || $playStation['uk'] ) { ?>
                        <li id="aboutGame" class="active">
                            <svg viewBox="0 0 585.15 451.63">
                                <use xlink:href="<?php echo sprite_url; ?>#playStation"></use>
                            </svg>
                            پلی استیشن
                        </li>
                    <?php } ?>

                    <?php
                    if( $pc['battle_net'] || $pc['epic_games'] || $pc['bethesda_net'] || $pc['gog_com'] || $pc['origin'] || $pc['rockstar'] || $pc['steam'] || $pc['uplay'] || $pc['nintendo'] ) { ?>
                        <li id="shoppingGuid">
                            <svg viewBox="0 0 37.01 25.03">
                                <use xlink:href="<?php echo sprite_url; ?>#pc"></use>
                            </svg>
                            کامپیوتر
                        </li>
                    <?php } ?>

                    <?php
                    if( $xbox ) { ?>
                        <li id="requiredsystem">
                            <svg viewBox="0 0 565.28 566.27">
                                <use xlink:href="<?php echo sprite_url; ?>#xbox"></use>
                            </svg>
                            ایکس باکس
                        </li>
                    <?php } ?>
                </ul>

                <div class="tab-content-holder">
                    <div id="aboutGame-content" class="tab-content active">
                        <?php
                        if( $playStation['us'] ) { ?>
                            <div class="product-wrap">
                                <div class="group-product">
                                    <div class="group-head">
                                        <h2 class="group-title ">
                                            <div class="icon full-img"><img class="" src="https://cdn.igame.ir/2020/09/usa.jpg" alt="USA"></div>
                                            ریجن آمریکا
                                        </h2>
                                    </div>

                                    <div class="giftcard-list">
                                        <?php
                                        foreach ( $playStation['us'] as $link ) {
                                            echo '
                                                <div class="giftcard-item">
                                                   <span class="title"><span>'.$link['name'].'</span></span>
                                                   <div class="price">'.$link['size'].'</div>
                                                    <div class="buy-action d-flex align-items-center">
                                                         <a download class="btn btn--red btn--small" href="'.$link['link'].'"> دانلود </a>
                                                    </div>
                                                </div>
                                            ';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php
                        if( $playStation['europe'] ) { ?>
                            <div class="product-wrap">
                                <div class="group-product">
                                    <div class="group-head">
                                        <h2 class="group-title ">
                                            <div class="icon full-img"></div>
                                            ریجن اروپا
                                        </h2>
                                    </div>

                                    <div class="giftcard-list">
                                        <?php
                                        foreach ( $playStation['europe'] as $link ) {
                                            echo '
                                                <div class="giftcard-item">
                                                   <span class="title"><span>'.$link['name'].'</span></span>
                                                   <div class="price">'.$link['size'].'</div>
                                                    <div class="buy-action d-flex align-items-center">
                                                         <a download class="btn btn--red btn--small" href="'.$link['link'].'"> دانلود </a>
                                                    </div>
                                                </div>
                                            ';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php
                        if( $playStation['uk'] ) { ?>
                            <div class="product-wrap">
                                <div class="group-product">
                                    <div class="group-head">
                                        <h2 class="group-title ">
                                            <div class="icon full-img"><img class="" src="https://cdn.igame.ir/2020/09/england.jpg" alt="UK"></div>
                                            ریجن UK
                                        </h2>
                                    </div>

                                    <div class="giftcard-list">
                                        <?php
                                        foreach ( $playStation['uk'] as $link ) {
                                            echo '
                                                <div class="giftcard-item">
                                                   <span class="title"><span>'.$link['name'].'</span></span>
                                                   <div class="price">'.$link['size'].'</div>
                                                    <div class="buy-action d-flex align-items-center">
                                                         <a download class="btn btn--red btn--small" href="'.$link['link'].'"> دانلود </a>
                                                    </div>
                                                </div>
                                            ';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>


                    <div id="shoppingGuid-content" class="tab-content">
                        <?php
                        if( $pc['epic_games'] ) { ?>
                            <div class="product-wrap">
                                <div class="group-product">
                                    <div class="group-head">
                                        <h2 class="group-title ">
                                            Epic Games
                                        </h2>
                                    </div>

                                    <div class="giftcard-list">
                                        <?php
                                        foreach ( $pc['epic_games'] as $link ) {
                                            echo '
                                                <div class="giftcard-item">
                                                   <span class="title"><span>'.$link['name'].'</span></span>
                                                   <div class="price">'.$link['size'].'</div>
                                                    <div class="buy-action d-flex align-items-center">
                                                         <a download class="btn btn--red btn--small" href="'.$link['link'].'"> دانلود </a>
                                                    </div>
                                                </div>
                                            ';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php
                        if( $pc['battle_net'] ) { ?>
                            <div class="product-wrap">
                                <div class="group-product">
                                    <div class="group-head">
                                        <h2 class="group-title ">
                                            Battle.net
                                        </h2>
                                    </div>

                                    <div class="giftcard-list">
                                        <?php
                                        foreach ( $pc['battle_net'] as $link ) {
                                            echo '
                                                <div class="giftcard-item">
                                                   <span class="title"><span>'.$link['name'].'</span></span>
                                                   <div class="price">'.$link['size'].'</div>
                                                    <div class="buy-action d-flex align-items-center">
                                                         <a download class="btn btn--red btn--small" href="'.$link['link'].'"> دانلود </a>
                                                    </div>
                                                </div>
                                            ';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php
                        if( $pc['bethesda_net'] ) { ?>
                            <div class="product-wrap">
                                <div class="group-product">
                                    <div class="group-head">
                                        <h2 class="group-title ">
                                            Bethesda.net
                                        </h2>
                                    </div>

                                    <div class="giftcard-list">
                                        <?php
                                        foreach ( $pc['bethesda_net'] as $link ) {
                                            echo '
                                                <div class="giftcard-item">
                                                   <span class="title"><span>'.$link['name'].'</span></span>
                                                   <div class="price">'.$link['size'].'</div>
                                                    <div class="buy-action d-flex align-items-center">
                                                         <a download class="btn btn--red btn--small" href="'.$link['link'].'"> دانلود </a>
                                                    </div>
                                                </div>
                                            ';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php
                        if( $pc['gog_com'] ) { ?>
                            <div class="product-wrap">
                                <div class="group-product">
                                    <div class="group-head">
                                        <h2 class="group-title ">
                                            Gog.com
                                        </h2>
                                    </div>

                                    <div class="giftcard-list">
                                        <?php
                                        foreach ( $pc['gog_com'] as $link ) {
                                            echo '
                                                <div class="giftcard-item">
                                                   <span class="title"><span>'.$link['name'].'</span></span>
                                                   <div class="price">'.$link['size'].'</div>
                                                    <div class="buy-action d-flex align-items-center">
                                                         <a download class="btn btn--red btn--small" href="'.$link['link'].'"> دانلود </a>
                                                    </div>
                                                </div>
                                            ';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php
                        if( $pc['nintendo'] ) { ?>
                            <div class="product-wrap">
                                <div class="group-product">
                                    <div class="group-head">
                                        <h2 class="group-title ">
                                            Nintendo
                                        </h2>
                                    </div>

                                    <div class="giftcard-list">
                                        <?php
                                        foreach ( $pc['nintendo'] as $link ) {
                                            echo '
                                                <div class="giftcard-item">
                                                   <span class="title"><span>'.$link['name'].'</span></span>
                                                   <div class="price">'.$link['size'].'</div>
                                                    <div class="buy-action d-flex align-items-center">
                                                         <a download class="btn btn--red btn--small" href="'.$link['link'].'"> دانلود </a>
                                                    </div>
                                                </div>
                                            ';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php
                        if( $pc['origin'] ) { ?>
                            <div class="product-wrap">
                                <div class="group-product">
                                    <div class="group-head">
                                        <h2 class="group-title ">
                                            Origin
                                        </h2>
                                    </div>

                                    <div class="giftcard-list">
                                        <?php
                                        foreach ( $pc['origin'] as $link ) {
                                            echo '
                                                <div class="giftcard-item">
                                                   <span class="title"><span>'.$link['name'].'</span></span>
                                                   <div class="price">'.$link['size'].'</div>
                                                    <div class="buy-action d-flex align-items-center">
                                                         <a download class="btn btn--red btn--small" href="'.$link['link'].'"> دانلود </a>
                                                    </div>
                                                </div>
                                            ';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php
                        if( $pc['rockstar'] ) { ?>
                            <div class="product-wrap">
                                <div class="group-product">
                                    <div class="group-head">
                                        <h2 class="group-title ">
                                            Rockstar
                                        </h2>
                                    </div>

                                    <div class="giftcard-list">
                                        <?php
                                        foreach ( $pc['rockstar'] as $link ) {
                                            echo '
                                                <div class="giftcard-item">
                                                   <span class="title"><span>'.$link['name'].'</span></span>
                                                   <div class="price">'.$link['size'].'</div>
                                                    <div class="buy-action d-flex align-items-center">
                                                         <a download class="btn btn--red btn--small" href="'.$link['link'].'"> دانلود </a>
                                                    </div>
                                                </div>
                                            ';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php
                        if( $pc['steam'] ) { ?>
                            <div class="product-wrap">
                                <div class="group-product">
                                    <div class="group-head">
                                        <h2 class="group-title ">
                                            Steam
                                        </h2>
                                    </div>

                                    <div class="giftcard-list">
                                        <?php
                                        foreach ( $pc['steam'] as $link ) {
                                            echo '
                                                <div class="giftcard-item">
                                                   <span class="title"><span>'.$link['name'].'</span></span>
                                                   <div class="price">'.$link['size'].'</div>
                                                    <div class="buy-action d-flex align-items-center">
                                                         <a download class="btn btn--red btn--small" href="'.$link['link'].'"> دانلود </a>
                                                    </div>
                                                </div>
                                            ';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php
                        if( $pc['uplay'] ) { ?>
                            <div class="product-wrap">
                                <div class="group-product">
                                    <div class="group-head">
                                        <h2 class="group-title ">
                                            Uplay
                                        </h2>
                                    </div>

                                    <div class="giftcard-list">
                                        <?php
                                        foreach ( $pc['uplay'] as $link ) {
                                            echo '
                                                <div class="giftcard-item">
                                                   <span class="title"><span>'.$link['name'].'</span></span>
                                                   <div class="price">'.$link['size'].'</div>
                                                    <div class="buy-action d-flex align-items-center">
                                                         <a download class="btn btn--red btn--small" href="'.$link['link'].'"> دانلود </a>
                                                    </div>
                                                </div>
                                            ';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>


                    <div id="requiredsystem-content" class="tab-content">
                        <?php
                        if( $xbox ) { ?>
                            <div class="product-wrap">
                                <div class="group-product">
                                    <div class="giftcard-list">
                                        <?php
                                        foreach ( $xbox as $link ) {
                                            echo '
                                                <div class="giftcard-item">
                                                   <span class="title"><span>'.$link['name'].'</span></span>
                                                   <div class="price">'.$link['size'].'</div>
                                                    <div class="buy-action d-flex align-items-center">
                                                         <a download class="btn btn--red btn--small" href="'.$link['link'].'"> دانلود </a>
                                                    </div>
                                                </div>
                                            ';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

    <?php endwhile; // End of the loop.

get_footer();