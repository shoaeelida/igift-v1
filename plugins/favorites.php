<?php

add_filter('favorites/button/html', 'favorites_html', 0 , 3);
add_filter('favorites/button/html', 'favorites_html', 0 , 3);

function favorites_html( $html, $post_id , $site_id ){
    if( is_wc_endpoint_url( 'favorites' ) ){
        // replace active_heart_svg
        $html = str_replace('{active_heart_svg}', '<svg viewBox="0 0 75.29 75.29"><use xlink:href="'.sprite_url.'#close"></use></svg>',$html);
        // replace heart_svg
        $html = str_replace('{heart_svg}', '<svg viewBox="0 0 75.29 75.29"><use xlink:href="'.sprite_url.'#close"></use></svg>',$html);
        // add حذف tooltip
        $html .= 'حذف';
    }elseif( is_account_page() ){
        // replace active_heart_svg
        $html = str_replace('{active_heart_svg}', '<svg viewBox="0 0 75.29 75.29"><use xlink:href="'.sprite_url.'#close"></use></svg>',$html);
        // replace heart_svg
        $html = str_replace('{heart_svg}', '<svg viewBox="0 0 75.29 75.29"><use xlink:href="'.sprite_url.'#close"></use></svg>',$html);
        // add حذف tooltip
        $html .= '<span class="tooltip">حذف</span>';
    }else{
        $html = str_replace('{active_heart_svg}', '<svg viewBox="0 0 195.2 178.61"><use xlink:href="'.sprite_url.'#heart"></use></svg>',$html);
        $html = str_replace('{heart_svg}', '<svg viewBox="0 0 195.2 178.61"><use xlink:href="'.sprite_url.'#heart"></use></svg>',$html);
    }
    return $html;

}