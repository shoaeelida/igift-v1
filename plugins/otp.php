<?php

function show_modal() {
    get_template_part('template-parts/user/loginModal');
}
add_action( 'wp_footer', 'show_modal' );



/**
 * OTP SEND AJAX
 **/
add_action('wp_ajax_otp_send' , 'otp_send');
add_action('wp_ajax_nopriv_otp_send','otp_send');
function otp_send(){
    $phone = $_POST['phone'];
    $_SESSION['phone'] = $phone;
    send_verify_code($phone);
    wp_die();
}

/**
 * OTP VERIFY AJAX
 **/
add_action('wp_ajax_otp_verify' , 'otp_verify');
add_action('wp_ajax_nopriv_otp_verify','otp_verify');
function otp_verify(){
    $code = $_POST['code'];
    verify_code($code);
    wp_die();
}


/**
 * OTP EXPIRE AJAX
 **/
add_action('wp_ajax_otp_expire' , 'otp_expire');
add_action('wp_ajax_nopriv_otp_expire','otp_expire');
function otp_expire(){
    $_SESSION['verify_code'] = 0;
    echo json_response(200, 'کد تایید منقضی شد');
    wp_die();
}




function json_response($code, $message = [], array $errors = [])
{

    $status = array(
        200 => '200 OK',
        400 => '400 Bad Request',
        403 => '403 Forbidden',
        422 => 'Unprocessable Entity',
        500 => '500 Internal Server Error'
    );

    if( !isset($status[$code]) ){
        $code = 500;
    }

    // clear the old headers
    header_remove();
    // set the actual code
    http_response_code($code);
    // set the header to make sure cache is forced
    header("Cache-Control: no-transform,public,max-age=300,s-maxage=900");
    header("Access-Control-Allow-Origin: *");
    // treat this as json
    header("Content-Type: application/json; charset=UTF-8");

    // ok, validation error, or failure
    header('Status: '.$status[$code]);
    // return the encoded json
    return json_encode(array(
        'response' => ( !$message || empty($message) ) ? [] : $message,
        'errors' => ( !$errors || empty($errors) ) ? [] : $errors,
        'status' => ( $code < 300 && empty($errors) ) ? true : false, // success or not?
    ));

}


function send_verify_code ($to) {
    $errors = [];

    if( $to != '' || !empty($to) ) {
        if( !in_array($to,['09131131313']) ){
            $generated_code = rand(100000, 999999);
        }else{
            $generated_code = 123456;
        }
    }

    $message = ' رمز یکبار مصرف شما '.$generated_code.' می باشد. آی گیفت ';

    $_SESSION['verify_code'] = $generated_code;

    $sms_send = playSendSms($to ,$message, array());


    if( $to != '' || !empty($to) ) {
        if( $sms_send ) {
            echo json_response(200, array(
                'sent' => true,
            ));
        }
        else {
            $errors[] = 'ارسال کد تایید با شکست مواجه شد';
            echo json_response(200, array('sent' => false), $errors);
        }
    }
    else {
        $errors[] = 'لطفا شماره موبایل را بصورت صحیح وارد کنید';
        echo json_response(200, array('sent' => false,), $errors);
    }

}




function passless_login ($id) {
    if ( !is_wp_error( get_user_by('ID', $id) ) )
    {
        wp_clear_auth_cookie();
        wp_set_current_user ( $id );
        wp_set_auth_cookie  ( $id );
        $redirect_to = user_admin_url();
        wp_safe_redirect( $redirect_to );
        exit();
    }
}


function verify_code ($code) {

    $errors = [];
    if( $code != '' || !empty($code) ) {
        if( $code == $_SESSION['verify_code'] ) {
            $existing1 = username_exists($_SESSION['phone']);
            $existing2 = username_exists(substr($_SESSION['phone'], 1));

            if ( !$existing1 && !$existing2 ) {
                $userdata = array(
                    'user_login'    =>   $_SESSION['phone'],
                    'user_pass'     =>   md5('12345678'),
                    'display_name'     =>   $_SESSION['phone'],
                    'role' => 'customer'
                );
                $user_id = wp_insert_user( $userdata );
                if( $user_id ) {
                    update_user_meta( $user_id, 'digits_phone_no', $_SESSION['phone'] );
                    update_user_meta( $user_id, '_billing_phone', $_SESSION['phone'] );
                    echo json_response(200, array(
                        'verified' => true,
                    ));
                }
                passless_login($user_id);
            }
            else if ( $existing1 || $existing2) {
                echo json_response(200, array(
                    'verified' => true,
                ));
                if( $existing1 ) {
                    passless_login($existing1);
                }
                if( $existing2 ) {
                    passless_login($existing2);
                }
            }
        }
        else {
            $errors[] = 'کد وارد شده صحیح نیست و یا منقضی شده است';
            echo json_response(200, array('verified' => false,), $errors);
        }
    }
    else {
        $errors[] = 'لطفا کد پیامک شده را وارد نمایید';
        echo json_response(200, array('verified' => false,), $errors);
    }

}