<?php


/**
    * Template Name: Confirm
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

        <article id="post-118" class="post-118 page type-page status-publish hentry entry">
		<header class="entry-header">
		
<h1 class="entry-title">تایید حساب کاربری</h1>
	</header>
	
	<div class="entry-content">

    <?php
    /*
        <ul class="collapsible popout">
<li class="active">
<div class="collapsible-header red-text" tabindex="0"><i class="material-icons">phone_iphone</i>تایید شماره موبایل</div>
<div class="collapsible-body" style="display: block;">
<div class="nikan-confirm-box">
<div class="nikan-confirm-notice" id="mobile-response" style="display: none;"></div>
<div id="confirm-sms-send">
<form method="post" action="confirmsms_send" id="sms-send-form" _lpchecked="1">
<div class="row">
<div class="col s6 input-field">
							<i class="material-icons prefix">phone_iphone</i><br />
							<input type="text" pattern="[0-9]*|[۰-۹]*" class="validate valid" data-length="11" name="user_mobile" id="user_mobile" placeholder="مانند: 09123456789" value="" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAZ9JREFUOBGVU7uKwkAUPXmID5AttNyFYBGwsLGwFBUFF/wOhfyE5jPcxkZt/IHFxg+wsZJtrFwS8NWIohZm545xNkp8XcjMnbnnnJk790YyTfPTcZwm+z7whEmSNGWwaqPR+Ca4/AqZCO5BX+STkcBTJ5/gp9HLkb2BR34kEoGu6xewlwQ0TUOxWPQXCIVCIhAMBsEeS6y9MbHpOirNlUoF6XQanU4Hq9UKhmHAsiy0Wq2L2DWZ1i+l4Ccg1et1hwJ0zd1uxzGUwn6/98OLPZbiL1vUxA3OZEI8IhOGlfKdTU3+BrThZ5lMBoVCAev1Gr1eD7PZDIFAALIs80NIRNzAT4DIw+EQm80G2WyWQ1KpFHK5nICr1NvezhIR5iyXSyQSCUSjUSiKgnK5jGQyCVVVEYvF0O12oeTz+R+GJfk3L5n8yWTC+yEej3OxwWCA4/GI7XaLfr/P0/jvlis2VadUKvH+IFK73YZt2yCxcDiM6ZR+SuDuI45GI4zHY8zncxwOB05YLBZ8Pg83BajOjEilummEuVeFmtssvgJurPYHGEKbZ/T0eqIAAAAASUVORK5CYII=&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: left center; cursor: auto;"><br />
							<label for="user_mobile" class="active">شماره موبایل<span class="required">*</span></label><br />
							<span class="helper-text" data-error="شماره موبایل یک عدد 11 رقمی می باشد."></span>
						</div>
<div class="col s6 input-field">
							<input type="submit" id="submit_sms_send" class="btn-large  disabled" value="ارسال کد تایید" disabled="">
						</div>
</p></div>
<p>					<input type="hidden" name="user_id" id="user_id" value="1"><br />
					<input type="hidden" id="security" name="security" value="af2a547f18"><input type="hidden" name="_wp_http_referer" value="/gift/%d8%aa%d8%a7%db%8c%db%8c%d8%af-%d8%ad%d8%b3%d8%a7%d8%a8-%da%a9%d8%a7%d8%b1%d8%a8%d8%b1%db%8c/">				</form>
</p></div>
<div id="confirm-sms-code" style="display:none">
<form method="post" action="confirmsms_verify" id="sms-verify-form">
<div class="row">
<div class="col s6 input-field">
								<i class="material-icons prefix">fingerprint</i><br />
								<input type="text" pattern="[0-9]*|[۰-۹]*" class="validate" data-length="6" name="user_code" id="user_code" placeholder="کد تایید را وارد کنید" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAZ9JREFUOBGVU7uKwkAUPXmID5AttNyFYBGwsLGwFBUFF/wOhfyE5jPcxkZt/IHFxg+wsZJtrFwS8NWIohZm545xNkp8XcjMnbnnnJk790YyTfPTcZwm+z7whEmSNGWwaqPR+Ca4/AqZCO5BX+STkcBTJ5/gp9HLkb2BR34kEoGu6xewlwQ0TUOxWPQXCIVCIhAMBsEeS6y9MbHpOirNlUoF6XQanU4Hq9UKhmHAsiy0Wq2L2DWZ1i+l4Ccg1et1hwJ0zd1uxzGUwn6/98OLPZbiL1vUxA3OZEI8IhOGlfKdTU3+BrThZ5lMBoVCAev1Gr1eD7PZDIFAALIs80NIRNzAT4DIw+EQm80G2WyWQ1KpFHK5nICr1NvezhIR5iyXSyQSCUSjUSiKgnK5jGQyCVVVEYvF0O12oeTz+R+GJfk3L5n8yWTC+yEej3OxwWCA4/GI7XaLfr/P0/jvlis2VadUKvH+IFK73YZt2yCxcDiM6ZR+SuDuI45GI4zHY8zncxwOB05YLBZ8Pg83BajOjEilummEuVeFmtssvgJurPYHGEKbZ/T0eqIAAAAASUVORK5CYII=&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: left center;" autocomplete="off"><br />
								<label for="user_code" class="active">کد تایید<span class="required">*</span></label><br />
								<span class="helper-text" data-error="کد تایید یک عدد 6 رقمی می باشد."></span>
							</div>
<div class="col s6 input-field">
								<input type="submit" id="submit_sms_verify" class="btn-large" value="تایید شماره موبایل"></p>
<div class="btn-floating pulse btn-large verify-counter" id="mobile-timer"></div>
</p></div>
</p></div>
<p>						<input type="hidden" name="user_id" id="user_id" value="1"><br />
						<input type="hidden" id="security" name="security" value="7285dc68db"><input type="hidden" name="_wp_http_referer" value="/gift/%d8%aa%d8%a7%db%8c%db%8c%d8%af-%d8%ad%d8%b3%d8%a7%d8%a8-%da%a9%d8%a7%d8%b1%d8%a8%d8%b1%db%8c/">					</form>
</p></div>
<div id="mobile-progress" class="progress">
<div class="indeterminate"></div>
</div></div>
</p></div>
</li>
<li class="">
<div class="collapsible-header red-text" tabindex="0"><i class="material-icons">phone</i>تایید شماره تلفن</div>
<div class="collapsible-body" style="">
<div class="nikan-confirm-box">
<div class="nikan-confirm-notice error" id="phone-response" style="display: block;">لطفا شماره تلفن خود را وارد کنید.</div>
<div id="confirm-phone-send">
<form method="post" action="confirmphone_send" id="phone-send-form" _lpchecked="1">
<div class="row">
<div class="col s6 input-field">
							<i class="material-icons prefix">phone</i><br />
							<input type="text" pattern="[0-9]*|[۰-۹]*" class="validate" data-length="11" name="user_phone" id="user_phone" placeholder="مانند: 02133445566" value="" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAZ9JREFUOBGVU7uKwkAUPXmID5AttNyFYBGwsLGwFBUFF/wOhfyE5jPcxkZt/IHFxg+wsZJtrFwS8NWIohZm545xNkp8XcjMnbnnnJk790YyTfPTcZwm+z7whEmSNGWwaqPR+Ca4/AqZCO5BX+STkcBTJ5/gp9HLkb2BR34kEoGu6xewlwQ0TUOxWPQXCIVCIhAMBsEeS6y9MbHpOirNlUoF6XQanU4Hq9UKhmHAsiy0Wq2L2DWZ1i+l4Ccg1et1hwJ0zd1uxzGUwn6/98OLPZbiL1vUxA3OZEI8IhOGlfKdTU3+BrThZ5lMBoVCAev1Gr1eD7PZDIFAALIs80NIRNzAT4DIw+EQm80G2WyWQ1KpFHK5nICr1NvezhIR5iyXSyQSCUSjUSiKgnK5jGQyCVVVEYvF0O12oeTz+R+GJfk3L5n8yWTC+yEej3OxwWCA4/GI7XaLfr/P0/jvlis2VadUKvH+IFK73YZt2yCxcDiM6ZR+SuDuI45GI4zHY8zncxwOB05YLBZ8Pg83BajOjEilummEuVeFmtssvgJurPYHGEKbZ/T0eqIAAAAASUVORK5CYII=&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: left center; cursor: auto;" autocomplete="off"><br />
							<label for="user_phone" class="active">شماره تلفن<span class="required">*</span></label><br />
							<span class="helper-text" data-error="شماره تلفن یک عدد 11 رقمی می باشد."></span>
						</div>
<div class="col s6 input-field">
							<input type="submit" id="submit_phone_send" class="btn-large" value="ارسال کد تایید">
						</div>
</p></div>
<p>					<input type="hidden" name="user_id" id="user_id" value="1"><br />
					<input type="hidden" id="security" name="security" value="a5d79c94d0"><input type="hidden" name="_wp_http_referer" value="/gift/%d8%aa%d8%a7%db%8c%db%8c%d8%af-%d8%ad%d8%b3%d8%a7%d8%a8-%da%a9%d8%a7%d8%b1%d8%a8%d8%b1%db%8c/">				</form>
</p></div>
<div id="confirm-phone-code" style="display:none">
<form method="post" action="confirmphone_verify" id="phone-verify-form">
<div class="row">
<div class="col s6 input-field">
								<i class="material-icons prefix">fingerprint</i><br />
								<input type="text" pattern="[0-9]*|[۰-۹]*" class="validate" data-length="6" name="user_code" id="user_code" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAZ9JREFUOBGVU7uKwkAUPXmID5AttNyFYBGwsLGwFBUFF/wOhfyE5jPcxkZt/IHFxg+wsZJtrFwS8NWIohZm545xNkp8XcjMnbnnnJk790YyTfPTcZwm+z7whEmSNGWwaqPR+Ca4/AqZCO5BX+STkcBTJ5/gp9HLkb2BR34kEoGu6xewlwQ0TUOxWPQXCIVCIhAMBsEeS6y9MbHpOirNlUoF6XQanU4Hq9UKhmHAsiy0Wq2L2DWZ1i+l4Ccg1et1hwJ0zd1uxzGUwn6/98OLPZbiL1vUxA3OZEI8IhOGlfKdTU3+BrThZ5lMBoVCAev1Gr1eD7PZDIFAALIs80NIRNzAT4DIw+EQm80G2WyWQ1KpFHK5nICr1NvezhIR5iyXSyQSCUSjUSiKgnK5jGQyCVVVEYvF0O12oeTz+R+GJfk3L5n8yWTC+yEej3OxwWCA4/GI7XaLfr/P0/jvlis2VadUKvH+IFK73YZt2yCxcDiM6ZR+SuDuI45GI4zHY8zncxwOB05YLBZ8Pg83BajOjEilummEuVeFmtssvgJurPYHGEKbZ/T0eqIAAAAASUVORK5CYII=&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: left center;" autocomplete="off"><br />
								<label for="user_code">کد تایید<span class="required">*</span></label><br />
								<span class="helper-text" data-error="کد تایید یک عدد 6 رقمی می باشد."></span>
							</div>
<div class="col s6 input-field">
								<input type="submit" id="submit_phone_verify" class="btn-large" value="تایید شماره تلفن"></p>
<div class="btn-floating pulse btn-large verify-counter" id="phone-timer"></div>
</p></div>
</p></div>
<p>						<input type="hidden" name="user_id" id="user_id" value="1"><br />
						<input type="hidden" id="security" name="security" value="1c81ede469"><input type="hidden" name="_wp_http_referer" value="/gift/%d8%aa%d8%a7%db%8c%db%8c%d8%af-%d8%ad%d8%b3%d8%a7%d8%a8-%da%a9%d8%a7%d8%b1%d8%a8%d8%b1%db%8c/">					</form>
</p></div>
<div id="phone-progress" class="progress hide">
<div class="indeterminate"></div>
</div></div>
</p></div>
</li>
<li class="">
<div class="collapsible-header red-text" tabindex="0"><i class="material-icons">assignment_ind</i>تایید کارت ملی</div>
<div class="collapsible-body" style="">
<div class="nikan-confirm-box">
<div class="card">
<div class="card-image">
						<img class="activator responsive-img" src="http://localhost/gift/wp-content/uploads/2019/12/59121cbcae653a1ba2367bfb-1.jpg"><br />
						<a class="btn-floating pulse halfway-fab waves-effect waves-light deep-orange left"><i class="material-icons">adjust</i></a>
					</div>
<div class="card-content">
						<span class="card-title activator blue-grey-text">کد ملی شما: 2420285425</span></p>
<p class="deep-orange-text"><i class="material-icons right">adjust</i>کارت ملی شما در انتظار تایید می باشد.</p>
</p></div>
</p></div>
<div id="phone-progress" class="progress hide">
<div class="indeterminate"></div>
</div></div>
</p></div>
</li>
</ul>
*/
 
    echo do_shortcode( '[nikan-confirm-account]' );

?>

    </div><!-- .entry-content -->
</article>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
