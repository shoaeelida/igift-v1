$(document).ready(function () {
  $("#popular-game-slider").owlCarousel({
    loop: true,
    margin: 10,
    items: 1,
    rtl: true,
    nav: false,
    dots: false,
    autoWidth: true,
    center: true,
    //autoplay: true,
    URLhashListener: true,
    startPosition: "URLHash",
    onTranslate: addClass,
  });
  function addClass() {
    $(".popular-game-slider-nav .nav-item").removeClass("active");
    var tmp = window.location.hash;
    $("." + tmp.split("#")[1]).addClass("active");
    return tmp;
  }
  var interval = setInterval(function () {
    var active = $(".service-items.active");
    var num = active.attr("data-num");
    num = parseInt(num) + 1;
    if (num <= 5) {
      active.removeClass("active");
      $(".service-items[data-num=" + num + "]").addClass("active");
    } else {
      num = 1;
      active.removeClass("active");
      $(".service-items[data-num=" + num + "]").addClass("active");
    }
  }, 3000);
  //************ */
  $("#desc-carousel").owlCarousel({
    loop: true,
    responsiveClass: true,
    autoplay: true,
    rtl: true,
    nav: false,
    dots: false,
    center: true,
    responsive: {
      0: {
        items: 3,
        touchDrag: true,
        mouseDrag: true,
        loop: true,
      },
      1200: {
        items: 5,
        touchDrag: true,
        mouseDrag: true,
        margin: 20,
      },
      1670: {
        items: 7,
        margin: 70,
        autoplay: false,
      },
    },
  });
});
