<?php
/*
Template Name: PS Giftcard
Template Post Type: landing
*/
$landing_assets = theme_cdn . '/landings/ps-giftcard/';

// if( isset($_GET['cdn']) && $_GET['cdn'] == 'true' ){
    $landing_sprite = site_url . 'assets/landings/ps-giftcard/sprite.svg';
// }else{
    // $landing_sprite = $landing_assets . 'img/sprite.svg';
// }

$landing_id = get_the_ID();
?>
<!DOCTYPE html>
<html <?php echo play_language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php the_title(); ?></title>
    <link rel="stylesheet" href="<?php echo $landing_assets; ?>css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $landing_assets; ?>css/main.css">
    <?php do_action('wpseo_head'); ?>
    <?php get_template_part('template-parts/header/header', 'scripts'); ?>
</head>
<body>
<?php
$header = get_field('header', $landing_id);
$header_giftcard_list_ids = get_field('header_giftcard_list', $landing_id);
if ($header or $header_giftcard_list_ids) { ?>
    <header id="header">
        <nav class="nav">
            <div class="container">
                <a href="<?php echo site_url; ?>" title="<?php echo site_title; ?>"><img class="logo lazy"
                                                                                         data-src="<?php echo $landing_assets; ?>img/light-logo.svg"
                                                                                         alt="<?php echo site_title; ?>"></a>
                <a href="<?php echo site_url; ?>" class="white-btn">صفحه اصلی</a>
            </div>
        </nav>
        <div class="hero">
            <div class="intro">
                <div class="gamepad-wrapper">
                    <img class="circles lazy" data-src="<?php echo $landing_assets; ?>img/circles.png">
                    <img class="gradient-rectangle lazy"
                         data-src="<?php echo $landing_assets; ?>img/gradient-rectangle.png" alt="">
                    <img class="gamepad lazy" data-src="<?php echo $landing_assets; ?>img/gamepad.png" alt="">
                </div>
                <?php echo (isset($header['title'])) ? '<h1 class="title">' . $header['title'] . '</h1>' : ''; ?>
                <?php echo (isset($header['desc'])) ? '<p class="description">' . $header['desc'] . '</p>' : ''; ?>
                <?php if (isset($header['link'])) {
                    $target = (isset($header['link']['target'])) ? $header['link']['target'] : '';
                    if (isset($header['link']['url']) && isset($header['link']['title'])) {
                        echo '<a href="' . $header['link']['url'] . '" class="red-btn">
                                ' . $header['link']['title'] . '
                                <svg>
                                    <use xlink:href="'.$landing_sprite.'#arrow-right"></use>
                                </svg>
                            </a>';
                    }
                } ?>
            </div>
            <div class="services">
                <div class="owl-carousel" id="service-carousel">
                    <?php
                    $header_giftcard_list_args = array(
                        'post_type' => array('giftcard'),
                        'post_status' => array('publish'),
                        'post__in' => $header_giftcard_list_ids
                    );
                    $header_giftcard_list = new WP_Query($header_giftcard_list_args);
                    // The Loop
                    if ($header_giftcard_list->have_posts()) {
                        while ($header_giftcard_list->have_posts()) {
                            $header_giftcard_list->the_post();
                            echo '<a href="#"><div class="service-item" style="background-image: url(' . get_the_post_thumbnail_url() . ')"></div></a>';
                        }
                    } else {
                        echo 'لطفا موردی اضاف کنید';
                    }
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
        </div>
    </header>
<?php } ?>
<main id="main">

    <?php
    $content_box = get_field('content_box', $landing_id);
    $giftcard_list_ids = get_field('giftcard_list', $landing_id);
    if ($content_box or $giftcard_list_ids) { ?>
    <div class="products-slider-wrapper">
        <div class="container">
                <div class="text-content">
                    <?php echo (isset($content_box['title'])) ? '<h4>' . $content_box['title'] . '</h4>' : ''; ?>
                    <?php echo (isset($content_box['desc'])) ? '<p>' . $content_box['desc'] . '</p>' : ''; ?>
                    <?php if (isset($content_box['link'])) {
                        $target = (isset($content_box['link']['target'])) ? $content_box['link']['target'] : '';
                        if (isset($content_box['link']['url']) && isset($content_box['link']['title'])) {
                            echo '<a href="' . $content_box['link']['url'] . '" class="red-btn">
                                ' . $content_box['link']['title'] . '
                                <svg>
                                    <use xlink:href="'.$landing_sprite.'#arrow-right"></use>
                                </svg>
                            </a>';
                        }
                    } ?>
                </div>
                <div class="slider-wrapper">
                    <div class="owl-carousel" id="products-slider">
                        <?php
                        $args = array(
                            'post_type' => array('giftcard'),
                            'post_status' => array('publish'),
                            'post__in' => $giftcard_list_ids
                        );
                        $giftcard = new WP_Query($args);
                        // The Loop
                        if ($giftcard->have_posts()) {
                            while ($giftcard->have_posts()) {
                                $giftcard->the_post();
                                set_query_var( 'product_id', get_the_ID() );
                                get_template_part('template-parts/content/content', 'product-item-landing-style-2');
                            }
                        } else {
                            echo 'لطفا موردی اضاف کنید';
                        }
                        wp_reset_postdata();
                        ?>
                    </div>
                </div>
        </div>
    </div>
    <?php } ?>


    <?php
    $first_content_carousel = get_field('first_content_carousel', $landing_id);
    $card_list = get_field('card-list', $landing_id);
    if ($first_content_carousel or $card_list) { ?>
        <div class="ps4-account">
            <div class="container">
                <div class="ps4-account-carousel-wrapper">
                    <div id="ps4-account-carousel-counter"></div>
                    <div class="owl-carousel" id="ps4-account-carousel">
                        <?php foreach ($first_content_carousel as $key => $fcc) { ?>
                        <div class="carousel-content">
                        <?php echo (isset($fcc['above_title'])) ? '<p class="above-title">' . $fcc['above_title'] . '</p>' : ''; ?>
                        <?php echo (isset($fcc['title'])) ? '<h4 class="title">' . $fcc['title'] . '</p>' : ''; ?>
                        <?php echo (isset($fcc['desc'])) ? '<p class="description">' . $fcc['desc'] . '</p>' : ''; ?>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="card-list">
                    <div id="card-list-carousel">
                        <?php foreach ($card_list as $key => $cl) { ?>
                            <div class="card-item">
                                <?php echo (isset($cl['img'])) ? '<img class="lazy" data-src="' . $cl['img']['url'] . '" alt="' . $cl['img']['alt'] . '">' : ''; ?>
                                <div>
                                    <?php echo (isset($cl['title'])) ? '<h4 class="title">' . $cl['title'] . '</h4>' : ''; ?>
                                    <?php echo (isset($cl['desc'])) ? '<p class="description">' . $cl['desc'] . '</p>' : ''; ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>


    <?php $call_to_action = get_field('call_to_action', $landing_id);
    if ($call_to_action) { ?>
    <div class="cta-v2">
        <img class="circle c1 lazy" data-src="<?php echo $landing_assets; ?>img/circle.svg">
        <img class="circle c2 lazy" data-src="<?php echo $landing_assets; ?>img/circle-2.svg">
        <?php echo (isset($call_to_action['img']) && !empty($call_to_action['img'])) ? '<img class="cta-img lazy" data-src="' . $call_to_action['img']['url'] . '" alt="' . $cl['img']['alt'] . '">' : ''; ?>
        <?php echo (isset($call_to_action['title'])) ? '<h4 class="cta-title">' . $call_to_action['title'] . '</h4>' : ''; ?>
        <?php echo (isset($call_to_action['desc'])) ? '<p class="cta-description">' . $call_to_action['desc'] . '</p>' : ''; ?>
        <?php if (isset($call_to_action['link'])) {
                $target = (isset($call_to_action['link']['target'])) ? $call_to_action['link']['target'] : '';
                if (isset($call_to_action['link']['url']) && isset($call_to_action['link']['title'])) {
                    echo '<a href="' . $call_to_action['link']['url'] . '" class="white-btn-v2 cta-btn">
                                ' . $call_to_action['link']['title'] . '
                                <svg>
                                    <use xlink:href="'.$landing_sprite.'#x"></use>
                                </svg>
                            </a>';
                }
            } ?>
    </div>
    <?php } ?>

    <?php
    $second_content_carousel = get_field('second_content_carousel', $landing_id);
    $second_gift_card_list_id = get_field('second-gift-card-list', $landing_id);
    if ($second_content_carousel or $second_gift_card_list_id) { ?>
    <div class="gift-card-v2">
        <div class="bg">
            <img class="ps-btn lazy" data-src="<?php echo $landing_assets; ?>img/ps-button.png">
            <div class="gift-cards-v2-carousel-wrapper">
                <div id="gift-cards-v2-carousel-counter"></div>
                <div class="owl-carousel" id="gift-cards-v2-carousel">
                        <?php foreach ($second_content_carousel as $key => $scc) { ?>
                            <div class="carousel-content">
                                <?php echo (isset($scc['title'])) ? '<h4 class="title">' . $scc['title'] . '</p>' : ''; ?>
                                <?php echo (isset($scc['desc'])) ? '<p class="description">' . $scc['desc'] . '</p>' : ''; ?>
                            </div>
                        <?php } ?>
                </div>
            </div>
        </div>

        <div class="gift-card-list owl-carousel" id="gift-cards-v2-carousel-2">
            <?php
            $second_gift_card_list_args = array(
                'post_type' => array('giftcard'),
                'post_status' => array('publish'),
                'post__in' => $second_gift_card_list_id
            );
            $second_gift_card_list_item = new WP_Query($second_gift_card_list_args);
            // The Loop
            if ($second_gift_card_list_item->have_posts()) {
                while ($second_gift_card_list_item->have_posts()) {
                    $second_gift_card_list_item->the_post();
                    echo '<a href="'.get_post_permalink().'"><div class="gift-card-item lazy" style="background-image: url('.get_the_post_thumbnail_url().');background-size: cover"></div></a>';
                }
            } else {
                echo 'لطفا موردی اضاف کنید';
            }
            wp_reset_postdata();
            ?>
        </div>
        <img class="circle c1 lazy" data-src="<?php echo $landing_assets; ?>img/circle.svg">
        <img class="circle c2 lazy" data-src="<?php echo $landing_assets; ?>img/circle-2.svg">
    </div>
    <?php } ?>


    <?php
    $features = get_field('features', $landing_id);
    if ($features) { ?>
        <div class="ordering-steps">
            <div class="container">
                <?php foreach ($features as $key => $fe) {
                    echo '<div class="step-item" data-step="' . $key . '">';
                    echo '<div class="icon">';
                    switch ($key) {
                        case 1:
                            echo '<svg><use xlink:href="'.$landing_sprite.'#enter"></use></svg>';
                            break;
                        case 2:
                            echo '<svg><use xlink:href="'.$landing_sprite.'#search"></use></svg>';
                            break;
                        case 3:
                            echo '<svg><use xlink:href="'.$landing_sprite.'#credit-card"></use></svg>';
                            break;
                        case 4:
                            echo '<svg><use xlink:href="'.$landing_sprite.'#deliver"></use></svg>';
                            break;
                        default:
                            break;
                    }
                    echo '</div>';
                    echo '<h4 class="title">' . $fe['title'] . '</h4>';
                    echo '<p class="description">' . $fe['desc'] . '</p>';
                    echo '</div>';
                } ?>
            </div>
        </div>
    <?php } ?>
</main>
<footer id="footer">
    <div class="container container-xl">
        <div class="content">
            <div class="description">
                <img class="footer-logo lazy" data-src="<?php echo $landing_assets; ?>img/logo-white.svg" alt="IGame">
                <p>
                    تمامی كالاها و خدمات اين فروشگاه، حسب مورد دارای مجوزهای لازم از مراجع مربوطه می‌باشند و فعاليت‌های
                    اين سايت تابع قوانين و مقررات جمهوری اسلامی ايران است
                </p>
            </div>
            <?php get_template_part('template-parts/nav/nav', 'footer-landing'); ?>
        </div>
        <div class="copyright">
            © 2020 iGame.ir All rights reserved.
            <img class="mobile-logo lazy" data-src="<?php echo $landing_assets; ?>img/logo.svg" alt="IGame">
        </div>
    </div>
</footer>
<style>.lazy, .lazyload, .lazyloading {
        opacity: 0;
    }
    .lazyloaded {
        opacity: 1;
        transition: opacity 300ms;
    }</style>
<script src="<?php echo $landing_assets; ?>js/jquery-3.4.1.slim.min.js"></script>
<!-- <script src="<?php echo $landing_assets; ?>js/owl.carousel.min.js" defer></script> -->
<!-- <script src="<?php echo $landing_assets; ?>js/svgxuse.min.js" defer></script> -->
<script>
    var sprite_url = "<?php echo $landing_sprite; ?>";
    window.lazySizesConfig = window.lazySizesConfig || {};
    window.lazySizesConfig.lazyClass = 'lazy';
    lazySizesConfig.loadMode = 1;
    window.lazySizesConfig.init = false;
</script>
<script src="<?php echo get_template_directory_uri() . '/landings/'; ?>js/plugins.js"></script>
<script src="<?php echo $landing_assets; ?>js/script.js" defer></script>
<?php get_template_part('template-parts/footer/footer', 'scripts'); ?>
<script>
    lazySizes.init();
</script>
</body>
</html>