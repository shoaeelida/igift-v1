/********* Gift Cards Carousel *********/
$('#gift-cards-carousel').owlCarousel({
    loop:true,
    margin:0,
    responsiveClass:true,
    rtl : true,
    autoWidth:true,
    responsive:{
        0:{
            items:1,
        },
        510:{
            items:3,
        },
        992:{
            items:4,
        },
        1200:{
            items:5,
        },
        1570:{
            items:6,
        }
    }
});
/********* Gift Cards V2 Carousel *********/
$('#ps4-account-carousel').owlCarousel({
    loop:true,
    margin:0,
    rtl : true,
    items:1,
    nav:true,
    dots: true,
    navText: ["<svg><use xlink:href=\""+sprite_url+"#arrow-right\"></svg>","<svg><use xlink:href=\""+sprite_url+"#arrow-right\"></svg>"],
    responsiveClass:true,
    onInitialized  : counter1,
    onTranslated : counter1,
    responsive:{
        0:{
            items:1,
            margin:30,
            autoWidth:false,
        },
        1260:{
            items:1,
            autoWidth:true,
        },
    }
});
function counter1(event) {
    if (!event.namespace) {
        return;
    }
    var slides = event.relatedTarget;
    var item = slides.relative(slides.current());
    $('#ps4-account-carousel-counter').html("<span class='current'>"+ Number(++item) +"</span> / "+slides.items().length)
};
/********* Gift Cards V2 Carousel *********/
$('#gift-cards-v2-carousel').owlCarousel({
    loop:true,
    margin:0,
    rtl : true,
    items:1,
    nav:true,
    dots: true,
    navText: ["<svg><use xlink:href=\""+sprite_url+"#arrow-right\"></svg>","<svg><use xlink:href=\""+sprite_url+"#arrow-right\"></svg>"],
    responsiveClass:true,
    onInitialized  : counter2,
    onTranslated : counter2,
    responsive:{
        0:{
            items:1,
            margin:30,
            autoWidth:false,
        },
        1270:{
            items:1,
            autoWidth:true,
        },
    }
});
function counter2(event) {
    if (!event.namespace) {
        return;
    }
    var slides = event.relatedTarget;
    var item = slides.relative(slides.current());
    $('#gift-cards-v2-carousel-counter').html("<span class='current'>"+ Number(++item) +"</span> / "+slides.items().length)
};
/********* Gift Cards V2 Carousel 2 *********/
$('#gift-cards-v2-carousel-2').owlCarousel({
    loop:true,
    margin:0,
    responsiveClass:true,
    rtl : true,
    nav:false,
    dots: false,
    autoWidth:true,
    responsive:{
        0:{
            items:1,
            margin:10,
            touchDrag:true,
            mouseDrag: true,
            loop:true,
            autoplay: true,
        },
        400:{
            items:2,
            touchDrag:true,
            mouseDrag: true,
        },
        690:{
            items:3,
            autoplay: false,
        },
    }
});
/********* CARD LIST Carousel *********/
if ($(window).width() <= 576) {
    $('#card-list-carousel').addClass('owl-carousel');
    $('#card-list-carousel').owlCarousel({
        loop:false,
        margin:10,
        responsiveClass:true,
        rtl : true,
        nav:false,
        dots: false,
        autoWidth:true,
        responsive:{
            0:{
                items:3,
                touchDrag:true,
                mouseDrag: true,
                loop:true,
                autoplay: true,
            },
        }
    });
}
/********* Product slider *********/
$('#products-slider').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    rtl : true,
    //autoplay: true,
    center:true,
    navText: ["<svg><use xlink:href=\""+sprite_url+"#slider-arrow\"></svg>","<svg><use xlink:href=\""+sprite_url+"#slider-arrow\"></svg>"],
    smartSpeed:600,
    nav:true,
    responsive:{
        0:{
            items:3,
            nav:false,
            autoWidth:true,
        },
        660:{
            items:3,
            nav:true,
            autoWidth:false,
        }
    }
});
//uglifyjs js/jquery-3.4.1.slim.min.js js/owl.carousel.min.js js/svgxuse.min.js js/script.js -o js/main.min.js