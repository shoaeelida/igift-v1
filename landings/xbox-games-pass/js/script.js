/********* service Carousel *********/

$('#service-carousel').owlCarousel({
    loop:true,
    margin:23,
    responsiveClass:true,
    rtl : true,
    autoWidth:true,
    nav:false,
    responsive:{
        0:{
            items:3,
            margin:8
        },
        576:{
            items:3,
            margin:23,
        },
    }
});
/********* Gift Cards Carousel *********/

$('#gift-cards-carousel').owlCarousel({
    loop:true,
    margin:0,
    responsiveClass:true,
    rtl : true,
    autoWidth:true,
    nav:true,
    navText: ["<svg><use xlink:href=\""+sprite_url+"#arrow-left2\"></svg>","<svg><use xlink:href=\""+sprite_url+"#arrow-left2\"></svg>"],
    responsive:{
        0:{
            items:1,
        },
        510:{
            items:3,
        },
        992:{
            items:4,
        },
        1200:{
            items:5,
        },
        1570:{
            items:4,
        }
    }
});

/********* Gift Cards Carousel *********/

$('#gift-cards-carousel-2').owlCarousel({
    loop:true,
    margin:0,
    responsiveClass:true,
    rtl : true,
    autoWidth:true,
    nav:true,
    navText: ["<svg><use xlink:href=\""+sprite_url+"#arrow-left2\"></svg>","<svg><use xlink:href=\""+sprite_url+"#arrow-left2\"></svg>"],
    responsive:{
        0:{
            items:1,
        },
        510:{
            items:3,
        },
        992:{
            items:4,
        },
    }
});
/********* Gift Cards V2 Carousel *********/
$('#xbp-carousel').owlCarousel({
    loop:true,
    margin:0,
    rtl : true,
    items:1,
    nav:false,
    dots: true,
    responsiveClass:true,
    autoplay:false,
    URLhashListener:true,
    autoplayHoverPause:true,
    startPosition: 'URLHash',
    onInitialized  : counter1,
    onTranslate : counter1,
    onTranslated : addClass,
    responsive:{
        0:{
            items:1,
            margin:30,
            autoWidth:false,
        },
        1260:{
            items:1,
            autoWidth:true,
        },
    }
});
function counter1(event) {
    if (!event.namespace) {
        return;
    }
    var slides = event.relatedTarget;
    var item = slides.relative(slides.current());
    $('#xbp-counter').html("<span class='current'>"+ Number(++item) +"</span> / "+slides.items().length)
};
function addClass() {
    $(".xbp-slider .boxes .box").removeClass("activeItem");
    var tmp = window.location.hash;
    $(tmp.replace("#", ".")).addClass("activeItem");
    return tmp;
}
/********* Gift Cards V2 Carousel *********/
$('#gift-cards-v2-carousel').owlCarousel({
    loop:true,
    margin:0,
    rtl : true,
    items:1,
    nav:false,
    dots: true,
    responsiveClass:true,
    onInitialized  : counter2,
    onTranslated : counter2,
    responsive:{
        0:{
            items:1,
            margin:30,
            autoWidth:false,
        },
        1270:{
            items:1,
            autoWidth:true,
        },
    }
});
function counter2(event) {
    if (!event.namespace) {
        return;
    }
    var slides = event.relatedTarget;
    var item = slides.relative(slides.current());
    $('#gift-cards-v2-carousel-counter').html("<span class='current'>"+ Number(++item) +"</span> / "+slides.items().length)
};
/********* Gift Cards V2 Carousel 2 *********/

$('#gift-cards-v2-carousel-2').owlCarousel({
    loop:true,
    margin:0,
    responsiveClass:true,
    rtl : true,
    nav:true,
    dots: false,
    autoWidth:true,
    navText: ["<svg><use xlink:href=\""+sprite_url+"#arrow-left2\"></svg>","<svg><use xlink:href=\""+sprite_url+"#arrow-left2\"></svg>"],
    responsive:{
        0:{
            items:1,
            margin:10,
            touchDrag:true,
            mouseDrag: true,
            loop:true,
            autoplay: true,
        },
        400:{
            items:2,
            touchDrag:true,
            mouseDrag: true,
        },
        690:{
            items:3,
            autoplay: false,
        },
    }
});
/********* CARD LIST Carousel *********/
if ($(window).width() <= 550) {
    $('#card-list-carousel').addClass('owl-carousel');
    $('#card-list-carousel').owlCarousel({
        loop:false,
        margin:10,
        responsiveClass:true,
        rtl : true,
        nav:false,
        dots: false,
        //autoWidth:true,
        responsive:{
            0:{
                items:1,
                touchDrag:true,
                mouseDrag: true,
                loop:true,
                autoplay: true,
            },
        }
    });
}
//uglifyjs js/jquery-3.4.1.slim.min.js js/owl.carousel.min.js js/svgxuse.min.js js/script.js -o js/main.min.js

// Scroll down
$(".scroll-down").on('click', function() {
    var tmp = $("#main .gift-card").offset().top;
    var tmp = $("#main .gift-card").offset().top;
    $("html, body").animate({
        scrollTop: tmp+'px'
    }, 1000);
});

if ($(window).width() <= 1200) {
    $('#boxes').addClass('owl-carousel');
    $('#boxes').owlCarousel({
        loop:false,
        margin:10,
        responsiveClass:true,
        rtl : true,
        autoWidth:true,
        dots:false,
        nav:false,
        responsive:{
            0:{
                items:4,
            },
        }
    });
}