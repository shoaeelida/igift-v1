<?php
/*
Template Name: Xbox Games pass
Template Post Type: landing
*/
$landing_assets = theme_cdn . '/landings/xbox-games-pass/';

// if( isset($_GET['cdn']) && $_GET['cdn'] == 'true' ){
    $landing_sprite = site_url . 'assets/landings/xbox-games-pass/sprite.svg';
// }else{
    // $landing_sprite = $landing_assets . 'img/sprite.svg';
// }

$landing_id = get_the_ID();
?>
<!DOCTYPE html>
<html <?php echo play_language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php the_title(); ?></title>
    <link rel="stylesheet" href="<?php echo $landing_assets; ?>css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $landing_assets; ?>css/main.css">
    <?php do_action('wpseo_head'); ?>
    <?php get_template_part('template-parts/header/header', 'scripts'); ?>
</head>
<body>
<?php
$header = get_field('header', $landing_id);
if ($header) { ?>
    <header id="header">
        <nav class="nav">
            <div class="container">
                <a href="<?php echo site_url; ?>" title="<?php echo site_title; ?>"><img class="logo lazy" data-src="<?php echo $landing_assets; ?>img/light-logo.svg" alt="<?php echo site_title; ?>"></a>
                <a href="<?php echo site_url; ?>" class="white-btn">صفحه اصلی</a>
            </div>
        </nav>
        <div class="hero">
            <div class="intro">
                <?php echo (isset($header['title'])) ? '<h1 class="title">' . $header['title'] . '</h1>' : ''; ?>
                <?php echo (isset($header['desc'])) ? '<p class="description">' . $header['desc'] . '</p>' : ''; ?>
                <?php if (isset($header['link'])) {
                    $target = (isset($header['link']['target'])) ? $header['link']['target'] : '';
                    if (isset($header['link']['url']) && isset($header['link']['title'])) {
                        echo '<a href="' . $header['link']['url'] . '" class="red-btn">
                                ' . $header['link']['title'] . '
                                <svg>
                                    <use xlink:href="'.$landing_sprite.'#arrow-right"></use>
                                </svg>
                            </a>';
                    }
                } ?>
            </div>
            <div class="services">
                <div class="owl-carousel" id="service-carousel">
                    <?php
                    $products_array = array();
                    if( $header['show_last_products'] ){
                        $products_array = get_posts(
                            array(
                                'posts_per_page' => 20,
                                'post_status' => array('publish'),
                                'post_type' => 'game',
                                'fields' => 'ids',
                            )
                        );
                    }else{
                        $products_array = $header['products'];
                    }    
                    if( $products_array && !empty($products_array) ){
                        foreach ($products_array as $product_id ) {
                            ?>
                            <a href="<?php echo get_permalink($product_id) ?>">
                                <div class="service-item">
                                    <img class="lazy" data-src="<?php echo get_the_post_thumbnail_url($product_id) ?>" alt="">
                                    <h4 style="min-height: 48px"> <?php echo get_the_title($product_id)  ?> </h4>
                                </div>
                            </a>
                            <?php
                        }
                    } else {
                        echo 'لطفا موردی اضاف کنید';
                    }
                    ?>
                </div>
            </div>
            <div class="scroll-down">
                <span>بیشتر</span>
                <svg>
                    <use xlink:href="<?php echo $landing_sprite; ?>#scroll-down"></use>
                </svg>
            </div>
        </div>
    </header>
<?php } ?>
<main id="main">
    <?php $games_list1 = get_field('games_list_1', $landing_id);
    if ($games_list1) {
        $platform = playGetTermDate($games_list1['platform'],'game_platform');      
        ?>
        <div class="gift-card one">
            <div class="container">
                <div class="section-title">
                    <h2><?php echo $games_list1['title']?></h2>
                    <span><?php echo $games_list1['desc']?></span>
                </div>
            </div>
            <div class="owl-carousel" id="gift-cards-carousel">
                    <?php
                    $products_array = array();
                    if( $games_list1['show_last_products'] ){
                        $products_array = get_posts(
                            array(
                                'posts_per_page' => 20,
                                'post_status' => array('publish'),
                                'post_type' => 'game',
                                'fields' => 'ids',
                            )
                        );
                    }else{
                        $products_array = $games_list1['products'];
                    }    
                    if( $products_array && !empty($products_array) ){
                        foreach ($products_array as $product_id ) {
                            set_query_var( 'product_id', $product_id );
                            set_query_var( 'landing_assets', $landing_assets );
                            set_query_var( 'landing_sprite', $landing_sprite );
                            set_query_var( 'main_platform', $platform );
                            get_template_part('template-parts/content/content', 'product-item-landing-xbg');
                        }
                    } else {
                        echo 'لطفا موردی اضاف کنید';
                    }
                    ?>
                </div>
        </div>
    <?php } ?>

    <?php $xbp_slider = get_field('xbp-slider', $landing_id);
    if ($xbp_slider) { ?>
        <div class="xbp-slider">
            <div class="container">
                <div class="xbp-wrapper">
                    <div id="xbp-counter"></div>
                    <div class="owl-carousel" id="xbp-carousel">
                        <?php foreach ($xbp_slider as $xs) { ?>
                            <div class="carousel-content" data-hash="<?php echo $xs['class'] ?>">
                                <?php echo (isset($xs['title'])) ? '<h4 class="title">' . $xs['title'] . '</h4>' : '' ?>
                                <?php echo (isset($xs['desc'])) ? '<p class="description">' . $xs['desc'] . '</p>' : '' ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <img class="circle c1 lazy" data-src="<?php echo $landing_assets ?>img/circle.svg">
                <img class="circle c2 lazy"  data-src="<?php echo $landing_assets ?>img/circle-2.svg">
                <div class="boxes" id="boxes">
                    <?php foreach ($xbp_slider as $key=> $xs) { ?>
                    <a href="#<?php echo $xs['class'] ?>">
                        <div class="box <?php echo $key === 0 ? 'activeItem' : '' ?> <?php echo $xs['class'] ?>">
                            <div class="icon-circle">
                                <img class="lazy" data-src="<?php echo $xs['icon']['img'] ?>">
                            </div>
                            <span><?php echo $key + 1 ?></span>
                            <h4><?php echo $xs['icon']['title'] ?></h4>
                            <p><?php echo $xs['icon']['desc'] ?></p>
                        </div>
                    </a>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php $giftcard_list = get_field('giftcard_list', $landing_id);
    if ($giftcard_list) { ?>
        <div class="gift-card two">
            <div class="container">
                <div class="section-title">
                    <h2><?php echo $giftcard_list['title']?></h2>
                    <span><?php echo $giftcard_list['desc']?></span>
                </div>
            </div>
            <div class="container">
                <div class="owl-carousel" id="gift-cards-carousel-2">
                    <?php
                    $products_array = array();
                    if( $giftcard_list['show_last_products'] ){
                        $products_array = get_posts(
                            array(
                                'posts_per_page' => 20,
                                'post_status' => array('publish'),
                                'post_type' => 'giftcard',
                                'fields' => 'ids',
                            )
                        );
                    }else{
                        $products_array = $giftcard_list['products'];
                    }    
                    if( $products_array && !empty($products_array) ){
                        foreach ($products_array as $product_id ) {
                            set_query_var( 'product_id', $product_id );
                            set_query_var( 'landing_assets', $landing_assets );
                            set_query_var( 'landing_sprite', $landing_sprite );
                            get_template_part('template-parts/content/content', 'product-item-landing-style-3');
                        }
                    } else {
                        echo 'لطفا موردی اضاف کنید';
                    }
                    ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php $features = get_field('features', $landing_id);
    if ($features) { ?>
        <div class="ordering-steps">
            <div class="container">
                <?php foreach ($features as $key => $fe) {
                    echo '<div class="step-item" data-step="' . $key . '">';
                    echo '<div class="icon">';
                    switch ($key) {
                        case 1:
                            echo '<svg><use xlink:href="'.$landing_sprite.'#enter"></use></svg>';
                            break;
                        case 2:
                            echo '<svg><use xlink:href="'.$landing_sprite.'#search"></use></svg>';
                            break;
                        case 3:
                            echo '<svg><use xlink:href="'.$landing_sprite.'#credit-card"></use></svg>';
                            break;
                        case 4:
                            echo '<svg><use xlink:href="'.$landing_sprite.'#deliver"></use></svg>';
                            break;
                        default:
                            break;
                    }
                    echo '</div>';
                    echo '<h4 class="title">' . $fe['title'] . '</h4>';
                    echo '<p class="description">' . $fe['desc'] . '</p>';
                    echo '</div>';
                } ?>
            </div>
        </div>
    <?php } ?>

</main>
<footer id="footer">
    <div class="container container-xl">
        <div class="content">
            <div class="description">
                <img class="footer-logo lazy" data-src="<?php echo $landing_assets; ?>img/logo-white.svg" alt="IGame">
                <p>
                    تمامی كالاها و خدمات اين فروشگاه، حسب مورد دارای مجوزهای لازم از مراجع مربوطه می‌باشند و فعاليت‌های
                    اين سايت تابع قوانين و مقررات جمهوری اسلامی ايران است
                </p>
            </div>
            <?php get_template_part('template-parts/nav/nav', 'footer-landing'); ?>
        </div>
        <div class="copyright">
            © 2020 iGame.ir All rights reserved.
            <img class="mobile-logo lazy" data-src="<?php echo $landing_assets; ?>img/logo.svg" alt="IGame">
        </div>
    </div>
</footer>
<style>.lazy, .lazyload, .lazyloading {
        opacity: 0;
    }

    .lazyloaded {
        opacity: 1;
        transition: opacity 300ms;
    }</style>
<script src="<?php echo $landing_assets; ?>js/jquery-3.4.1.min.js"></script>
<!-- <script src="<?php echo $landing_assets; ?>js/owl.carousel.min.js" defer></script> -->
<!-- <script src="<?php echo $landing_assets; ?>js/svgxuse.min.js" defer></script> -->
<script>
    var sprite_url = "<?php echo $landing_sprite; ?>";
    window.lazySizesConfig = window.lazySizesConfig || {};
    window.lazySizesConfig.lazyClass = 'lazy';
    lazySizesConfig.loadMode = 1;
    window.lazySizesConfig.init = false;
</script>
<script src="<?php echo get_template_directory_uri() . '/landings/'; ?>js/plugins.js"></script>
<script src="<?php echo $landing_assets; ?>js/script.js" defer></script>
<?php get_template_part('template-parts/footer/footer', 'scripts'); ?>
<script>
    lazySizes.init();
</script>
</body>
</html>
