<?php
/*
Template Name: PUBG
Template Post Type: landing
*/
$landing_assets = theme_cdn . '/landings/pubg/';

// if( isset($_GET['cdn']) && $_GET['cdn'] == 'true' ){
    $landing_sprite = site_url . 'assets/landings/pubg/sprite.svg';
// }else{
    // $landing_sprite = $landing_assets . 'img/sprite.svg';
// }

$landing_id = get_the_ID();
?>
<!DOCTYPE html>
<html <?php echo play_language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php the_title(); ?></title>
    <link rel="stylesheet" href="<?php echo $landing_assets; ?>css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $landing_assets; ?>css/main.css">
    <?php do_action('wpseo_head'); ?>
    <?php get_template_part('template-parts/header/header', 'scripts'); ?>
</head>
<body>
<?php
$header = get_field('header', $landing_id);
if ($header) { ?>
    <header id="header">
        <img class="c1 lazy" data-src="<?php echo $landing_assets ?>img/circle-2.svg">
        <img class="c2 lazy" data-src="<?php echo $landing_assets ?>img/circle-2.svg" >
        <img class="c3 lazy" data-src="<?php echo $landing_assets ?>img/circle-2.svg">
        <img class="c4 lazy" data-src="<?php echo $landing_assets ?>img/circle-2.svg">
        <img class="c5 lazy" data-src="<?php echo $landing_assets ?>img/circle.svg" >
        <img class="c6 lazy" data-src="<?php echo $landing_assets ?>img/circle.svg">
        <nav class="nav">
            <div class="container">
                <a href="<?php echo site_url; ?>" title="<?php echo site_title; ?>"><img class="logo lazy"
                                                                                         data-src="<?php echo $landing_assets; ?>img/light-logo.svg" alt="<?php echo site_title; ?>"></a>
                <a href="<?php echo site_url; ?>" class="white-btn">صفحه اصلی</a>
            </div>
        </nav>
        <div class="hero">
            <div class="owl-carousel" id="hero-carousel">
                <?php foreach ($header['character'] as $ch) { ?>
                    <div class="item"><img src="<?php echo $ch ?>"></div>
                <?php } ?>
                <div class="item"><img src="<?php echo $landing_assets ?>img/p1.png" alt=""></div>
                <div class="item"><img src="<?php echo $landing_assets ?>img/p2.png" alt=""></div>
                <div class="item"><img src="<?php echo $landing_assets ?>img/p3.png" alt=""></div>
                <div class="item"><img src="<?php echo $landing_assets ?>img/p3.png" alt=""></div>
            </div>
            <div class="intro">
                <?php echo (isset($header['title'])) ? '<h1 class="title">' . $header['title'] . '</h1>' : ''; ?>
                <?php echo (isset($header['desc'])) ? '<p class="description">' . $header['desc'] . '</p>' : ''; ?>
                <?php if (isset($header['link'])) {
                    $target = (isset($header['link']['target'])) ? $header['link']['target'] : '';
                    if (isset($header['link']['url']) && isset($header['link']['title'])) {
                        echo '<a href="' . $header['link']['url'] . '" class="red-btn">
                                ' . $header['link']['title'] . '
                                <svg>
                                    <use xlink:href="'.$landing_sprite.'#arrow-right"></use>
                                </svg>
                            </a>';
                    }
                } ?>
            </div>
            <div class="container-xl">
                <img class="woman lazy" data-src="<?php echo $header['platforms']['img'] ?>">
                <div class="pubg-platform">
                    <h2><?php echo $header['platforms']['title'] ?></h2>
                    <p><?php echo $header['platforms']['desc'] ?>د</p>
                    <div class="platforms">
                        <?php foreach ($header['platforms']['platform'] as $p) {?>
                            <a href="<?php echo $p['link'] ?>" class="<?php echo $p['class'] ?>">
                                <img src=" <?php echo $p['icon'] ?>">
                                <?php echo $p['title'] ?>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="container-xl">
                <img class="location lazy" data-src="<?php echo $header['location-img'] ?>">
            </div>
        </div>
    </header>
<?php } ?>
<main id="main">
    <?php
    $ingame_pay = get_field('ingame-pay', $landing_id);
    if( $ingame_pay ){ ?>
    <div class="ingame-pay">
        <img class="c1" src="<?php echo $landing_assets ?>img/circle-2.svg" alt="">
        <img class="c2" src="<?php echo $landing_assets ?>img/circle.svg" alt="">
        <div class="container">
            <div class="content">
                <?php echo (isset($ingame_pay['title'])) ? '<h2 class="title">' . $ingame_pay['title'] . '</h2>' : ''; ?>
                <?php echo (isset($ingame_pay['desc'])) ? '<p>' . $ingame_pay['desc'] . '</p>' : ''; ?>
                <?php if (isset($ingame_pay['link'])) {
                    $target = (isset($ingame_pay['link']['target'])) ? $ingame_pay['link']['target'] : '';
                    if (isset($ingame_pay['link']['url']) && isset($ingame_pay['link']['title'])) {
                        echo '<a href="' . $ingame_pay['link']['url'] . '" class="red-btn">
                                ' . $ingame_pay['link']['title'] . '
                                <svg>
                                    <use xlink:href="'.$landing_sprite.'#arrow-right"></use>
                                </svg>
                            </a>';
                    }
                } ?>
            </div>
            <?php echo (isset($ingame_pay['img'])) ? '<img class="img lazy" data-src="'.$ingame_pay['img'].'">' : ''; ?>
        </div>
    </div>
    <?php } ?>

    <?php
    $interduce_pubg = get_field('interduce_pubg', $landing_id);
    if( $interduce_pubg ){ ?>
        <div class="buy-pubg">
            <div class="container">
                <?php echo (isset($interduce_pubg['img'])) ? '<img class="vr-man lazy" data-src="'.$interduce_pubg['img'].'">' : ''; ?>
                <div class="content">
                    <?php echo (isset($interduce_pubg['title'])) ? '<h2>' . $interduce_pubg['title'] . '</h2>' : ''; ?>
                    <?php echo (isset($interduce_pubg['desc'])) ? '<p>' . $interduce_pubg['desc'] . '</p>' : ''; ?>
                    <?php if (isset($interduce_pubg['buy-link'])) {
                        $target = (isset($interduce_pubg['buy-link']['target'])) ? $interduce_pubg['buy-link']['target'] : '';
                        if (isset($interduce_pubg['buy-link']['url']) && isset($interduce_pubg['buy-link']['title'])) {
                            echo '<a href="' . $interduce_pubg['buy-link']['url'] . '" class="red-btn">
                                ' . $interduce_pubg['buy-link']['title'] . '
                                 <svg><use xlink:href="'.$landing_sprite.'#arrow-right"></use></svg>
                            </a>';
                        }
                    } ?>
                    <?php if (isset($interduce_pubg['trailer_link'])) {
                        $target = (isset($interduce_pubg['trailer_link']['target'])) ? $interduce_pubg['trailer_link']['target'] : '';
                        if (isset($interduce_pubg['trailer_link']['url']) && isset($interduce_pubg['trailer_link']['title'])) {
                            echo '<a href="' . $interduce_pubg['trailer_link']['url'] . '" class="outline-btn">
                                ' . $interduce_pubg['trailer_link']['title'] . '
                                 <svg><use xlink:href="'.$landing_sprite.'#play"></use></svg>
                            </a>';
                        }
                    } ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php
    $cta = get_field('cta', $landing_id);
    if( $cta ){ ?>
        <div class="cta-v2">
            <?php echo (isset($cta['img'])) ? '<img class="cta-img lazy" data-src="'.$cta['img'].'">' : ''; ?>
            <?php echo (isset($cta['title'])) ? '<h1 class="cta-title">' . $cta['title'] . '</h1>' : ''; ?>
            <?php echo (isset($cta['desc'])) ? '<p class="cta-description">' . $cta['desc'] . '</p>' : ''; ?>
            <?php if (isset($cta['link'])) {
                $target = (isset($cta['link']['target'])) ? $cta['link']['target'] : '';
                if (isset($cta['link']['url']) && isset($cta['link']['title'])) {
                    echo '<a href="' . $cta['link']['url'] . '" class="white-btn-v2 cta-btn">
                                ' . $cta['link']['title'] . '
                                <svg>
                                    <use xlink:href="'.$landing_sprite.'#x"></use>
                                </svg>
                            </a>';
                }
            } ?>
        </div>
    <?php } ?>

    <?php
    $features = get_field('features', $landing_id);
    if( $features ){ ?>
        <div class="ordering-steps">
            <div class="container">
                <?php foreach ($features as $key => $fe) {
                    echo '<div class="step-item" data-step="'.$key.'">';
                    echo '<div class="icon">';
                    switch ($key) {
                        case 1:
                            echo '<svg><use xlink:href="'.$landing_sprite.'#enter"></use></svg>';
                            break;
                        case 2:
                            echo '<svg><use xlink:href="'.$landing_sprite.'#search"></use></svg>';
                            break;
                        case 3:
                            echo '<svg><use xlink:href="'.$landing_sprite.'#credit-card"></use></svg>';
                            break;
                        case 4:
                            echo '<svg><use xlink:href="'.$landing_sprite.'#deliver"></use></svg>';
                            break;
                        default:
                            break;
                    }
                    echo '</div>';
                    echo '<h4 class="title">'.$fe['title'].'</h4>';
                    echo '<p class="description">'.$fe['desc'].'</p>';
                    echo '</div>';
                } ?>
            </div>
        </div>
    <?php } ?>

</main>
<footer id="footer">
    <div class="container container-xl">
        <div class="content">
            <div class="description">
                <img class="footer-logo lazy" data-src="<?php echo $landing_assets; ?>img/logo-white.svg" alt="IGame">
                <p>
                    تمامی كالاها و خدمات اين فروشگاه، حسب مورد دارای مجوزهای لازم از مراجع مربوطه می‌باشند و فعاليت‌های
                    اين سايت تابع قوانين و مقررات جمهوری اسلامی ايران است
                </p>
            </div>
            <?php get_template_part('template-parts/nav/nav', 'footer-landing'); ?>
        </div>
        <div class="copyright">
            © 2020 iGame.ir All rights reserved.
            <img class="mobile-logo lazy" data-src="<?php echo $landing_assets; ?>img/logo.svg" alt="IGame">
        </div>
    </div>
</footer>
<style>.lazy, .lazyload, .lazyloading {
        opacity: 0;
    }
    .lazyloaded {
        opacity: 1;
        transition: opacity 300ms;
    }</style>
<script src="<?php echo $landing_assets; ?>js/jquery-3.4.1.slim.min.js"></script>
<!--<script src="<?php /*echo $landing_assets; */?>js/owl.carousel.min.js" defer></script>
<script src="<?php /*echo $landing_assets; */?>js/svgxuse.min.js" defer></script>-->
<script>
    var sprite_url = "<?php echo $landing_sprite; ?>";
    window.lazySizesConfig = window.lazySizesConfig || {};
    window.lazySizesConfig.lazyClass = 'lazy';
    lazySizesConfig.loadMode = 1;
    window.lazySizesConfig.init = false;
</script>
<script src="<?php echo get_template_directory_uri() . '/landings/'; ?>js/plugins.js"></script>

<script src="<?php echo $landing_assets; ?>js/script.js" defer></script>
<?php get_template_part('template-parts/footer/footer', 'scripts'); ?>
<script>
    lazySizes.init();
</script>
</body>
</html>

