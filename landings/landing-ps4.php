<?php

/*
Template Name: PS4
Template Post Type: landing
*/

$landing_assets = theme_cdn . '/landings/ps4/';

// if( isset($_GET['cdn']) && $_GET['cdn'] == 'true' ){
    $landing_sprite = site_url . 'assets/landings/ps4/sprite.svg';
// }else{
    // $landing_sprite = $landing_assets . 'img/sprite.svg';
// }s

$landing_id = get_the_ID();

?>

<!DOCTYPE html>
<html <?php echo play_language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php the_title(); ?></title>
    <link rel="stylesheet" href="<?php echo $landing_assets; ?>css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $landing_assets; ?>css/main.css">
    <?php do_action('wpseo_head'); ?>
    <?php get_template_part('template-parts/header/header', 'scripts'); ?>
</head>
<body>
<header id="header">
    <nav class="nav">
        <div class="container">
            <a href="<?php echo site_url; ?>" title="<?php echo site_title; ?>"><img class="logo lazy"
                                                                                     data-src="<?php echo $landing_assets; ?>img/light-logo.svg"
                                                                                     alt="<?php echo site_title; ?>"></a>
            <a href="<?php echo site_url; ?>" class="white-btn">صفحه اصلی</a>
        </div>
    </nav>
    <div class="container">
        <?php
        $header = get_field('header', $landing_id);
        if ($header) { ?>
            <div class="hero">
                <div class="intro">
                    <?php echo (isset($header['title'])) ? '<h1 class="title">' . $header['title'] . '</h1>' : ''; ?>
                    <?php echo (isset($header['desc'])) ? '<p class="description">' . $header['desc'] . '</p>' : ''; ?>
                    <?php if (isset($header['link'])) {
                        $target = (isset($header['link']['target'])) ? $header['link']['target'] : '';
                        if (isset($header['link']['url']) && isset($header['link']['title'])) {
                            echo '<a href="' . $header['link']['url'] . '" class="red-btn">
                                ' . $header['link']['title'] . '
                                <svg>
                                    <use xlink:href="'.$landing_sprite.'#arrow-right"></use>
                                </svg>
                            </a>';
                        }
                    } ?>
                    <img class="gamepad lazy" data-src="<?php echo $landing_assets; ?>img/gamepad-2.png" alt="">
                </div>
                <div class="services">
                    <img class="circles lazy" data-src="<?php echo $landing_assets; ?>img/circles.png">
                    <img class="gradient-circle lazy" data-src="<?php echo $landing_assets; ?>img/gradient-circle.png"
                         alt="">
                    <img class="gradient-rectangle lazy"
                         data-src="<?php echo $landing_assets; ?>img/gradient-rectangle.png" alt="">
                    <img class="gamepad lazy" data-src="<?php echo $landing_assets; ?>img/gamepad.png" alt="">
                </div>
            </div>
        <?php } ?>
    </div>
</header>
<main id="main">
    <?php
    $products_list = get_field('products_list', $landing_id);
    if ($products_list) { 
        $platform = playGetTermDate($products_list['platform'],'game_platform');
        ?>
        <div class="gift-card">
            <div class="container">
                <div class="section-title">
                    <?php
                    echo (isset($products_list['title'])) ? '<h2>' . $products_list['title'] . '</h2>' : '';
                    if (isset($products_list['links']) && !empty($products_list['links'])) { ?>
                        <div class="filter-card">
                            <ul>
                                <?php foreach ($products_list['links'] as $link) {
                                    $link = (isset($link['link'])) ? $link['link'] : null;
                                    if ($link) {
                                        $target = (isset($link['target'])) ? $link['target'] : '';
                                        if (isset($link['url']) && isset($link['title'])) {
                                            echo '<li><a href="' . $link['url'] . '">' . $link['title'] . '</a></li>';
                                        }
                                    }
                                } ?>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="owl-carousel" id="gift-cards-carousel">
                <?php
                $products_array = array();
                if ($products_list['show_last_products']) {
                    $products_array = get_posts(
                        array(
                            'posts_per_page' => 20,
                            'post_type' => 'game',
                            'fields' => 'ids',
                        )
                    );
                } else {
                    $products_array = $products_list['products'];
                }

                if ($products_array && !empty($products_array)) {
                    foreach ($products_array as $product_id) {
                        set_query_var('product_id', $product_id);
                        set_query_var('landing_assets', $landing_assets);
                        set_query_var('landing_sprite', $landing_sprite);
                        set_query_var('main_platform', $platform);
                        get_template_part('template-parts/content/content', 'product-item-landing');
                    }
                }
                ?>
            </div>
        </div>
    <?php } ?>
    <?php
    $slides = get_field('slides', $landing_id);
    if ($slides) { ?>
        <div class="ps4-account">
            <div class="container">
                <img class="circle c1 lazy" data-src="<?php echo $landing_assets; ?>img/circle.svg">
                <img class="circle c2 lazy" data-src="<?php echo $landing_assets; ?>img/circle-2.svg">
                <img class="circle c3 lazy" data-src="<?php echo $landing_assets; ?>img/circle-2.svg">
                <img class="circle c4 lazy" data-src="<?php echo $landing_assets; ?>img/circle.svg">
                <?php
                if (isset($slides['slide']) && !empty($slides['slide'])) { ?>
                    <div class="ps4-account-carousel-wrapper">
                        <div id="ps4-account-carousel-counter"></div>
                        <div class="owl-carousel" id="ps4-account-carousel">
                            <?php
                            foreach ($slides['slide'] as $slide) {
                                echo '<div class="carousel-content">
                                    <h4 class="title">' . $slide['title'] . '</h4>
                                    <p class="description">' . $slide['content'] . '</p>
                                </div>';
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                }
                if (isset($slides['text_icons']) && !empty($slides['text_icons'])) { ?>
                    <div class="holder-services">
                        <div class="" id="ps4-service-carousel">
                            <?php
                            foreach ($slides['text_icons'] as $key => $icon) {
                                $class = 'service-items ';
                                $icons = '';
                                $num = $key + 1;
                                switch ($num) {
                                    case 1:
                                        $class .= 'medium box1 active';
                                        $icons .= (isset($icon['icon']) && $icon['icon']) ? '<img class="color lazy" data-src="' . $icon['icon'] . '">' : '';
                                        $icons .= (isset($icon['icon_hover']) && $icon['icon_hover']) ? '<img class="white lazy" data-src="' . $icon['icon_hover'] . '">' : '';
                                        break;
                                    case 2:
                                        $class .= 'large box2';
                                        $icons .= (isset($icon['icon_hover']) && $icon['icon_hover']) ? '<img class="white lazy" data-src="' . $icon['icon_hover'] . '">' : '';
                                        $icons .= (isset($icon['icon']) && $icon['icon']) ? '<img class="color lazy" data-src="' . $icon['icon'] . '">' : '';
                                        break;
                                    case 3:
                                        $class .= 'small box3';
                                        $icons .= (isset($icon['icon_hover']) && $icon['icon_hover']) ? '<img class="white lazy" data-src="' . $icon['icon_hover'] . '">' : '';
                                        $icons .= (isset($icon['icon']) && $icon['icon']) ? '<img class="color lazy" data-src="' . $icon['icon'] . '">' : '';
                                        break;
                                    case 4:
                                        $class .= 'medium box4';
                                        $icons .= (isset($icon['icon']) && $icon['icon']) ? '<img class="color lazy" data-src="' . $icon['icon'] . '">' : '';
                                        $icons .= (isset($icon['icon_hover']) && $icon['icon_hover']) ? '<img class="white lazy" data-src="' . $icon['icon_hover'] . '">' : '';
                                        break;
                                    default:
                                        break;
                                }
                                echo '<div data-num="' . $num . '" class="' . $class . '">';
                                echo '<div class="service-wrap">';
                                echo $icons;
                                echo (isset($icon['title'])) ? '<span>' . $icon['title'] . '</span>' : '';
                                echo (isset($icon['subtitle'])) ? $icon['subtitle'] : '';
                                echo '</div>';
                                echo '</div>';
                            }
                            ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <?php
    $popular_products = get_field('products_list_popular', $landing_id);
    if ($popular_products) {
        $platform = playGetTermDate($popular_products['platform'],'game_platform');      
      ?>
        <div class="popular-game">
            <div class="container">
                <div class="section-title">
                    <?php echo (isset($popular_products['title'])) ? '<h2>' . $popular_products['title'] . '</h2>' : ''; ?>
                </div>
                <div class="slider-content">
                    <?php
                    $products_array = array();
                    $products_array = $popular_products['products'];
                    if ($products_array && !empty($products_array)) { ?>
                        <div class="popular-game-slider owl-carousel" id="popular-game-slider">
                            <?php foreach ($products_array as $key => $product_id) {
                                $num = $key + 1;
                                $url = get_post_permalink($product_id);
                                if( $platform && !empty($platform ) ){
                                    $url .= '?platform='.$platform['slug'];
                                }
                                echo '<a href="' . $url . '" class="game-item" data-num="' . $num . '" data-hash="slide-' . $num . '">';
                                echo (has_post_thumbnail($product_id)) ? '<div class="img" style="background-image: url(' . esc_url(get_the_post_thumbnail_url($product_id, 'full')) . ')" alt="' . plain_product_title(get_the_title($product_id)) . '"></div>' : '';
                                echo '<svg class="favorite"><use xlink:href="'.$landing_sprite.'#love"></use></svg>';
                                echo '<h4 class="title">' . plain_product_title(get_the_title($product_id)) . '</h4>';
                                echo '</a>';
                            } ?>
                        </div>
                        <div class="popular-game-slider-nav">
                            <?php
                            $count = count($products_array);
                            foreach ($products_array as $key => $product_id) {
                                $num = $key + 1;

                                if ($num == 1 || $num == 4) {
                                    echo '<div>';
                                }

                                if ($num == 1) {
                                    echo '<div class="nav-item slide-' . $num . ' active" data-num="' . $num . '"><a href="#slide-' . $num . '">' . plain_product_title(get_the_title($product_id)) . '</a></div>';
                                } else {
                                    echo '<div class="nav-item slide-' . $num . '" data-num="' . $num . '"><a href="#slide-' . $num . '">' . plain_product_title(get_the_title($product_id)) . '</a></div>';
                                }

                                if ($num == 3 || $num >= $count) {
                                    echo '</div>';
                                }
                            } ?>

                        </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    <?php } ?>
    <?php
    $features = get_field('features', $landing_id);
    if ($features) { ?>
        <div class="ordering-steps">
            <div class="container">
                <?php foreach ($features as $key => $fe) {
                    echo '<div class="step-item" data-step="' . $key . '">';
                    echo '<div class="icon">';
                    switch ($key) {
                        case 1:
                            echo '<svg><use xlink:href="'.$landing_sprite.'#enter"></use></svg>';
                            break;
                        case 2:
                            echo '<svg><use xlink:href="'.$landing_sprite.'#search"></use></svg>';
                            break;
                        case 3:
                            echo '<svg><use xlink:href="'.$landing_sprite.'#credit-card"></use></svg>';
                            break;
                        case 4:
                            echo '<svg><use xlink:href="'.$landing_sprite.'#deliver"></use></svg>';
                            break;
                        default:
                            break;
                    }
                    echo '</div>';
                    echo '<h4 class="title">' . $fe['title'] . '</h4>';
                    echo '<p class="description">' . $fe['desc'] . '</p>';
                    echo '</div>';
                } ?>
            </div>
        </div>
    <?php } ?>
</main>
<footer id="footer">
    <div class="container container-xl">
        <div class="content">
            <div class="description">
                <img class="footer-logo lazy" data-src="<?php echo $landing_assets; ?>img/logo-white.svg" alt="IGame">
                <p>
                    تمامی كالاها و خدمات اين فروشگاه، حسب مورد دارای مجوزهای لازم از مراجع مربوطه می‌باشند و فعاليت‌های
                    اين سايت تابع قوانين و مقررات جمهوری اسلامی ايران است
                </p>
            </div>
            <?php get_template_part('template-parts/nav/nav', 'footer-landing'); ?>
        </div>
        <div class="copyright">
            © 2020 iGame.ir All rights reserved.
            <img class="mobile-logo lazy" data-src="<?php echo $landing_assets; ?>img/logo.svg" alt="IGame">
        </div>
    </div>
</footer>
<style>.lazy, .lazyload, .lazyloading {
        opacity: 0;
    }

    .lazyloaded {
        opacity: 1;
        transition: opacity 300ms;
    }</style>
<script src="<?php echo $landing_assets; ?>js/jquery-3.4.1.slim.min.js"></script>
<!-- <script src="<?php echo $landing_assets; ?>js/owl.carousel.min.js" defer></script> -->
<!-- <script src="<?php echo $landing_assets; ?>js/svgxuse.min.js" defer></script> -->
<script>
    var sprite_url = "<?php echo $landing_sprite; ?>";
    window.lazySizesConfig = window.lazySizesConfig || {};
    window.lazySizesConfig.lazyClass = 'lazy';
    lazySizesConfig.loadMode = 1;
    window.lazySizesConfig.init = false;
</script>
<script src="<?php echo get_template_directory_uri() . '/landings/'; ?>js/plugins.js"></script>
<script src="<?php echo $landing_assets; ?>js/script.js" defer></script>
<?php get_template_part('template-parts/footer/footer', 'scripts'); ?>
<script>
    lazySizes.init();
</script>
</body>
</html>