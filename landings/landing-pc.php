<?php

/*
Template Name: PC
Template Post Type: landing
*/

$landing_assets = theme_cdn . '/landings/pc/';

// if( isset($_GET['cdn']) && $_GET['cdn'] == 'true' ){
    $landing_sprite = site_url . 'assets/landings/pc/sprite.svg';
// }else{
    // $landing_sprite = $landing_assets . 'img/sprite.svg';
// }

$landing_id = get_the_ID();

?>


<!DOCTYPE html>
<html <?php echo play_language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php the_title(); ?></title>
    <link rel="stylesheet" href="<?php echo $landing_assets; ?>css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $landing_assets; ?>css/main.css">
    <?php do_action( 'wpseo_head' ); ?>
    <?php get_template_part( 'template-parts/header/header', 'scripts' ); ?>
</head>
<body>
<header id="header">
    <nav class="nav">
        <div class="container">
            <a href="<?php echo site_url; ?>" title="<?php echo site_title; ?>"><img class="logo lazy" data-src="<?php echo $landing_assets; ?>img/light-logo.svg" alt="<?php echo site_title; ?>"></a>
            <a href="<?php echo site_url; ?>" class="white-btn">صفحه اصلی</a>
        </div>
    </nav>
    <div class="container">
        <?php 
        $header = get_field('header', $landing_id);
        if( $header ){ ?>
            <div class="hero">
                <div class="intro">
                    <?php echo (isset($header['title'])) ? '<h1 class="title">'.$header['title'].'</h1>' : ''; ?>
                    <?php echo (isset($header['desc'])) ? '<p class="description">'.$header['desc'].'</p>' : ''; ?>
                    <?php if(isset($header['link'])) {
                        $target = ( isset($header['link']['target']) ) ? $header['link']['target'] : '';
                        if ( isset($header['link']['url']) && isset($header['link']['title']) ) {
                            echo '<a href="'.$header['link']['url'].'" class="red-btn">
                                '.$header['link']['title'].'
                                <svg>
                                    <use xlink:href="'.$landing_sprite.'#arrow-right"></use>
                                </svg>
                            </a>';
                        }
                    } ?>
                </div>
            </div>
        <?php } ?>
    </div>
</header>
<main id="main">
    <?php 
    $products_list = get_field('platform_products_list_1', $landing_id);
    if( $products_list && $products_list['active'] ){ 
        if( $products_list['platform'] ){
            $platform = playGetTermDate($products_list['platform'],'game_platform');
            ?>
            <div class="gift-card">
                <div class="container-products">
                    <div class="products-wrapper">
                        <div class="products-wrapper-head">
                            <?php echo ($platform['icon']) ? '<img class="origin lazy" data-src="'.$platform['icon'].'" alt="'.$platform['name'].'">' : ''; ?>
                            <?php echo (isset($products_list['title'])) ? '<h4>'.$products_list['title'].'</h4>' : ''; ?>
                            <?php echo '<span><a href="'.$platform['url'].'" title="نمایش همه بازی های '.$platform['name'].'">نمایش همه</a></span>'; ?>
                        </div>
                        <div class="owl-carousel" id="origin-carousel">
                            <?php
                            $products_array = array();
                            if( $products_list['show_last_products'] ){
                                $products_array = get_posts(
                                    array(
                                        'posts_per_page' => 20,
                                        'post_type' => 'game',
                                        'fields' => 'ids',
                                        'tax_query' => array(
                                            'taxonomy' => 'game_platform',
                                            'field' => 'term_id',
                                            'terms' => $products_list['platform'],
                                        )
                                    )
                                );
                            }else{
                                $products_array = $products_list['products'];
                            }    
                            
                            if( $products_array && !empty($products_array) ){
                                foreach ($products_array as $product_id ) {
                                    set_query_var( 'product_id', $product_id );
                                    set_query_var( 'landing_assets', $landing_assets );
                                    set_query_var( 'landing_sprite', $landing_sprite );
                                    set_query_var( 'main_platform', $platform );
                                    get_template_part( 'template-parts/content/content', 'product-item-landing' );
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php
        }
    } ?>

    <?php 
    $platform_products_type_2 = get_field('platform_products_list_type_2', $landing_id);
    if( $platform_products_type_2 ){
        if( $platform_products_type_2['platform'] ){
            $platform = playGetTermDate($platform_products_type_2['platform'],'game_platform');
            ?>

            <div class="products-slider-wrapper">
                <div class="container">
                    <div class="text-content">
                        <h4>
                            <?php echo ($platform['icon']) ? '<img src="'.$platform['icon'].'" alt="'.$platform['name'].'">' : ''; ?>
                            <?php echo (isset($platform_products_type_2['title'])) ? $platform_products_type_2['title'] : ''; ?>
                        </h4>
                        <?php echo (isset($header['desc'])) ? '<p>'.$header['desc'].'</p>' : ''; ?>
                        <a href="<?php echo $platform['url']; ?>" class="red-btn">همه بازی ها
                            <svg>
                                <use xlink:href="<?php echo $landing_sprite; ?>#arrow-right"></use>
                            </svg>
                        </a>
                    </div>
                    <div class="slider-wrapper">
                        <div class="owl-carousel" id="products-slider">
                            <?php
                            $products_array = array();
                            if( $platform_products_type_2['show_last_products'] ){
                                $products_array = get_posts(
                                    array(
                                        'posts_per_page' => 20,
                                        'post_type' => 'game',
                                        'fields' => 'ids',
                                        'tax_query' => array(
                                            'taxonomy' => 'game_platform',
                                            'field' => 'term_id',
                                            'terms' => $platform_products_type_2['platform'],
                                        )
                                    )
                                );
                            }else{
                                $products_array = $platform_products_type_2['products'];
                            }    
                            
                            if( $products_array && !empty($products_array) ){
                                foreach ($products_array as $product_id ) {
                                    if( $product_id && get_post_status ( $product_id ) == 'publish' ){

                                        $product_obj = get_product_obj( $product_id );

                                        ?>
                                        <div class="slider-item">
                                            <img src="<?php echo $product_obj['thumbnail_url']; ?>" alt="">
                                            <div class="product-info">
                                                <h4><a href="<?php echo $product_obj['url']; ?>" title="<?php echo $product_obj['title']; ?>"><?php echo $product_obj['name']; ?></a></h4>
                                                <div class="prices">
                                                  <?php 
                                                    if( $product_obj['sale_price_raw'] && $product_obj['sale_percent'] ){
                                                        echo '<span class="off-percentage">%'.$product_obj['sale_percent'].'</span>';
                                                        echo '<del class="original-price">'.play_price($product_obj['original_price_raw']).'</del>';
                                                        echo '<span class="sale-price">'.play_price($product_obj['sale_price_raw']).'</span>';
                                                    }else{
                                                        echo '<span class="sale-price">'.play_price($product_obj['price']).'</span>';
                                                    }
                                                    ?>
                                                </div>
                                                <a href="<?php echo $product_obj['url']; ?>" title="<?php echo $product_obj['title']; ?>"><div class="add-to-cart">+</div></a>
                                            </div>
                                        </div>
    
                                        <?php

                                    }
                                  
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php
        }
    }
    ?>
 
    <?php 
    $products_list = get_field('platform_products_list_2', $landing_id);
    if( $products_list && $products_list['active'] ){ 
        if( $products_list['platform'] ){
            $platform = playGetTermDate($products_list['platform'],'game_platform');
            ?>
            <div class="gift-card two">
                <div class="container-products">
                    <div class="products-wrapper">
                        <div class="products-wrapper-head">
                            <?php echo ($platform['icon']) ? '<img class="epic lazy" data-src="'.$platform['icon'].'" alt="'.$platform['name'].'">' : ''; ?>
                            <?php echo (isset($products_list['title'])) ? '<h4>'.$products_list['title'].'</h4>' : ''; ?>
                            <?php echo '<span><a href="'.$platform['url'].'" title="نمایش همه بازی های '.$platform['name'].'">نمایش همه</a></span>'; ?>
                        </div>
                        <div class="owl-carousel" id="epic-carousel">
                            <?php
                            $products_array = array();
                            if( $products_list['show_last_products'] ){
                                $products_array = get_posts(
                                    array(
                                        'posts_per_page' => 20,
                                        'post_type' => 'game',
                                        'fields' => 'ids',
                                        'tax_query' => array(
                                            'taxonomy' => 'game_platform',
                                            'field' => 'term_id',
                                            'terms' => $products_list['platform'],
                                        )
                                    )
                                );
                            }else{
                                $products_array = $products_list['products'];
                            }    
                            
                            if( $products_array && !empty($products_array) ){
                                foreach ($products_array as $product_id ) {
                                    set_query_var( 'product_id', $product_id );
                                    set_query_var( 'landing_assets', $landing_assets );
                                    set_query_var( 'landing_sprite', $landing_sprite );
                                    set_query_var( 'main_platform', $platform );
                                    get_template_part( 'template-parts/content/content', 'product-item-landing' );
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php
        }
    } ?>

    <?php 
    /*$cta = get_field('cta_v2', $landing_id);
    if( $cta ){ ?>
        <div class="cta-v2">
            <img class="circle c1 lazy" data-src="<?php echo $landing_assets; ?>img/circle.svg">
            <img class="circle c2 lazy" data-src="<?php echo $landing_assets; ?>img/circle-2.svg">
            <img class="circle c3 lazy" data-src="<?php echo $landing_assets; ?>img/circle-2.svg">
            <img class="circle c4 lazy" data-src="<?php echo $landing_assets; ?>img/circle.svg">
            <img class="cta-img lazy" data-src="<?php echo $landing_assets; ?>img/cta-v2.svg" alt="">
            <?php echo (isset($cta['title'])) ? '<h4 class="cta-title">'.$cta['title'].'</h4>' : ''; ?>
            <?php echo (isset($cta['subtitle'])) ? '<p class="cta-description">'.$cta['subtitle'].'</p>' : ''; ?>
            <?php if(isset($cta['link'])) {
                 $target = ( isset($cta['link']['target']) ) ? $cta['link']['target'] : '';
                 if ( isset($cta['link']['url']) && isset($cta['link']['title']) ) {
                     echo '<a href="'.$cta['link']['url'].'" class="white-btn-v2 cta-btn">
                        <svg>
                            <use xlink:href="'.$landing_sprite.'#x"></use>
                        </svg>
                        '.$cta['link']['title'].'
                     </a>';
                 }
            } ?>
          
        </div>
    <?php }
    */ ?>

    <?php 
    $products_list = get_field('platform_products_list_3', $landing_id);
    if( $products_list && $products_list['active'] ){ 
        if( $products_list['platform'] ){
            $platform = playGetTermDate($products_list['platform'],'game_platform');
            ?>
            <div class="gift-card tree">
                <div class="container-products">
                    <div class="products-wrapper">
                        <div class="products-wrapper-head">
                            <?php echo ($platform['icon']) ? '<img class="uplay lazy" data-src="'.$platform['icon'].'" alt="'.$platform['name'].'">' : ''; ?>
                            <?php echo (isset($products_list['title'])) ? '<h4>'.$products_list['title'].'</h4>' : ''; ?>
                            <?php echo '<span><a href="'.$platform['url'].'" title="نمایش همه بازی های '.$platform['name'].'">نمایش همه</a></span>'; ?>
                        </div>
                        <div class="owl-carousel" id="uplay-carousel">
                            <?php
                            $products_array = array();
                            if( $products_list['show_last_products'] ){
                                $products_array = get_posts(
                                    array(
                                        'posts_per_page' => 20,
                                        'post_type' => 'game',
                                        'fields' => 'ids',
                                        'tax_query' => array(
                                            'taxonomy' => 'game_platform',
                                            'field' => 'term_id',
                                            'terms' => $products_list['platform'],
                                        )
                                    )
                                );
                            }else{
                                $products_array = $products_list['products'];
                            }    
                            
                            if( $products_array && !empty($products_array) ){
                                foreach ($products_array as $product_id ) {
                                    set_query_var( 'product_id', $product_id );
                                    set_query_var( 'landing_assets', $landing_assets );
                                    set_query_var( 'landing_sprite', $landing_sprite );
                                    set_query_var( 'main_platform', $platform );
                                    get_template_part( 'template-parts/content/content', 'product-item-landing' );
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php
        }
    } ?>

    <?php 
    $slides = get_field('slides', $landing_id);
    if( $slides ){ ?>
        <div class="ps4-account">
            <div class="container">
                <img class="circle c1 lazy" data-src="<?php echo $landing_assets; ?>img/circle.svg">
                <img class="circle c2 lazy" data-src="<?php echo $landing_assets; ?>img/circle-2.svg">
                <img class="circle c3 lazy" data-src="<?php echo $landing_assets; ?>img/circle-2.svg">
                <img class="circle c4 lazy" data-src="<?php echo $landing_assets; ?>img/circle.svg">
                <?php
                if( isset($slides['slide']) && !empty($slides['slide']) ){ ?>
                    <div class="ps4-account-carousel-wrapper">
                        <div id="ps4-account-carousel-counter"></div>
                        <div class="owl-carousel" id="ps4-account-carousel">
                            <?php
                            foreach ($slides['slide'] as $slide ) {
                                echo '<div class="carousel-content">
                                    <h4 class="title">'.$slide['title'].'</h4>
                                    <p class="description">'.$slide['content'].'</p>
                                </div>';
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                }
                if( isset($slides['text_icons']) && !empty($slides['text_icons']) ){ ?>
                    <div class="holder-services">
                        <div class="" id="ps4-service-carousel">
                            <?php 
                            foreach ($slides['text_icons'] as $key => $icon) {
                                $class = 'service-items ';
                                $icons = '';
                                $num = $key + 1;
                                switch ($num) {
                                    case 1:
                                        $class .= 'medium box1 active';
                                        $icons .= (isset($icon['icon']) && $icon['icon']) ? '<img class="color lazy" data-src="'.$icon['icon'].'">' : '';
                                        $icons .= (isset($icon['icon_hover']) && $icon['icon_hover']) ? '<img class="white lazy" data-src="'.$icon['icon_hover'].'">' : '';
                                        break;
                                    case 2:
                                        $class .= 'large box2';
                                        $icons .= (isset($icon['icon_hover']) && $icon['icon_hover']) ? '<img class="white lazy" data-src="'.$icon['icon_hover'].'">' : '';
                                        $icons .= (isset($icon['icon']) && $icon['icon']) ? '<img class="color lazy" data-src="'.$icon['icon'].'">' : '';
                                        break;
                                    case 3:
                                        $class .= 'small box3';
                                        $icons .= (isset($icon['icon_hover']) && $icon['icon_hover']) ? '<img class="white lazy" data-src="'.$icon['icon_hover'].'">' : '';
                                        $icons .= (isset($icon['icon']) && $icon['icon']) ? '<img class="color lazy" data-src="'.$icon['icon'].'">' : '';
                                        break;
                                    case 4:
                                        $class .= 'medium box4';
                                        $icons .= (isset($icon['icon']) && $icon['icon']) ? '<img class="color lazy" data-src="'.$icon['icon'].'">' : '';
                                        $icons .= (isset($icon['icon_hover']) && $icon['icon_hover']) ? '<img class="white lazy" data-src="'.$icon['icon_hover'].'">' : '';
                                        break;
                                    default:
                                        break;
                                }
                                echo '<div data-num="'. $num .'" class="'.$class.'">';
                                    echo '<div class="service-wrap">';
                                        echo $icons;
                                        echo (isset($icon['title'])) ? '<span>'.$icon['title'].'</span>' : '';
                                        echo (isset($icon['subtitle'])) ? $icon['subtitle'] : '';
                                    echo '</div>';
                                echo '</div>';
                            }
                            ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>

    <?php 
    $products_list = get_field('platform_products_list_4', $landing_id);
    if( $products_list && $products_list['active'] ){ 
        if( $products_list['platform'] ){
            $platform = playGetTermDate($products_list['platform'],'game_platform');
            ?>
            <div class="gift-card four">
                <div class="container-products">
                    <div class="products-wrapper">
                        <div class="products-wrapper-head">
                            <?php echo ($platform['icon']) ? '<img class="blizzard lazy" data-src="'.$platform['icon'].'" alt="'.$platform['name'].'">' : ''; ?>
                            <?php echo (isset($products_list['title'])) ? '<h4>'.$products_list['title'].'</h4>' : ''; ?>
                            <?php echo '<span><a href="'.$platform['url'].'" title="نمایش همه بازی های '.$platform['name'].'">نمایش همه</a></span>'; ?>
                        </div>
                        <div class="owl-carousel" id="blizzard-carousel">
                            <?php
                            $products_array = array();
                            if( $products_list['show_last_products'] ){
                                $products_array = get_posts(
                                    array(
                                        'posts_per_page' => 20,
                                        'post_type' => 'game',
                                        'fields' => 'ids',
                                        'tax_query' => array(
                                            'taxonomy' => 'game_platform',
                                            'field' => 'term_id',
                                            'terms' => $products_list['platform'],
                                        )
                                    )
                                );
                            }else{
                                $products_array = $products_list['products'];
                            }    
                            
                            if( $products_array && !empty($products_array) ){
                                foreach ($products_array as $product_id ) {
                                    set_query_var( 'product_id', $product_id );
                                    set_query_var( 'landing_assets', $landing_assets );
                                    set_query_var( 'landing_sprite', $landing_sprite );
                                    set_query_var( 'main_platform', $platform );
                                    get_template_part( 'template-parts/content/content', 'product-item-landing' );
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php
        }
    } ?>

    <?php 
    $products_list = get_field('platform_products_list_5', $landing_id);
    if( $products_list && $products_list['active'] ){ 
        if( $products_list['platform'] ){
            $platform = playGetTermDate($products_list['platform'],'game_platform');
            ?>
            <div class="gift-card five">
                <div class="container-products">
                    <div class="section-title">
                        <?php echo (isset($products_list['title'])) ? '<h2>'.$products_list['title'].'</h2>' : ''; ?>
                        <?php echo (isset($products_list['desc'])) ? '<p>'.$products_list['desc'].'</p>' : ''; ?>
                    </div>
                </div>
                <div class="owl-carousel" id="rockstar-carousel">
                    <?php
                    $products_array = array();
                    if( $products_list['show_last_products'] ){
                        $products_array = get_posts(
                            array(
                                'posts_per_page' => 20,
                                'post_type' => 'game',
                                'fields' => 'ids',
                                'tax_query' => array(
                                    'taxonomy' => 'game_platform',
                                    'field' => 'term_id',
                                    'terms' => $products_list['platform'],
                                )
                            )
                        );
                    }else{
                        $products_array = $products_list['products'];
                    }    
                    
                    if( $products_array && !empty($products_array) ){
                        foreach ($products_array as $product_id ) {
                            set_query_var( 'product_id', $product_id );
                            set_query_var( 'landing_assets', $landing_assets );
                            set_query_var( 'landing_sprite', $landing_sprite );
                            set_query_var( 'main_platform', $platform );
                            get_template_part( 'template-parts/content/content', 'product-item-landing' );
                        }
                    }
                    ?>
                </div>
            </div>
            <?php
        }
    } ?>

    <?php 
    $products_list = get_field('platform_products_list_6', $landing_id);
    if( $products_list && $products_list['active'] ){ 
        if( $products_list['platform'] ){
            $platform = playGetTermDate($products_list['platform'],'game_platform');
            ?>
            <div class="gift-card six">
                <div class="container-products">
                    <div class="products-wrapper">
                        <div class="products-wrapper-head">
                            <?php 
                            if( isset($products_list['custom_icon']) && !empty($products_list['custom_icon']) ){
                                echo '<img class="gog lazy" data-src="'.$products_list['custom_icon'].'" alt="'.$products_list['title'].'">';
                            }else{
                                echo ($platform['icon']) ? '<img class="gog lazy" data-src="'.$platform['icon'].'" alt="'.$platform['name'].'">' : '';
                            }
                            ?>
                            <?php echo (isset($products_list['title'])) ? '<h4>'.$products_list['title'].'</h4>' : ''; ?>
                            <?php
                            if( isset($products_list['custom_link']) && !empty($products_list['custom_link']) ){
                                $target = ( isset($products_list['custom_link']['target']) ) ? $products_list['custom_link']['target'] : '';
                                if ( isset($products_list['custom_link']['url']) && isset($products_list['custom_link']['title']) ) {
                                    echo '<span><a href="'.$products_list['custom_link']['url'].'">
                                        '.$products_list['custom_link']['title'].'
                                    </a></span>';
                                }
                            }else{
                                echo '<span><a href="'.$platform['url'].'" title="نمایش همه بازی های '.$platform['name'].'">نمایش همه</a></span>'; 
                            } ?>
                        </div>
                        <div class="owl-carousel" id="gog-carousel">
                            <?php
                            $products_array = array();
                            if( $products_list['show_last_products'] ){
                                $products_array = get_posts(
                                    array(
                                        'posts_per_page' => 20,
                                        'post_type' => 'game',
                                        'fields' => 'ids',
                                        'tax_query' => array(
                                            'taxonomy' => 'game_platform',
                                            'field' => 'term_id',
                                            'terms' => $products_list['platform'],
                                        )
                                    )
                                );
                            }else{
                                $products_array = $products_list['products'];
                            }    
                            
                            if( $products_array && !empty($products_array) ){
                                foreach ($products_array as $product_id ) {
                                    set_query_var( 'product_id', $product_id );
                                    set_query_var( 'landing_assets', $landing_assets );
                                    set_query_var( 'landing_sprite', $landing_sprite );
                                    set_query_var( 'main_platform', $platform );
                                    get_template_part( 'template-parts/content/content', 'product-item-landing' );
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php
        }
    } ?>

    <?php 
    /*
    $popular_products = get_field('products_list_popular', $landing_id);
    if( $popular_products ){ 
        $products_array = $popular_products['products'];
        ?>
        <div class="popular-game">
            
            <?php
            if( $products_array && !empty($products_array) ){ ?>

                <div class="popular-game-slider owl-carousel" id="popular-game-slider">
                    <?php foreach ($products_array as $key => $product_id ) {
                        $num = $key + 1;
                        $cover = get_field('cover', $product_id);
                        echo '<a href="'.get_post_permalink($product_id).'" class="game-item" data-hash="slide-'.$num.'">';
                            echo ($cover) ? '<div class="img" style="background-image: url('.$cover.')" alt="'.plain_product_title(get_the_title($product_id)).'"></div>' : '';
                        echo '</a>';
                    } ?>
                </div>

                <div class="mini-popular-game-slider owl-carousel" id="mini-popular-game-slider">
                    <?php foreach ($products_array as $key => $product_id ) {
                        $num = $key + 1;
                        echo '<a href="'.get_post_permalink($product_id).'" class="mini-game-item" data-num="'.$num.'" data-hash="slide-'.$num.'">';
                            echo (has_post_thumbnail($product_id)) ? '<div class="img" style="background-image: url('.esc_url(get_the_post_thumbnail_url( $product_id, 'full' )).')" alt="'.plain_product_title(get_the_title($product_id)).'"></div>' : '';
                            echo '<svg class="favorite"><use xlink:href="'.$landing_sprite.'#love"></use></svg>';
                            echo '<h4 class="title">'.plain_product_title(get_the_title($product_id)).'</h4>';
                        echo '</a>';
                    } ?>
                </div>

                <div class="popular-game-slider-nav">
                    <?php echo (isset($popular_products['title'])) ? '<div class="section-title"><h2>'.$popular_products['title'].'</h2></div>' : ''; ?>
                    <div class="d-flex">
                        <?php 
                        $count = count($products_array);
                        foreach ($products_array as $key => $product_id ) {
                            $num = $key + 1;

                            if( $num == 1 || $num == 4 ){
                                echo '<div>';
                            }

                                if( $num == 1 ){
                                    echo '<div class="nav-item slide-'.$num.' active" data-num="'.$num.'"><a href="#slide-'.$num.'">'.plain_product_title(get_the_title($product_id)).'</a></div>';
                                }else{
                                    echo '<div class="nav-item slide-'.$num.'" data-num="'.$num.'"><a href="#slide-'.$num.'">'.plain_product_title(get_the_title($product_id)).'</a></div>';
                                }

                            if( $num == 3 || $num >= $count ){
                                echo '</div>';
                            }
                        } ?>
                    </div>
                </div>

            <?php } ?>

        </div>
    <?php }
    */ ?>

    <?php 
    $features = get_field('features', $landing_id);
    if( $features ){ ?>
        <div class="ordering-steps">
            <div class="container">
                <?php foreach ($features as $key => $fe) {
                    echo '<div class="step-item" data-step="'.$key.'">';
                        echo '<div class="icon">';
                            switch ($key) {
                                case 1:
                                    echo '<svg><use xlink:href="'.$landing_sprite.'#enter"></use></svg>';
                                    break;
                                case 2:
                                    echo '<svg><use xlink:href="'.$landing_sprite.'#search"></use></svg>';
                                    break;
                                case 3:
                                    echo '<svg><use xlink:href="'.$landing_sprite.'#credit-card"></use></svg>';
                                    break;
                                case 4:
                                    echo '<svg><use xlink:href="'.$landing_sprite.'#deliver"></use></svg>';
                                    break;
                                default:
                                    break;
                            }
                        echo '</div>';
                        echo '<h4 class="title">'.$fe['title'].'</h4>';
                        echo '<p class="description">'.$fe['desc'].'</p>';
                    echo '</div>';
                } ?>
            </div>
        </div>
    <?php } ?>

</main>
<footer id="footer">
    <div class="container container-xl">
        <div class="content">
            <div class="description">
                <img class="footer-logo lazy" data-src="<?php echo $landing_assets; ?>img/logo-white.svg" alt="IGame">
                <p>
                    تمامی كالاها و خدمات اين فروشگاه، حسب مورد دارای مجوزهای لازم از مراجع مربوطه می‌باشند و فعاليت‌های اين سايت تابع قوانين و مقررات جمهوری اسلامی ايران است
                </p>
            </div>
            <?php get_template_part( 'template-parts/nav/nav', 'footer-landing' ); ?>
        </div>
        <div class="copyright">
            © 2020 iGame.ir All rights reserved.
            <img class="mobile-logo lazy" data-src="<?php echo $landing_assets; ?>img/logo.svg" alt="IGame">
        </div>
    </div>
</footer>
<style>.lazy,.lazyload,.lazyloading {opacity: 0;}.lazyloaded {opacity: 1;transition: opacity 300ms;}</style>
<script src="<?php echo $landing_assets; ?>js/jquery-3.4.1.slim.min.js"></script>
<script>
var sprite_url = "<?php echo $landing_sprite; ?>";
window.lazySizesConfig = window.lazySizesConfig || {};
window.lazySizesConfig.lazyClass = 'lazy';
lazySizesConfig.loadMode = 1;
window.lazySizesConfig.init = false;
</script>
<script src="<?php echo get_template_directory_uri() . '/landings/'; ?>js/plugins.js"></script>
<script src="<?php echo $landing_assets; ?>js/script.js"></script>
<?php get_template_part( 'template-parts/footer/footer', 'scripts' ); ?>
<script>
lazySizes.init();
</script>
</body>
</html>