/********* origin Cards Carousel *********/
$("#origin-carousel").owlCarousel({
  loop: true,
  margin: 25,
  responsiveClass: true,
  rtl: true,
  autoWidth: true,
  responsive: {
    0: {
      items: 1,
    },
    510: {
      items: 3,
    },
    992: {
      items: 4,
    },
    1200: {
      items: 5,
    },
    1570: {
      items: 6,
    },
  },
});
/********* Product slider *********/
$("#products-slider").owlCarousel({
  loop: true,
  margin: 10,
  responsiveClass: true,
  rtl: true,
  //autoplay: true,
  center: true,
  navText: [
    '<svg><use xlink:href="' + sprite_url + '#arrow-right"></svg>',
    '<svg><use xlink:href="' + sprite_url + '#arrow-right"></svg>',
  ],
  smartSpeed: 600,
  nav: true,
  responsive: {
    0: {
      items: 3,
      nav: false,
      autoWidth: true,
    },
    660: {
      items: 3,
      nav: true,
      autoWidth: false,
    },
  },
});
/********* origin Cards Carousel *********/
$("#epic-carousel").owlCarousel({
  loop: true,
  margin: 25,
  responsiveClass: true,
  rtl: true,
  autoWidth: true,
  responsive: {
    0: {
      items: 1,
    },
    510: {
      items: 3,
    },
    992: {
      items: 4,
    },
    1200: {
      items: 5,
    },
    1570: {
      items: 6,
    },
  },
});
/********* uplay Cards Carousel *********/
$("#uplay-carousel").owlCarousel({
  loop: true,
  margin: 25,
  responsiveClass: true,
  rtl: true,
  autoWidth: true,
  responsive: {
    0: {
      items: 1,
    },
    510: {
      items: 3,
    },
    992: {
      items: 4,
    },
    1200: {
      items: 5,
    },
    1570: {
      items: 6,
    },
  },
});
/********* blizzard Cards Carousel *********/
$("#blizzard-carousel").owlCarousel({
  loop: true,
  margin: 25,
  responsiveClass: true,
  rtl: true,
  autoWidth: true,
  responsive: {
    0: {
      items: 1,
    },
    510: {
      items: 3,
    },
    992: {
      items: 4,
    },
    1200: {
      items: 5,
    },
    1570: {
      items: 6,
    },
  },
});
/********* rockstar Cards Carousel *********/
$("#rockstar-carousel").owlCarousel({
  loop: true,
  margin: 20,
  responsiveClass: true,
  rtl: true,
  autoWidth: true,
  nav: true,
  navText: [
    '<svg><use xlink:href="' + sprite_url + '#arrow-right"></svg>',
    '<svg><use xlink:href="' + sprite_url + '#arrow-right"></svg>',
  ],
  responsive: {
    0: {
      items: 1,
      margin: 0,
    },
    510: {
      items: 4,
    },
    992: {
      items: 4,
      margin: 20,
    },
    1200: {
      items: 5,
      margin: 20,
    },
    1570: {
      items: 5,
      margin: 20,
    },
  },
});
/********* gog Cards Carousel *********/
$("#gog-carousel").owlCarousel({
  loop: true,
  margin: 25,
  responsiveClass: true,
  rtl: true,
  autoWidth: true,
  responsive: {
    0: {
      items: 1,
    },
    510: {
      items: 3,
    },
    992: {
      items: 4,
    },
    1200: {
      items: 5,
    },
    1570: {
      items: 6,
    },
  },
});
/********* Gift Cards V2 Carousel *********/
$("#ps4-account-carousel").owlCarousel({
  loop: true,
  margin: 0,
  rtl: true,
  items: 1,
  nav: true,
  dots: true,
  navText: [
    '<svg><use xlink:href="' + sprite_url + '#arrow-right"></svg>',
    '<svg><use xlink:href="' + sprite_url + '#arrow-right"></svg>',
  ],
  responsiveClass: true,
  onInitialized: counter1,
  onTranslated: counter1,
  responsive: {
    0: {
      items: 1,
      margin: 30,
      autoWidth: false,
    },
    1270: {
      items: 1,
      autoWidth: true,
    },
  },
});
function counter1(event) {
  if (!event.namespace) {
    return;
  }
  var slides = event.relatedTarget;
  var item = slides.relative(slides.current());
  $("#ps4-account-carousel-counter").html(
    "<span class='current'>" +
      Number(++item) +
      "</span> / " +
      slides.items().length
  );
}
/********* popular-game-slider *********/
$("#popular-game-slider").owlCarousel({
  loop: true,
  margin: 10,
  items: 1,
  rtl: true,
  nav: false,
  dots: false,
  autoWidth: true,
  autoplay: true,
  URLhashListener: true,
  startPosition: "URLHash",
  onTranslate: addClass,
});
function addClass() {
  $(".popular-game-slider-nav .nav-item").removeClass("active");
  var tmp = window.location.hash;
  $("." + tmp.split("#")[1]).addClass("active");
  return tmp;
}
$("#mini-popular-game-slider").owlCarousel({
  loop: true,
  margin: 10,
  items: 1,
  rtl: true,
  nav: false,
  dots: false,
  autoWidth: true,
  autoplay: true,
  URLhashListener: true,
  startPosition: "URLHash",
  onTranslate: addClass,
});
function addClass() {
  $(".popular-game-slider-nav .nav-item").removeClass("active");
  var tmp = window.location.hash;
  $("." + tmp.split("#")[1]).addClass("active");
  return tmp;
}

var interval = setInterval(function () {
  var active = $(".service-items.active");
  var num = active.attr("data-num");
  num = parseInt(num) + 1;
  if (num <= 5) {
    active.removeClass("active");
    $(".service-items[data-num=" + num + "]").addClass("active");
  } else {
    num = 1;
    active.removeClass("active");
    $(".service-items[data-num=" + num + "]").addClass("active");
  }
}, 3000);
if ($(window).width() <= 992) {
  $("#ps4-service-carousel").addClass("owl-carousel");
  $("#ps4-service-carousel").owlCarousel({
    loop: true,
    margin: 40,
    responsiveClass: true,
    rtl: true,
    autoWidth: true,
    dots: false,
    nav: false,
    responsive: {
      0: {
        items: 4,
      },
    },
  });
}

//uglifyjs js/owl.carousel.min.js js/svgxuse.min.js js/script.js -o js/main.min.js
