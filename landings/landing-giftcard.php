<?php

/*
Template Name: Giftcard
Template Post Type: landing
*/

$landing_assets = theme_cdn . '/landings/giftcard/';

// if( isset($_GET['cdn']) && $_GET['cdn'] == 'true' ){
    $landing_sprite = site_url . 'assets/landings/giftcard/sprite.svg';
// }else{
    // $landing_sprite = $landing_assets . 'img/sprite.svg';
// }


$landing_id = get_the_ID();

?>

<!DOCTYPE html>
<html <?php echo play_language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php the_title(); ?></title>
    <link rel="stylesheet" href="<?php echo $landing_assets; ?>css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $landing_assets; ?>css/main.css">
    <?php do_action( 'wpseo_head' ); ?>
    <?php get_template_part( 'template-parts/header/header', 'scripts' ); ?>
</head>
<body>
<header id="header">
    <nav class="nav">
        <div class="container">
        <a href="<?php echo site_url; ?>" title="<?php echo site_title; ?>"><img class="logo lazy" data-src="<?php echo $landing_assets; ?>img/logo.svg" alt="<?php echo site_title; ?>"></a>
        <a href="<?php echo site_url; ?>" class="white-btn">صفحه اصلی</a>
        </div>
    </nav>
    <?php 
    $giftcard_cards = get_field('giftcard_cards', $landing_id);
    if( $giftcard_cards ){
        ?>
        <div class="hero">
            <div class="services">
                <img class="circles lazy" data-src="<?php echo $landing_assets; ?>img/circles.png">
                <div class="owl-carousel" id="service-carousel">
                    <?php if( isset($giftcard_cards['xbox']) ){ ?>
                        <a href="<?php echo $giftcard_cards['xbox']['link']; ?>" title="<?php echo $giftcard_cards['xbox']['title']; ?>">
                            <div class="service-item xbox-gift-card">
                                <h4 class="title"><?php echo $giftcard_cards['xbox']['title']; ?></h4>
                                <span class="from-price"><?php echo $giftcard_cards['xbox']['price']; ?></span>
                                <span class="buy-icon" ><svg><use xlink:href="<?php echo $landing_sprite; ?>#shopping-cart"></use></svg></span>
                            </div>
                        </a>
                    <?php } ?>
                    <?php if( isset($giftcard_cards['netflix']) ){ ?>
                        <a href="<?php echo $giftcard_cards['netflix']['link']; ?>" title="<?php echo $giftcard_cards['netflix']['title']; ?>">
                            <div class="service-item netflix-gift-card">
                                <h4 class="title"><?php echo $giftcard_cards['netflix']['title']; ?></h4>
                                <span class="from-price"><?php echo $giftcard_cards['netflix']['price']; ?></span>
                                <span class="buy-icon" ><svg><use xlink:href="<?php echo $landing_sprite; ?>#shopping-cart"></use></svg></span>
                            </div>
                        </a>
                    <?php } ?> 
                    <?php if( isset($giftcard_cards['apple']) ){ ?>
                        <a href="<?php echo $giftcard_cards['apple']['link']; ?>" title="<?php echo $giftcard_cards['apple']['title']; ?>">
                            <div class="service-item apple-gift-card">
                                <h4 class="title"><?php echo $giftcard_cards['apple']['title']; ?></h4>
                                <span class="from-price"><?php echo $giftcard_cards['apple']['price']; ?></span>
                                <span class="buy-icon" ><svg><use xlink:href="<?php echo $landing_sprite; ?>#shopping-cart"></use></svg></span>
                            </div>
                        </a>
                    <?php } ?> 
                </div>
            </div>
            <div class="intro">
                <?php 
                echo (isset($giftcard_cards['title'])) ? '<h1 class="title">'.$giftcard_cards['title'].'</h1>' : '';
                echo (isset($giftcard_cards['desc'])) ? '<p class="description">'.$giftcard_cards['desc'].'</p>' : '';
                if( isset($giftcard_cards['link']) ){
                    $target = ( isset($giftcard_cards['link']['target']) ) ? $giftcard_cards['link']['target'] : '';
                    if ( isset($giftcard_cards['link']['url']) && isset($giftcard_cards['link']['title']) ) {
                        echo ' <a href="'.$giftcard_cards['link']['url'].'" class="red-btn">
                            '.$giftcard_cards['link']['title'].'
                            <svg>
                                <use xlink:href="'.$landing_sprite.'#arrow-right"></use>
                            </svg>
                        </a>';
                    }
                }
                ?>
            </div>
        </div>
    <?php } ?>

</header>
<main id="main">
    <?php
    $giftcards = get_field('giftcards', $landing_id);
    if( $giftcards ){ ?>
        <div class="gift-card">
            <div class="container">
                <div class="section-title">
                    <?php echo (!empty($giftcards['title'])) ? '<h2>'.$giftcards['title'].'</h2>' : ''; ?>
                </div>
            </div>
            <div class="owl-carousel" id="gift-cards-carousel">
                <?php
                $products_array = array();
                if( $giftcards['show_last_products'] ){
                    $products_array = get_posts(
                        array(
                            'posts_per_page' => 20,
                            'post_type' => 'giftcard',
                            'fields' => 'ids',
                        )
                    );
                }else{
                    $products_array = $giftcards['products'];
                }    
                
                if( $products_array && !empty($products_array) ){
                    foreach ($products_array as $product_id ) {
                        set_query_var( 'product_id', $product_id );
                        set_query_var( 'landing_assets', $landing_assets );
                        set_query_var( 'landing_sprite', $landing_sprite );
                        get_template_part( 'template-parts/content/content', 'product-item-gift-landing' );
                    }
                }
                
                ?>

            </div>
        </div>
    <?php } 
    $slides = get_field('slides', $landing_id);
    if( $slides ){ ?>
        <div class="gift-card-v2">
            <div class="bg">
                <div class="card-illustration">
                    <img class="card lazy" data-src="<?php echo $landing_assets; ?>img/gift-card-bg-2.svg" alt="">
                    <img class="card lazy" data-src="<?php echo $landing_assets; ?>img/gift-card-bg-1.svg" alt="">
                    <img class="logo lazy" data-src="<?php echo $landing_assets; ?>img/logo-white.svg" alt="">
                </div>
                <?php
                if( isset($slides['slide']) && !empty($slides['slide']) ){ 
                    ?>
                    <div class="gift-cards-v2-carousel-wrapper">
                        <div id="gift-cards-v2-carousel-counter"></div>
                        <div class="owl-carousel" id="gift-cards-v2-carousel">
                            <?php
                            foreach ($slides['slide'] as $slide ) {
                                echo '<div class="carousel-content">
                                    <h4 class="title">'.$slide['title'].'</h4>
                                    <p class="description">'.$slide['content'].'</p>
                                </div>';
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="gift-card-list owl-carousel" id="gift-cards-v2-carousel-2">
                <?php
                 if( isset($slides['spotify']) && !empty($slides['spotify']) ){ 
                    echo '<a href="'.$slides['spotify']['link'].'" title="'.$slides['spotify']['title'].'">
                        <div class="gift-card-item spotify">
                            <h4>'.$slides['spotify']['title'].'</h4>
                            <p>'.$slides['spotify']['desc'].'</p>
                        </div>
                    </a>';
                }
                if( isset($slides['ps4']) && !empty($slides['ps4']) ){ 
                    echo '<a href="'.$slides['ps4']['link'].'" title="'.$slides['ps4']['title'].'">
                        <div class="gift-card-item ps4">
                            <h4>'.$slides['ps4']['title'].'</h4>
                            <p>'.$slides['ps4']['desc'].'</p>
                        </div>
                    </a>';
                }
                if( isset($slides['steam']) && !empty($slides['steam']) ){ 
                    echo '<a href="'.$slides['steam']['link'].'" title="'.$slides['steam']['title'].'">
                        <div class="gift-card-item steam">
                            <h4>'.$slides['steam']['title'].'</h4>
                            <p>'.$slides['steam']['desc'].'</p>
                        </div>
                    </a>';
                }
                ?>
            </div>
            <img class="circle c1 lazy" data-src="<?php echo $landing_assets; ?>img/circle.svg">
            <img class="circle c2 lazy" data-src="<?php echo $landing_assets; ?>img/circle-2.svg">
        </div>
    <?php }
    $features = get_field('features', $landing_id);
    if( $features ){ ?>
        <div class="ordering-steps">
            <div class="container">
                <?php foreach ($features as $key => $fe) {
                    echo '<div class="step-item" data-step="'.$key.'">';
                        echo '<div class="icon">';
                            switch ($key) {
                                case 1:
                                    echo '<svg><use xlink:href="'.$landing_sprite.'#enter"></use></svg>';
                                    break;
                                case 2:
                                    echo '<svg><use xlink:href="'.$landing_sprite.'#search"></use></svg>';
                                    break;
                                case 3:
                                    echo '<svg><use xlink:href="'.$landing_sprite.'#credit-card"></use></svg>';
                                    break;
                                case 4:
                                    echo '<svg><use xlink:href="'.$landing_sprite.'#deliver"></use></svg>';
                                    break;
                                default:
                                    break;
                            }
                        echo '</div>';
                        echo '<h4 class="title">'.$fe['title'].'</h4>';
                        echo '<p class="description">'.$fe['desc'].'</p>';
                    echo '</div>';
                } ?>
            </div>
        </div>
    <?php } ?>
</main>
<footer id="footer">
    <div class="container container-xl">
        <div class="content">
            <div class="description">
                <img class="footer-logo lazy" data-src="<?php echo $landing_assets; ?>img/logo-white.svg" alt="IGame">
                <p>
                    تمامی كالاها و خدمات اين فروشگاه، حسب مورد دارای مجوزهای لازم از مراجع مربوطه می‌باشند و فعاليت‌های اين سايت تابع قوانين و مقررات جمهوری اسلامی ايران است
                </p>
            </div>
            <?php get_template_part( 'template-parts/nav/nav', 'footer-landing' ); ?>
        </div>
        <div class="copyright">
            © 2020 iGame.ir All rights reserved.
            <img class="mobile-logo lazy" data-src="<?php echo $landing_assets; ?>img/logo.svg" alt="IGame">
        </div>
    </div>
</footer>
<style>.lazy,.lazyload,.lazyloading {opacity: 0;}.lazyloaded {opacity: 1;transition: opacity 300ms;}</style>
<script src="<?php echo $landing_assets; ?>js/jquery-3.4.1.slim.min.js"></script>
<script src="<?php echo $landing_assets; ?>js/main.min.js"></script>
<script>
var sprite_url = "<?php echo $landing_sprite; ?>";
window.lazySizesConfig = window.lazySizesConfig || {};
window.lazySizesConfig.lazyClass = 'lazy';
lazySizesConfig.loadMode = 1;
window.lazySizesConfig.init = false;
</script>
<script src="<?php echo get_template_directory_uri() . '/landings/'; ?>js/plugins.js"></script>
<?php get_template_part( 'template-parts/footer/footer', 'scripts' ); ?>
<script>
lazySizes.init();
</script>
</body>
</html>