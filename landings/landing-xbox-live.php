<?php
/*
Template Name: Xbox Live
Template Post Type: landing
*/
$landing_assets = theme_cdn . '/landings/xbox-live/';

// if( isset($_GET['cdn']) && $_GET['cdn'] == 'true' ){
    $landing_sprite = site_url . 'assets/landings/xbox-live/sprite.svg';
// }else{
    // $landing_sprite = $landing_assets . 'img/sprite.svg';
// }
$landing_id = get_the_ID();
?>
<!DOCTYPE html>
<html <?php echo play_language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php the_title(); ?></title>
    <link rel="stylesheet" href="<?php echo $landing_assets; ?>css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $landing_assets; ?>css/main.css">
    <?php do_action('wpseo_head'); ?>
    <?php get_template_part('template-parts/header/header', 'scripts'); ?>
</head>
<body>
<?php
$header = get_field('header', $landing_id);
if ($header) { ?>
    <header id="header">
        <nav class="nav">
            <div class="container">
                <a href="<?php echo site_url; ?>" title="<?php echo site_title; ?>"><img class="logo lazy"
                                                                                         data-src="<?php echo $landing_assets; ?>img/logo.svg"
                                                                                         alt="<?php echo site_title; ?>"></a>
                <a href="<?php echo site_url; ?>" class="white-btn">صفحه اصلی</a>
            </div>
        </nav>
        <div class="hero">
            <div class="intro">
                <?php echo (isset($header['title'])) ? '<h1 class="title"><img src="' . $header['img'] . '" alt="">' . $header['title'] . '</h1>' : ''; ?>
                <?php echo (isset($header['desc'])) ? '<p class="description">' . $header['desc'] . '</p>' : ''; ?>
                <?php if (isset($header['link'])) {
                    $target = (isset($header['link']['target'])) ? $header['link']['target'] : '';
                    if (isset($header['link']['url']) && isset($header['link']['title'])) {
                        echo '<a href="' . $header['link']['url'] . '" class="red-btn">
                                ' . $header['link']['title'] . '
                                <svg>
                                    <use xlink:href="'.$landing_sprite.'#arrow-right"></use>
                                </svg>
                            </a>';
                    }
                } ?>
            </div>
            <div class="gamepad">
                <img class="lazy" data-src="<?php echo $landing_assets; ?>img/gamepad.png">
            </div>
            <div class="scroll-down">
                <span>بیشتر</span>
                <svg>
                    <use xlink:href="<?php echo $landing_sprite; ?>#scroll-down"></use>
                </svg>
            </div>
        </div>
    </header>
<?php } ?>
<main id="main">
    <?php $xbox_live_features = get_field('xbox-live-features', $landing_id);
    if ($xbox_live_features) { ?>
        <div class="feature-box">
            <h2 class="title">هیجان بازی با Xbox Live Gold</h2>
            <div class="container">
                <div class="owl-carousel icon-box-wrapper" id="feature-box">
                    <?php foreach ($xbox_live_features as $key => $xlf) { ?>
                        <div class="icon-box <?php echo $xlf['class'] ?>">
                            <div class="icon">
                                <?php echo (isset($xlf['color-icon'])) ? '<img class="color" src=" ' . $xlf['color-icon'] . ' ">' : '' ?>
                                <?php echo (isset($xlf['white-icon'])) ? '<img class="white" src=" ' . $xlf['white-icon'] . ' ">' : '' ?>
                            </div>
                            <?php echo (isset($xlf['title'])) ? '<h4 class="title">' . $xlf['title'] . '</h4>' : '' ?>
                            <?php echo (isset($xlf['desc'])) ? '<p class="description">' . $xlf['desc'] . '</p>' : '' ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php $giftcard_list_id = get_field('giftcard_list', $landing_id);
    if ($giftcard_list_id) { ?>
        <div class="gift-card two">
            <div class="container">
                <div class="section-title">
                    <h2>دسترسی آسان</h2>
                    <span>با آی گیفت، شما به راحتی می توانید با انتخاب سایت و وارد نمودن کالای مورد نظر در سریع ترین زمان ممکن با کمترین کارمزد تجربه خریدی لذت بخش و با آرامش را داشته باشید</span>
                </div>
            </div>
            <div class="container">
                <div class="owl-carousel" id="gift-cards-carousel-2">
                    <?php
                    $args = array(
                        'post_type' => array('giftcard'),
                        'post_status' => array('publish'),
                        'post__in' => $giftcard_list_id
                    );
                    $giftcard = new WP_Query($args);
                    // The Loop
                    if ($giftcard->have_posts()) {
                        while ($giftcard->have_posts()) {
                            $giftcard->the_post();
                            set_query_var( 'product_id', get_the_ID() );
                            set_query_var( 'landing_assets', $landing_assets );
                            set_query_var( 'landing_sprite', $landing_sprite );
                            get_template_part('template-parts/content/content', 'product-item-landing-style-3');
                        }
                    } else {
                        echo 'لطفا موردی اضاف کنید';
                    }
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php $content_box = get_field('content-box', $landing_id);
    if ($content_box) { ?>
        <div class="xbox-live">
            <div class="container">
                <div class="content">
                    <?php echo (isset($content_box['title'])) ? '<h4 class="title">' . $content_box['title'] . '</h4>' : '' ?>
                    <?php echo (isset($content_box['desc'])) ? '<p class="description">' . $content_box['desc'] . '</p>' : '' ?>
                    <?php if (isset($content_box['link'])) {
                        $target = (isset($content_box['link']['target'])) ? $content_box['link']['target'] : '';
                        if (isset($content_box['link']['url']) && isset($content_box['link']['title'])) {
                            echo '<a href="' . $content_box['link']['url'] . '" class="red-btn">
                                ' . $content_box['link']['title'] . '
                                <svg>
                                    <use xlink:href="'.$landing_sprite.'#arrow-right"></use>
                                </svg>
                            </a>';
                        }
                    } ?>
                </div>
                <div class="img">
                    <img class="lazy" data-src="<?php echo $content_box['img'] ?>">
                </div>
            </div>
        </div>
    <?php } ?>

    <?php $features = get_field('features', $landing_id);
    if ($features) { ?>
        <div class="ordering-steps">
            <div class="container">
                <?php foreach ($features as $key => $fe) {
                    echo '<div class="step-item" data-step="' . $key . '">';
                    echo '<div class="icon">';
                    switch ($key) {
                        case 1:
                            echo '<svg><use xlink:href="'.$landing_sprite.'#enter"></use></svg>';
                            break;
                        case 2:
                            echo '<svg><use xlink:href="'.$landing_sprite.'#search"></use></svg>';
                            break;
                        case 3:
                            echo '<svg><use xlink:href="'.$landing_sprite.'#credit-card"></use></svg>';
                            break;
                        case 4:
                            echo '<svg><use xlink:href="'.$landing_sprite.'#deliver"></use></svg>';
                            break;
                        default:
                            break;
                    }
                    echo '</div>';
                    echo '<h4 class="title">' . $fe['title'] . '</h4>';
                    echo '<p class="description">' . $fe['desc'] . '</p>';
                    echo '</div>';
                } ?>
            </div>
        </div>
    <?php } ?>

</main>
<footer id="footer">
    <div class="container container-xl">
        <div class="content">
            <div class="description">
                <img class="footer-logo lazy" data-src="<?php echo $landing_assets; ?>img/logo-white.svg" alt="IGame">
                <p>
                    تمامی كالاها و خدمات اين فروشگاه، حسب مورد دارای مجوزهای لازم از مراجع مربوطه می‌باشند و فعاليت‌های
                    اين سايت تابع قوانين و مقررات جمهوری اسلامی ايران است
                </p>
            </div>
            <?php get_template_part('template-parts/nav/nav', 'footer-landing'); ?>
        </div>
        <div class="copyright">
            © 2020 iGame.ir All rights reserved.
            <img class="mobile-logo lazy" data-src="<?php echo $landing_assets; ?>img/logo.svg" alt="IGame">
        </div>
    </div>
</footer>
<style>.lazy, .lazyload, .lazyloading {
        opacity: 0;
    }

    .lazyloaded {
        opacity: 1;
        transition: opacity 300ms;
    }</style>
<script src="<?php echo $landing_assets; ?>js/jquery-3.4.1.min.js"></script>
<!-- <script src="<?php echo $landing_assets; ?>js/owl.carousel.min.js" defer></script> -->
<!-- <script src="<?php echo $landing_assets; ?>js/svgxuse.min.js" defer></script> -->
<script>
    var sprite_url = "<?php echo $landing_sprite; ?>";
    window.lazySizesConfig = window.lazySizesConfig || {};
    window.lazySizesConfig.lazyClass = 'lazy';
    lazySizesConfig.loadMode = 1;
    window.lazySizesConfig.init = false;
</script>
<script src="<?php echo get_template_directory_uri() . '/landings/'; ?>js/plugins.js"></script>
<script src="<?php echo $landing_assets; ?>js/script.js" defer></script>
<?php get_template_part('template-parts/footer/footer', 'scripts'); ?>
<script>
    lazySizes.init();
</script>
</body>
</html>