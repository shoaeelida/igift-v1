<?php

/*
Template Name: Origin Access
Template Post Type: landing
*/

$landing_assets = theme_cdn . '/landings/origin-access/';

// if( isset($_GET['cdn']) && $_GET['cdn'] == 'true' ){
    $landing_sprite = site_url . 'assets/landings/origin-access/sprite.svg';
// }else{
    // $landing_sprite = $landing_assets . 'img/sprite.svg';
// }

$landing_id = get_the_ID();
?>
<!DOCTYPE html>
<html <?php echo play_language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php the_title(); ?></title>
    <link rel="stylesheet" href="<?php echo $landing_assets; ?>css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $landing_assets; ?>css/main.css">
    <?php do_action( 'wpseo_head' ); ?>
    <?php get_template_part( 'template-parts/header/header', 'scripts' ); ?>
</head>
<body>
<header id="header">
    <nav class="nav">
        <div class="container">
            <a href="<?php echo site_url; ?>" title="<?php echo site_title; ?>"><img class="logo lazy" data-src="<?php echo $landing_assets; ?>img/light-logo.svg" alt="<?php echo site_title; ?>"></a>
            <a href="<?php echo site_url; ?>" class="white-btn">صفحه اصلی</a>
        </div>
    </nav>
    <?php
    $header = get_field('header', $landing_id);
    if( $header ){ ?>
        <div class="hero">
            <div class="container">
                <div class="intro">
                    <?php echo (isset($header['title'])) ? '<h1 class="title"><img class="lazy" data-src="'.$header['logo'].'">'.$header['title'].'</h1>' : ''; ?>
                    <?php echo (isset($header['desc'])) ? '<p class="description">'.$header['desc'].'</p>' : ''; ?>
                    <?php if(isset($header['link'])) {
                        $target = ( isset($header['link']['target']) ) ? $header['link']['target'] : '';
                        if ( isset($header['link']['url']) && isset($header['link']['title']) ) {
                            echo '<a href="'.$header['link']['url'].'" class="red-btn">
                                '.$header['link']['title'].'
                                <svg><use xlink:href="'.$landing_sprite.'#arrow-right"></use></svg>
                            </a>';
                        }
                    } ?>
                </div>
                <div class="slider-wrapper">
                    <div class="owl-carousel" id="products-slider">
                        <?php
                        $products_array = array();
                        if( $header['show_last_products'] ){
                            $products_array = get_posts(
                                array(
                                    'posts_per_page' => 10,
                                    'post_status' => array('publish'),
                                    'post_type' => 'game',
                                    'fields' => 'ids',
                                )
                            );
                        }else{
                            $products_array = $header['products'];
                        }    
                        if( $products_array && !empty($products_array) ){
                            foreach ($products_array as $product_id ) {
                                set_query_var( 'product_id', $product_id );
                                set_query_var( 'landing_assets', $landing_assets );
                                set_query_var( 'landing_sprite', $landing_sprite );
                                get_template_part('template-parts/content/content', 'product-item-landing-style-4');
                            }
                        } else {
                            echo 'لطفا موردی اضاف کنید';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="scroll-down">
                <span>بیشتر</span>
                <svg>
                    <use xlink:href="<?php echo $landing_sprite; ?>#scroll-down"></use>
                </svg>
            </div>
        </div>
    <?php } ?>
</header>
<main id="main">
    <?php
    $product_list = get_field('products_list', $landing_id);
    if( $product_list ){
        $platform = playGetTermDate($product_list['platform'],'game_platform');      
    ?>
    <div class="gift-card">
        <div class="container">
            <div class="section-title">
                <?php echo (!empty($product_list['title'])) ? '<h2>'.$product_list['title'].'</h2>' : ''; ?>
                <?php echo (!empty($product_list['desc'])) ? '<span>'.$product_list['desc'].'</span>' : ''; ?>
            </div>
        </div>
        <div class="owl-carousel" id="gift-cards-carousel">
            <?php
            $products_array = array();
            if( $product_list['show_last_products'] ){
                $products_array = get_posts(
                    array(
                        'posts_per_page' => 20,
                        'post_status' => array('publish'),
                        'post_type' => 'game',
                        'fields' => 'ids',
                    )
                );
            }else{
                $products_array = $product_list['products'];
            }    
            if( $products_array && !empty($products_array) ){
                foreach ($products_array as $product_id ) {
                    set_query_var( 'product_id', $product_id );
                    set_query_var( 'landing_assets', $landing_assets );
                    set_query_var( 'landing_sprite', $landing_sprite );
                    set_query_var( 'main_platform', $platform );
                    get_template_part('template-parts/content/content', 'product-item-landing-origin');
                }
            } else {
                echo 'لطفا موردی اضاف کنید';
            }
            ?>
        </div>
    </div>
    <?php } ?>

    <?php
    $slides = get_field('slides', $landing_id);
    if ($slides) { ?>
    <div class="apex-legends">
        <img class="c1 lazy" data-src="<?php echo $landing_assets; ?>img/circle.svg" alt="">
        <img class="c2 lazy" data-src="<?php echo $landing_assets; ?>img/circle-2.svg" alt="">
        <div class="container">
            <div class="img">
                <img class="lazy" data-src="<?php echo $landing_assets; ?>img/apex-legends.png" alt="">
            </div>
            <div class="apex-legends-wrapper">
                <div id="apex-legends-slider-counter"></div>
                <div class="apex-legends-slider owl-carousel" id="apex-legends-slider">
                    <?php
                    if (!empty($slides['slide'])) {
                        foreach ($slides['slide'] as $item) { ?>
                            <div class="content">
                                <h4 class="title">
                                    <img class="logo lazy" data-src="<?php echo $landing_assets; ?>img/apex-legends-logo.png" alt="">
                                    <?php echo (isset($item['title'])) ? $item['title'] : ''; ?>
                                    <?php echo (isset($item['platform_name'])) ? '<span class="platform">'.$item['platform_name'].'<img class="lazy" data-src="'.$item['platform_img'].'"></span>' : ''; ?>
                                </h4>
                                <?php echo (isset($item['content'])) ? '<p class="description">'.$item['content'].'</p>' : ''; ?>
                                <?php if(isset($item['link'])) {
                                    $target = ( isset($item['link']['target']) ) ? $item['link']['target'] : '';
                                    if ( isset($item['link']['url']) && isset($item['link']['title']) ) {
                                        echo '<a href="'.$item['link']['url'].'" class="red-btn">
                                            '.$item['link']['title'].'
                                            <svg><use xlink:href="'.$landing_sprite.'#arrow-right"></use></svg>
                                        </a>';
                                    }
                                } ?>
                            </div>
                        <?php
                        }
                    } ?>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>

    <?php
    $show_collection = get_field('game_collection', $landing_id);
    if ($show_collection['show']) { ?>
    <div class="game-list">
        <div class="container">
            <div class="section-title">
                <?php echo (isset($show_collection['title'])) ? '<h2>'.$show_collection['title'].'</h2>' : ''; ?>
            </div>
            <div class="games-img">
                <?php echo (isset($show_collection['img'])) ? '<img class="lazy" data-src="'.$show_collection['img'].'">' : ''; ?>
            </div>
            <?php if(isset($show_collection['link'])) {
                $target = ( isset($show_collection['link']['target']) ) ? $show_collection['link']['target'] : '';
                if ( isset($show_collection['link']['url']) && isset($show_collection['link']['title']) ) {
                    echo '<a href="'.$show_collection['link']['url'].'" class="more">
                                '.$show_collection['link']['title'].'
                            </a>';
                }
            } ?>
        </div>
    </div>
    <?php } ?>

    <?php
    $features = get_field('features', $landing_id);
    if ($features) { ?>
    <div class="ordering-steps">
        <div class="container">
            <?php foreach ($features as $key => $fe) {
                echo '<div class="step-item" data-step="' . $key . '">';
                echo '<div class="icon">';
                switch ($key) {
                    case 1:
                        echo '<svg><use xlink:href="' .$landing_sprite.'#enter"></use></svg>';
                        break;
                    case 2:
                        echo '<svg><use xlink:href="' .$landing_sprite.'#search"></use></svg>';
                        break;
                    case 3:
                        echo '<svg><use xlink:href="' .$landing_sprite.'#credit-card"></use></svg>';
                        break;
                    case 4:
                        echo '<svg><use xlink:href="' .$landing_sprite.'#deliver"></use></svg>';
                        break;
                    default:
                        break;
                }
                echo '</div>';
                echo '<h4 class="title">' . $fe['title'] . '</h4>';
                echo '<p class="description">' . $fe['desc'] . '</p>';
                echo '</div>';
            } ?>
        </div>
    </div>
    <?php } ?>

</main>
<footer id="footer">
    <div class="container container-xl">
        <div class="content">
            <div class="description">
                <img class="footer-logo lazy" data-src="<?php echo $landing_assets; ?>img/logo-white.svg" alt="IGame">
                <p>
                    تمامی كالاها و خدمات اين فروشگاه، حسب مورد دارای مجوزهای لازم از مراجع مربوطه می‌باشند و فعاليت‌های اين سايت تابع قوانين و مقررات جمهوری اسلامی ايران است
                </p>
            </div>
            <?php get_template_part( 'template-parts/nav/nav', 'footer-landing' ); ?>
        </div>
        <div class="copyright">
            © 2020 iGame.ir All rights reserved.
            <img class="mobile-logo lazy" data-src="<?php echo $landing_assets; ?>img/logo.svg" alt="IGame">
        </div>
    </div>
</footer>
<style>.lazy,.lazyload,.lazyloading {opacity: 0;}.lazyloaded {opacity: 1;transition: opacity 300ms;}</style>
<script src="<?php echo $landing_assets; ?>js/jquery-3.4.1.min.js"></script>
<script>
    var sprite_url = "<?php echo $landing_sprite; ?>";
    window.lazySizesConfig = window.lazySizesConfig || {};
    window.lazySizesConfig.lazyClass = 'lazy';
    lazySizesConfig.loadMode = 1;
    window.lazySizesConfig.init = false;
</script>
<script src="<?php echo get_template_directory_uri() . '/landings/'; ?>js/plugins.js"></script>
<script src="<?php echo $landing_assets; ?>js/script.js"></script>
<?php get_template_part( 'template-parts/footer/footer', 'scripts' ); ?>
<script>
    lazySizes.init();
</script>
</body>
</html>