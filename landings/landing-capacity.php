<?php
/*
Template Name: capacity
Template Post Type: landing
*/
$landing_assets = theme_cdn . '/landings/capacity/';
$landing_sprite = site_url . 'assets/landings/capacity/sprite.svg';
$landing_id = get_the_ID();
?>

<!DOCTYPE html>
<html <?php echo play_language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php the_title(); ?></title>
    <link rel="stylesheet" href="<?php echo $landing_assets; ?>css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $landing_assets; ?>css/style.css">
    <?php do_action('wpseo_head'); ?>
    <?php get_template_part('template-parts/header/header', 'scripts'); ?>
</head>
<body>
<header id="header">
    <nav class="nav">
        <div class="container">
            <img class="logo" src="<?php echo $landing_assets; ?>img/light-logo.svg" alt="iGame Logo" />
            <a href="<?php echo site_url; ?>" class="white-btn">صفحه اصلی</a>
        </div>
    </nav>
    <div class="container">
        <?php
        $header = get_field('header', $landing_id);
        if ($header) { ?>
            <div class="hero">
                <div class="intro">
                    <?php echo (isset($header['title'])) ? '<h1 class="title">' . $header['title'] . '</h1>' : ''; ?>
                    <?php echo (isset($header['desc'])) ? '<p class="description">' . $header['desc'] . '</p>' : ''; ?>
                    <?php if (isset($header['link'])) {
                        $target = (isset($header['link']['target'])) ? $header['link']['target'] : '';
                        if (isset($header['link']['url']) && isset($header['link']['title'])) {
                            echo '<a href="' . $header['link']['url'] . '" class="red-btn">
                                ' . $header['link']['title'] . '
                                <svg>
                                    <use xlink:href="'.$landing_sprite.'#arrow-right"></use>
                                </svg>
                            </a>';
                        }
                    } ?>
                </div>
                <div class="services">
                    <img class="circles" src="<?php echo $landing_assets; ?>img/circles.png" />
                    <img class="gradient-circle" src="<?php echo $landing_assets; ?>img/gradient-circle.png" alt="" />
                    <img
                            class="gradient-rectangle"
                            src="<?php echo $landing_assets; ?>img/gradient-rectangle.png"
                            alt=""
                    />
                    <img class="gamepad" src="<?php echo $landing_assets; ?>img/gamepad.png" alt="" />
                </div>
            </div>
        <?php } ?>



    </div>
</header>
<main id="main">



    <div class="container">
    <?php
    $products_list = get_field('products_list', $landing_id);
    if ($products_list) {
        $platform = playGetTermDate($products_list['platform'],'game_platform');
        ?>

            <div class="product-list">
                <h2 class="list-title"><?php echo $products_list['title'] ?></h2>

            <?php
            $products_array = array();
            if ($products_list['show_last_products']) {
                $products_array = get_posts(
                    array(
                        'posts_per_page' => 20,
                        'post_type' => 'game',
                        'fields' => 'ids',
                        'tag__in' => array(173),
                    )
                );
            } else {
                $products_array = $products_list['products'];
            }

            if ($products_array && !empty($products_array)) {
                foreach ($products_array as $product_id) {
                    set_query_var('product_id', $product_id);
                    set_query_var('landing_assets', $landing_assets);
                    set_query_var('landing_sprite', $landing_sprite);
                    set_query_var('main_platform', $platform);
                    get_template_part('template-parts/content/content', 'product-item-landing');
                }
            }
            ?>
            </div>
    <?php } ?>

    <?php
    $first_text_gp = get_field('first_text_gp', $landing_id);
    if ($first_text_gp) { ?>
        <div class="description-dark-box">
            <h2><?php echo $first_text_gp['title'] ?></h2>
            <p><?php echo $first_text_gp['text'] ?></p>
            <div class="gamepad">
                <img src="<?php echo $landing_assets; ?>img/gamepad-2.png" alt="" />
            </div>
            <img class="circle" src="<?php echo $landing_assets; ?>img/circle.svg" alt="" />
        </div>
    <?php } ?>

    <?php
    $second_text_gp = get_field('second_text_gp', $landing_id);
    if ($second_text_gp) { ?>
        <div class="description-light-box">
            <div class="cover">
                <img src="<?php echo $landing_assets; ?>img/cover2.png" alt="" />
            </div>
            <div class="desc-content">
                <h2><?php echo $second_text_gp['title'] ?></h2>
                <p><?php echo $second_text_gp['text'] ?></p>
            </div>
        </div>
    <?php } ?>
    </div>

    <?php
    $third_text_gp = get_field('third_text_gp', $landing_id);
    if ($third_text_gp) { ?>
    <div class="account-properties">
        <div class="container">
            <div class="properties-holder">
                <div class="property-item">
                    <h3 class="title"><?php echo $third_text_gp['title1'] ?></h3>
                    <div class="cover">
                        <p><?php echo $third_text_gp['text1'] ?></p>
                    </div>
                </div>
                <div class="property-item">
                    <h3 class="title"><?php echo $third_text_gp['title2'] ?></h3>
                    <div class="cover">
                        <p><?php echo $third_text_gp['text2'] ?></p>
                    </div>
                </div>
                <div class="property-item">
                    <h3 class="title"><?php echo $third_text_gp['title3'] ?></h3>
                    <div class="cover">
                        <p><?php echo $third_text_gp['text3'] ?></p>
                    </div>
                </div>
            </div>
        </div>
        <img class="circle c1" src="<?php echo $landing_assets; ?>img/circle-2.svg" />
        <img class="circle c2" src="<?php echo $landing_assets; ?>img/circle-2.svg" />
        <img class="circle c3" src="<?php echo $landing_assets; ?>img/circle.svg" />
        <img class="circle c4" src="<?php echo $landing_assets; ?>img/circle.svg" />
    </div>
    <?php } ?>



    <?php
    /*
    $popular_products = get_field('products_list_popular', $landing_id);
    if ($popular_products) {
        $platform = playGetTermDate($popular_products['platform'],'game_platform');
        ?>
        <div class="popular-game">
            <div class="container">
                <div class="section-title">
                    <?php echo (isset($popular_products['title'])) ? '<h2>' . $popular_products['title'] . '</h2>' : ''; ?>
                </div>
                <div class="slider-content">
                    <?php
                    $products_array = array();
                    $products_array = $popular_products['products'];
                    if ($products_array && !empty($products_array)) { ?>
                        <div class="popular-game-slider owl-carousel" id="popular-game-slider">
                            <?php foreach ($products_array as $key => $product_id) {
                                $num = $key + 1;
                                $url = get_post_permalink($product_id);
                                if( $platform && !empty($platform ) ){
                                    $url .= '?platform='.$platform['slug'];
                                }
                                echo '<a href="' . $url . '" class="game-item" data-num="' . $num . '" data-hash="slide-' . $num . '">';
                                echo (has_post_thumbnail($product_id)) ? '<div class="img" style="background-image: url(' . esc_url(get_the_post_thumbnail_url($product_id, 'full')) . ')" alt="' . plain_product_title(get_the_title($product_id)) . '"></div>' : '';
                                echo '<svg class="favorite"><use xlink:href="'.$landing_sprite.'#love"></use></svg>';
                                echo '<h4 class="title">' . plain_product_title(get_the_title($product_id)) . '</h4>';
                                echo '</a>';
                            } ?>
                        </div>
                        <div class="popular-game-slider-nav">
                            <?php
                            $count = count($products_array);
                            foreach ($products_array as $key => $product_id) {
                                $num = $key + 1;

                                if ($num == 1 || $num == 4) {
                                    echo '<div>';
                                }

                                if ($num == 1) {
                                    echo '<div class="nav-item slide-' . $num . ' active" data-num="' . $num . '"><a href="#slide-' . $num . '">' . plain_product_title(get_the_title($product_id)) . '</a></div>';
                                } else {
                                    echo '<div class="nav-item slide-' . $num . '" data-num="' . $num . '"><a href="#slide-' . $num . '">' . plain_product_title(get_the_title($product_id)) . '</a></div>';
                                }

                                if ($num == 3 || $num >= $count) {
                                    echo '</div>';
                                }
                            } ?>

                        </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    <?php }
    */ ?>




    <div class="description-by-carousel">
        <!-- <div class="circle">
            <img src="<?php echo $landing_assets; ?>img/circle-2.svg" />
        </div> -->
        <div class="container">
            <?php
            /*
            <div class="carousel-wrap">
                <div class="owl-carousel" id="desc-carousel">
                    <div class="item"><img src="<?php echo $landing_assets; ?>img/p1.png" alt="" /></div>
                    <div class="item"><img src="<?php echo $landing_assets; ?>img/p2.png" alt="" /></div>
                    <div class="item"><img src="<?php echo $landing_assets; ?>img/p3.png" alt="" /></div>
                    <div class="item"><img src="<?php echo $landing_assets; ?>img/p4.png" alt="" /></div>
                    <div class="item"><img src="<?php echo $landing_assets; ?>img/p5.png" alt="" /></div>
                    <div class="item"><img src="<?php echo $landing_assets; ?>img/p6.png" alt="" /></div>
                    <div class="item"><img src="<?php echo $landing_assets; ?>img/p7.png" alt="" /></div>
                    <div class="item"><img src="<?php echo $landing_assets; ?>img/p2.png" alt="" /></div>
                    <div class="item"><img src="<?php echo $landing_assets; ?>img/p3.png" alt="" /></div>
                </div>
            </div>
            <?php
            */

            $fourth_text_gp = get_field('fourth_text_gp', $landing_id);
            if ($fourth_text_gp) { ?>
            <div class="description-wrap">
                <div class="right-side">
                    <h2 class="title"><?php echo $fourth_text_gp['title'] ?></h2>
                    <p><?php echo $fourth_text_gp['text'] ?></p>
                </div>
                <div class="left-side">
                    <h3 class="title"><?php echo $fourth_text_gp['list_title'] ?></h3>

                    <?php
                    $list_items = $fourth_text_gp['list'];
                    foreach ($list_items as $key => $item) { ?>
                        <p><?php echo $item['text'] ?></p>
                    <? } ?>

                </div>
            </div>
            <?php } ?>
        </div>
    </div>

    <?php
    /*
    $features = get_field('features', $landing_id);
    if ($features) { ?>
    <div class="ordering-steps">
        <div class="container">
            <?php foreach ($features as $key => $fe) {
                echo '<div class="step-item" data-step="' . $key . '">';
                echo '<div class="icon">';
                switch ($key) {
                    case 1:
                        echo '<svg><use xlink:href="'.$landing_sprite.'#enter"></use></svg>';
                        break;
                    case 2:
                        echo '<svg><use xlink:href="'.$landing_sprite.'#search"></use></svg>';
                        break;
                    case 3:
                        echo '<svg><use xlink:href="'.$landing_sprite.'#credit-card"></use></svg>';
                        break;
                    case 4:
                        echo '<svg><use xlink:href="'.$landing_sprite.'#deliver"></use></svg>';
                        break;
                    default:
                        break;
                }
                echo '</div>';
                echo '<h4 class="title">' . $fe['title'] . '</h4>';
                echo '<p class="description">' . $fe['desc'] . '</p>';
                echo '</div>';
            } ?>
        </div>
    </div>
    <?php } 
    */?>

</main>
<footer id="footer">
    <img class="circle c1" src="<?php echo $landing_assets; ?>img/circle.svg" />
    <img class="circle c2" src="<?php echo $landing_assets; ?>img/circle-2.svg" />
    <img class="circle c3" src="<?php echo $landing_assets; ?>img/circle-2.svg" />
    <img class="circle c4" src="<?php echo $landing_assets; ?>img/circle.svg" />
    <div class="container container-xl">
        <div class="content">
            <div class="description">
                <img class="footer-logo" src="<?php echo $landing_assets; ?>img/logo-white.svg" alt="IGame" />
                <p>
                    تمامی كالاها و خدمات اين فروشگاه، حسب مورد دارای مجوزهای لازم از
                    مراجع مربوطه می‌باشند و فعاليت‌های اين سايت تابع قوانين و مقررات
                    جمهوری اسلامی ايران است
                </p>
            </div>
            <?php get_template_part('template-parts/nav/nav', 'footer-landing'); ?>
        </div>
        <div class="copyright">
            © 2020 iGame.ir All rights reserved.
            <img class="mobile-logo" src="<?php echo $landing_assets; ?>img/logo.svg" alt="IGame" />
        </div>
    </div>
</footer>
<style>.lazy, .lazyload, .lazyloading {
        opacity: 0;
    }

    .lazyloaded {
        opacity: 1;
        transition: opacity 300ms;
    }</style>
<script src="<?php echo $landing_assets; ?>js/jquery-3.4.1.slim.min.js"></script>
<!-- <script src="<?php echo $landing_assets; ?>js/jquery-3.4.1.min.js"></script> -->
<!-- <script src="<?php echo $landing_assets; ?>js/owl.carousel.min.js"></script> -->
<script>
    var sprite_url = "<?php echo $landing_sprite; ?>";
    window.lazySizesConfig = window.lazySizesConfig || {};
    window.lazySizesConfig.lazyClass = 'lazy';
    lazySizesConfig.loadMode = 1;
    window.lazySizesConfig.init = false;
</script>
<script src="<?php echo theme_cdn . '/landings/'; ?>js/plugins.js"></script>
<script src="<?php echo $landing_assets; ?>js/script.js" defer></script>
<?php get_template_part('template-parts/footer/footer', 'scripts'); ?>
<script>
    lazySizes.init();
</script>
</body>
</html>
