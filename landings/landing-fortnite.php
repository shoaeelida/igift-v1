<?php
/*
Template Name: Fortnite
Template Post Type: landing
*/

$landing_assets = theme_cdn . '/landings/fortnite/';

// if( isset($_GET['cdn']) && $_GET['cdn'] == 'true' ){
    $landing_sprite = site_url . 'assets/landings/fortnite/sprite.svg';
// }else{
    // $landing_sprite = $landing_assets . 'img/sprite.svg';
// }

$landing_id = get_the_ID();
?>
<!DOCTYPE html>
<html <?php echo play_language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php the_title(); ?></title>
    <link rel="stylesheet" href="<?php echo $landing_assets; ?>css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $landing_assets; ?>css/main.css">
    <?php do_action('wpseo_head'); ?>
    <?php get_template_part('template-parts/header/header', 'scripts'); ?>
</head>
<body>
<?php
$header = get_field('header', $landing_id);
if ($header) { ?>
    <header id="header">
        <nav class="nav">
            <div class="container">
                <a href="<?php echo site_url; ?>" title="<?php echo site_title; ?>"><img class="logo lazy"
                                                                                         data-src="<?php echo $landing_assets; ?>img/light-logo.svg" alt="<?php echo site_title; ?>"></a>
                <a href="<?php echo site_url; ?>" class="white-btn">صفحه اصلی</a>
            </div>
        </nav>
        <div class="hero">
            <div class="container">
            <?php echo (isset($header['img'])) ? '<img class="lazy fortnite" data-src="'.$header['img'].'">' : ''; ?>
            <div class="intro">
                <?php echo (isset($header['title'])) ? '<h1 class="title">' . $header['title'] . '</h1>' : ''; ?>
                <?php echo (isset($header['desc'])) ? '<p class="description">' . $header['desc'] . '</p>' : ''; ?>
                <?php if (isset($header['link'])) {
                    $target = (isset($header['link']['target'])) ? $header['link']['target'] : '';
                    if (isset($header['link']['url']) && isset($header['link']['title'])) {
                        echo '<a href="' . $header['link']['url'] . '" class="red-btn">
                                ' . $header['link']['title'] . '
                                <svg>
                                    <use xlink:href="'.$landing_sprite.'#arrow-right"></use>
                                </svg>
                            </a>';
                    }
                } ?>
                <?php if (isset($header['trailer_link'])) {
                    $target = (isset($header['trailer_link']['target'])) ? $header['trailer_link']['target'] : '';
                    if (isset($header['trailer_link']['url']) && isset($header['trailer_link']['title'])) {
                        echo '<a href="' . $header['trailer_link']['url'] . '" class="outline-btn">
                                ' . $header['trailer_link']['title'] . '
                                <svg>
                                    <use xlink:href="'.$landing_sprite.'#play"></use>
                                </svg>
                            </a>';
                    }
                } ?>
            </div>
            </div>
            <div class="scroll-down">
                <span>بیشتر</span>
                <svg>
                    <use xlink:href="<?php echo $landing_sprite ?>#scroll-down"></use>
                </svg>
            </div>
        </div>
    </header>
<?php } ?>
<main id="main">
    <?php
    $product_list = get_field('product_list', $landing_id);
    if( $product_list ){ ?>
        <div class="news">
            <div class="container">
                <?php
                if( $product_list['products'] && !empty($product_list['products']) ){
                    $c = 0;
                    $all = count($product_list['products']);
                
                    foreach ($product_list['products'] as $p ) {
                        $c++;
                     
                        if ( $c == 1 ) {
                            $row_id = 'nc1';
                        }elseif( $c == 3 ){
                            $row_id = 'nc2';
                        }elseif( $c == 5 ){
                            $row_id = 'nc3';
                        }else{
                            $row_id = null;
                        }

                        // start row
                        echo ( $row_id ) ? '<div class="row" id="'.$row_id.'">' : '';

                        $product_url = (!empty($p['product'])) ? get_the_permalink( $p['product'] ) : null;
                        $img = (!empty($p['img'])) ? $p['img'] : '';
                        $label = (!empty($p['label'])) ? $p['label'] : '';
                        $title = (!empty($p['title'])) ? $p['title'] : '';
                        $desc = (!empty($p['desc'])) ? $p['desc'] : '';
                        
                        $class = 'news-item';
                        $class .= ($c == 1) ? ' first' : '';
                        $class .= ($c == 2) ? ' second' : '';
                        $class .= ($c == 4) ? ' third' : '';
                        $class .= ($c == 3) ? ' fourth' : '';
                        $class .= ($c == 5) ? ' fifth' : '';
                        $class .= ($c == 6) ? ' sixth' : '';
                        $class .= ($c == 7) ? ' seventh' : '';
                        // product
                        echo ($product_url) ? '<a href="'.$product_url.'">' : ''; 
                            echo '<div class="'.$class.'">';
                                echo (!empty($img)) ? '<img class="lazy" data-src="'.$img.'" alt="'.$title.'">' : ''; 
                                echo (!empty($label)) ? '<span class="hot-tag"><svg><use xlink:href="'.$landing_sprite .'#fire"></use></svg>'.$label.'</span>' : ''; 
                                echo (!empty($title)) ? '<h4>'.$title.'</h4>' : ''; 
                                echo (!empty($desc)) ? '<p>'.$desc.'</p>' : ''; 
                            echo '</div>'; 
                        echo ($product_url) ? '</a>' : ''; 

                        // close row
                        echo ( $c == 2 || $c == 4 || $c == $all ) ? '</div>' : '';

                    }
                }
                if (isset($product_list['link'])) {
                    $target = (isset($product_list['link']['target'])) ? $product_list['link']['target'] : '';
                    if (isset($product_list['link']['url']) && isset($product_list['link']['title'])) {
                        echo '<a href="' . $product_list['link']['url'] . '" class="more">
                                ' . $product_list['link']['title'] . '
                            </a>';
                    }
                } ?>
            </div>
            <div class="container">
                <div class="section-title">
                    <?php echo (isset($product_list['title'])) ? '<h2>' . $product_list['title'] . '</h2>' : ''; ?>
                    <?php echo (isset($product_list['desc'])) ? '<span>' . $product_list['desc'] . '</span>' : ''; ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php
    $build_fortnite = get_field('build_fortnite', $landing_id);
    if( $build_fortnite ){ ?>
        <div class="fortnite-building">
            <img src="<?php echo $landing_assets ?>img/circle-2.svg" class="c1">
            <div class="right-img">
                <img class="lazy" data-src="<?php echo $landing_assets ?>img/v.png">
            </div>
            <div class="content">
                <?php echo (isset($build_fortnite['img'])) ? '<img class="lazy" data-src="'.$build_fortnite['img'].'">' : ''; ?>
                <?php echo (isset($build_fortnite['title'])) ? '<h4>' . $build_fortnite['title'] . '</h4>' : ''; ?>
                <?php echo (isset($build_fortnite['desc'])) ? '<p>' . $build_fortnite['desc'] . '</p>' : ''; ?>
                <?php if (isset($build_fortnite['link'])) {
                    $target = (isset($build_fortnite['link']['target'])) ? $build_fortnite['link']['target'] : '';
                    if (isset($build_fortnite['link']['url']) && isset($build_fortnite['link']['title'])) {
                        echo '<a href="' . $build_fortnite['link']['url'] . '" class="red-btn">
                                ' . $build_fortnite['link']['title'] . '
                                <svg>
                                    <use xlink:href="'.$landing_sprite.'#arrow-right"></use>
                                </svg>
                            </a>';
                    }
                } ?>
            </div>
            <div class="left-img">
                <img class="lazy" data-src="<?php echo $landing_assets ?>img/man.png">
            </div>
        </div>
    <?php } ?>

    <?php
    $features = get_field('features', $landing_id);
    if( $features ){ ?>
        <div class="ordering-steps">
            <div class="container">
                <?php foreach ($features as $key => $fe) {
                    echo '<div class="step-item" data-step="'.$key.'">';
                    echo '<div class="icon">';
                    switch ($key) {
                        case 1:
                            echo '<svg><use xlink:href="'.$landing_sprite .'#enter"></use></svg>';
                            break;
                        case 2:
                            echo '<svg><use xlink:href="'.$landing_sprite .'#search"></use></svg>';
                            break;
                        case 3:
                            echo '<svg><use xlink:href="'.$landing_sprite .'#credit-card"></use></svg>';
                            break;
                        case 4:
                            echo '<svg><use xlink:href="'.$landing_sprite .'#deliver"></use></svg>';
                            break;
                        default:
                            break;
                    }
                    echo '</div>';
                    echo '<h4 class="title">'.$fe['title'].'</h4>';
                    echo '<p class="description">'.$fe['desc'].'</p>';
                    echo '</div>';
                } ?>
            </div>
        </div>
    <?php } ?>

</main>
<footer id="footer">
    <div class="container container-xl">
        <div class="content">
            <div class="description">
                <img class="footer-logo lazy" data-src="<?php echo $landing_assets; ?>img/logo-white.svg" alt="IGame">
                <p>
                    تمامی كالاها و خدمات اين فروشگاه، حسب مورد دارای مجوزهای لازم از مراجع مربوطه می‌باشند و فعاليت‌های
                    اين سايت تابع قوانين و مقررات جمهوری اسلامی ايران است
                </p>
            </div>
            <?php get_template_part('template-parts/nav/nav', 'footer-landing'); ?>
        </div>
        <div class="copyright">
            © 2020 iGame.ir All rights reserved.
            <img class="mobile-logo lazy" data-src="<?php echo $landing_assets; ?>img/logo.svg" alt="IGame">
        </div>
    </div>
</footer>
<style>.lazy, .lazyload, .lazyloading {
        opacity: 0;
    }
    .lazyloaded {
        opacity: 1;
        transition: opacity 300ms;
    }</style>
<script src="<?php echo $landing_assets; ?>js/jquery-3.4.1.min.js"></script>
<!--<script src="<?php /*echo $landing_assets; */?>js/owl.carousel.min.js" defer></script>
<script src="<?php /*echo $landing_assets; */?>js/svgxuse.min.js" defer></script>-->
<script>
    var sprite_url = "<?php echo $landing_sprite; ?>";
    window.lazySizesConfig = window.lazySizesConfig || {};
    window.lazySizesConfig.lazyClass = 'lazy';
    lazySizesConfig.loadMode = 1;
    window.lazySizesConfig.init = false;
</script>
<script src="<?php echo theme_cdn . '/landings/'; ?>js/plugins.js"></script>
<script src="<?php echo $landing_assets; ?>js/script.js" defer></script>
<?php get_template_part('template-parts/footer/footer', 'scripts'); ?>
<script>
    lazySizes.init();
</script>
</body>
</html>
