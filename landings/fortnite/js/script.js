/********* Product slider *********/

$('#products-slider').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    rtl : true,
    //autoplay: true,
    center:true,
    navText: ["<svg><use xlink:href=\"img/sprite.svg#arrow-right\"></svg>","<svg><use xlink:href=\"img/sprite.svg#arrow-right\"></svg>"],
    smartSpeed:600,
    nav:true,
    responsive:{
        0:{
            items:3,
            nav:false,
            autoWidth:true,
            margin:36,
        },
        660:{
            items:3,
            nav:true,
            autoWidth:false,
            margin:10,
        }
    }
});

/********* Gift Cards Carousel *********/

$('#gift-cards-carousel').owlCarousel({
    loop:true,
    margin:0,
    responsiveClass:true,
    rtl : true,
    autoWidth:true,
    responsive:{
        0:{
            items:1,
        },
        510:{
            items:3,
        },
        992:{
            items:4,
        },
        1200:{
            items:5,
        },
        1570:{
            items:6,
        }
    }
});

/********* apex-legends-slider *********/

$('#apex-legends-slider').owlCarousel({
    loop:false,
    margin:20,
    rtl : true,
    items:1,
    nav:false,
    dots: true,
    responsiveClass:true,
    onInitialized  : counter1,
    onTranslated : counter1,
    autoWidth:true,
    responsive:{
        0:{
            items:1,
            margin:30,
            autoWidth:false,
        },
        1200:{
            items:1,
            autoWidth:true,
        },
    }
});
function counter1(event) {
    if (!event.namespace) {
        return;
    }
    var slides = event.relatedTarget;
    var item = slides.relative(slides.current());
    $('#apex-legends-slider-counter').html("<span class='current'>"+ Number(++item) +"</span> / "+slides.items().length)
} 
// Scroll down
$(".scroll-down").on('click', function() {
    var tmp = $("#main .news").offset().top;
    $("html, body").animate({
        scrollTop: tmp+'px'
    }, 1000);
});

//uglifyjs js/jquery-3.4.1.slim.min.js js/owl.carousel.min.js js/svgxuse.min.js js/script.js -o js/main.min.js
if ($(window).width() <= 800) {
    $('#nc1, #nc2').addClass('owl-carousel');
    $('#nc1, #nc2').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        rtl : true,
        autoWidth:true,
        dots:false,
        //autoplay:true,
        nav:false,
        responsive:{
            0:{
                items:2,
            },
        }
    });
}