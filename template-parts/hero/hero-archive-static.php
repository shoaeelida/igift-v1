<?php

$term = null;
$image = null;
$title = '';
$desc = null;
$categorize_holder = [
    'game' => '<a href="'.get_post_type_archive_link( 'game' ).'" class="cat-item">
            <svg class="cat-icon" viewBox="0 0 495.28 250.42">
                <use xlink:href="'.sprite_url.'#game"></use>
            </svg>
            بازی ها
            <svg class="arrow" viewBox="0 0 12.56 19.46">
                <use xlink:href="'.sprite_url.'#arrow"></use>
            </svg>
        </a>',
    'dlc' => '<a href="'.get_post_type_archive_link( 'dlc' ).'" class="cat-item">
        <svg class="cat-icon" viewBox="0 0 495.28 250.42">
            <use xlink:href="'.sprite_url.'#game"></use>
        </svg>
        پک های الحاقی
        <svg class="arrow" viewBox="0 0 12.56 19.46">
            <use xlink:href="'.sprite_url.'#arrow"></use>
        </svg>
    </a>', 
    'giftcard' => '<a href="'.get_post_type_archive_link( 'giftcard' ).'" class="cat-item">
        <svg class="cat-icon" viewBox="0 -12 585.00001 585">
            <use xlink:href="'.sprite_url.'#gift"></use>
        </svg>
        گیفت کارت ها
        <svg class="arrow" viewBox="0 0 12.56 19.46">
            <use xlink:href="'.sprite_url.'#arrow"></use>
        </svg>
    </a>',
];

if( is_post_type_archive('game') ) {
    $game_archive = get_field('game_archive', 'option'); 
    if( $game_archive ){
        $title = (isset($game_archive['title'])) ? $game_archive['title'] : '';
        $desc = (isset($game_archive['desc'])) ? $game_archive['desc'] : '';
        $image = (isset($game_archive['img'])) ? $game_archive['img'] : '';
    }
    unset($categorize_holder['game']);
}elseif( is_post_type_archive('dlc') ) {
    $dlc_archive = get_field('dlc_archive', 'option'); 
    if( $dlc_archive ){
        $title = (isset($dlc_archive['title'])) ? $dlc_archive['title'] : '';
        $desc = (isset($dlc_archive['desc'])) ? $dlc_archive['desc'] : '';
        $image = (isset($dlc_archive['img'])) ? $dlc_archive['img'] : '';
    }
    unset($categorize_holder['dlc']);
}elseif( is_post_type_archive('giftcard') ) {
    $giftcard_archive = get_field('giftcard_archive', 'option'); 
    if( $giftcard_archive ){
        $title = (isset($giftcard_archive['title'])) ? $giftcard_archive['title'] : '';
        $desc = (isset($giftcard_archive['desc'])) ? $giftcard_archive['desc'] : '';
        $image = (isset($giftcard_archive['img'])) ? $giftcard_archive['img'] : '';
    }
    unset($categorize_holder['giftcard']);
}else{
    unset($categorize_holder['game']);
    $term = get_queried_object();
    if( $term ){
        $image = wp_get_attachment_image_src( get_field('img', $term) ,'full');
        $image = ( $image ) ? $image[0] : null;
        $title = get_field('title', $term);
        $title = ( !$title ) ? $term->name : $title;
        $desc = get_field('desc', $term);
    }
}

if( !$image ){
    $image = get_field('archive_img', 'option');
}

?>

<div class="archive-hero" <?php echo ($image) ? 'style="background-image:url(\''.$image.'\');"' : ''; ?>>
    <div class="cover-hero"></div>
    <div class="archive-hero__content">
        <h1 class="title"><?php echo $title; ?></h1>
        <div class="categorize-holder">
            <?php 
            foreach ($categorize_holder as $cat) {
                echo $cat;
            }
            ?>
        </div>
        <?php echo (!empty($desc)) ? '<p>'.$desc.'</p>' : ''; ?>
        <!-- <a href="#" class="more">بیشتر</a> -->
    </div>
</div>