<div class="home-hero">
    <div class="hero-description">
        <h1 class="hero-title">
            خرید گیفت کارت اورجینال 
            <span>همراه با تحویل آنی و کارمزد کم </span>
        </h1>
        <div class="advantage">
            <strong class="title">امتیازات ویژه خرید گیفت کارت از فروشگاه igift.ir</strong>
            <span class="advantage-item">
            وجود پروتکل امنیتی  ssl برای برقراری ارتباطات ایمن
            </span>
            <span class="advantage-item">
            ضمین ارائه بهترین و ارزان‌ترین قیمت گیفت کارت  با توجه به نرخ ارز
            </span>
            <span class="advantage-item">
            ارائه انواع گیفت کارت از قیمت‌های پایین یک دلاری تا بالاترین قیمت‌های ممکن
            </span>
            <span class="advantage-item">
            پشتیبانی 24 ساعته  به صورت تلفنی و چت آنلاینپشتیبانی 24 ساعته  به صورت تلفنی و چت آنلاین
            </span>
        </div>
        <div class="btn-holder">
            <a href="<?php echo get_post_type_archive_link('giftcard'); ?>" class="btn btn--red btn--text--right fs-15 bold">
                مشاهده لیست گیفت کارت‌ها
                <svg viewBox="0 0 12.56 19.47">
                    <use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
                </svg>
            </a>
        </div>
    </div>
    <div class="absolute-content">
        <div class="abs-card music">
            <div class="image-holder">
                <img src="<?php echo theme_assets; ?>/imgs/abs1.jpg" alt="" />
            </div>
            <div class="card-desc">
                <div class="icon">
                    <svg viewBox="0 0 168 168">
                        <use xlink:href="<?php echo sprite_url; ?>#spotify"></use>
                    </svg>
                </div>
                <span>گیفت کارت‌های</span>
                سرویس پخش موسیقی
            </div>
        </div>
        <div class="abs-card amazon">
            <div class="image-holder">
                <img id="amazon" src="<?php echo theme_assets; ?>/imgs/amazon.png" alt="" />
                <img id="box" src="<?php echo theme_assets; ?>/imgs/box.png" alt="" />
            </div>
        </div>
        <div class="abs-card travel">
            <div class="card-desc">
                <div class="icon">
                    <svg viewBox="0 0 35.92 35.77">
                        <use xlink:href="<?php echo sprite_url; ?>#travel"></use>
                    </svg>
                </div>
                <span>گیفت کارت‌های</span>
                مربوط به سفر
            </div>
            <div class="image-holder">
                <img id="travel" src="<?php echo theme_assets; ?>/imgs/travel.png" alt="" />
            </div>
        </div>
        <div class="abs-card game">
            <div class="image-holder">
                <img id="game" src="<?php echo theme_assets; ?>/imgs/game.png" alt="" />
            </div>
            <div class="title">گیفت کارت بازی‌ها</div>
            <div id="platform-carousel" class="platform-carousel owl-carousel">
                <div class="game-platform">
                    <svg id="playStation" viewBox="0 0 585.15 451.63">
                        <use xlink:href="<?php echo sprite_url; ?>#playStation"></use>
                    </svg>
                </div>
                <div class="game-platform">
                    <svg id="xbox" viewBox="0 0 565.28 566.27">
                        <use xlink:href="<?php echo sprite_url; ?>#xbox"></use>
                    </svg>
                </div>
                <div class="game-platform">
                    <svg id="nintendo" viewBox="0 0 550.0 550.0">
                        <use xlink:href="<?php echo sprite_url; ?>#nintendo"></use>
                    </svg>
                </div>
            </div>
        </div>
        <svg id="line" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 726.94 593.81">
            <defs>
                <linearGradient id="Degradado_sin_nombre_71" x1="179.45" y1="278.68" x2="418.19" y2="278.68" gradientTransform="matrix(0.75, 0.66, -0.84, 0.95, 333.13, -277.24)" gradientUnits="userSpaceOnUse">
                    <stop offset="0" stop-color="#7a99ed" />
                    <stop offset="1" stop-color="#fefcff" />
                </linearGradient>
            </defs>
            <path d="M-8.58,510.78c6-20.41,27.31-63.45,51.75-91.18,1.61-1.83,3.24-3.59,4.85-5.23,23.42-24,43.46-34.51,63.63-43.95l2.81-1.32c19.57-9.12,46.36-21.61,63-47.18s17.21-52.7,17.63-74.42l0-1c.53-26.07,1.85-50.39,3-71.84l.5-9.27c.51-9.58,1.08-20.43,10.34-31.61,8.18-9.89,23.77-18,41.68-21.72a246,246,0,0,1,28.9-3.8c7-.61,13.45-1.08,19.7-1.53,8.85-.64,17.2-1.25,24.67-2.11,14.1-1.63,26.84-4.13,37.44-6.36a330,330,0,0,0,61.5-18.65c15.57-6.33,34.22-14.94,57-26.32,20.58-10.34,44.88-23.23,74.3-39.4,65.74-36,119.23-67.71,163.54-96.82l.65,1.41C674-52.36,620.46-20.66,554.67,15.41,525.23,31.59,500.9,44.5,480.29,54.85c-22.83,11.4-41.53,20-57.15,26.39A332.71,332.71,0,0,1,361.31,100c-10.64,2.24-23.42,4.75-37.59,6.38-7.51.86-15.88,1.47-24.75,2.12-6.24.45-12.7.92-19.67,1.53a241.55,241.55,0,0,0-28.7,3.77c-17.47,3.61-32.59,11.44-40.45,20.94-8.78,10.61-9.31,20.65-9.83,30.37l-.5,9.27c-1.16,21.44-2.48,45.74-3,71.78l0,1c-.42,22-.94,49.33-18,75.51s-44.16,38.8-64,48L112,372c-20,9.35-39.82,19.71-63,43.47-1.58,1.62-3.18,3.35-4.76,5.14C20.08,448.06-1,490.69-7,510.91Z" transform="translate(8.58 82.91)" fill="url(#Degradado_sin_nombre_71)" />
        </svg>
    </div>
</div>