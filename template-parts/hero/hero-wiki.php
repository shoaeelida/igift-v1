<div class="gameinfo-hero">
    <div class="cover-bg">
        <img src="<?php echo get_field('cover') ?>" alt="<?php echo the_title() ?>">
    </div>
    <div class="mainContainer by-padding">
        <div class="gamehero-content">
            <div class="image-holder">
                <img src="<?php echo get_the_post_thumbnail_url() ?>" alt="<?php echo the_title() ?>">
            </div>
            <h1 class="title"><?php echo the_title() ?></h1>
        </div>
    </div>
</div>