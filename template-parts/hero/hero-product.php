<?php
get_template_part('template-parts/parts/data-structure');
$product_obj = get_product_obj();
if ($product_obj) {

    $type_name = 'محصول';

    switch ($product_obj['type']) {
        case 'giftcard':
            $type_name = 'گیفت کارت';
            break;
        case 'game':
            $type_name = 'بازی';
            break;
        default:
            break;
    }

    $product_ID = $product_obj['id'];
    $main_product = (isset($product_obj['main_product'])) ? $product_obj['main_product'] : NULL;

    $canAddToFavorite = '';
    $favoritesButton = '';
    $inUserFavorites = false;

    if (is_user_logged_in()) {
        $canAddToFavorite = true;
        $favoritesButton = get_favorites_button($product_ID);
        $inUserFavorites = (strpos($favoritesButton, 'active') !== false) ? true : false;
    }

    $min_price = $product_obj['min_price'];

    // $ratingValue = ic_get_reviews_average($product_obj['id']);
    // $reviewCount = ic_get_product_review_totals($product_obj['id'])['total'];

    echo '<div class="ajax-fav-btn" style="display:none;">' . $favoritesButton . '</div>';

    $cover_style = (isset($product_obj['cover']) && !empty($product_obj['cover'])) ? ' style="background-image:url(' . $product_obj['cover'] . ');"' : '';

    if ($product_obj['type'] == 'game' || $product_obj['type'] == 'dlc' ) { ?>
        <div class="product-hero">
            <div class="back-cover">
                <img src="<?php echo $product_obj['cover'] ?>">
            </div>
            <div class="product-hero__master">
                <div class="product-img">
                    <?php echo play_render_image($product_obj['thumbnail_url'],$product_obj['title']); ?>
                </div>
                <div class="tag-social">
                    <div class="tag-wrapper">
                        <?php if ($product_obj['special']) { ?>
                            <span class="special-tag">
                                <svg viewBox="0 0 384 512">
                                    <use xlink:href="<?php echo sprite_url; ?>#fire"></use>
                                </svg>
                                ویژه
                            </span>
                        <?php } ?>
                        <?php if ($product_obj['pre_order']) { ?>
                            <span class="prebuy-tag"> پیش خرید </span>
                        <?php } ?>
                    </div>
                    <div class="social-wrapper">
                        <?php 
                        echo ($inUserFavorites) ? '<li class="social-item like active">' : '<li class="social-item like">';
                        echo '<svg class="heart" viewBox="0 0 195.2 178.61"><use xlink:href="' . sprite_url . '#heart"></use></svg>';
                        echo '<svg class="heart-fill" viewBox="0 0 51.997 51.997"><use xlink:href="' . sprite_url . '#heart-fill"></use></svg>';
                        echo '</li>';
                        ?>
                        <li class="social-item share">
                            <svg viewBox="0 0 165.03 226.5">
                                <use xlink:href="<?php echo sprite_url; ?>#share"></use>
                            </svg>
                            <ul class="social-holder">
                                <li>
                                    <a data-name="facebook"
                                       href="https://www.facebook.com/sharer.php?u=<?php echo $product_obj['url']; ?>"
                                       title="اشتراک گذاری در فیسبوک" target="_blank">
                                        <svg viewBox="0 0 352 352">
                                            <use xlink:href="<?php echo sprite_url; ?>#facebook"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a data-name="email" title="اشتراک گذاری با ایمیل"
                                       href="mailto:?subject=<?php echo $product_obj['title']; ?>&amp;body=<?php echo wp_strip_all_tags($product_obj['desc']) ?>">
                                        <svg viewBox="0 0 24 24">
                                            <use xlink:href="<?php echo sprite_url; ?>#mail"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a data-name="twitter" title="اشتراک گذاری در توییتر"
                                       href="https://twitter.com/share?text=<?php echo $product_obj['title']; ?>=<?php echo $product_obj['url']; ?>"
                                       target="_blank">
                                        <svg viewBox="0 0 638.44 518.87">
                                            <use xlink:href="<?php echo sprite_url; ?>#twitter"></use>
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </div>
                </div>
                <div class="product-detail">
                    <div class="name-wrap">
                        <h1 class="product-name"><?php echo $product_obj['name']; ?></h1>
                        <?php if (isset($product_obj['genres']) && !empty($product_obj['genres'])) {
                            echo '<ul class="product-genre">';
                            $max_g = count($product_obj['genres']);
                            foreach ($product_obj['genres'] as $c => $genre) {
                                echo '<li><a href="' . $genre['url'] . '" title="' . $genre['name'] . '">' . $genre['name'] . '</a></li>';
                                echo (($c + 1) >= $max_g) ? '' : ', ';
                            }
                            echo '</ul>';
                        }
                        /*
                        <div class="rate">
                            <div class="star-wrap">
                                <?php for ($i=1; $i <= 5; $i++) { 
                                    $gold = ($i <= $ratingValue) ? 'gold' : '';
                                    echo '<svg class="'.$gold.'" viewBox="0 0 24 24">
                                    <use xlink:href="'.sprite_url.'#fillStar"></use>
                                    </svg>';
                                } ?>
                            </div>
                            <?php echo "$ratingValue/5" ?>
                        </div>
                        */
                        ?>
                    </div>
                    <?php if((float)$min_price > 0){ ?>
                        <div class="price">
                            شروع قیمت از
                            <span>
                                <?php echo play_price($min_price); ?>
                                <b class="currency">تومان</b>
                            </span>
                        </div>
                    <?php } ?>
                    <a title="<?php echo $product_obj['title']; ?>" class="add-to-cart btn btn--red"
                        href="#products-list">
                        خرید <?php echo $type_name; ?>
                        <svg viewBox="0 0 10.38 10.38">
                            <use xlink:href="<?php echo sprite_url; ?>#add"></use>
                        </svg>
                    </a>
                </div>
                <div class="property-wrap">
                    <?php if ($product_obj['platforms_html']) { ?>
                        <div class="property-info">
                            <div class="property-head">
                                <span class="title"> پلتفرم </span>
                                <!-- <a class="link-more" href="#">
                                    <svg viewBox="0 0 205.4 205.4">
                                        <use xlink:href="<?php echo sprite_url; ?>#info"></use>
                                    </svg>
                                    توضیحات بیشتر
                                </a> -->
                            </div>
                            <ul class="property-holder">
                                <?php echo $product_obj['platforms_html']; ?>
                            </ul>
                        </div>
                    <?php } ?>
                    <?php if ($product_obj['product_types'] && !empty($product_obj['product_types'])) { ?>
                        <div class="property-info">
                            <div class="property-head">
                                <span class="title"> نوع بازی </span>
                                <!-- <a class="link-more" href="#">
                                    <svg viewBox="0 0 205.4 205.4">
                                        <use xlink:href="<?php echo sprite_url; ?>#info"></use>
                                    </svg>
                                    توضیحات
                                </a> -->
                            </div>
                            <ul class="property-holder">
                                <?php 
                                foreach ($product_obj['product_types'] as $type) {
                                    echo '<li>'.$type['name'].'</li>';
                                }
                                ?>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

    <?php } elseif ($product_obj['type'] == 'giftcard') { ?>
        <div class="giftcard-hero">
            <div class="back-cover">
                <img src="<?php echo theme_assets ?>/imgs/giftcard-header.jpg">
            </div>
            <div class="giftcard-hero__master">
                <div class="giftcard-img">
                    <div class="hole"></div>
                    <?php echo play_render_image($product_obj['thumbnail_url'],$product_obj['title'],'coverIt'); ?>
                </div>
                <div class="hero-content">
                    <div class="tag-wrapper">
                        <?php if ($product_obj['special']) { ?>
                            <span class="special-tag">
                                <svg viewBox="0 0 384 512">
                                    <use xlink:href="<?php echo sprite_url; ?>#fire"></use>
                                </svg>
                                ویژه
                            </span>
                        <?php } ?>
                    </div>
                    <div class="product-title">
                        <h1><?php echo $product_obj['name']; ?></h1>
                        <?php if ($product_obj['regions_name']) { ?>
                            <div class="region">
                                <?php echo $product_obj['regions_name']; ?>
                            </div>
                        <?php } elseif (!empty($product_obj['region']['name'])) {
                            echo '<div class="region">' . $product_obj['region']['name'] . '</div>';
                        } ?>
                    </div>
                </div>
                <div class="social-wrapper">
                    <?php //if( $canAddToFavorite ){
                    echo ($inUserFavorites) ? '<div class="social-item like mb-0 ml-4 active">' : '<div class="social-item mb-0 ml-4 like">';
                    echo '<svg class="heart" viewBox="0 0 195.2 178.61"><use xlink:href="' . sprite_url . '#heart"></use></svg>';
                    echo '<svg class="heart-fill" viewBox="0 0 51.997 51.997"><use xlink:href="' . sprite_url . '#heart-fill"></use></svg>';
                    echo '</div>';
                    //}
                    ?>
                    <div class="social-item share ml-0">
                        <svg viewBox="0 0 165.03 226.5">
                            <use xlink:href="<?php echo sprite_url; ?>#share"></use>
                        </svg>
                        <ul class="social-holder">
                            <li>
                                <a data-name="facebook"
                                    href="https://www.facebook.com/sharer.php?u=<?php echo $product_obj['url']; ?>"
                                    title="اشتراک گذاری در فیسبوک" target="_blank">
                                    <svg viewBox="0 0 352 352">
                                        <use xlink:href="<?php echo sprite_url; ?>#facebook"></use>
                                    </svg>
                                </a>
                            </li>
                            <li>
                                <a data-name="email" title="اشتراک گذاری با ایمیل"
                                    href="mailto:?subject=<?php echo $product_obj['title']; ?>&amp;body=<?php echo wp_strip_all_tags($product_obj['desc']) ?>">
                                    <svg viewBox="0 0 24 24">
                                        <use xlink:href="<?php echo sprite_url; ?>#mail"></use>
                                    </svg>
                                </a>
                            </li>
                            <li>
                                <a data-name="twitter" title="اشتراک گذاری در توییتر"
                                    href="https://twitter.com/share?text=<?php echo $product_obj['title']; ?>=<?php echo $product_obj['url']; ?>"
                                    target="_blank">
                                    <svg viewBox="0 0 638.44 518.87">
                                        <use xlink:href="<?php echo sprite_url; ?>#twitter"></use>
                                    </svg>
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    <?php }

}