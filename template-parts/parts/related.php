<?php

// Related Section
$args = array(
    'post_type' => $product_obj['type'],
    'fields' => 'ids',
    'no_found_rows' => true,
    'update_post_meta_cache' => false,
    'update_post_term_cache' => false,
    'posts_per_page' => 30,
    'post__not_in' => array($ID),

);
$posts_ids = get_posts($args);
if (!empty($posts_ids)) { ?>
    <div class="product-suggest wide-card-holder suggested">
        <div class="title-warp container">
            <div class="section-title">
                <h2>
                    <?php
                    switch ($product_obj['type']) {
                        case 'giftcard':
                            echo 'گیفت کارت های پیشنهادی';
                            break;
                        case 'game':
                            echo 'بازی‌های پیشنهادی';
                            break;
                        case 'dlc':
                            echo 'پک های پیشنهادی';
                            break;
                        default:
                            echo 'محصولات پیشنهادی';
                            break;
                    }
                    ?>
                </h2>
            </div>
            <a href="<?php echo get_post_type_archive_link($product_obj['type']); ?>" title="نمایش همه" class="show-all">
                نمایش همه
            </a>
        </div>
        <div class="wide-carousel owl-carousel">
            <?php foreach ($posts_ids as $product_id) {
                set_query_var('product_id', $product_id);
                get_template_part('template-parts/content/content', 'product-card');
            } ?>
        </div>
    </div>
<?php } ?>