<?php
$contact_page_url = '';

global $playOptions;
if( $playOptions ){
    $contact_page_id = $playOptions->getOption( 'contact_page' );
    $contact_page_url = ($contact_page_id) ? get_permalink( $contact_page_id ) : '';
}
if( $contact_page_url ){
    echo '<div class="helpFooter">
        <div class="container position-relative d-flex justify-content-center justify-content-sm-between align-items-center flex-wrap">
            <span class="qSymbol">؟</span>
            <h3 class="title">جواب سوال خود را نیافتید؟ از ما بپرسید...</h3>
            <a title="ثبت سوال" class="btn-start btn btn--red btn--large btn--text btn--text--right register" href="'.$contact_page_url.'">
                ثبت سوال
                <svg viewBox="0 0 12.56 19.46"><use xlink:href="'.sprite_url.'#arrow"></use></svg>
            </a>
        </div>
    </div>';
}
?>
