<?php
$helps_cats = get_terms('help_cats');
$active_cat = null;
if( is_tax( 'help_cats' ) ){
    $active_cat = get_queried_object();
}
?>
<div class="tabs tabs-help">
    <div class="container">
        <div class="tabsContainer">
            <a title="همه" class="tab <?php echo (is_post_type_archive( 'help' )) ? 'active' : ''; ?>" href="<?php echo get_post_type_archive_link( 'help' ); ?>">
                <svg viewBox="0 0 236.46 200.53"><use xlink:href="<?php echo sprite_url; ?>#menu"></use></svg>
                همه
            </a>
            <?php if( $helps_cats ){
                foreach ($helps_cats as $cat ) {
                    $active = ( $active_cat && $cat->term_id == $active_cat->term_id ) ? 'active' : '';
                    echo '<a title="'.$cat->name.'" class="tab '.$active.'" href="'.get_term_link( $cat->term_id, 'help_cats' ).'">'.$cat->name.'</a>';
                }
            } ?>
        </div>
    </div>
</div>