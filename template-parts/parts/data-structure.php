<?php
$product_obj = get_product_obj(get_the_ID());
$comments = (array)get_comments('post_id=' . get_the_ID());
$schema_arr = [];
if ($comments) {
    foreach ($comments as $comment) {
        if ($comment->comment_approved == 1) {
            $meta = get_comment_meta($comment->comment_ID);
            $name = isset($meta["ic_review_title"][0]) ? $meta["ic_review_title"][0] : '';
            $ratingValue = isset($meta["ic_review_rating"][0]) ? $meta["ic_review_rating"][0] : '';
            array_push($schema_arr, '{
                "@type": "Review",
                "author": "' . $comment->comment_author . '",
                "datePublished": "' . $comment->comment_date . '",
                "description": "' . $comment->comment_content . '",
                "name": "' . $name . '",
                "reviewRating": {
                    "@type": "Rating",
                    "bestRating": "5",
                    "ratingValue": "' . $ratingValue . '",
                    "worstRating": "1"
                }}');
        }
    }
}
$child_products = (isset($product_obj['child_products'])) ? $product_obj['child_products'] : null;
function cmp_2($a, $b) {
    return $a["price"] > $b["price"];
}
if( !empty($child_products) ){
    usort($child_products, "cmp_2");
    $available_count = 0;
    $price_rage = [];
    foreach ($child_products as $wc_product_obj) {
        if (empty($price_rage[0]) && $wc_product_obj["price"] !== 0) $price_rage[0] = $wc_product_obj["price"];
        if ($wc_product_obj["price"] !== 0) {
            $available_count++;
            $price_rage[1] = $wc_product_obj["price"];
        }
    }
}

?>
<script type="application/ld+json">
{
    "@context": "https://schema.org/",
    "@type": "Product",
    "name": "<?php echo $product_obj['title'] ?>",
    "image": [
        "<?php echo $product_obj['thumbnail_url']; ?>"
    ],
    "description": "<?php echo $product_obj['product_data_structure']['desc'] ?>",
    "sku": "<?php echo $product_obj['id'] ?>",
    "mpn": "<?php echo $product_obj['id'] ?>",
    "brand": {
        "@type": "Brand",
        "name": "<?php echo $product_obj['product_data_structure']['brand'] ?>"
    },
    <?php if (count($schema_arr) > 0) { ?>
    "aggregateRating": {
        "@type": "AggregateRating",
        "ratingValue": "<?php echo ic_get_reviews_average($product_obj['id']) ?>",
        "reviewCount": "<?php echo ic_get_product_review_totals($product_obj['id'])['total'] ?>"
    },
    "review": [
    <?php
$numItems = count($schema_arr);
$i = 0;
foreach ($schema_arr as $key => $item) {
    if (++$i === $numItems) {
        echo $item;
    } else {
        echo $item . ',';
    }
}
?>
    ],
    <?php } ?>
    <?php if ($available_count > 0) { ?>
    "offers": {
        "@type": "AggregateOffer",
        "url": "<?php echo $product_obj['url'] ?>",
        "lowPrice": "<?php echo $price_rage[0] ?>",
        "highPrice": "<?php echo $price_rage[1] ?>",
        "priceCurrency" : "IRT",
        "offerCount": "<?php echo $available_count ?>",
        "availability": "https://schema.org/InStock"
    }
    <?php } ?>
}
</script>