<section class="subscribe-section">
    <div class="subscribe-wrap">
        <h2 class="title">
            <svg class="icon" viewBox="0 0 306.78 233.73">
                <use xlink:href="<?php echo sprite_url; ?>#email"></use>
            </svg>
            خبرنامه ایمیلی آی گیفت
        </h2>
        <span class="tip">
            با عضویت در خبرنامه ما از تخفیف های روزانه باخبر شوید!
        </span>
        <form class="subscribe-form">
            <div class="input-holder">
                <input type="email" placeholder="ایمیل خود رو در این قسمت وارد کنید" />
                <button type="submit" class="btn btn--red btn--text--right btn--small">
                    ثبت ایمیل
                    <svg viewBox="0 0 12.56 19.46">
                        <use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
                    </svg>
                </button>
            </div>
        </form>
    </div>
</section>