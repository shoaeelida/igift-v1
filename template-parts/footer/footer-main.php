<?php if( is_home() || is_front_page() ){ 
        
        if( ( $html = get_transient( 'footer_home' ) ) == false ){
        
            global $playOptions;

            $email = 0;
            $phone = 0;
            $p = 0;
            $instagram = 0;
            $youtube = 0;
            $aparat = 0;
            $twitter = 0;
            
            if( $playOptions ){
                $email = $playOptions->getOption( 'email' );
                $phone = $playOptions->getOption( 'phone' );
                $p = str_replace('021','',wp_strip_all_tags($phone));
                $p = str_replace(' ','',$p);
                $p = '021'.$p;
                $instagram = $playOptions->getOption( 'instagram' );
                $youtube = $playOptions->getOption( 'youtube' );
                $aparat = $playOptions->getOption( 'aparat' );
                $twitter = $playOptions->getOption( 'twitter' );
            }
            
            ob_start();

            ?>
    
            <footer class="home-footer">
                <div class="responsive-footer">
                    <div class="contact-info">
                        <?php if( $phone && !empty($phone) ){ ?>
                            <a class="number" href="tel:<?php echo $p; ?>" title="پشتیبانی 24 ساعته آی گیفت">
                                <svg viewBox="0 0 301.81 301.81">
                                    <use xlink:href="<?php echo sprite_url; ?>#phone"></use>
                                </svg>
                                <span><?php echo $phone; ?></span>
                            </a>
                        <?php } ?>
                        <span class="right"> تمامی حقوق محصولات نزد آی گیفت محفوظ است. </span>
                    </div>
                    <div class="portal-holder">
                        <div class="namads-saman"><img class="lazy lazySimple" src="<?php echo theme_assets; ?>/imgs/portal2.png"></div></a>
                        <div class="namads-enamad" title="نماد اعتماد آی گیفت" data-url="https://trustseal.enamad.ir/?id=154759&amp;Code=SE0vgicLw4jyKaJ22ufT"><img referrerpolicy="origin" src="https://trustseal.enamad.ir/logo.aspx?id=154759&amp;Code=SE0vgicLw4jyKaJ22ufT"><img class="lazy lazySimple" src="<?php echo theme_assets; ?>/imgs/enamad.png"></div>
                    </div>
                </div>
                <div class="footer-description-home">
                    <div class="mainContainer">
                        <p>امروزه با گسترش دستگاه‌های دیجیتال، شبکه‌های مختلف سرگرمی و بازی در دسترس عموم قرار گرفته‌اند. گیفت کارت یک روش پرداخت ساده برای این دسته از شبکه‌ها ارائه می‌دهد.</p>
                        <p>سایت آی‌گیفت با ارائه انواع گیفت‌ کارت‌های مختلف به کاربران و مشتریان خود امکان دسترسی به شبکه‌های سرگرمی و بازی بین‌المللی را در اختیار این عزیزان در داخل ایران می‌گذارد. گیفت کارت‌ها اعتبارهای پیش‌پرداختی هستند که برای خرید یک اشتراک یا سرویس کاربرد دارند. آنها از سوی شرکت‌ها و برندهای عمده ارائه دهنده خدمات و محصولات، تولید و عرضه می‌شوند تا مشتریان این برندها به سادگی امکان تامین و پرداخت هزینه خدمات آنها را داشته باشند. برای مثال <a href="https://igift.ir/giftcard/6412/%d8%ae%d8%b1%db%8c%d8%af-%da%af%db%8c%d9%81%d8%aa-%da%a9%d8%a7%d8%b1%d8%aa-%d8%a2%db%8c%d8%aa%d9%88%d9%86%d8%b2/">گیفت کارت اپل آیتونز</a> از طرف شرکت اپل صادر می‌شود و کاربران با داشتن آن می‌توانند انواع محتوای مورد نیاز خود، از جمله موسیقی، فیلم یا اپلیکیشن را از فروشگاه اپل خریداری و دریافت کنند.</p>
                        <p>سایت آی‌گیفت با شناخت و آگاهی از کارکردهای مثبت گیفت‌ کارت، بستر دیجیتال امنی ایجاد کرده تا مشتریان ایرانی بتوانند به سادگی به گیفت کارت‌های مورد نیاز خود دسترسی داشته باشند. به این ترتیب کاربران سایت می‌توانند به سادگی اقدام به خرید گیفت کارت‌های گوگل پلی، اپل، <a href="https://igift.ir/giftcard/6406/%d8%ae%d8%b1%db%8c%d8%af-%da%af%db%8c%d9%81%d8%aa-%da%a9%d8%a7%d8%b1%d8%aa-%d8%a2%d9%85%d8%a7%d8%b2%d9%88%d9%86/">آمازون</a>، نتفلیکس، پلی‌استیشن، استیم، اسپاتیفای، مایکروسافت و <a href="https://igift.ir/giftcard/6460/%d8%ae%d8%b1%db%8c%d8%af-%da%af%db%8c%d9%81%d8%aa-%da%a9%d8%a7%d8%b1%d8%aa-%d8%a7%db%8c%da%a9%d8%b3-%d8%a8%d8%a7%da%a9%d8%b3/">ایکس‌باکس</a>، رابلاکس و یا هر نوع دیگری از گیفت کارت کنند. با استفاده از این گیفت کارت‌ها کاربران امکان دسترسی به محتواهایی مثل اپلیکیشن‌های مختلف، موسیقی‌ها و فیلم‌های مورد علاقه، بازی‌های جذاب و موارد دیگر را به دست بیاورند.</p>
                        <p>از آنجا که گیفت کارت‌ها از طرف برندهای بسیار بزرگ صادر و توزیع می‌شوند، معمولا از سوی این شرکت‌ها مزایایی نیز برای آنها در نظر گرفته می‌شود. برای مثال، کاربرانی که از <a href="https://igift.ir/giftcard/6418/%d8%ae%d8%b1%db%8c%d8%af-%da%af%db%8c%d9%81%d8%aa-%da%a9%d8%a7%d8%b1%d8%aa-%d9%be%d9%84%db%8c-%d8%a7%d8%b3%d8%aa%db%8c%d8%b4%d9%86/">گیفت کارت پلی‌استیشن</a> برای شارژ حساب خود در شبکه پلی‌استیشن استفاده می‌کنند از امکانات ویژه‌ای مثل دانلود بازی‌ها قبل از رونمایی رسمی، تخفیف خرید بازی، و تخفیف برنامه‌های عضویت برخوردار می‌شوند.</p>
                        <p>امروزه دستگاه‌های پخش هوشمند و تحت شبکه وارد خانه‌های ما شده است. بسیاری از این دستگاه‌ها قابلیت پشتیبانی از شبکه‌های بین‌المللی سرگرمی را دارند. این شبکه‌ها رایگان نیستند، و هزینه آنها را می‌توان به راحتی با گیفت کارت مرتبط پرداخت. برای مثال با داشتن گیفت کارت Hulu یا گیفت کارت نتفلیکس، می‌توانید تلویزیون هوشمند خود را به این شبکه‌های سرگرمی بین‌المللی متصل نموده و از برنامه‌های جذاب و جدید آنها نهایت لذت را ببرید.</p>
                        <p>علاوه بر موارد فوق، بسیاری از متخصصان برنامه‌نویسی، محققان، و فعالان مشاغل مختلف نیاز به دسترسی به بعضی از شبکه‌های بین‌المللی و پرداخت حق عضویت آنها دارند. گیفت کارت‌های مختلف به این دسته از کاربران که برای واریز وجه با استفاده از کارت بانکی خود مشکل دارند، راهی ساده برای پرداخت هزینه‌ها ارائه می‌دهد. این دسته از کاربران می‌توانند انواع پرداخت‌های مرتبط با حرفه خود را از طریق گیفت کارت‌های مربوطه انجام دهند.</p>
                        <p>مجموعه آی‌گیفت با توجه به موارد استفاده فراوان گیفت کارت اقدام به افتتاح این فروشگاه حرفه‌ای نموده تا انواع گیفت کارت‌های مختلف را به کاربران محترم عرضه کند. اگر به دنبال گیفت کارت خاصی هستید و آن را در فروشگاه پیدا نمی‌کنید، می‌توانید از داخل پروفایل کاربری خود آن را سفارش دهید تا آن را به مجموعه آی‌گیفت اضافه کرده و در اسرع وقت به شما تحویل دهیم.</p>
                    </div>
                </div>
                <div class="mainContainer">
                    <div class="main-link">
                        <div class="page-link">
                            <span>لینک های اصلی</span>
                            <?php get_template_part( 'template-parts/nav/nav', 'footer' ); ?>
                        </div>
                        <div class="social-link">
                            <?php
                            echo ($twitter) ? '<a href="'.$twitter.'" title="آی گیفت در توییتر" target="_blank"><svg viewBox="0 0 638.44 518.87"><use xlink:href="'.sprite_url.'#twitter"></use></svg></a>' : '';
                            echo ($instagram) ? '<a href="'.$instagram.'" title="آی گیفت در اینستاگرام" target="_blank"><svg viewBox="0 0 551.03 551.03"><use xlink:href="'.sprite_url.'#instagram"></use></svg></a>' : '';
                            echo ($youtube) ? '<a href="'.$youtube.'" title="آی گیفت در یوتیوب" target="_blank"><svg viewBox="5.368 13.434 53.9 37.855"><use xlink:href="'.sprite_url.'#youtube"></use></svg></a>' : '';
                            echo ($aparat) ? '<a href="'.$aparat.'" title="آی گیفت در آپارات" target="_blank"><svg viewBox="0 0 48 48"><use xlink:href="'.sprite_url.'#aparat"></use></svg></a>' : '';
                            ?>
                        </div>
                    </div>
                </div>
                <div class="middle-section">
                    <div class="mainContainer">
                        <div class="sub-link">
                            <?php get_template_part( 'template-parts/nav/nav', 'footer_buyGame' ); ?>
                            <?php get_template_part( 'template-parts/nav/nav', 'footer_buyGiftcard' ); ?>
                            <?php get_template_part( 'template-parts/nav/nav', 'footer_link' ); ?>
                        </div>
                        <div class="site-info">
                            <?php if( $phone && !empty($phone) ){ ?>
                                <a class="phone-info" href="tel:<?php echo $p; ?>" title="پشتیبانی 24 ساعته آی گیفت">
                                    <svg viewBox="0 0 301.81 301.81">
                                        <use xlink:href="<?php echo sprite_url; ?>#phone"></use>
                                    </svg>
                                    <span><?php echo $phone; ?></span>
                                    <strong>پشتیبانی 24 ساعته</strong>
                                </a>
                            <?php } ?>
                            <div class="namads">
                                <div class="namads-saman"><img class="lazy lazySimple" src="<?php echo theme_assets; ?>/imgs/portal2.png"></div></a>
                                <div class="namads-enamad" title="نماد اعتماد آی گیفت" data-url="https://trustseal.enamad.ir/?id=154759&amp;Code=SE0vgicLw4jyKaJ22ufT"><img class="lazy lazySimple" src="<?php echo theme_assets; ?>/imgs/enamad.png"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mainContainer">
                    <div class="copy-right-home">
                        Copyright 2020 All rights reserved.تمامی حقوق محصولات نزد آی گیفت محفوظ است   
                        <img class="lazy" data-src="<?php echo theme_assets; ?>/imgs/logo-footer.png">
                    </div>
                </div>
            </footer>

            <?php
            $html = ob_get_contents();
            ob_end_clean();

            set_transient( 'footer_home' , $html, HOUR_IN_SECONDS );

        }

        echo $html;
            
    }else{ 

        if( ( $html = get_transient( 'footer_home_simple' ) ) == false ){
        
            ob_start();
            
            ?>

            <footer>
                <img class="circle c1 lazy lazySimple" data-src="<?php echo theme_assets; ?>/imgs/circle2.svg">
                <img class="circle c2 lazy lazySimple"  data-src="<?php echo theme_assets; ?>/imgs/circle1.svg">
                <div class="container">
                    <div class="footer-content">
                        <div class="footer-description">
                            <img class="footer-logo lazy lazySimple" data-src="<?php echo theme_assets; ?>/imgs/whiteLogo.svg">
                            <p>تمامی كالاها و خدمات اين فروشگاه، حسب مورد دارای مجوزهای لازم از مراجع مربوطه می‌باشند و فعاليت‌های اين سايت تابع قوانين و مقررات جمهوری اسلامی ايران است.</p>
                        </div>
                        <?php get_template_part( 'template-parts/nav/nav', 'footer' ); ?>
                    </div>
                    <div class="copy-right">
                        © 2020 iGame.ir All rights reserved.
                        <img class="responsive-logo lazy lazySimple" data-src="<?php echo theme_assets; ?>/imgs/responsiveLogofooter.svg" alt="IGame">
                    </div>
                </div>
            </footer>

            <?php 
            $html = ob_get_contents();
            ob_end_clean();

            set_transient( 'footer_home_simple' , $html, HOUR_IN_SECONDS );

        }

        echo $html;

    } ?>
</div> 
<!--  close page-content -->
<div class="chat-btn"></div>
<?php

$footer_sub_key = ( is_user_logged_in() ) ? 'footer_subl' : 'footer_sub';

if( ( $html = get_transient( $footer_sub_key ) ) == false ){
    ob_start();
    get_template_part( 'template-parts/steps/steps' ); 
    get_template_part( 'template-parts/nav/nav', 'mobile' ); 
    $html = ob_get_contents();
    ob_end_clean();
    set_transient( $footer_sub_key , $html, HOUR_IN_SECONDS );
}

echo $html;

get_template_part( 'template-parts/footer/footer', 'foot' ); 

?>

</body>
</html>
