	<footer class="copyright">
		<img class="circle c1" src="<?php echo get_template_directory_uri() ?>/dist/imgs/circle2.svg">
		<div class="container">
			<div class="copy-right">
				© 2020 iGame.ir All rights reserved.
			</div>
		</div>
	</footer>

	<?php get_template_part( 'template-parts/steps/steps' ); ?>
	<?php get_template_part( 'template-parts/nav/nav', 'mobile' ); ?>

	<?php wp_footer(); ?>
