<?php 
if( ( $nav = get_transient( 'nav_footer_buyGame' ) ) == false ){
    $nav = wp_nav_menu( array(
        'theme_location'  => 'footer_buyGame',
        'container'       => 'ul',
        'menu_class'      => 'buy-game',
        'echo'            => false,
        'depth'           => 0,
    ) );
    set_transient( 'nav_footer_buyGame' , $nav, HOUR_IN_SECONDS );
}
echo str_replace('class="buy-game">','class="buy-game"><span>خرید بازی</span>',$nav);