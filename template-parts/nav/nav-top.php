<?php 
if( ( $nav = get_transient( 'nav_top' ) ) == false ){
    $nav = wp_nav_menu( array(
        'theme_location'  => 'top',
        'container'       => 'ul',
        'menu_class'      => 'nav-menu',
        'echo'            => false,
        'depth'           => 0,
    ) );
    set_transient( 'nav_top' , $nav, HOUR_IN_SECONDS );
}
echo $nav;