<?php
global $playOptions;

$email = 0;
$phone = 0;
$p = 0;
$instagram = 0;
$youtube = 0;
$aparat = 0;
$twitter = 0;

if( $playOptions ){
    $email = $playOptions->getOption( 'email' );
    $phone = $playOptions->getOption( 'phone' );
    $p = str_replace('021','',wp_strip_all_tags($phone));
    $p = str_replace(' ','',$p);
    $p = '021'.$p;
    $instagram = $playOptions->getOption( 'instagram' );
    $youtube = $playOptions->getOption( 'youtube' );
    $aparat = $playOptions->getOption( 'aparat' );
    $twitter = $playOptions->getOption( 'twitter' );
    
}
?>
<div class="responsive-menu">
    <div class="menu-holder">
        <div class="responsive-menu__top">
            <button class="menu-close">
                <svg viewBox="0 0 75.29 75.29">
                  <use xlink:href="<?php echo sprite_url; ?>#close"></use>
                </svg>
            </button>
            <a href="<?php echo site_url; ?>" title="<?php echo site_title; ?>">
                <img src="<?php echo theme_assets; ?>/imgs/logo.svg">
            </a>
        </div>
        <div class="login-logout">
            <?php
            if( is_user_logged_in() ){
                echo '<a title="پنل کاربری" href="'.get_permalink( get_option('woocommerce_myaccount_page_id') ).'" class="register-wrap btn btn--large btn--red">
                <div class="dashboard-btn">پنل کاربری</div>
                </a>';
            }else{
                echo '<a title="ورود یا ثبت نام" href="?login_modal=true" class="register-wrap btn btn--large btn--red digits-login-modal open-login-modal" onclick="jQuery(\'this\').digits_login_modal(jQuery(this));return false;" attr-disclick="1" type="1">
                    <div class="login-btn">ورود</div>
                    <div class="register-btn">ثبت نام</div>
                </a>';
            } ?>
        </div>
        <?php
        // if( ( $nav = get_transient( 'nav_mobile' ) ) == false ){
            // $nav = wp_nav_menu( array(
            //     'theme_location'  => 'mobile',
            //     'container'       => 'ul',
            //     'menu_class'      => 'menu-item',
            //     'echo'            => false,
            //     'depth'           => 0,
            // ) );
            // set_transient( 'nav_mobile' , $nav, HOUR_IN_SECONDS );
        // }
        //echo $nav; ?>
        <ul class="menu-item">
            <li>
                <a title="خرید بازی" href="<?php echo site_url; ?>game/">
                    <svg viewBox="0 0 357.57 298.03" style="fill:none; stroke: #9293a4; stroke-width: 30px;">
                        <use xlink:href="<?php echo sprite_url; ?>#userPlus"></use>
                    </svg>
                    خرید بازی
                </a>
            </li>
            <li>
                <a title="خرید گیفت کارت" href="<?php echo site_url; ?>giftcard/">
                    <svg  viewBox="0 0 437 326.11" >
                        <use xlink:href="<?php echo sprite_url; ?>#giftIcon"></use>
                    </svg>
                    خرید گیفت کارت
                </a>
            </li>
            <!-- <li>
                <a href="<?php echo site_url; ?>faqs/">
                    <svg viewBox="0 0 359.33 359.33">
                        <use xlink:href="<?php echo sprite_url; ?>#question"></use>
                    </svg>
                    سوالات متداول
                </a>
            </li> -->
            <li>
                <a title="تماس با ما" href="<?php echo site_url; ?>contact-us/">
                    <svg viewBox="0 0 359.33 359.33">
                        <use xlink:href="<?php echo sprite_url; ?>#question"></use>
                    </svg>
                    تماس با ما
                </a>
            </li>
            <li>
                <a title=" مجله خبری" href="<?php echo site_url; ?>blog/">
                    <svg viewBox="0 0 359.33 359.33">
                        <use xlink:href="<?php echo sprite_url; ?>#question"></use>
                    </svg>
                    مجله خبری
                </a>
            </li>
        </ul>
    </div>
    <div class="contact-info__holder">
        <div class="social-link">
            <?php
            echo ($twitter) ? '<a href="'.$twitter.'" title="آی گیفت در توییتر" target="_blank"><svg viewBox="0 0 638.44 518.87"><use xlink:href="'.sprite_url.'#twitter"></use></svg></a>' : '';
            echo ($instagram) ? '<a href="'.$instagram.'" title="آی گیفت در اینستاگرام" target="_blank"><svg viewBox="0 0 551.03 551.03"><use xlink:href="'.sprite_url.'#instagram"></use></svg></a>' : '';
            echo ($youtube) ? '<a href="'.$youtube.'" title="آی گیفت در یوتیوب" target="_blank"><svg viewBox="5.368 13.434 53.9 37.855"><use xlink:href="'.sprite_url.'#youtube"></use></svg></a>' : '';
            echo ($aparat) ? '<a href="'.$aparat.'" title="آی گیفت در آپارات" target="_blank"><svg viewBox="0 0 48 48"><use xlink:href="'.sprite_url.'#aparat"></use></svg></a>' : '';
           ?>
        </div>
        <div class="contact-info">
            <?php if ( $phone && !empty($phone)) { ?>
                <a class="phone-info" href="tel:<?php echo $p; ?>" title="پشتیبانی 24 ساعته آی گیفت">
                  <svg viewBox="0 0 301.81 301.81">
                      <use xlink:href="<?php echo sprite_url; ?>#phone"></use>
                  </svg>
                  <span><?php echo $phone; ?></span>
                  <strong>پشتیبانی 24 ساعته</strong>
                </a>
            <?php } ?>
            <?php if ( $email && !empty($email)) { ?>
              <a class="email-info" href="mailto:<?php echo $email; ?>" title="<?php echo $email; ?>">
                <?php echo $email; ?>
                <svg viewBox="0 0 306.78 233.73">
                    <use xlink:href="<?php echo sprite_url; ?>#email"></use>
                </svg>
                </a>
            <?php } ?>
        </div>
    </div>
</div>