<?php 
if( ( $nav = get_transient( 'nav_footer_link' ) ) == false ){
    $nav = wp_nav_menu( array(
        'theme_location'  => 'footer_link',
        'container'       => 'ul',
        'menu_class'      => 'igame-link',
        'echo'           => false,
        'depth'           => 0,
    ) );
    set_transient( 'nav_footer_link' , $nav, HOUR_IN_SECONDS );
}
echo str_replace('class="igame-link">','class="igame-link"><span>همراه با آی گیفت</span>',$nav);
