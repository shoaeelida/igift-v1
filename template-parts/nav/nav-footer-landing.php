<?php 
if( ( $nav = get_transient( 'nav_footer_landing' ) ) == false ){
    $nav = wp_nav_menu( array(
        'theme_location'  => 'footer_landing',
        'container'       => 'ul',
        'menu_class'      => 'menu',
        'echo'           => false,
        'depth'           => 0,
    ) );
    set_transient( 'nav_footer_landing' , $nav, HOUR_IN_SECONDS );
}
echo $nav; 