<?php 
if( ( $nav = get_transient( 'nav_footer' ) ) == false ){
    $nav = wp_nav_menu( array(
        'theme_location'  => 'footer',
        'container'       => 'ul',
        'menu_class'      => 'footer-menu',
        'echo'           => false,
        'depth'           => 0,
    ) );
    set_transient( 'nav_footer' , $nav, HOUR_IN_SECONDS );
}
echo $nav; 