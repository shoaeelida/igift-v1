<?php
if( ( $nav = get_transient( 'nav_footer_buyGiftcard' ) ) == false ){
    $nav = wp_nav_menu( array(
        'theme_location'  => 'footer_buyGiftcard',
        'container'       => 'ul',
        'menu_class'      => 'buy-giftcard',
        'echo'           => false,
        'depth'           => 0,
    ) );
    set_transient( 'nav_footer_buyGiftcard' , $nav, HOUR_IN_SECONDS );
}
echo str_replace('class="buy-giftcard">','class="buy-giftcard"><span>خرید گیفت کارت</span>',$nav);