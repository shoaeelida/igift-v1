<?php
$data = get_sub_field('data');
$title = get_sub_field('title');
$desc = get_sub_field('desc');
$lists = get_field('bestselling_products');
?>

<section class="bestselling-section">
    <div class="description">
        <h2 class="title">
        <div class="icon">
            <svg viewBox="0 0 384 512">
            <use xlink:href="<?php echo sprite_url; ?>#hot"></use>
            </svg>
        </div>
        پرفروش‌ترین‌ها
        </h2>
        <p>
        سایت آی گیفت، امکان خرید انواع گیفت کارت‌ها را با تخفیف ویژه برای شما کاربران گرامی فراهم کرده است. از جمله محصولات بی‌نظیر سایت igift.ir می توان به گیفت کارت اپل، پلی استیشن، استیم، ایکس باکس، گوگل پلی، آمازون و  …اشاره کرد. تخفیف‌های ویژه این وبسایتجهت دسترسی راحت‌تر کاربران ایرانی به انواع گیفت کارت با قیمت مناسب‌تر است.
        </p>
    </div>
    <div id="bestselling-carousel" class="carousel-holder owl-carousel">
        <?php
        // The Loop
        if (!empty($lists)) {
            foreach ($lists as $product_id) {
                set_query_var('product_id', $product_id);            
                // get_template_part('template-parts/content/content', 'product-card-small');
                get_template_part('template-parts/content/content', 'product-card');
            }
        } else {
            echo 'لطفا موردی اضافه کنید';
        }
        ?>
    </div>
</section>