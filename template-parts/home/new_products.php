<?php
$lists = get_field('new_products');
?>

<section class="product-wrap">
    <div class="container">
        <h2 class="title">
            <div class="icon">
                <svg viewBox="0 0 324.79 340.1">
                    <use xlink:href="<?php echo sprite_url; ?>#cart"></use>
                </svg>
            </div>
            بهترین و پرفروش‌ترین گیفت کارت‌ها
        </h2>
        <div class="product-list">
            <?php
            if (!empty($lists)) {
                foreach ($lists as $product_id) {
                    set_query_var('product_id', $product_id);            
                    get_template_part('template-parts/content/content', 'product-card');
                }
            } else {
                echo 'لطفا موردی اضافه کنید';
            }
            ?>
            <div class="btn-holder">
                <a href="<?php echo get_post_type_archive_link( 'giftcard' ); ?>" class="btn btn--white fs-15"> مشاهده لیست جامع گیفت کارت ها</a>
            </div>
        </div>
    </div>
</section>