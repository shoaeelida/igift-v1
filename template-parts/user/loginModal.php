<div class="login-modal-wrapper" id="login-modal-wrapper"></div>
<div class="login-modal-content p-0" id="login-modal-content">
    <div class="instant-registration m-0">
        <div class="mainContainer">
            <div class="registration-wrapper m-0">
                <div class="container registration-step active" id="login-step1">
                    <div class="registration-step1">
                        <span class="input-lbl">شماره تلفن همراه</span>
                        <div class="input-holder">
                            <input type="text" placeholder="مانند : 09379998877" id="login-phone-input"/>
                            <svg viewBox="0 0 301.81 301.81">
                                <use xlink:href="<?php echo sprite_url ?>#phone"></use>
                            </svg>
                        </div>
                        <div class="btn btn--red btn--small mt-2" id="send-otp-error" style="display:none;width: 100%"></div>
                        <div class="condition-tip">
                            <div class="tip-icon">
                                <svg viewBox="0 0 359.33 359.33">
                                    <use xlink:href="<?php echo sprite_url ?>#question"></use>
                                </svg>
                            </div>
                            پس از وارد کردن شماره تلفن همراه ، روی دکمه “تایید شماره
                            همراه” کلیک کنید.
                        </div>
                        <button id="login-phone-button" class="btn btn--dark btn--small step-btn" >
                            تایید شماره همراه
                        </button>
                    </div>
                </div>
                <div class="container registration-step" id="login-step2" style="display: none">
                    <div class="registration-step2 loginVerifyCtd" data-time="01:00">
                        <div class="condition-icon">
                            <svg viewBox="0 0 306.78 320.04">
                                <use xlink:href="<?php echo sprite_url ?>#msg"></use>
                            </svg>
                        </div>
                        <div class="condition-description">
                            آی گیفت پیامکی برای شما ارسال کرده است
                            <span>کد تاییدیه 6 رقمی ذکر شده را در کادر زیر وارد کنید</span>
                        </div>
                        <div class="input-holder">
                            <input type="text" placeholder="" id="verify-phone-input"/>
                            <svg viewBox="0 0 246.35 314.83">
                                <use xlink:href="<?php echo sprite_url ?>#lock"></use>
                            </svg>
                        </div>
                        <div class="btn btn--red btn--small mt-2" id="verify-otp-error" style="display:none;"></div>
                        <div class="btn-holder">
                            <button type="button" id="login-step-1" class="change-number btn btn--transparent btn--small step-btn">
                                تغییر شماره
                            </button>
                            <button type="button" id="verify-phone-button" class="btn btn--small btn--red btn--text--right step-btn" >
                                تایید
                                <svg viewBox="0 0 12.56 19.46">
                                    <use xlink:href="<?php echo sprite_url ?>#arrow"></use>
                                </svg>
                            </button>
                        </div>
                        <div class="resend verify-resend" style="display: none">
                            پیامکی را دریافت نکردید ؟
                            <button type="button" class="verify-resend-btn" data-phone="">ارسال مجدد</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>