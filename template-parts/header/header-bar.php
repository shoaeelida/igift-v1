<?php 

global $playOptions;

$email = ( $playOptions ) ? $playOptions->getOption( 'email' ) : 0;
$phone = ( $playOptions ) ? $playOptions->getOption( 'phone' ) : 0;

$instagram = 0;
$youtube = 0;
$aparat = 0;
$twitter = 0;
$p = 0;

if( $playOptions ){
    $email = $playOptions->getOption( 'email' );
    $phone = $playOptions->getOption( 'phone' );
    $p = str_replace('021','',wp_strip_all_tags($phone));
    $p = str_replace(' ','',$p);
    $p = '021'.$p;
    $instagram = $playOptions->getOption( 'instagram' );
    $youtube = $playOptions->getOption( 'youtube' );
    $aparat = $playOptions->getOption( 'aparat' );
    $twitter = $playOptions->getOption( 'twitter' );
}

?>

<header <?php echo ( is_home() || is_front_page() ) ? 'class="home-header"' : ''; ?>>
    <div class="navbar-holder">
        <nav class="top-head">
            <span class="follow-me"> ما را در شبکه های اجتماعی دنبال کنید </span>
            <?php get_template_part( 'template-parts/nav/nav', 'top' ); ?>
            <div class="support-info">
                <div class="header-social support-item">
                    <?php
                    echo ($instagram) ? '<a href="'.$instagram.'" title="آی گیفت در اینستاگرام" target="_blank"><svg id="h-instagram" viewBox="0 0 551.03 551.03"><use xlink:href="'.sprite_url.'#instagram"></use></svg></a>' : '';
                    echo ($twitter) ? '<a href="'.$twitter.'" title="آی گیفت در توییتر" target="_blank"><svg id="h-twitter" viewBox="0 0 638.44 518.87"><use xlink:href="'.sprite_url.'#twitter"></use></svg></a>' : '';
                    echo ($youtube) ? '<a href="'.$youtube.'" title="آی گیفت در یوتیوب" target="_blank"><svg id="h-youtube"viewBox="5.368 13.434 53.9 37.855"><use xlink:href="'.sprite_url.'#youtube"></use></svg></a>' : '';
                    echo ($aparat) ? '<a href="'.$aparat.'" title="آی گیفت در آپارات" target="_blank"><svg id="h-aparat" viewBox="0 0 48 48"><use xlink:href="'.sprite_url.'#aparat"></use></svg></a>' : '';
                    ?>
                </div>
                <?php if( $phone && !empty($phone) ){ 
                    ?>
                    <a class="support-phone support-item" href="tel:<?php echo $p; ?>" title="<?php echo $phone; ?>">
                        <!-- <b>91005060</b> 021 -->
                        <?php echo $phone; ?>
                        <svg viewBox="0 0 301.81 301.81">
                            <use xlink:href="<?php echo sprite_url; ?>#phone"></use>
                        </svg>
                    </a>
                <?php } ?>
                <?php /*if( $email && !empty($email) ){ ?>
                    <div class="support-email support-item">
                        <?php echo $email; ?>
                    </div>
                <?php }*/ ?>
            </div>
        </nav>
        <?php 
        get_template_part( 'template-parts/header/header', 'searchbar' );
        ?>
    </div>
    <?php 
    if( is_home() || is_front_page() ){
        get_template_part( 'template-parts/hero/hero', 'main' );
    } elseif ( is_singular('game') ) {
        get_template_part( 'template-parts/hero/hero', 'product' );
    } elseif ( is_singular('dlc') ) {
        get_template_part( 'template-parts/hero/hero', 'product' );
    }elseif ( is_singular('giftcard') ) {
        get_template_part( 'template-parts/hero/hero', 'product' );
    }elseif ( is_singular('wiki') ) {
        get_template_part( 'template-parts/hero/hero', 'wiki' );
    } elseif (
        is_post_type_archive('game') ||
        is_post_type_archive('giftcard') ||
        is_post_type_archive('dlc') ||
        is_tax('game_platform') ||
        is_tax('dlc_platform') ||
        is_tax('regions') ||
        is_tag() ||
        is_tax('genres') 
    ) {
        get_template_part( 'template-parts/hero/hero', 'archive' );
    }
    ?>
</header>

