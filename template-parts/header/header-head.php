<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="profile" href="https://gmpg.org/xfn/11" />
<link rel="alternate" hreflang="fa-IR" href="<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" />
<?php echo ( isset($_GET['login_modal']) ) ? '<meta name="robots" content="noindex,follow"/>' : ''; ?>
<?php wp_head(); ?>
<?php get_template_part( 'template-parts/header/header', 'scripts' ); ?>
<link rel="preload" href="<?php echo theme_cdn . '/fonts/YekanBakh/YekanBakh-Medium.woff2' ?>" as="font" type="font/woff2" crossorigin>
<link rel="preload" href="<?php echo theme_cdn . '/fonts/YekanBakh/YekanBakh-Medium.woff' ?>" as="font" type="font/woff" crossorigin>
<link rel="preload" href="<?php echo theme_cdn . '/fonts/YekanBakh/YekanBakh-Bold.woff2' ?>" as="font" type="font/woff2" crossorigin>
<link rel="preload" href="<?php echo theme_cdn . '/fonts/YekanBakh/YekanBakh-Bold.woff' ?>" as="font" type="font/woff" crossorigin>
<link rel="preload" href="<?php echo theme_cdn . '/fonts/YekanBakh/YekanBakh-Heavy.woff2' ?>" as="font" type="font/woff2" crossorigin>
<link rel="preload" href="<?php echo theme_cdn . '/fonts/YekanBakh/YekanBakh-Heavy.woff' ?>" as="font" type="font/woff" crossorigin>
<link rel="preload" href="<?php echo theme_cdn . '/fonts/iGameNumbers-Heavy.woff2' ?>" as="font" type="font/woff2" crossorigin>
<link rel="preload" href="<?php echo theme_cdn . '/fonts/iGameNumbers-Heavy.woff' ?>" as="font" type="font/woff" crossorigin>