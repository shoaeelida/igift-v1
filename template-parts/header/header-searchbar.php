<?php

$is_dark = (is_account_page() || is_cart() || is_checkout() || is_404()) ? true : false;
$can_sticky = (is_home() || is_front_page() || is_singular('game') || is_singular('giftcard') || is_singular('dlc')) ? true : false;

?>
<div class="searchbar<?php echo ($is_dark) ? ' dark' : ''; ?><?php echo ($can_sticky) ? ' can_sticky' : ''; ?>">
    <button class="mini-menu">
        <svg viewBox="0 0 236.46 200.53">
            <use xlink:href="<?php echo sprite_url; ?>#menu"></use>
        </svg>
    </button>
    <a href="<?php echo site_url; ?>"><img class="logo" src="<?php echo theme_assets; ?>/imgs/logo<?php echo ($is_dark) ? '-dark' : ''; ?>.png" alt="<?php echo site_title; ?>"></a>
    <form class="input-search search-by-url">
        <input type="search" value="" placeholder="نام گیفت کارت را وارد کنید" name="s" autocomplete="off">
        <div class="searchResult" style="display:none;"></div>
    </form>
    <div class="cart-register">
        <?php
        if (is_user_logged_in()) {
            $user_meta = play_user_meta();
            $user_name = (!empty($user_meta["phone"])) ? $user_meta["phone"] : $user_meta["name"];
        ?>
            <div class="dashboard-btn-holder">
                <div class="dashboard-menu">
                    <div class="personal-info">
                        <figure class="avatar">
                            <img src="<?php echo theme_assets; ?>/imgs/user_pic.png" alt="<?php echo $user_name; ?>">
                        </figure>
                        <div>
                            <span class="name"><?php echo $user_name; ?></span>
                            <span class="see-profile"><a href="<?php echo wc_get_account_endpoint_url('dashboard'); ?>" title="مشاهده حساب کاربری">مشاهده حساب کاربری</a></span>
                        </div>
                    </div>
                    <ul>
                        <li><a class="orders" title="سفارش های من" href="<?php echo wc_get_account_endpoint_url('orders') ?>"> <?php echo '<svg  viewBox="0 0 520 519.97"><use xlink:href="' . sprite_url . '#orderList"></use></svg>' ?>سفارش های من</a></li>
                        <li><a class="support" title="تیکت و پشتیبانی" href="<?php echo wc_get_account_endpoint_url('support') ?>"> <?php echo '<svg  viewBox="0 0 520 519.97"><use xlink:href="' . sprite_url . '#support"></use></svg>' ?> تیکت و پشتیبانی</a></li>
                        <li><a class="favorites" title="علاقه مندی ها" href="<?php echo wc_get_account_endpoint_url('favorites') ?>"> <?php echo '<svg  viewBox="0 0 195.2 178.61"><use xlink:href="' . sprite_url . '#heart"></use></svg>' ?>علاقه مندی ها</a></li>
                        <li><a class="logout" title="خروج" href="<?php echo wp_logout_url(get_home_url()) ?>"><svg viewBox="0 0 122.94 129.14">
                                    <use xlink:href="<?php echo sprite_url; ?>#logout"></use>
                                </svg> خروج از حساب</a></li>
                    </ul>
                </div>
            <?php
            echo '<a title="پنل کاربری" href="' . get_permalink(get_option('woocommerce_myaccount_page_id')) . '" class="register-wrap btn btn--small btn--transparent">
                    <div class="dashboard-btn">پنل کاربری</div>
                    <svg class="usericon" viewBox="0 0 209.81 238.81">
                        <use xlink:href="' . sprite_url . '#user"></use>
                    </svg>
                </a>
            </div>';
        } else {
            echo '<a title="ورود یا ثبت نام" href="#" class="open-login-modal register-wrap btn btn--small btn--transparent digits-login-modal">
                <div class="login-btn">ورود</div>
                <div class="register-btn">ثبت نام</div>
                <svg class="usericon" viewBox="0 0 209.81 238.81">
                    <use xlink:href="' . sprite_url . '#user"></use>
                </svg>
            </a>';
        } ?>
            <div class="search-icon">
              <svg viewBox="0 0 17.99 18">
                <use xlink:href="<?php echo sprite_url; ?>#search-icon"></use>
              </svg>
            </div>
            <div class="cart-icon <?php echo (WC()->cart->is_empty()) ? 'empty' : ''; ?>" data-count="<?php echo WC()->cart->get_cart_contents_count(); ?>">
                <a title="سبد خرید" href="<?php echo wc_get_cart_url(); ?>">
                    <svg class="cart-svg" viewBox="0 0 324.79 340.1">
                        <use xlink:href="<?php echo sprite_url; ?>#cart"></use>
                    </svg>
                </a>
                <div class="shopping-cart">
                    <?php echo woocommerce_mini_cart(); ?>
                </div>
            </div>
            </div>
    </div>
    <div class="otp-modal-wrapper" style="display: none">
        <div class="otp-modal-body">
            <span class="otp-close-btn">×</span>
            <div class="otp-login-cover"></div>
            <div class="otp-form">
                <div class="otp-login active">
                    <div class="otp-step-one active">
                        <label for="phone-number-l">شماره همراه خود را وارد کنید ...</label>
                        <input id="phone-number-l" type="text" placeholder="شماره تماس">
                        <button class="otp-btn red modal-send-sms-btn-l">ورود</button>
                        <div class="otp-or">یا</div>
                        <button class="otp-btn outline go-to-register">ثبت نام</button>
                    </div>
                    <div class="otp-step-two">
                        <label for="verify-code-l">لطفا کد فعال سازی پیامک شده به را وارد کنید</label>
                        <input id="verify-code-l" type="text" placeholder="-----">
                        <button class="otp-btn red modal-verify-sms-btn-l">ارسال</button>
                        <span class="otp-countdown otp-countdown-l"> ارسال مجدد کد فعال سازی <span>00:00</span></span>
                    </div>
                </div>
                <div class="otp-register">
                    <div class="otp-step-one">
                        <label for="phone-number-r">شماره همراه خود را وارد کنید ...</label>
                        <input id="phone-number-r" type="text" placeholder="شماره تماس">
                        <button class="otp-btn red modal-send-sms-btn-r">ثبت نام</button>
                        <span class="otp-back-to-login go-to-login">بازگشت به ورود</span>
                    </div>
                    <div class="otp-step-two">
                        <label for="verify-code-r">لطفا کد فعال سازی پیامک شده به را وارد کنید</label>
                        <input id="verify-code-r" type="text" max="5" placeholder="-----">
                        <button class="otp-btn red red modal-verify-sms-btn-r">ارسال</button>
                        <span class="otp-countdown otp-countdown-r"> <span>00:00</span>ارسال مجدد کد فعال سازی </span>
                    </div>
                </div>
            </div>
        </div>
    </div>