<!DOCTYPE html>
<html <?php echo play_language_attributes(); ?>>

<head>

    <?php get_template_part( 'template-parts/header/header', 'head' ); ?>

</head>

<body>
<input type="hidden" id="ajax_url" value="<?php echo admin_url('admin-ajax.php'); ?>">
<input type="hidden" id="site_url" value="<?php echo site_url ?>">

<!-- <div class="page-content"> -->
    <?php wp_body_open(); ?>
    <?php get_template_part( 'template-parts/header/header', 'bar' ); ?>
