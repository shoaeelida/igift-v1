<?php

if( $product_id && get_post_status ( $product_id ) == 'publish' ){

    $gift_product_obj = get_product_obj( $product_id );

    ?>

    <a href="<?php echo $gift_product_obj['url']; ?>" class="product-card transparent-white hover-dark normal-size">
       
            <div class="image-holder">
              <?php echo play_render_image($gift_product_obj['thumbnail_url'],$gift_product_obj['title']); ?>
            </div>
        
             <div class="product-title">
                 <?php echo $gift_product_obj['name']; ?>
             </div>
                 <div class="product-bottom">
                  <div class="product-card-price">
                    شروع قیمت از
                    <?php if( isset($gift_product_obj['min_price']) && !empty($gift_product_obj['min_price']) ){
                    echo '<span class="sale-price"> '.$gift_product_obj['min_price'].'</span>';
                } ?>
                  </div>
                  <button class="add-to-cart btn btn--circle btn--transparent">
                   <svg viewBox="0 0 10.38 10.38">
                       <use xlink:href="<?php echo $landing_sprite ?>#plusIcon"></use>
                    </svg>
                  </button>
                </div>
     </a>

<?php
}

