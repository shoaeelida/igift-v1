<?php 
$product_obj = get_product_obj($product_id);
if ($product_obj) { 

    $product_url = $product_obj['url'];

    $price = $product_obj['price'];
    $original_price = $product_obj['original_price_raw'];
    $sale_percent = $product_obj['sale_percent'];
    $sale_price = $product_obj['sale_price_raw'];

    if( isset($main_platform) && !empty($main_platform) ){
        $product_url .= '?platform='.$main_platform['slug'];
        if( isset($product_obj['platform_prices'][$main_platform['slug']]) ){
            $price = $product_obj['platform_prices'][$main_platform['slug']]['price'];
            $original_price = $product_obj['platform_prices'][$main_platform['slug']]['original_price_raw'];
            $sale_percent = $product_obj['platform_prices'][$main_platform['slug']]['sale_percent'];
            $sale_price = $product_obj['platform_prices'][$main_platform['slug']]['sale_price_raw'];
        }
    }

    $en_name = isset($product_obj['en_name']) ? $product_obj['en_name'] : '';
    $color = isset($product_obj['color']) && !empty($product_obj['color']) ? 'style="background:'.$product_obj['color'].';"' : '';
    $icon = isset($product_obj['icon']) ? $product_obj['icon'] : '';
    $mini_desc = isset($product_obj['mini_desc']) ? $product_obj['mini_desc'] : '';
    
    ?>

    <a class="product-card small-size <?php echo empty($color) ? 'purple' : ''; ?>" <?php echo $color; ?> href="<?php echo $product_obj['url']; ?>" title="<?php echo $product_obj['name']; ?>">
        <?php echo !empty($icon) ? '<div class="icon"><img src="'.$icon.'"></div>' : ''; ?>
        <?php echo !empty($icon) ? '<img class="main-cover" src="'.$icon.'">' : ''; ?>
        <div class="product-desc">
            <h3 class="head-title"><?php echo $product_obj['title']; ?></h3>
        </div>
        <div class="card-bottom">
            <div class="price">
                <?php 
                if( $sale_price && $sale_percent && (float)$sale_price > 0 ){
                    echo '<span> شروع از</span>';
                    echo '<b>'.play_price($sale_price).'</b>';
                    echo 'تومان';
                }elseif( (float)$price > 0 ){
                    echo '<span> شروع از</span>';
                    echo '<b>'.play_price($price).'</b>';
                    echo 'تومان';
                } ?>
            </div>
            <div class="btn btn--red btn-card-link">
                <svg viewBox="0 0 12.56 19.47">
                    <use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
                </svg>
            </div>
        </div>
    </a>

<?php } ?>
