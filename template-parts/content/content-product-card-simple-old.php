<?php 
$product_obj = get_product_obj($product_id);
$image_src = (!empty($product_obj['big_thumbnail'])) ? $product_obj['big_thumbnail'] : $product_obj['thumbnail_url'];
if ($product_obj) { 
    ?>
    <div class="simple-card">
        <a href="<?php echo $product_obj['url']; ?>" title="<?php echo $product_obj['name']; ?>">
            <?php echo play_render_image($image_src,$product_obj['title']) ?>
        </a>
    </div>
<?php } ?>
