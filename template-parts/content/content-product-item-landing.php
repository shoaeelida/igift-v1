<?php

if( $product_id && get_post_status ( $product_id ) == 'publish' ){

    $product_obj = get_product_obj( $product_id );
  
    $price = $product_obj['price'];
    $original_price = $product_obj['original_price_raw'];
    $sale_percent = $product_obj['sale_percent'];
    $sale_price = $product_obj['sale_price_raw'];

    $product_url = $product_obj['url'];
    
    if( isset($main_platform) && !empty($main_platform) ){
        $product_url .= '?platform='.$main_platform['slug'];
        if( isset($product_obj['platform_prices'][$main_platform['slug']]) ){
            $price = $product_obj['platform_prices'][$main_platform['slug']]['price'];
            $original_price = $product_obj['platform_prices'][$main_platform['slug']]['original_price_raw'];
            $sale_percent = $product_obj['platform_prices'][$main_platform['slug']]['sale_percent'];
            $sale_price = $product_obj['platform_prices'][$main_platform['slug']]['sale_price_raw'];
        }
    }
    ?>

    <a href="<?php echo $product_url; ?>"
        class="product-card white hover-light normal-size" >
       
            <div class="image-holder">
                <?php if( $product_obj['special'] ){ ?>
                    <span class="special-tag" style="z-index:1;">
                        <svg viewBox="0 0 384 512">
                            <use xlink:href="<?php echo $landing_sprite; ?>#fire"></use>
                        </svg>
                        ویژه
                    </span>
                <?php } ?>
                <?php if( $product_obj['pre_order'] ){ ?>
                    <span class="special-tag preorder-tag" style="z-index:1;">
                        <svg viewBox="0 0 384 512">
                            <use xlink:href="<?php echo $landing_sprite; ?>#fire"></use>
                        </svg>
                        پیش خرید
                    </span>
                <?php } ?>
                <?php echo play_render_image($product_obj['thumbnail_url'],$product_obj['title']); ?>
            </div>
        
            <div class="product-title">
                  <?php echo $product_obj['name']; ?>
                </div>

        <div class="product-bottom">
            
            <div class="product-card-price">
                <?php 
                if( $sale_price && $sale_percent ){
                    echo 'شروع قیمت از';
                    echo '<span class="sale-price">'.play_price($sale_price).'</span>';
                }else{
                     echo 'شروع قیمت از';
                    echo '<span class="sale-price">'.play_price($price).'</span>';
                }
                ?>
            </div>
            <button class="add-to-cart btn btn--circle btn--transparent">
                <svg viewBox="0 0 10.38 10.38">
                    <use xlink:href="<?php echo $landing_sprite; ?>#plusIcon"></use>
                </svg>
            </button>
           
        </div>
    </a>
  

<?php
}