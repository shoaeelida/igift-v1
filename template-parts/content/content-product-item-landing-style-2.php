<?php
if( isset($product_id) ){ ?>
    <div class="slider-item">
        <a href="<?php echo get_permalink($product_id) ?>" title="<?php echo get_the_title($product_id); ?>">
            <?php echo play_render_image(get_the_post_thumbnail_url($product_id),get_the_title($product_id)); ?>
            <div class="product-info">
                <a href="<?php echo get_permalink($product_id) ?>">
                    <div class="add-to-cart">+</div>
                </a>
            </div>
        </a>
    </div>
<?php } ?>
