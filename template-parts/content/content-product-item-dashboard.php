<?php

if( $product_id && get_post_status ( $product_id ) == 'publish' ){

    $product_obj = get_product_obj( $product_id );

    ?>


    <div class="newGames-item">
        <div class="details">
            <div class="labels">
                <?php
                if( !empty($product_obj['main_product_platform']) ){
                    echo (isset($product_obj['main_product_platform']['name'])) ? '<span>'.$product_obj['main_product_platform']['name'].'</span>' : '';
                }
                echo ( !empty($product_obj['regions_name']) ) ? '<span>'.$product_obj['regions_name'].'</span>' : '';
                ?>
            </div>
            <h2 class="gameTitle"><a href="<?php echo $product_obj['url']; ?>" title="<?php echo $product_obj['title']; ?>"><?php echo $product_obj['title']; ?></a></h2>
            <div class="priceBox">
                <a href="<?php echo $product_obj['url']; ?>" title="<?php echo $product_obj['title']; ?>" class="add-to-cart">+</a>
                <div class="price">
                    <?php
                    if( $product_obj['sale_price'] && $product_obj['sale_percent'] ){
                        echo '<div class="former">';
                        echo '<span class="percent">%'.$product_obj['sale_percent'].'</span>';
                        echo '<span class="total">'.$product_obj['original_price'].'</span>';
                        echo '</div>';
                        echo '<div class="later">'.$product_obj['sale_price'].'</div>';
                    }else{
                        echo '<div class="later">'.$product_obj['price_html'].'</div>';
                    }
                    ?>
                </div>
            </div>
        </div>

        <figure class="image">
            <a href="<?php echo $product_obj['url']; ?>" title="<?php echo $product_obj['title']; ?>">
                <?php echo play_render_image($product_obj['thumbnail_url'],$product_obj['title']); ?>
            </a>
        </figure>

    </div>

<?php
}