<?php $product_obj = get_product_obj( $product_id );
if( $product_obj ){ ?>
    <div class="product-item">
        <a href="<?php echo $product_obj['url'] ?>" title="<?php echo $product_obj['title'] ?>">
            <div class="image-holder">
                <?php echo play_render_image($product_obj['thumbnail_url'],$product_obj['title']); ?>
                <div class="product-info">
                    <?php
                    if( isset($main_platform) && $main_platform ) {
                        echo '<div class="product-platform">';
                        echo (isset($main_platform['name'])) ?  '<span>'. $main_platform['name'].'</span>' : '';
                        echo (isset($main_platform['icon'])) ? '<img src="'.$main_platform['icon'].'" alt="'.$main_platform['name'].'" >' : '';
                        echo ' </div>';
                    } elseif( !empty($product_obj['main_product_platform']) ) {
                        echo '<div class="product-platform">';
                        echo (isset($product_obj['main_product_platform']['name'])) ? $product_obj['main_product_platform']['name'] : '';
                        echo (isset($product_obj['main_product_platform']['icon'])) ? '<img src="'.$product_obj['main_product_platform']['icon'].'" alt="'.$product_obj['main_product_platform']['name'].'" >' : '';
                        echo ' </div>';
                    } ?>
                </div>
            </div>
        </a>
    </div>
<?php
}