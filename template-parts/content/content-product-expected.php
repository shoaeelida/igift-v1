<?php 
$product_obj = get_product_obj($product_id);
$image_src = (!empty($product_obj['big_thumbnail'])) ? $product_obj['big_thumbnail'] : $product_obj['thumbnail_url'];
if ($product_obj) { ?>
    <div class="expected-item">
        <div class="slider-cover">
            <?php echo play_render_image($image_src,$product_obj['title']) ?>
        </div>
        <div class="slider-content">
            <div class="slider-description">
                <h2 class="slider-title"><?php echo $product_obj['title']; ?></h2>
                <?php echo (!empty($product_obj['release_date'])) ? "<p><span>زمان انتشار</span>".$product_obj['release_date']."</p>" : ''; ?>
            </div>
            <a href="<?php echo $product_obj['url']; ?>" title="<?php echo $product_obj['name']; ?>" class="btn">
                پیش خرید
                <svg viewBox="0 0 10.38 10.38">
                    <use xlink:href="<?php echo sprite_url; ?>#plusIcon"></use>
                </svg>
            </a>
        </div>
    </div>
<?php } ?>
