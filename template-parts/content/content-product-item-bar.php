<?php

if( !isset($wc_product_obj) ){
    return;
}

$product_id = $wc_product_obj['id'];

if( $product_id && get_post_status ( $product_id ) == 'publish' ){

    $parent_url = get_the_permalink();
    $data_currency = '';
    $giftcard_currency = '';
    $giftcard_price = 0;

    $data_subscribe = '';
    $subscribe_type = '';
    $subscribe_duration = 0;

    $title = '';

    if (isset($wc_product_obj['platform']['slug'])) {
        if ( in_array($wc_product_obj['platform']['slug'],['xbox-one','ps4','nintendo']) ) {
            $platform_name = $wc_product_obj['platform']['slug'];
        } else {
            $platform_name = 'pc';
        }
    }

    if( !$is_giftcard ){

        // $title .= (isset($wc_product_obj['edition']) && !empty($wc_product_obj['edition'])) ? $wc_product_obj['edition'] : $wc_product_obj['title'];
        
        ?>

        <div class="product-detail" <?php echo $hide_row ? 'style="display:none;" data-ismore="1"' : 'data-ismore="0"' ?>>
            <div class="platform-icon">
                <?php
                // if( $is_giftcard ){
                //     echo (isset($products_icon)) ? play_render_image($products_icon,$wc_product_obj['title']) : '';
                // }else{
                    $plname = ( isset($wc_product_obj['platform']['name']) ) ? $wc_product_obj['platform']['name'] : '';
                    echo (isset($wc_product_obj['platform']['icon'])) ? play_render_image($wc_product_obj['platform']['icon'],$wc_product_obj['platform']['name']) : '';
                    echo $plname;
                // }
                echo (isset($wc_product_obj['region']['name']) && !empty($wc_product_obj['region']['name'])) ? '<span class="region">ریجن ' . $wc_product_obj['region']['name'] . '</span>' : ''; 
                ?>
            </div>
            <div class="product-info">
                <div class="name-wrap">
                    <h2 class="product-name">
                        <?php
                        if (isset($wc_product_obj['product_type'])) {
                            if ($wc_product_obj['product_type'] == 'cdkey') {
                                $product_perfix = 'سی دی کی رسمی';
                            } elseif ($wc_product_obj['product_type'] == 'direct') {
                                $product_perfix = 'خرید مستقیم از';
                            } elseif ($wc_product_obj['product_type'] == 'account') {
                                $product_perfix = 'اکانت';
                            } elseif ($wc_product_obj['product_type'] == 'giftcard_steam') {
                                $product_perfix = '  استیم گیفت  ';
                            }
                        }
                        if ($wc_product_obj['product_type'] == 'cdkey') echo 'کد دیجیتال بازی (CDKey)';
                        elseif ($wc_product_obj['product_type'] == 'account') echo 'اکانت ظرفیت کامل';
                        elseif ($wc_product_obj['product_type'] == 'shared_account') {
                            $shared_capacity = product_capacity_name($wc_product_obj['shared_capacity']);
							echo (!empty($shared_capacity)) ? $shared_capacity : '';
                        }
                        else echo (isset($wc_product_obj['platform']['name']) && !empty($wc_product_obj['platform']['name'])) ? $product_perfix . ' ' . $wc_product_obj['platform']['name'] : '';
                        ?>
                    </h2>
                    <ul class="product-caption">
                        <?php
                        // if (!$is_giftcard) {
                        //     if ($wc_product_obj['product_type'] !== 'shared_account') echo '<li>'.$title.'</li>';
                        // }
                        echo (isset($wc_product_obj['edition']) && !empty($wc_product_obj['edition'])) ? '<li>' . $wc_product_obj['edition'] . '</li>' : '';
                        echo (isset($wc_product_obj['region']['name']) && !empty($wc_product_obj['region']['name'])) ? '<li>ریجن ' . $wc_product_obj['region']['name'] . '</li>' : ''; 
                        echo (!empty($giftcard_type_str)) ? '<li class="price">' . $giftcard_type_str . '</li>' : ''; 
                        ?>
                    </ul>
                </div>
                <div class="product-buy">
                    <?php
                    if( $wc_product_obj['can_buy'] ){
                        echo '<span class="sale-price">'.play_price($wc_product_obj['price']).' <b>تومان</b></span>';
                        $cart_count = matched_cart_items($product_id);
                        if( $cart_count['count'] > 0 ){ ?>
                            <div class="buy-action selected <?php /*echo $classes*/?>">
                                <a id="add-to-cart-<?php echo $product_id; ?>" class="open-add btn btn--large btn--text--left remove remove_from_cart_button" data-product_id="<?php echo $wc_product_obj['id']; ?>" data-quantity="1" data-product_sku="" href="<?php echo $parent_url . $wc_product_obj['add_to_cart_url']; ?>" title="افزودن “<?php echo $wc_product_obj['title']; ?>” به سبد خرید" rel="nofollow">
                                    <svg class="add-icon" viewBox="0 0 10.38 10.38">
                                        <use xlink:href="<?php echo sprite_url; ?>#add"></use>
                                    </svg>
                                    <svg class="selected-icon" viewBox="0 0 63.23 42.44">
                                        <use xlink:href="<?php echo sprite_url; ?>#checkmark"></use>
                                    </svg>
                                    <span>خرید</span>
                                </a>
                                <?php
                                ?>
                                <span class="msg">
                                    به سبد خرید اضافه شد
                                </span>
                            </div>
                        <?php } else{ ?>
                            <div class="buy-action <?php /*echo $classes*/?>">
                                <a id="add-to-cart-<?php echo $product_id; ?>" class="open-add btn btn--large btn--text--left add_to_cart_button ajax_add_to_cart" data-product_id="<?php echo $wc_product_obj['id']; ?>" data-quantity="1" data-product_sku="" href="<?php echo $parent_url . $wc_product_obj['add_to_cart_url']; ?>" title="افزودن “<?php echo $wc_product_obj['title']; ?>” به سبد خرید" rel="nofollow">
                                    <svg class="add-icon" viewBox="0 0 10.38 10.38">
                                        <use xlink:href="<?php echo sprite_url; ?>#add"></use>
                                    </svg>
                                    <svg class="selected-icon" viewBox="0 0 63.23 42.44">
                                        <use xlink:href="<?php echo sprite_url; ?>#checkmark"></use>
                                    </svg>
                                    <span>خرید</span>
                                </a>
                                <span class="msg">
                                    به سبد خرید اضافه شد
                                </span>
                            </div>
                        <?php  }
                    }else{ ?>
                        <div class="buy-action">
                            <a class="open-chat inquiry-btn btn btn--large" rel="nofollow" title="استعلام قیمت">
                                <span>استعلام قیمت</span>
                            </a>
                        </div>
                        <?php
                    } ?>
                </div>
            </div>
        </div>
        <?php
        if( !empty($wc_product_obj['platform']['activation']) ){
            echo '<div class="product-tip">
                <svg class="tip-icon" viewBox="0 0 119.53 107.51">
                    <use xlink:href="'.sprite_url.'#warning"></use>
                </svg>
                این محصول فقط برروی '.$plname.' فعال میشود!
                <a class="activation-guid" href="'.$wc_product_obj['platform']['activation'].'">
                    راهنمای فعالسازی '.$plname.'
                </a>
            </div>';
        } ?>

    <?php }else{ 
        
        $giftcard_type_str = '';

        if( isset($wc_product_obj['giftcard_type']) ){
            switch ($wc_product_obj['giftcard_type']) {
                case 'default':
                    $giftcard_currency = ( isset($wc_product_obj['giftcard_currency']) && !empty($wc_product_obj['giftcard_currency']) ) ? ' ' . $wc_product_obj['giftcard_currency']['label'] : '';
                    $giftcard_price = ( isset($wc_product_obj['giftcard_price']) && !empty($wc_product_obj['giftcard_price']) ) ? ' ' . $wc_product_obj['giftcard_price'] : 0;
                    $data_currency = trim($giftcard_price . '_' . $wc_product_obj['giftcard_currency']['value']);
                    break;
                case 'subscribe':
                    $subscribe_duration = ( isset($wc_product_obj['subscribe_duration']) && !empty($wc_product_obj['subscribe_duration']) ) ? ' ' . $wc_product_obj['subscribe_duration'] : 0;
                    $subscribe_type = ( isset($wc_product_obj['subscribe_type']) && !empty($wc_product_obj['subscribe_type']) ) ? ' ' . $wc_product_obj['subscribe_type']['label'] : '';
                    $data_subscribe = trim($subscribe_duration . '_' . $wc_product_obj['subscribe_type']['value']);
                    break;
                default:
                    break;
            }
        }
        
        $title .= $wc_product_obj['title'];
        if( isset($wc_product_obj['giftcard_type']) ){
            switch ($wc_product_obj['giftcard_type']) {
                case 'default':
                    // $title .= ( !empty($giftcard_currency) ) ? '<span class="product-name-price"><span>' . $giftcard_price . '</span>' . $giftcard_currency . '</span>' : '';
                    $title = ( !empty($giftcard_currency) ) ? '<span><span>' . $giftcard_price . '</span>' . $giftcard_currency . '</span>' : '';
                    $giftcard_type_str = ( !empty($giftcard_currency) ) ? '<span>' . $giftcard_price . '</span><span>' . $giftcard_currency . '</span>' : '';
                    break;
                case 'subscribe':
                    // $title .= ( !empty($subscribe_type) ) ? '<span class="product-name-price"><span>' . $subscribe_duration . '</span>' . $subscribe_type . '</span>' : '';
                    $title = ( !empty($subscribe_type) ) ? '<span><span>' . $subscribe_duration . '</span>' . $subscribe_type . '</span>' : '';
                    $giftcard_type_str = ( !empty($subscribe_type) ) ? '<span>' . $subscribe_duration . '</span><span>' . $subscribe_type . '</span>' : '';
                    break;
                default:
                    break;
            }
        }

        ?>

        <div class="giftcard-item">

            <?php 
            echo (isset($products_icon)) ? play_render_image($products_icon,$wc_product_obj['title'],'platform-img') : '';
            ?>

            <span class="title"><?php echo $title; ?></span>
            <?php
            if( $wc_product_obj['can_buy'] ){
                $cart_count = matched_cart_items($product_id);
                echo '<div class="price"><b>'.play_price($wc_product_obj['price']).'</b> تومان</div>';
                if( $cart_count['count'] > 0 ){ ?>
                    <div class="buy-action selected <?php /*echo $classes*/?>">
                        <a id="add-to-cart-<?php echo $product_id; ?>" class="open-add btn btn--large btn--text--left remove remove_from_cart_button" data-product_id="<?php echo $wc_product_obj['id']; ?>" data-quantity="1" data-product_sku="" href="<?php echo $parent_url . $wc_product_obj['add_to_cart_url']; ?>" title="افزودن “<?php echo $wc_product_obj['title']; ?>” به سبد خرید" rel="nofollow">
                            <svg class="add-icon" viewBox="0 0 10.38 10.38">
                                <use xlink:href="<?php echo sprite_url; ?>#add"></use>
                            </svg>
                            <svg class="selected-icon" viewBox="0 0 63.23 42.44">
                                <use xlink:href="<?php echo sprite_url; ?>#checkmark"></use>
                            </svg>
                            <span>خرید</span>
                        </a>
                        <?php
                        ?>
                        <span class="msg">
                            به سبد خرید اضافه شد
                        </span>
                    </div>
                <?php } else{ ?>
                    <div class="buy-action <?php /*echo $classes*/?>">
                        <a id="add-to-cart-<?php echo $product_id; ?>" class="open-add btn btn--large btn--text--left add_to_cart_button ajax_add_to_cart" data-product_id="<?php echo $wc_product_obj['id']; ?>" data-quantity="1" data-product_sku="" href="<?php echo $parent_url . $wc_product_obj['add_to_cart_url']; ?>" title="افزودن “<?php echo $wc_product_obj['title']; ?>” به سبد خرید" rel="nofollow">
                            <svg class="add-icon" viewBox="0 0 10.38 10.38">
                                <use xlink:href="<?php echo sprite_url; ?>#add"></use>
                            </svg>
                            <svg class="selected-icon" viewBox="0 0 63.23 42.44">
                                <use xlink:href="<?php echo sprite_url; ?>#checkmark"></use>
                            </svg>
                            <span>خرید</span>
                        </a>
                        <span class="msg">
                            به سبد خرید اضافه شد
                        </span>
                    </div>
                <?php  }
            }else{ ?>
                <div class="buy-action">
                    <a class="open-chat inquiry-btn btn btn--large" rel="nofollow" title="استعلام قیمت">
                        <span>استعلام قیمت</span>
                    </a>
                </div>
                <?php
            } ?>
        </div>

    <?php }

}