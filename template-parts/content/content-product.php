<?php

$product_obj = get_product_obj();
if ($product_obj) {

    $ID = $product_obj['id'];

    set_query_var('product_obj', $product_obj);
    set_query_var('ID', $ID);

    $type_name = 'محصول';

    $is_giftcard = ($product_obj['type'] == 'giftcard') ? true : false;
    $is_game = ($product_obj['type'] == 'game') ? true : false;
    $is_dlc = ($product_obj['type'] == 'dlc') ? true : false;
    $platform_tax = '';
    if ($is_game) {
        $platform_tax = 'game_platform';
    } elseif ($is_dlc) {
        $platform_tax = 'dlc_platform';
    }
    set_query_var('is_giftcard', $is_giftcard);
    set_query_var('is_game', $is_game);
    set_query_var('is_dlc', $is_dlc);

    $comments_open = comments_open($ID);
    $main_product = (isset($product_obj['main_product'])) ? $product_obj['main_product'] : null;
    $child_products = (isset($product_obj['child_products'])) ? $product_obj['child_products'] : null;
    $recommended_system = (isset($product_obj['recommended_system'])) ? $product_obj['recommended_system'] : null;
    $require_system = (isset($product_obj['require_system'])) ? $product_obj['require_system'] : null;
    $gameplay = (isset($product_obj['gameplay'])) ? $product_obj['gameplay'] : null;
    $features = (isset($product_obj['features'])) ? $product_obj['features'] : null;
    $shopping_steps = (isset($product_obj['shopping_steps'])) ? $product_obj['shopping_steps'] : null;
    $active_side_product = (isset($product_obj['active_side_product'])) ? $product_obj['active_side_product'] : null;
    $side_products = (isset($product_obj['side_products'])) ? $product_obj['side_products'] : null;
    $helps = (isset($product_obj['helps'])) ? $product_obj['helps'] : null;
    $trailer_link = (isset($product_obj['trailer_link'])) ? $product_obj['trailer_link'] : null;
    $trailer_link_desc = (isset($product_obj['trailer_link_desc'])) ? $product_obj['trailer_link_desc'] : null;
    $regions = (isset($product_obj['regions'])) ? $product_obj['regions'] : false;
    $platforms = (isset($product_obj['platforms'])) ? $product_obj['platforms'] : false;
    $products_icon = (isset($product_obj['products_icon'])) ? $product_obj['products_icon'] : null;
    $download_links = get_field('download_links');
    set_query_var('products_icon', $products_icon);

    $active_currency = (isset($_GET['currency']) && !empty($_GET['currency'])) ? $_GET['currency'] : null;
    $active_subscribe = (isset($_GET['subscribe']) && !empty($_GET['subscribe'])) ? $_GET['subscribe'] : null;

    $child_platforms = [];
    $child_regions = [];
    if (!$is_giftcard) {
        if( !empty($child_products) ){
            foreach ($child_products as $product) {
                $platform = [];
                switch ($product['platform']['slug']) {
                    case 'ps4':
                        $platform = [
                            'slug' => 'ps4',
                            'name' => 'پلی استیشن',
                            'icon' => '<div class="icon blue">
                                <svg viewBox="0 0 585.15 451.63">
                                    <use xlink:href="'.sprite_url.'#playStation"></use>
                                </svg>
                            </div>'
                        ];
                        break;
                    case 'xbox-one':
                        $platform = [
                            'slug' => 'xbox',
                            'name' => 'ایکس باکس',
                            'icon' => '<div class="icon white">
                                <img src="'.theme_assets.'/imgs/platform/xbox-color.svg" alt="ایکس باکس" />
                            </div>'
                        ];
                        break;
                    default:
                        $platform = [
                            'slug' => 'pc',
                            'name' => 'کامپیوتر',
                            'icon' => '<div class="icon dark">
                                <svg viewBox="0 0 37.01 25.03">
                                <use xlink:href="'.sprite_url.'#pc"></use>
                                </svg>
                            </div>'
                        ];
                        break;
                }
                if( !isset($child_platforms[$platform['slug']]) ){
                    $child_platforms[$platform['slug']] = $platform;
                }
                $child_platforms[$platform['slug']]['products'][] = $product;
            }
        }
    }else{
        if( !empty($child_products) ){
            foreach ($child_products as $product) {
                if( !isset($child_regions[$product['region']['slug']]) ){
                    $child_regions[$product['region']['slug']] = $product['region'];
                }
                $child_regions[$product['region']['slug']]['products'][] = $product;
            }
        }
    }

    $list_title = 'قیمت ' . $product_obj['name'];

    if( $is_game ){
        $type_name = 'بازی';
        // $list_title = 'قیمت ' . $product_obj['name'];
    }elseif( $is_giftcard ){
        $type_name = 'گیفت کارت';
        // $list_title = 'قیمت گیفت کارت ' . $product_obj['name'];
    }

    $features_title = "ویژگیهای کلیدی $type_name";
    if( $is_giftcard ){
        $features_title = "ویژگیهای کلیدی " . $product_obj['name'];
    }

    if ($is_game) { ?>
        <div class="product-introduction">
            <div class="introduction-content">
                <?php
                // echo '<div id="xbox_capacity_rules" class="d-none">'.get_field('xbox_capacity_rules', 'option').'</div>';
                // echo '<div id="ps4_capacity_rules" class="d-none">'.get_field('ps4_capacity_rules', 'option').'</div>';
                ?>
                <div class="btn-holder">
                    <?php if (isset($trailer_link) && !empty($trailer_link)) {
                        echo '<a id="go-to-trailer" href="#description-wrap" title="تماشای تریلر بازی" target="_blank" rel="nofollow" class="btn btn--small btn--dark btn--text--left">
                            تماشای تریلر بازی
                            <svg viewBox="0 0 76.5 99.72">
                                <use xlink:href="' . sprite_url . '#arrow-fill"></use> 
                            </svg>
                        </a>';
                    } ?>
                    <a id="go-to-about" class="more-btn btn btn--small btn--transparent" href="#description-wrap" title="توضیحات کامل بازی">توضیحات کامل بازی</a>
                    <?php
                    if( $active_side_product ) { ?>
                        <a class="more-btn btn btn--small btn--transparent" href="<?php echo site_url ?>/dlc/relatedGame/<?php echo get_the_ID() ?>" >پک های الحاقی</a>
                    <?php }
                    ?>
                </div>
                <div class="introduction-text">
                    <?php
                    if (!empty($product_obj['desc'])) {
                        echo '<p>';
                        echo str_replace("[&hellip;]", "", $product_obj['desc']);
                        echo '</p>';
                        if ($product_obj['content'] && !empty($product_obj['content'])) {
                            echo '<a class="more-intro" href="#description-wrap" title="بیشتــر"><svg viewBox="0 0 12.56 19.46"><use xlink:href="' . sprite_url . '#arrow"></use></svg></a>';
                        }
                    } ?>
                </div>
            </div>
        </div>
        <?php
    } elseif ($product_obj['type'] == 'giftcard') { ?>
        <div class="giftcard-introduction">
            <div class="introduction-content">
                <?php echo (!empty($product_obj['desc'])) ? '<p>' . $product_obj['desc'] . '</p>' : '' ?>
                <!-- <a href="#" class="guid-shop">
                    راهنمـای خرید و فعالسازی
                    <div class="icon">
                        <svg viewBox="0 0 125.13 237.23">
                            <use xlink:href="<?php echo sprite_url ?>#question-mark"></use>
                        </svg>
                    </div>
                </a> -->
            </div>
        </div>
    <?php } ?>

    <div class="<?php echo !$is_giftcard ? 'product' : 'gift'; ?>-main-content" id="products-list">

        <div class="container">
            <?php if ($child_products && !$is_giftcard) { ?>
                <div class="product-wrap" id="product-wrap">
                    <h2 class="select-title"> <?php echo $list_title; ?></h2>
                    <?php if (isset($product_obj['pre_order']) && $product_obj['pre_order'] == true) { ?>
                        <div class="notification-holder">
                            <div class="notify-item">
                                <svg class="notify-icon" viewBox="0 0 119.53 107.51">
                                    <use xlink:href="<?php echo sprite_url; ?>#warning"></use>
                                </svg>
                                <span class="notify-text">توجه</span>
                                <span class="notify-caption">این بازی به صورت پیش خرید می‌باشد و سی دی کی آن در روز عرضه جهانی محصول در اختیار شما قرار می‌گیرد.</span>
                            </div>
                        </div>
                    <?php }
                    if (isset($product_obj['custom-notic']) && !empty($product_obj['custom-notic'])) { ?>
                        <div class="notification-holder">
                            <div class="notify-item">
                                <svg class="notify-icon" viewBox="0 0 119.53 107.51">
                                    <use xlink:href="<?php echo sprite_url; ?>#warning"></use>
                                </svg>
                                <span class="notify-text">توجه</span>
                                <span class="notify-caption"><?php echo $product_obj['custom-notic']; ?></span>
                            </div>
                        </div>
                    <?php }

                    if( !empty($child_platforms) ){
                        foreach ($child_platforms as $platform ) { ?>
                            <div class="group-product">
                                <div class="group-head">
                                    <h2 class="group-title <?php !isset($platform['icon']) || empty($platform['icon']) ? 'group-title-noicon' : ''; ?>">
                                        <?php echo (isset($platform['icon'])) ? $platform['icon'] : ''; ?>
                                        خرید بازی <?php echo $platform['name']; ?>
                                    </h2>
                                    <?php
                                    /*<a href="#" class="download-data">
                                        <div class="icon">
                                            <svg viewBox="0 0 77.34 77.34">
                                                <use xlink:href="<?php echo sprite_url; ?>#flash"></use>
                                            </svg>
                                        </div>
                                        لینک های دانلود دیتا
                                    </a>*/
                                    ?>
                                </div>
                                <div class="group-body">
                                    <?php
                                    $show_more = $platform['products'] && count($platform['products']) > 3;
                                    foreach ($platform['products'] as $i => $wc_product_obj) {
                                        set_query_var('wc_product_obj', $wc_product_obj);
                                        set_query_var('is_giftcard', false);
                                        set_query_var('hide_row', $i + 1 > 3 );
                                        get_template_part('template-parts/content/content', 'product-item-bar');
                                    } 
                                    if( $show_more ){
                                        echo '<a href="#" class="load-more">
                                            <svg class="arrow" viewBox="0 0 12.56 19.46">
                                                <use xlink:href="'.sprite_url.'#arrow"></use>
                                            </svg>
                                            مشاهده همه موارد
                                        </a>';
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                        }
                    }

                    ?>
                </div>
            <?php } ?>
        </div>

        <?php if ($active_side_product) { ?>
            <div class="side-products">
                <img class="cover" src="<?php echo theme_assets; ?>/imgs/side-bg.jpg" alt="" />
                <div class="side-product-description">
                    <div class="icon">
                        <svg viewBox="0 0 495.28 250.42">
                            <use xlink:href="<?php echo sprite_url; ?>#game-white"></use>
                        </svg>
                    </div>
                    <h2 class="title"><?php echo $side_products["title"]; ?></h2>
                    <p><?php echo $side_products["desc"]; ?></p>
                </div>
                <div class="side-product-list owl-carousel">
                    <?php
                    $side_products_list = new WP_Query($side_products_args);
                    if (!empty($side_products["items"])) {
                        foreach ($side_products["items"] as $product_id) {
                            set_query_var('product_id', $product_id);
                            get_template_part('template-parts/content/content', 'product-card');
                        }
                    } else {
                        echo 'لطفا موردی اضاف کنید';
                    } ?>
                </div>
            </div>
        <?php } ?>

        <?php if (!$is_giftcard) {
            get_template_part('template-parts/parts/related');
        } ?>

        <?php if ($child_products && $is_giftcard) { ?>

            <div class="giftcard-list-wrap">
                <div class="product-wrap">
                    <h2 class="select-title"> <?php echo $list_title; ?></h2>
                    <?php
                    if( !empty($child_regions) ){
                        foreach ($child_regions as $region ) { ?>

                            <div class="group-product">
                                <div class="group-head">
                                    <h2 class="group-title <?php !isset($region['icon']) || empty($region['icon']) ? 'group-title-noicon' : ''; ?>">
                                        <?php echo (isset($region['icon'])) ? '<div class="icon full-img">' . play_render_image($region['icon'],$region['name'],'',true) . '</div>' : ''; ?>
                                        ریجن <?php echo $region['name']; ?>
                                    </h2>
                                    <?php
                                    /*
                                    <a href="#" class="guid-link">
                                        <div class="icon">
                                            <svg viewBox="0 0 125.13 237.23">
                                                <use xlink:href="<?php echo sprite_url; ?>#question-mark"></use>
                                            </svg>
                                        </div>
                                        راهنمـای خرید و فعالسازی
                                    </a>
                                    */
                                    ?>
                                </div>

                                <div class="giftcard-list">
                                    <?php
                                    foreach ($region['products'] as $wc_product_obj) {
                                        set_query_var('wc_product_obj', $wc_product_obj);
                                        set_query_var('is_giftcard', $is_giftcard);
                                        get_template_part('template-parts/content/content', 'product-item-bar');
                                    } ?>
                                </div>
                            </div>

                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        <?php } ?>

        <div class="description-wrap" id="description-wrap">
            <ul class="tab-holder">
                <li id="aboutGame" class="active">
                    <svg viewBox="0 0 236.46 200.53">
                        <use xlink:href="<?php echo sprite_url; ?>#menu"></use>
                    </svg>
                    درباره <?php echo $type_name; ?>
                </li>
                <?php if( $comments_open ){ ?>
                    <li id="comment">
                        <svg viewBox="0 0 181.01 148.29">
                            <use xlink:href="<?php echo sprite_url; ?>#comment"></use>
                        </svg>
                        نظرات کاربران
                    </li>
                <?php } ?>
                <?php if ($require_system or $recommended_system) { ?>
                    <li id="requiredsystem">
                        <svg viewBox="0 0 281.23 206.71">
                            <use xlink:href="<?php echo sprite_url; ?>#requiredsystem"></use>
                        </svg>
                        سیستم مورد نیاز
                    </li>
                <?php } ?>
                <?php if ($helps && isset($helps['new_help']) && !empty($helps['new_help']) ) { ?>
                    <li id="shoppingGuid">
                        <svg viewBox="0 0 139.16 141.38">
                            <use xlink:href="<?php echo sprite_url; ?>#shoppingGuid"></use>
                        </svg>
                        راهنمای خرید
                    </li>
                <?php  }
                /*<li id="gallery">
                    <svg viewBox="0 0 181.01 141.91">
                        <use xlink:href="<?php echo sprite_url; ?>#gallery"></use>
                    </svg>
                    گالری تصاویر
                </li>*/
                ?>
            </ul>
            <div class="tab-content-holder">
                <div id="aboutGame-content" class="tab-content active">
                    <?php if ($trailer_link) {
                        echo !empty($trailer_link_desc) ? "<div class=\"trailer-desc\">$trailer_link_desc</div>" : "";
                        $video_code = str_replace('https://www.aparat.com/v/', '', $trailer_link);
                        ?>
                        <div class="trailer-wrapper">
                            <div class="trailer-cover">
                                <style>
                                    .h_iframe-aparat_embed_frame {
                                        position: relative;
                                        width: 100%;
                                        height: 100%;
                                    }

                                    .h_iframe-aparat_embed_frame .ratio {
                                        display: block;
                                        width: 100%;
                                        height: auto;
                                    }

                                    .h_iframe-aparat_embed_frame iframe {
                                        position: absolute;
                                        top: 0;
                                        left: 0;
                                        width: 100%;
                                        height: 100%;
                                    }
                                </style>
                                <style id="igame-style">
                                    .trailer-wrapper .trailer-cover .play {
                                        width: 70px;
                                        height: 70px;
                                        -webkit-border-radius: 50%;
                                        border-radius: 50%;
                                        background: #f3303a;
                                        -webkit-box-shadow: 0 8px 27px 0 rgba(243, 48, 58, .23);
                                        box-shadow: 0 8px 27px 0 rgba(243, 48, 58, .23);
                                        position: relative;
                                    }
                                </style>
                                <div class="h_iframe-aparat_embed_frame"><span style="display: block;padding-top: 57%"></span><iframe id="aparat_iframe" src="https://www.aparat.com/video/video/embed/videohash/<?php echo $video_code ?>/vt/frame" allowFullScreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe></div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="description-content-box">
                        <div class="description-categorize">
                            <?php if ($product_obj['content'] && !empty($product_obj['content'])) { ?>
                                <a href="#about-section" id="about" class="cat-item">
                                    <div class="icon">
                                        <svg viewBox="0 0 205.4 205.4">
                                            <use xlink:href="<?php echo sprite_url ?>#about"></use>
                                        </svg>
                                    </div>
                                    <span class="title-text">درباره محصول</span>
                                </a>
                            <?php } ?>
                            <?php if ($features) { ?>
                                <a href="#properties-section" id="properties" class="cat-item">
                                    <div class="icon">
                                        <svg viewBox="0 0 196.67 219.04">
                                            <use xlink:href="<?php echo sprite_url ?>#properties"></use>
                                        </svg>
                                    </div>
                                    <span class="title-text"><?php echo $features_title; ?></span>
                                </a>
                            <?php } ?>
                        </div>
                        <div class="description-content">
                            <?php if ($product_obj['content'] && !empty($product_obj['content'])) { ?>
                                <div id="about-section" class="about-section desc-item">
                                    <div class="responsive-title">
                                        <div class="icon">
                                            <svg viewBox="0 0 205.4 205.4">
                                                <use xlink:href="<?php echo sprite_url ?>#about"></use>
                                            </svg>
                                        </div>
                                        <span class="title-text">درباره محصول</span>
                                    </div>
                                    <?php echo $product_obj['content']; ?>
                                    <?php echo isset($gameplay) ? $gameplay : '' ?>
                                </div>
                            <?php } ?>
                            <?php if ($features) { ?>
                                <div id="properties-section" class="properties-section desc-item">
                                    <div class="responsive-title">
                                        <div class="icon">
                                            <svg viewBox="0 0 196.67 219.04">
                                                <use xlink:href="<?php echo sprite_url ?>#properties"></use>
                                            </svg>
                                        </div>
                                        <span class="title-text"><?php echo $features_title; ?></span>
                                    </div>
                                    <?php
                                    foreach ($features as $f) {
                                        echo '<div class="property-item">' . $f['item'] . '</div>';
                                    }
                                    ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php if ($require_system or $recommended_system) { ?>
                    <div id="requiredsystem-content" class="tab-content">
                        <div class="description-content-box">
                            <div class="description-categorize">
                                <a href="#required-section" id="required" class="cat-item">
                                    <div class="icon">
                                        <svg viewBox="0 0 281.23 206.71">
                                            <use xlink:href="<?php echo sprite_url; ?>#system"></use>
                                        </svg>
                                    </div>
                                    <span class="title-text">سیستم مورد نیاز <?php echo $type_name; ?></span>
                                </a>
                            </div>
                            <div class="description-content">
                                <div id="required-section" class="required-section desc-item">
                                    <div class="responsive-title">
                                        <div class="icon">
                                            <svg viewBox="0 0 281.23 206.71">
                                                <use xlink:href="<?php echo sprite_url; ?>#system"></use>
                                            </svg>
                                        </div>
                                        <span class="title-text">سیستم مورد نیاز <?php echo $type_name; ?></span>
                                    </div>
                                    <?php if ($require_system) { ?>
                                        <div class="minimum">حداقل سیستم موردنیاز</div>
                                        <span class="required-part"> </span>
                                    <?php }
                                    /*if ($recommended_system) {
                                        <div class="suggested">
                                            <svg viewBox="0 0 297.5 286.44">
                                                <use xlink:href="<?php echo sprite_url ?>#star"></use>
                                            </svg>
                                            سیستم پیشنهادی
                                        </div>
                                    } */
                                    for ($i = 0; $i <= count($require_system); $i++) { ?>
                                        <div class="required-row">
                                            <span class="required-part"><?php echo isset($require_system[$i]['name']) ? $require_system[$i]['name'] : ''  ?></span>
                                            <div class="minimum"><?php echo isset($require_system[$i]['value']) ? $require_system[$i]['value'] : '' ?></div>
                                            <!-- <div class="suggested"><?php echo isset($recommended_system[$i]['value']) ? $recommended_system[$i]['value'] : '' ?></div> -->
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($helps && isset($helps['new_help']) && !empty($helps['new_help']) ) { ?>
                    <div id="shoppingGuid-content" class="tab-content">
                        <div class="description-content-box">
                            <div class="description-categorize">
                                <a href="#guide-section" id="guide" class="cat-item">
                                    <div class="icon">
                                        <svg viewBox="0 0 125.13 237.23">
                                            <use xlink:href="<?php echo sprite_url; ?>#question-mark"></use>
                                        </svg>
                                    </div>
                                    <span class="title-text">راهنمای خرید</span>
                                </a>
                            </div>
                            <div class="description-content">
                                <div id="guide-section" class="guide-section desc-item">
                                    <div class="responsive-title">
                                        <div class="icon">
                                            <svg viewBox="0 0 125.13 237.23">
                                                <use xlink:href="<?php echo sprite_url; ?>#question-mark"></use>
                                            </svg>
                                        </div>
                                        <span class="title-text">راهنمای خرید</span>
                                    </div>
                                    <div class="shopping-steps">
                                        <?php if ($shopping_steps) {
                                            foreach ($shopping_steps as $key => $value) {
                                                echo '<p data-num="' . ++$key . '">' . $value['step'] . '</p>';
                                            }
                                        } ?>
                                    </div>
                                </div>
                                <div class="questions-section desc-item">
                                    <?php foreach ($helps["new_help"] as $key => $value) {
                                        if (!empty($value["general-item"])) {
                                            for ($i = 0; $i < count($value["general-item"]); $i++) {
                                                echo '<div data-num="' . ($key + 1) . '" class="question-item">
                                                    <svg class="arrow" viewBox="0 0 12.56 19.46"><use xlink:href="' . sprite_url . '#arrow"></use></svg>
                                                    <span class="question">' . $value["general-item"][$i]->post_title . '</span>
                                                    <div class="answer">' . $value["general-item"][$i]->post_content . '</div>
                                                </div>';
                                            }
                                        } else {
                                            echo '<div data-num="' . ($key + 1) . '" class="question-item">
                                                <svg class="arrow" viewBox="0 0 12.56 19.46"><use xlink:href="' . sprite_url . '#arrow"></use></svg>
                                                <span class="question">' . $value["new_title"] . '</span>
                                                <div class="answer">' . $value["new_desc"] . '</div>
                                            </div>';
                                        }
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <!-- <div id="gallery-content" class="tab-content"></div> -->
                <?php if ($comments_open) { ?>
                    <div id="comment-content" class="tab-content">
                        <div class="description-content-box">
                            <div class="description-categorize">
                                <a href="#about-section" id="about" class="cat-item">
                                    <div class="icon">
                                        <svg viewBox="0 0 181.01 148.29">
                                            <use xlink:href="<?php echo sprite_url; ?>#comment"></use>
                                        </svg>
                                    </div>
                                    <span class="title-text">نظرات کاربران</span>
                                </a>
                            </div>
                            <div class="description-content">
                                <div class="responsive-title">
                                    <div class="icon">
                                        <svg viewBox="0 0 181.01 148.29">
                                            <use xlink:href="<?php echo sprite_url; ?>#comment"></use>
                                        </svg>
                                    </div>
                                    <span class="title-text">نظرات کاربران</span>
                                </div>
                                <div class="user-comment-wrap">
                                    <?php
                                    set_query_var('review_product_id', $product_obj["child_products"]);
                                    get_template_part('template-parts/parts/reviews');
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <?php if ($is_giftcard) {
            get_template_part('template-parts/parts/related');
        } ?>

    </div>

    <?php

    if($comments_open){
        echo '<div class="review-modal-wrapper"></div>';
        // if( get_comments_number( $ID )){
        echo do_shortcode('[reviews_form post_id="'.$ID.'"]');
        // }
    }


} ?>