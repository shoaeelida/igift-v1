<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
*/

?>

<div class="mainHeader mainHeader-help">
    <div class="container">
        <h1 class="title"><?php the_title(); ?></h1>
    </div>
</div>

<div class="simple-content simple-content-page mt-5">
    <div class="container">
        <?php the_content(); ?>
    </div>
</div>