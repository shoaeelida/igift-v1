<?php

if( $product_id && get_post_status ( $product_id ) == 'publish' ){

    $product_obj = get_product_obj( $product_id );

    ?>

    <a href="<?php echo $product_obj['url']; ?>" title="<?php echo $product_obj['title']; ?>" class="product-item">
        <div class="image-holder">
            <?php echo play_render_image($product_obj['thumbnail_url'],$product_obj['title']); ?>
        </div>
        <div class="product-title">
            <?php echo $product_obj['name']; ?>
        </div>
        <div class="product-bottom">
            <div class="product-card-price">
                <?php 
                if( $product_obj['sale_price'] && $product_obj['sale_percent'] ){
                    echo 'شروع قیمت از';
                    echo '<span class="sale-price">'.$product_obj['sale_price'].'</span>';
                }else{
                     echo 'شروع قیمت از';
                    echo '<span class="sale-price">'.$product_obj['price_html'].'</span>';
                }
                ?>
            </div>
        </div>
    </a>

<?php
}