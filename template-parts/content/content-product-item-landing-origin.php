<?php
$product_obj = get_product_obj( $product_id );
if( $product_obj ){ ?>

    <a href="<?php echo $product_obj['url']; ?>"
            class="product-card transparent-white hover-light normal-size" >
            <div class="image-holder">
              <?php echo play_render_image($product_obj['thumbnail_url'],$product_obj['title']); ?>
            </div>
            <div class="product-title">
                <?php echo $product_obj['name']; ?>
            </div>

        <div class="product-bottom">
              <div class="product-card-price">
                 <?php
                if( $product_obj['sale_price_raw'] && $product_obj['sale_percent'] ){
                     echo 'شروع قیمت از';
                    echo '<span class="sale-price">'.play_price($product_obj['sale_price_raw']).'</span>';
                }else{
                     echo 'شروع قیمت از';
                    echo '<span class="sale-price">'.play_price($product_obj['price']).'</span>';
                }
                ?>
              </div>
              <button class="add-to-cart btn btn--circle btn--transparent">
                <svg viewBox="0 0 10.38 10.38">
                  <use xlink:href="<?php echo $landing_sprite ?>#plusIcon"></use>
                </svg>
              </button>
        </div>
   
    </a>

<?php }