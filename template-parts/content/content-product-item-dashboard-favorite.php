<?php

if( $product_id && get_post_status ( $product_id ) == 'publish' ){

    $product_obj = get_product_obj( $product_id );

    ?>

    <div class="item">
        <div class="img">
            <?php echo play_render_image($product_obj['thumbnail_url'],$product_obj['title']); ?>
        </div>
        <div class="hoverLayer">
            <?php
            /**
             * Get the favorite button for a specified post
             * Post ID not required if inside the loop
             * @param $post_id int, defaults to current post
             * @param $site_id int, defaults to current blog/site
             */
            $favorite_btn = get_favorites_button($product_obj['id']);
            // add btn class
            $favorite_btn = str_replace('simplefavorite-button', 'btn simplefavorite-button',$favorite_btn);
            echo $favorite_btn;
            ?>
            <a class="btn" href="<?php echo $product_obj['url']; ?>" title="<?php echo $product_obj['title']; ?>">
                <svg viewBox="0 0 77.34 77.34"><use xlink:href="<?php echo sprite_url; ?>#flash"></use></svg>
                <span class="tooltip">رفتن به صفحه</span>
            </a>
        </div>
    </div>

<?php
}