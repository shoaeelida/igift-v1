<?php

if( $archive_product_id && get_post_status ( $archive_product_id ) == 'publish' ){

    $product_obj = get_product_obj( $archive_product_id );
    if ( $product_obj['type'] == 'giftcard' ) { ?>

        <a href="<?php echo $product_obj['url']; ?>" title="<?php echo $product_obj['title']; ?>" class="product-item">
            <div class="image-holder">
                <?php 
                if( $product_obj['special'] ){ ?>
                    <span class="special-tag">
                        <svg viewBox="0 0 384 512">
                            <use xlink:href="<?php echo sprite_url; ?>#fire"></use>
                        </svg>
                        ویژه
                    </span>
                <?php } ?>
                <?php if( $product_obj['pre_order'] ){ ?>
                    <span class="special-tag preorder-tag">
                        <svg viewBox="0 0 384 512">
                            <use xlink:href="<?php echo sprite_url; ?>#fire"></use>
                        </svg>
                        پیش خرید
                    </span>
                <?php } ?>
                <?php echo play_render_image($product_obj['thumbnail_url'],$product_obj['title']) ?>
            </div>
            <div class="product-title">
                <?php echo $product_obj['title']; ?>
            </div>
            <div class="product-bottom">
                <div class="product-card-price product-card-price-from">
                    <?php 
                    // if( $product_obj['sale_price'] && $product_obj['sale_percent'] ){
                    //     echo '<div class="original-price__holder">';
                    //         echo '<span class="sale-range">%'.$product_obj['sale_percent'].'</span>';
                    //         echo '<span class="original-price">'.$product_obj['original_price'].'</span>';
                    //     echo '</div>';
                    //     echo '<span class="sale-price">'.$product_obj['sale_price'].'</span>';
                    // }else{
                    //     echo '<span class="sale-price">'.$product_obj['price_html'].'</span>';
                    // }
                    if( isset($product_obj['min_price']) && !empty($product_obj['min_price']) ){
                        echo 'شروع قیمت از<span class="sale-price"> '.play_price($product_obj['min_price']).'</span>';
                    }
                    ?>
                </div>
                <button class="add-to-cart btn btn--circle btn--transparent">
                    +
                </button>
            </div>
        </a>

    <?php }else{ 
        
        $product_url = $product_obj['url'];

        $price = $product_obj['price'];
        $original_price = $product_obj['original_price_raw'];
        $sale_percent = $product_obj['sale_percent'];
        $sale_price = $product_obj['sale_price_raw'];

        if( isset($main_platform) && !empty($main_platform) ){
            $product_url .= '?platform='.$main_platform['slug'];
            if( isset($product_obj['platform_prices'][$main_platform['slug']]) ){
                $price = $product_obj['platform_prices'][$main_platform['slug']]['price'];
                $original_price = $product_obj['platform_prices'][$main_platform['slug']]['original_price_raw'];
                $sale_percent = $product_obj['platform_prices'][$main_platform['slug']]['sale_percent'];
                $sale_price = $product_obj['platform_prices'][$main_platform['slug']]['sale_price_raw'];
            }
        }

        if( !isset($main_platform) && empty($main_platform) ){
            $main_platform = $product_obj['main_product_platform'];
        }

        ?>

        <a href="<?php echo $product_url; ?>" title="<?php echo $product_obj['title']; ?>" class="product-item">
            <div class="image-holder">
                <?php if( $product_obj['special'] ){ ?>
                    <span class="special-tag">
                        <svg viewBox="0 0 384 512">
                            <use xlink:href="<?php echo sprite_url; ?>#fire"></use>
                        </svg>
                        ویژه
                    </span>
                <?php } ?>
                <?php if( $product_obj['pre_order'] ){ ?>
                    <span class="special-tag preorder-tag">
                        <svg viewBox="0 0 384 512">
                            <use xlink:href="<?php echo sprite_url; ?>#fire"></use>
                        </svg>
                        پیش خرید
                    </span>
                <?php } ?>
                <?php echo play_render_image($product_obj['thumbnail_url'],$product_obj['title']) ?>
            </div>
            <div class="product-title">
                <?php echo $product_obj['name']; ?>
            </div>
            <div class="product-bottom">
                <div class="product-card-price">
                    <?php 
                    if( $sale_price && $sale_percent ){
                        echo 'شروع قیمت از';
                        echo '<span class="sale-price">'.play_price($sale_price).'</span>';
                    }else{
                        echo 'شروع قیمت از';
                        echo '<span class="sale-price">'.play_price($price).'</span>';
                    }
                    ?>
                </div>
                <button class="add-to-cart btn btn--circle btn--transparent">
                    +
                </button>
            </div>
        </a>

    <?php }
    
}