<div class="afterpayment-content auto-redirect" data-url="<?php echo site_url; ?>">

    <div class="alert-box alert-box-failed">

        <div class="icon">
            <svg viewBox="0 0 128 128">
                <use xlink:href="<?php echo sprite_url; ?>#error"></use>
            </svg>
        </div>
        <h2>خطای 404</h2>
        <span>صفحه مورد نظر شما پیدا نشد</span>
        <div class="buttons">
            <a class="btn btn--red btn--large btn--text ml-3 btn--auto-redirect" href="<?php echo site_url; ?>" title="<?php echo site_title; ?>">
                صفحه اصلی
                <span class="redirect-seconds"></span>
                <svg viewBox="0 0 12.56 19.46">
                    <use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
                </svg>
            </a>
        </div>
    </div>

</div>