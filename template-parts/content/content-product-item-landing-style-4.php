<?php
$product_obj = get_product_obj( $product_id );
if( $product_obj ){ ?>

    <div class="slider-item">
        <?php echo play_render_image($product_obj['thumbnail_url'],$product_obj['title']); ?>
        <div class="product-bottom">
            <a href="<?php echo $product_obj['url'] ?>" title="<?php echo $product_obj['title'] ?>" style="flex: 0 0 100%">
                <h2 class="product-title">
                    <?php echo $product_obj['name']; ?>
                </h2>
            </a>
            <div class="product-card-price">
                <?php
                if( $product_obj['sale_price_raw'] && $product_obj['sale_percent'] ){
                    echo '<div class="original-price__holder">';
                    echo '<span class="sale-range">%'.$product_obj['sale_percent'].'</span>';
                    echo '<span class="original-price">'.play_price($product_obj['original_price_raw']).'</span>';
                    echo '</div>';
                    echo '<span class="sale-price">'.play_price($product_obj['sale_price_raw']).'</span>';
                }else{
                    echo '<span class="sale-price">'.play_price($product_obj['price']).'</span>';
                }
                ?>
            </div>
            <a href="<?php echo $product_obj['url'] ?>" title="<?php echo $product_obj['title'] ?>" class="add-to-basket">
                <svg class="icon">
                    <use xlink:href="<?php echo $landing_sprite; ?>#x"></use>
                </svg>
            </a>
        </div>
    </div>

<?php }
