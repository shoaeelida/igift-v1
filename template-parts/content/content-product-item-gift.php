<?php

if( $product_id && get_post_status ( $product_id ) == 'publish' ){

    $gift_product_obj = get_product_obj( $product_id );

    ?>

    <a href="<?php echo $gift_product_obj['url']; ?>" title="<?php echo $gift_product_obj['title']; ?>" class="product-card white normal-size">
        <div class="image-holder">
            <?php /*if( $gift_product_obj['special'] ){ ?>
                <span class="special-tag">
                    <svg viewBox="0 0 384 512">
                        <use xlink:href="<?php echo sprite_url; ?>#fire"></use>
                    </svg>
                    ویژه
                </span>
            <?php }*/ ?>
            <div class="hole"> 
            </div>
            <?php echo play_render_image($gift_product_obj['thumbnail_url'],$gift_product_obj['title']); ?>
        </div>
        <div class="product-title"><?php echo $gift_product_obj['title']; ?></div>
        <div class="product-bottom">
            <div class="product-card-price product-card-price-from">
                <?php 
                // if( $gift_product_obj['sale_price'] && $gift_product_obj['sale_percent'] ){
                //     echo '<div class="original-price__holder">';
                //         echo '<span class="sale-range">%'.$gift_product_obj['sale_percent'].'</span>';
                //         echo '<span class="original-price">'.$gift_product_obj['original_price'].'</span>';
                //     echo '</div>';
                //     echo '<span class="sale-price">'.$gift_product_obj['sale_price'].'</span>';
                // }else{
                //     echo '<span class="sale-price">'.$gift_product_obj['price_html'].'</span>';
                // }
                if( isset($gift_product_obj['min_price']) && !empty($gift_product_obj['min_price']) ){
                    echo 'شروع قیمت از<span class="sale-price"> '.play_price($gift_product_obj['min_price']).'</span>';
                }
                ?>
            </div>
            <button class="add-to-cart btn btn--circle btn--transparent">
                <svg viewBox="0 0 10.38 10.38">
                    <use xlink:href="<?php echo sprite_url ?>#plusIcon"></use>
                </svg>
            </button>
        </div>
    </a>

<?php
}