<?php

if( $product_id && get_post_status ( $product_id ) == 'publish' ){

    $product_obj = get_product_obj( $product_id );
    $product_url = $product_obj['url'];

    $price = $product_obj['price'];
    $original_price = $product_obj['original_price_raw'];
    $sale_percent = $product_obj['sale_percent'];
    $sale_price = $product_obj['sale_price_raw'];

    if( isset($main_platform) && !empty($main_platform) ){
        $product_url .= '?platform='.$main_platform['slug'];
        if( isset($product_obj['platform_prices'][$main_platform['slug']]) ){
            $price = $product_obj['platform_prices'][$main_platform['slug']]['price'];
            $original_price = $product_obj['platform_prices'][$main_platform['slug']]['original_price_raw'];
            $sale_percent = $product_obj['platform_prices'][$main_platform['slug']]['sale_percent'];
            $sale_price = $product_obj['platform_prices'][$main_platform['slug']]['sale_price_raw'];
        }
    }

    ?>

    <div class="product-item">
        <a href="<?php echo $product_url; ?>" title="<?php echo $product_obj['title']; ?>">
            <div class="image-holder">
                <?php echo play_render_image($product_obj['thumbnail_url'],$product_obj['title']); ?>
                <div class="product-info">
                    <?php 
                    if( isset($main_platform) && $main_platform ){
                        echo '<div class="product-platform">';
                            echo (isset($main_platform['icon_hover'])) ? '<img src="'.$main_platform['icon_hover'].'" alt="'.$main_platform['name'].'" >' : '';
                            echo (isset($main_platform['name'])) ? '<span>'.$main_platform['name'].'</span>' : '';
                        echo ' </div>';
                    }elseif( !empty($product_obj['main_product_platform']) ){
                        echo '<div class="product-platform">';
                            echo (isset($product_obj['main_product_platform']['icon_hover'])) ? '<img src="'.$product_obj['main_product_platform']['icon_hover'].'" alt="'.$product_obj['main_product_platform']['name'].'" >' : '';
                            echo (isset($product_obj['main_product_platform']['name'])) ? '<span>'.$product_obj['main_product_platform']['name'].'<span>' : '';
                        echo ' </div>';
                    } ?>
                </div>
            </div>
        </a>
        <div class="product-bottom">
            <a style="display: block;width: 100%;" href="<?php echo $product_obj['url']; ?>" title="<?php echo $product_obj['title']; ?>">
                <h2 class="product-title">
                    <?php echo $product_obj['name']; ?>
                </h2>
            </a>
            <div class="product-card-price">
                <?php 
                if( $sale_price && $sale_percent ){
                    echo '<div class="original-price__holder">';
                        echo '<span class="sale-range">%'.$sale_percent.'</span>';
                        echo '<span class="original-price">'.play_price($original_price).'</span>';
                    echo '</div>';
                    echo '<span class="sale-price">'.play_price($sale_price).'</span>';
                }else{
                    echo '<span class="sale-price">'.play_price($price).'</span>';
                }
                ?>
            </div>
            <a href="<?php echo $product_obj['url']; ?>" title="<?php echo $product_obj['title']; ?>" class="add-to-basket">
                <svg class="icon">
                    <use xlink:href="<?php echo $landing_sprite; ?>#x"></use>
                </svg>
            </a>
        </div>
        </a>
    </div>

<?php
}