<?php $product_obj = get_product_obj( $product_id );
if( $product_obj ){ ?>
    <div class="product-item">
        <a href="<?php echo $product_obj['url'] ?>" title="<?php echo $product_obj['name']; ?>">
            <div class="image-holder">
                <?php if( $product_obj['special'] ){ ?>
                    <span class="special-tag" style="z-index:1;">
                            <svg viewBox="0 0 384 512">
                                <use xlink:href="<?php echo $landing_sprite; ?>#fire"></use>
                            </svg>
                            ویژه
                        </span>
                <?php } ?>
                <?php if( $product_obj['pre_order'] ){ ?>
                    <span class="special-tag preorder-tag" style="z-index:1;">
                            <svg viewBox="0 0 384 512">
                                <use xlink:href="<?php echo $landing_sprite; ?>#fire"></use>
                            </svg>
                            پیش خرید
                        </span>
                <?php } ?>
                <?php echo play_render_image($product_obj['thumbnail_url'],$product_obj['title']); ?>
            </div>
        </a>
        <div class="product-bottom">
            <?php if ($expire_duration = get_field('expire_duration', $product_id)) { ?>
                <span class="duration">
                <?php echo $expire_duration ?>
                </span>
            <?php } ?>
            <a style="width: 100%;" href="<?php echo $product_obj['url'] ?>" title="<?php echo $product_obj['title'] ?>">
                <h2 class="product-title">
                    <?php echo $product_obj['name']; ?>
                </h2>
            </a>
            <div class="product-card-price">
                <?php
                if( $product_obj['sale_price_raw'] && $product_obj['sale_percent'] ){
                    echo '<div class="original-price__holder">';
                    echo '<span class="sale-range">%'.$product_obj['sale_percent'].'</span>';
                    echo '<span class="original-price">'.play_price($product_obj['original_price_raw']).'</span>';
                    echo '</div>';
                    echo '<span class="sale-price">'.play_price($product_obj['sale_price_raw']).'</span>';
                }else{
                    echo '<span class="sale-price">'.play_price($product_obj['price']).'</span>';
                }
                ?>
            </div>
            <a  href="<?php echo $product_obj['url']; ?>" title="<?php echo $product_obj['title']; ?>" class="add-to-basket">
                <svg class="icon">
                    <use xlink:href="<?php echo $landing_sprite; ?>#x"></use>
                </svg>
            </a>
        </div>
        </a>
    </div>

<?php
}
