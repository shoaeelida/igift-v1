<?php

$product_id = get_the_ID();

if( $product_id && get_post_status ( $product_id ) == 'publish' ){

    $product_obj = get_product_obj( $product_id );

    ?>

    <div class="product-card normal-size white">
        <div class="image-holder">
            <a href="<?php echo $product_obj['url']; ?>" title="<?php echo $product_obj['title']; ?>">
                <?php echo play_render_image($product_obj['thumbnail_url'],$product_obj['title']); ?>
            </a>
        </div>
        <div class="product-title">
            <a href="<?php echo $product_obj['url']; ?>" title="<?php echo $product_obj['title']; ?>">
                <?php echo $product_obj['title']; ?>
            </a>
        </div>
        <div class="product-bottom">
            <div class="product-card-price">
                <?php
                /**
                * Get the favorite button for a specified post
                * Post ID not required if inside the loop
                * @param $post_id int, defaults to current post
                * @param $site_id int, defaults to current blog/site
                */
                $favorite_btn = get_favorites_button($product_obj['id']);
                // add btn class
                $favorite_btn = str_replace('simplefavorite-button', 'simplefavorite-button delete',$favorite_btn);
                echo $favorite_btn;
                $type_name = 'محصول';
                switch ($product_obj['type']) {
                    case 'giftcard':
                        $type_name = 'گیفت کارت';
                        break;
                    case 'game':
                        $type_name = 'بازی';
                        break;
                    case 'dlc':
                        $type_name = 'پک';
                        break;
                    default:
                        break;
                }
                ?>
                <a class="enter" href="<?php echo $product_obj['url']; ?>" title="<?php echo $product_obj['title']; ?>">
                    <svg viewBox="0 0 77.34 77.34"><use xlink:href="<?php echo sprite_url; ?>#flash"></use></svg>
                    صفحه <?php echo $type_name; ?>
                </a>
            </div>
        </div>
    </div>

<?php
}