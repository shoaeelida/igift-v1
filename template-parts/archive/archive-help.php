
<?php get_header(); ?>

<div class="mainHeader mainHeader-help">
    <div class="container">
        <h1 class="title">راهنمای <span class="white">خرید</span></h1>
    </div>
</div>

<?php get_template_part( 'template-parts/parts/help', 'nav' ); ?>

<div class="help" id="help-box">
    <div class="container">
        <div class="questions">
            <?php
            $schema_arr = [];
            $page = (get_query_var( 'paged', 1 ));
            $posts_per_page = (get_query_var( 'posts_per_page', 1 ));
            $count = ( $page !== 0 ) ? ( $page * $posts_per_page ) - $posts_per_page  : 0;
            if( have_posts() ) {
                while ( have_posts() ) {
                    the_post(); 
                    $count++;
                    array_push($schema_arr,'{ "@type": "Question", "name": "'.get_the_title().'", "acceptedAnswer": { "@type": "Answer","text": "'.get_the_excerpt().'"}}')
                    ?>
                    <div class="question">
                        <a class="item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" data-question="<?php the_ID(); ?>">
                            <span class="num"><?php echo $count; ?></span>
                            <h2 class="title"><?php the_title(); ?></h2>
                            <svg class="icon" viewBox="0 0 12.56 19.46"><use xlink:href="<?php echo sprite_url; ?>#arrow"></use></svg>
                        </a>
                    </div>
                    <?php
                }
                pagination_bar();
            }
            ?>
        </div>
    </div>
</div>

<?php
get_template_part( 'template-parts/parts/help', 'help' ); ?>

<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "FAQPage",
        "mainEntity": [
    <?php
    $numItems = count($schema_arr);
    $i = 0;
    foreach ($schema_arr as $key => $item) {
        if(++$i === $numItems) {
            echo $item;
        } else {
            echo $item . ',';
        }
    }
    ?>
    ]}
</script>
<?php get_footer(); ?>