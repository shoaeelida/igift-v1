<?php 

global $wp_query;

$platform_tax = null;
$show_cats = false;
$for_term = 'all';
$term_obj = get_queried_object();
$game_archive = false;
$giftcard_archive = false;
$results_count = $wp_query->found_posts; 
$max_num_pages = $wp_query->max_num_pages;
$next_posts_page_link = ($results_count > 0 && $max_num_pages > 1 ) ? get_next_posts_page_link($max_num_pages) : '';

if( is_tax() ){
    $for_term = get_field('for', $term_obj);
    $for_term = (!$for_term) ? 'all' : $for_term;
}

if( is_post_type_archive( 'game' ) || is_tax( 'game_platform' ) ){
    $platform_tax = 'game_platform';
    $show_cats = true;
}

if( is_post_type_archive( 'dlc' ) || is_tax( 'dlc_platform' ) ){
    $platform_tax = 'dlc_platform';
    $show_cats = true;
}

if( is_post_type_archive( 'game' ) || is_tax( 'game_platform' ) || $for_term == 'game' || is_post_type_archive( 'dlc' ) || is_tax( 'dlc_platform' ) || $for_term == 'dlc' ){
    $game_archive = true;
}

if( is_post_type_archive( 'giftcard' ) || $for_term == 'giftcard' ){
    $giftcard_archive = true;
}

if( $giftcard_archive ){
    $show_cats = false;
}

// if( isset( $_GET['debug'] ) ){
//     var_dump( is_tax( 'genres' ) );
//     die();
// }

if( is_tax( 'genres' ) && !$giftcard_archive ){
    $show_cats = true;
}

// if this is a DLC archive of a specific game
//if( get_query_var( 'relatedGame' ) ){
//    $args = array(
//        'post_type' => 'dlc',
//        'post_status' => 'publish',
//        'posts_per_page' => -1,
//    );
//    $loop = new WP_Query( $args );
//    $results_count = $loop->found_posts;
//
//    global $wp;
//    $current_url = home_url( add_query_arg( array(), $wp->request ) );
//    $related_game = substr($current_url, strrpos($current_url, '/' )+1)."\n";
//}


$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
$regions = get_terms( 'regions', array('hide_empty' => true) );
$platforms = ($platform_tax) ? get_terms( $platform_tax, array('hide_empty' => true) ) : null;

$cats = null;
$parent_archive = null;

if ( is_tax('game_platform') ) {
    $parent_archive = get_post_type_archive_link( 'game' );
    if( $term ){
        $platform = [
            'name' => '',
            'slug' => '',
            'icon' => null,
            'icon_hover' => null,
        ];
        $platform_icon_id = get_field('icon', $term);
        $platform_icon_hover_id = get_field('icon_hover', $term);
        $platform['name'] = $term->name;
        $platform['slug'] = $term->slug;
        if($platform_icon_id){
            $icon = wp_get_attachment_image_src($platform_icon_id,'full');
            $platform['icon'] = ( $icon ) ? $icon[0] : null;
        }
        if($platform_icon_hover_id){
            $icon_hover = wp_get_attachment_image_src($platform_icon_hover_id,'full');
            $platform['icon_hover'] = ( $icon_hover ) ? $icon_hover[0] : '';
        }
        set_query_var( 'main_platform', $platform );
    }
}

// if( 
//     is_tax('game_cat') ||
//     is_post_type_archive( 'game' )
// ) {
//     $game_cats = get_terms( 'game_cat', array('hide_empty' => true) );
// }

$cats = get_terms( 'genres', array('hide_empty' => true) );


// price
$min_price = get_min_max_meta_value('min','products_minprice',$wp_query->request);
$max_price = get_min_max_meta_value('max','products_maxprice',$wp_query->request);

if( $min_price == $max_price ){
    $max_price = $max_price + 1000; 
}

$min_price_val = ( isset($_GET['min_price']) && (int)$_GET['min_price'] >= $min_price ) ? (int)$_GET['min_price'] : $min_price;
$max_price_val = ( isset($_GET['max_price']) && (int)$_GET['max_price'] <= $max_price ) ? (int)$_GET['max_price'] : $max_price;

$order_by = 'all';
$available_order_bys = ['all','date','popularity','price'];
if( isset($_GET['order_by']) && in_array($_GET['order_by'],$available_order_bys)) {
    $order_by = $_GET['order_by'];
}

?>
 
<?php get_header(); ?>

<div class="rout-holder">
    <div class="largContainer">
        <div class="product-rout">
        <?php breadcrumbs( ); ?>
        <span class="result-count">
            <b id="result-count"><?php echo $results_count; ?></b>
            نتیجه
        </span>
        </div>
    </div>
</div>

<div id="archive-content__holder">
    <div class="archive-content">
        <div class="filter-section-bg"></div>
        <div class="filter-section">
            <button class="close-filter">
                <svg viewBox="0 0 75.29 75.29">
                    <use xlink:href="<?php echo sprite_url; ?>#close"></use>
                </svg>
            </button>
            <?php if( $show_cats && ( $parent_archive ||  $cats ) ){ ?>
                <div class="filter-box">
                    <div class="filter-row flex-row">
                        <?php echo ( $cats ) ? '<span>طبقه بندی ها</span>' : ''; ?>
                    </div>
                    <?php if( $cats ){ 
                        foreach( $cats as $cat ){
                            echo '<a class="filter-row" href="'.get_term_link( $cat->term_id, 'genres' ).'" title="'.$cat->name.'">'.$cat->name.'</a>';
                        } 
                    } ?>
                </div>
            <?php } ?>
            <div class="filter-box">
                <div class="filter-row flex-row">
                    فیلتر کردن بر اساس
                    <button class="delete-filter" id="delete-filters" style="display:none;">
                        <svg viewBox="0 0 75.29 75.29">
                            <use xlink:href="<?php echo sprite_url; ?>#close"></use>
                        </svg>
                        حذف
                    </button>
                </div>
                <?php /* if( !$giftcard_archive ){  ?>
                    <div class="expandable open">
                        <div class="filter-row toggle-title">
                            <svg viewBox="0 0 12.56 19.46">
                                <use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
                            </svg>
                            <h2>قیمت <span class="rate">(تومان)</span></h2>
                        </div>
                            <div style="display: block;" class="filter-row toggle-content">
                                <div class="slider-holder">
                                    <div id="range" data-minprice="<?php echo $min_price; ?>" data-minpriceval="<?php echo $min_price_val; ?>"  data-maxprice="<?php echo $max_price; ?>" data-maxpriceval="<?php echo $max_price_val; ?>"> </div>
                                    <span id="slider-snap-value-lower"></span>
                                    <span id="slider-snap-value-upper"></span>
                                </div>
                                <form class="input-price-holder">
                                    از 
                                    <input type="text" class="mininput" placeholder="<?php echo play_price( $min_price_val ); ?>" >
                                    
                                    تا 
                                    <input type="text" class="maxinput" placeholder="<?php echo play_price( $max_price_val ); ?>" >

                                </form>
                        </div>
                    </div>
                <?php } */ ?>
                <?php if( $platforms ){ 
                    $active_platforms = [];
                    if( is_tax( 'game_platform' ) ){
                        $active_platforms = array( get_queried_object()->slug );
                    }
                    if(isset($_GET[$platform_tax])){
                        $active_platforms = explode(',',$_GET[$platform_tax]);
                    }else{
                        if(!$active_platforms || empty($active_platforms)){
                            $active_platforms = [];
                        }
                    }
                   
                    ?>
                    <div class="expandable open">
                        <div class="filter-row toggle-title">
                            <svg viewBox="0 0 12.56 19.46">
                                <use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
                            </svg>
                            <h2>پلتفرم <span>platform</span></h2>
                        </div>
                        <div class="filter-row toggle-content " style="display: block;" >
                            <?php foreach( $platforms as $platform ){
                                if( in_array($platform->slug,$active_platforms) ){
                                    echo '<input type="checkbox" id="platform-'.$platform->term_id.'" value="'.$platform->slug.'" name="'.$platform_tax.'[]" checked/>';
                                }else{
                                    echo '<input type="checkbox" id="platform-'.$platform->term_id.'" value="'.$platform->slug.'" name="'.$platform_tax.'[]" />';
                                }
                                echo '<label for="platform-'.$platform->term_id.'" >
                                    <div class="check-box"><svg viewBox="0 0 63.23 42.44"><use xlink:href="'.sprite_url.'#checkmark"></use></svg></div>
                                    '.$platform->name.'
                                </label>';
                            } ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if( $regions ){
                    $active_regions = (isset($_GET['regions'])) ? explode(',',$_GET['regions']) : [];
                    ?>
                    <div class="expandable open">
                        <div class="filter-row toggle-title">
                            <svg viewBox="0 0 12.56 19.46">
                                <use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
                            </svg>
                            <h2>ریجن <span>region</span></h2>
                        </div>
                        <div class="filter-row toggle-content " style="display: block;" >
                            <?php foreach( $regions as $region ){
                                $for = get_field('for', $region);
                                $for = (!$for) ? 'all' : $for;
                                $can_echo = true;
                                // var_dump($for_term);
                                // var_dump($for);
                                if( $for !== 'all' ){
                                    switch ($for) {
                                        case 'game':
                                            if( !$game_archive ){
                                                $can_echo = false;
                                            }
                                            break;
                                        case 'giftcard':
                                            if( !$giftcard_archive ){
                                                $can_echo = false;
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                if( $can_echo ){
                                    if( in_array($region->slug,$active_regions) ){
                                        echo '<input type="checkbox" id="region-'.$region->term_id.'" value="'.$region->slug.'" name="regions[]" checked/>';
                                    }else{
                                        echo '<input type="checkbox" id="region-'.$region->term_id.'" value="'.$region->slug.'" name="regions[]" />';
                                    }
                                    echo '<label for="region-'.$region->term_id.'" >
                                        <div class="check-box"><svg viewBox="0 0 63.23 42.44"><use xlink:href="'.sprite_url.'#checkmark"></use></svg></div>
                                        '.$region->name.'
                                    </label>';
                                }
                            } ?>
                        </div>
                    </div>
                <?php } ?>
                
            </div>
        </div>
        <div class="archive-result" style="margin: 0 auto;">
            <div class="sort-wrap" id="orders-filter">
                <div class="result-search">
                    <form>
                        <input name="search" type="text" placeholder="برای جستجو نام محصول رو وارد کن">
                        <svg viewBox="0 0 18.01 18.01">
                            <use xlink:href="<?php echo sprite_url ?>#search1"></use>
                        </svg>
                    </form>
                </div>
                <div class="sortby">
                    <svg class="sort-icon" viewBox="0 0 236.46 200.53">
                        <use xlink:href="<?php echo sprite_url ?>#menu"></use>
                    </svg>
                    مرتب سازی بر اساس
                    <div class="sort-holder">
                        <svg class="arrow" viewBox="0 0 12.56 19.46">
                            <use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
                        </svg>
                        <span class="selected">همه</span>
                        <div class="select-item"> 
                            <input type="radio" id="s1" name="order" value="all" <?php echo ($order_by == 'all') ? 'checked' : ''; ?>>
                            <label class="all-product" for="s1">
                                <svg viewBox="0 0 236.46 200.53">
                                        <use xlink:href="<?php echo sprite_url; ?>#menu"></use>
                                </svg>
                                همه
                            </label>
                            <input type="radio" id="s2" value="date" name="order" <?php echo ($order_by == 'date') ? 'checked' : ''; ?>>
                            <label for="s2">جدیدترین ها</label>
                            <input type="radio" id="s3" value="popularity" name="order" <?php echo ($order_by == 'popularity') ? 'checked' : ''; ?>>
                            <label for="s3">محبوب ترین ها</label>
                            <input type="radio" id="s4" value="price" name="order" <?php echo ($order_by == 'price') ? 'checked' : ''; ?>>
                            <label for="s4">ارزان ترین ها</label>
                        </div>
                    </div>
                    <div class="open-filter">
                        <svg viewBox="0 0 196.67 219.04"><use xlink:href="<?php echo sprite_url; ?>#filter"></use></svg>
                    </div>
                </div>
            </div>
            <div id="product-result-wrapper" class="result-wrapper card-list-holder">
                    <?php
//                    echo '<div style="display: none">'.$GLOBALS['related_game_loop'].'</div>';
//                    if( $GLOBALS['related_game_loop'] ) {
//                        if( $GLOBALS['related_game_loop']->have_posts() ){
//                            while ($GLOBALS['related_game_loop']->have_posts()) {
//                                $GLOBALS['related_game_loop']->the_post();
//                                $product_id = get_the_ID();
//                                set_query_var('product_id', $product_id);
//                                if( $GLOBALS['related_game'] == get_field('related_game')[0] ) {
//                                    get_template_part('template-parts/content/content', 'product-card');
//                                }
//                            }
//                        }
//                    }
//                    else {
//                        if( have_posts() ){
//                            while (have_posts()) {
//                                the_post();
//                                $product_id = get_the_ID();
//                                set_query_var('product_id', $product_id);
//                                get_template_part('template-parts/content/content', 'product-card');
//                            }
//                        }
//                    }
                    if( have_posts() ){
                        while (have_posts()) {
                            the_post();
                            $product_id = get_the_ID();
                            set_query_var('product_id', $product_id);
                            get_template_part('template-parts/content/content', 'product-card');
                        }
                    }
                    ?>
            </div>
            <?php 
            if( $next_posts_page_link && !empty($next_posts_page_link) ){
                echo '<div class="more-product">
                    <a href="#" class="more-product-btn btn btn--transparent btn--small" data-nexturl="'.get_next_posts_page_link($max_num_pages).'">
                        محصولات بیشتر
                    </a>
                </div>';
            }
            ?>
        </div>
    </div>
</div>

<?php

get_template_part( 'template-parts/parts/subscribe' );

get_footer(); ?>