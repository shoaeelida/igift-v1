<?php 

// if( ( $devices = get_transient( 'order_devices' ) ) == false ){
	$devices = get_field('order_devices', 'option'); 
	set_transient( 'order_devices' , $devices, HOUR_IN_SECONDS );
// }

// if( ( $platforms = get_transient( 'order_platforms' ) ) == false ){
	$platforms = get_field('order_platforms', 'option'); 
	set_transient( 'order_platforms' , $platforms, HOUR_IN_SECONDS );
// }

?>

<!-- order modal -->
<div id="ordModal" class="modalOverlay transparent">
	<!-- <a class="cancel"></a> -->

	<div class="modalContent">

		<!-- step 0 -->
		<div class="orderModalContent step-0" id="order-step-0" style="display: none">
		
			<a class="closeModal" href="#">
				<svg viewBox="0 0 75.29 75.29"><use xlink:href="<?php echo sprite_url; ?>#close"></use></svg>
			</a>

			<div class="titleBar">
				<h2 class="title">لینک محصول مورد نظر خود را وارد کنید</h2>
				<!-- <div class="hint">
					<span class="icon">?</span>
					راهنمای ثبت سفارش
				</div> -->
			</div>
			<form class="search-by-url">
				<div class="inputGroup mode2 width_100">
					<input class="transparent left" type="url" placeholder="لینک محصول">
					<svg viewBox="0 0 271.24 271.24"><use xlink:href="<?php echo sprite_url; ?>#attach"></use></svg>
				</div>
				<div class="searchResult" style="display:none;"></div>
				<div class="buttons mt-4 text-center">
					<button type="submit" class="btn btn--red btn--large btn--icon step-0-submit" id="step-0-submit">
						<svg viewBox="0 0 18.01 18.01">
							<use xlink:href="<?php echo sprite_url; ?>#search"></use>
						</svg>
					</button>
				</div>
			</form>
		</div>

		<!-- step 1 -->
		<div class="orderModalContent step-1" id="order-step-1" style="display:none;">
			<a title="بستن" class="closeModal" href="#">
				<svg viewBox="0 0 75.29 75.29"><use xlink:href="<?php echo sprite_url; ?>#close"></use></svg>
			</a>
		
			<!-- <h2 class="title" style="display:none;">لینکی که وارد کرده اید تحت پشتیبانی <svg class="icon" viewBox="0 0 1215.02 364.71"><use xlink:href="<?php echo sprite_url; ?>#igameLogoText"></use></svg> نیست!</h2> -->
			<div class="inputGroup width_100">
				<input class="transparent left" type="text" placeholder="لینک محصول">
				<svg viewBox="0 0 301.81 301.81"><use xlink:href="<?php echo sprite_url; ?>#phone"></use></svg>
			</div>
			<p class="desc">
				<!-- اما --> میتوانید سفارش خود را بصورت دستی ثبت کنید<br>
				تا آی گیفت محصول شما را برایتان خریداری کند
			</p>
			<div class="buttons">
				<a title="ثبت سفارش" class="btn-start btn btn--red btn--large btn--text btn--text--right ml-3 submitBtn">
					ثبت سفارش
					<svg viewBox="0 0 12.56 19.46">
						<use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
					</svg>
				</a>
				<a title="نمیخوام" class="btn-start btn btn--transparent btn--large btn--text closeModalBtn">
					نمیخوام!
				</a>
			</div>
			<div class="text-center mt-4">
				<a title="آی گیفت چه سایتهایی را پشتیبانی میکند؟">آی گیفت چه سایتهایی را پشتیبانی میکند؟</a>
			</div>
		</div>
		
		<?php if( is_user_logged_in() ){ ?>

			<!-- step 2 -->
			<div class="orderModalContent step-2" id="order-step-2" style="display:none;">
				<a title="بستن" class="closeModal" href="#">
					<svg viewBox="0 0 75.29 75.29"><use xlink:href="<?php echo sprite_url; ?>#close"></use></svg>
				</a>
		
				<div class="titleBar">
					<h2 class="title">دستگاه خود را انتخاب کنید</h2>
					<!-- <div class="hint">
						<span class="icon">?</span>
						راهنمای ثبت سفارش
					</div> -->
				</div>
				<div class="deviceSelect">
					<?php
					if( $devices ){
						foreach ( $devices as $device ) {
							$icon = wp_get_attachment_image_src( $device['icon'] );
							$icon = ( $icon ) ? '<img src="'.$icon[0] . '" alt="'.$device['name'].'">' : '';
							echo '<div class="item">
								<input type="radio" name="device" value="'.$device['id'].'" id="device-'.$device['id'].'" hidden>
								<label class="label" for="device-'.$device['id'].'">
									<span class="iconHolder">
										'.$icon.'
									</span>
									<span class="name">'.$device['name'].'</span>
								</label>
							</div>';
						}
					}
					?>
				</div>
                <div class="buttons mt-3 text-center mt-sm-5">
					<a title="تغییر لینک" class="btn-start btn btn--transparent btn--large btn--text btn--text--left ml-3 open-step revert" data-action="show" data-step="1">
						<svg viewBox="0 0 12.56 19.46" stroke="#fff">
							<use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
						</svg>
						تغییر لینک
					</a>
					<a title="ثبت و خرید" class="btn-start btn btn--red btn--large btn--text btn--text--right ml-3 submitBtn">
						ثبت و خرید
						<svg viewBox="0 0 12.56 19.46">
							<use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
						</svg>
					</a>
				</div>
			</div>

			<!-- step 3 -->
			<div class="orderModalContent position-relative step-3" id="order-step-3" style="display:none;">
				<a title="بستن" class="closeModal" href="#">
					<svg viewBox="0 0 75.29 75.29"><use xlink:href="<?php echo sprite_url; ?>#close"></use></svg>
				</a>
				<div class="titleBar">
					<h2 class="title">اطلاعات محصول خود را وارد کنید</h2>
					<!-- <div class="hint">
						<span class="icon">?</span>
						راهنمای ثبت سفارش
					</div> -->
				</div>
				<div class="prdInfoForm width_100">
					<div class="row1">
						<input type="text" class="product-name" placeholder="نام بازی">
						<div class="platform">
							<span class="selected">
								<span class="text">پلتفرم</span>
								<svg class="icon" viewBox="0 0 12.56 19.46"><use xlink:href="<?php echo sprite_url; ?>#arrow"></use></svg>
							</span>
							<div class="select">
								<?php
								foreach ($platforms as $platform) {
									$icon = wp_get_attachment_image_src( $platform['icon'] );
									$icon = ( $icon ) ? '<img src="'.$icon[0] . '" alt="'.$platform['name'].'">' : '';
									echo '<div class="item" data-id="'.$platform['id'].'" data-name="'.$platform['name'].'">
										<span class="persian">'.$icon.' '. $platform['name'] . '</span>
										<span class="english">'.$platform['en_name'].'</span>
									</div>';
								}
								?>
								<div class="item" data-id="others" data-name="سایر پلتفرم ها">
									<div class="text-center width_100 heavy persian">سایر پلتفرم ها</div>
								</div>
							</div>
						</div>
					</div>
					<div class="validation">
						<span class="icon">?</span>
						پلتفرم بازی را انتخاب کنید. <a href="#" title="پلتفرم چیه؟">پلتفرم چیه؟</a>
					</div>
					<div class="accRow platform-data" style="display:none;">
						<input type="text" class="platform_username" placeholder="نام کاربری">
						<input type="password" class="platform_password" placeholder="کلمه عبور">
					</div>
					<div class="priceRow">
						<div class="inputGroup">
							<input type="text" class="input-price" placeholder="قیمت اصلی بازی">
							<!-- currency select -->
							<div class="select currSelect" data-type="USD">
								<span class="selected">
									<svg class="icon" viewBox="0 0 12.56 19.46"><use xlink:href="<?php echo sprite_url; ?>#arrow"></use></svg>
									<span class="text">دلار</span>
								</span>
								<div class="popUp">
									<div class="item" data-type="USD">دلار</div>
									<div class="item" data-type="EUR">یورو</div>
								</div>
							</div>
						</div>
						<div class="converted">
							= <span class="bold converted-price">0</span>
							<span class="unit">تومان</span>
						</div>
					</div>
					<div class="validation">
						<span class="icon">?</span>
						قیمت بازی را به ارز اصلی وارد کنید<a href="#" title="آی گیفت قیمت را چطور محاسبه میکند؟">آی گیفت قیمت را چطور محاسبه میکند؟</a>
						<!-- <a class="action">مشاهده ریز هزینه ها</a> -->
						<div class="clearfix"></div>
					</div>
					<textarea class="width_100 order-desc" placeholder="توضیحات" rows="4"></textarea>
				</div>
				<div class="buttons mt-5 text-center">
					<a title="تغییر لینک" class="btn-start btn btn--transparent btn--large btn--text btn--text--left ml-3 open-step revert" data-action="show" data-step="1">
						<svg viewBox="0 0 12.56 19.46" stroke="#fff">
							<use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
						</svg>
						تغییر لینک
					</a>
					<a title="ثبت و خرید" class="btn-start btn btn--red btn--large btn--text btn--text--right ml-3 submitBtn">
						ثبت و خرید
						<svg viewBox="0 0 12.56 19.46">
							<use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
						</svg>
					</a>
				</div>

				<!-- existing product box -->
				<!-- <div class="existing">
					<header class="header">
						<div class="title">
							<span class="iconHolder">
								<span class="icon">!</span>
							</span>
							موجود است!
						</div>
						<svg class="closeIcon" viewBox="0 0 75.29 75.29"><use xlink:href="<?php echo sprite_url; ?>#close"></use></svg>
					</header>
					<div class="item">
						<img class="image" src="<?php echo theme_assets; ?>/imgs/dynamicImg/battleFront.jpg" alt="">
						<div class="p-xl-3">
							<h2 class="title">Star Wars Battlefront 2 Origin Key GLOBAL</h2>
							<div class="priceBox">
								<span class="discount">35%</span>
								<div class="price">
									430,000
									<span class="unit">تومان</span>
								</div>
							</div>
							<a class="buy btn btn--large width_100 btn--transparent mt-4 text-center p-2">
								<span class="plus">+</span>
								خرید
							</a>
						</div>
					</div>
				</div> -->

				<!-- price details box -->
				<!-- <div class="priceDetailsBox">
					<div class="header">
						<svg class="closeIcon" viewBox="0 0 75.29 75.29"><use xlink:href="<?php echo sprite_url; ?>#close"></use></svg>
					</div>
					<div class="content">
						<div class="prdPrice">
							<div class="pb-3">قیمت اصلی بازی</div>
							<div class="text-left">
								<span class="price dollarPrice">$24</span>
								<span class="price tomanPrice">254,900</span>
								<span>تومان</span>
							</div>
						</div>
						<div class="wage">
							<span>کارمزد آی گیفت</span>
							<span>
								<span class="price">5,000</span>
								تومان
							</span>
						</div>
						<div class="accountPrice">
							<span>هزینه ثبت اکانت</span>
							<span>
								<span class="price">10,000</span>
								تومان
							</span>
						</div>
						<div class="total">
							<span>مجموع</span>
							<span>
								<span class="price">270,000</span>
								تومان
							</span>
						</div>
					</div>
				</div> -->
			</div>

			<!-- step 4 -->
			<div class="orderModalContent position-relative step-4" id="order-step-4" style="display:none;"></div>

			<!-- step 6 -->
			<div class="orderModalContent step-6" id="order-step-6" style="display:none;">
                <img width="150px" src="<?php echo theme_assets; ?>/imgs/payLoading.svg" alt="loading">
                <h3 class="title">لطفا کمی صبر کنید</h3>
                <p>در حال انتقال به درگاه بانکی ...</p>
            </div>

		<?php } ?>

	</div>
</div>

<?php if( !is_user_logged_in() ){ ?>

	<div id="regModal" class="modalOverlay">
		<a title="انصراف" class="cancel" href="#"></a>
		<div class="modalContent">
			<div class="regModalContent">
				<h2 class="title">واسه ثبت سفـارش اول بایـد تو سایـت ثبت نـام کنـی!</h2>
				<h3 class="subTitle">اول تو سایت ثبت نام کن و بعد دوباره این صفحه رو باز کن</h3>
				<a title="ثبت نام در آی گیفت" href="?login_modal=true" class="link digits-login-modal" onclick="jQuery('this').digits_login_modal(jQuery(this));return false;" attr-disclick="1" type="1">
					<svg viewBox="0 0 10.38 10.38"><use xlink:href="<?php echo sprite_url; ?>#add"></use></svg>
					ثبت نام در آی گیفت
				</a>
				<a href="?login_modal=true" class="bold digits-login-modal" onclick="jQuery('this').digits_login_modal(jQuery(this));return false;" attr-disclick="1" type="1">قبلا ثبت نام کردی ؟ از اینجا وارد شو</a>
				<img class="circ1 lazy" data-src="<?php echo theme_assets; ?>/imgs/circle1.svg" alt="">
				<img class="circ2 lazy" data-src="<?php echo theme_assets; ?>/imgs/circle1.svg" alt="">
				<img class="circ3 lazy" data-src="<?php echo theme_assets; ?>/imgs/circle2.svg" alt="">
			</div>
		</div>
	</div>

<?php } ?>