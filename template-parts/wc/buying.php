
<?php

if ( ! defined('ABSPATH')) exit();

get_header();

$t = time();

?>

<div class="timer-content">
    <div class="counter-content">
        <img class="timer-icon" src="<?php echo theme_assets; ?>/imgs/timer.svg" alt="">
        <div class="counter-holder" id="buying-counter-holder" style="display: none;">
            <span class="seconds">00</span>
            <b>:</b>
            <span class="minutes">00</span>
        </div>
        <h2 class="wait">لطفا کمـی صبـر کنیـد</h2>
        <span class="tip">درحال خرید بازی برای شما هستیم ...</span>
    </div>
    <div class="return-link-content">
        <span class="desc">
            میتونی این صفحه رو ببندی و ما بعد از خرید از طریق پیامک بهت اطلاع
            بدیم.
        </span>
        <ul class="route">
            <li><a href="<?php echo wc_get_account_endpoint_url('dashboard'); ?>">پنل کاربری </a></li>
            <li><a href="<?php echo wc_get_account_endpoint_url('orders'); ?>">لیست سفارشات</a></li>
        </ul>
        <!-- <a href="#" class="btn btn--transparent btn--text--right btn--small">
            بازگشت
            <svg viewBox="0 0 12.56 19.46">
            <use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
            </svg>
        </a> -->
    </div>
</div>


<?php

get_template_part( 'template-parts/footer/footer', 'foot' ); 