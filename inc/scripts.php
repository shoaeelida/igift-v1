<?php


function app_scripts() {

    if( is_admin() ){
        return;
    }

    $logged_in = is_user_logged_in();
    $theme = wp_get_theme();
    $theme_ver = $theme->get( 'Version' );

    $show_admin_bar = false;
    
    global $playOptions;

    if( !$logged_in ){
        wp_deregister_style( 'simple-favorites' );
        wp_deregister_script( 'favorites' );
    }else{
        $user = wp_get_current_user();
        $allowed_roles = array( 'editor', 'administrator', 'author' );
        if ( array_intersect( $allowed_roles, $user->roles ) ) {
            $show_admin_bar = is_admin_bar_showing();
        }
    }

    if( !$show_admin_bar ){
        wp_deregister_style( 'dashicons' );
    }

    wp_deregister_script( 'wc-address-i18n' );
    wp_deregister_script( 'wc-country-select' );
    wp_deregister_script( 'zoom' );
    wp_deregister_script( 'wc-cart-fragments' );
    wp_deregister_style( 'wc-cart-fragments' );

    wp_deregister_style( 'woocommerce-layout' );
    wp_deregister_style( 'woocommerce-smallscreen' );
    wp_deregister_style( 'woocommerce-general' );
    wp_deregister_style( 'woocommerce_prettyPhoto_css' );
    wp_deregister_style( 'tf-compiled-options-play' );
    wp_deregister_script( 'prettyPhoto' );
    wp_deregister_script( 'flexslider' );
    wp_deregister_script( 'wc-add-to-cart' );
    
    wp_deregister_style( 'wc-block-style' );
    wp_deregister_style( 'yith-wcwl-font-awesome' );
    
    wp_deregister_script( 'woocommerce' );
    wp_deregister_script( 'jquery-blockui' );
    
    if( !is_page() || !is_page_template( 'page-contact.php' ) ){
        wp_deregister_script( 'jquery-form' );
        wp_deregister_script( 'contact-form-7' );
        wp_deregister_style( 'contact-form-7' );
        wp_deregister_style( 'contact-form-7-rtl' );
    }

    wp_deregister_style('nice-select');
    wp_deregister_style( 'google-roboto-regular' );

    wp_dequeue_style( 'wp-block-library' );
    wp_deregister_style( 'wp-block-library' );
    wp_dequeue_style( 'wp-block-library-rtl' );
    wp_deregister_style( 'wp-block-library-rtl' );

    wp_dequeue_style( 'newsletter' );
    wp_deregister_style( 'newsletter' );
    wp_deregister_script('newsletter-subscription');

    wp_dequeue_style('al_ic_revs_styles');
    wp_deregister_script('al_ic_revs_scripts');
    
    wp_enqueue_style( 'plugins', theme_assets . '/styles/plugins.min.css', null,$theme_ver );
    wp_enqueue_style( 'style', theme_assets . '/styles/screen.min.css', null,$theme_ver );

    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', theme_assets . '/scripts/jquery-3.4.1.min.js', array(), $theme_ver, true);

    // if (is_singular('game')) {
    //     wp_register_script('sweetalert', theme_assets . '/scripts/sweetalert2.js', array('jquery'), $theme_ver, true);
    //     wp_enqueue_script('sweetalert');
    // }

    if( is_account_page() ){
        wp_enqueue_style( 'emojionearea.min', theme_assets . '/styles/emojionearea.min.css', null, null );
        wp_register_script('emojionearea.min', theme_assets . '/scripts/emojionearea.min.js', array('jquery'), false, true );
        wp_enqueue_script('emojionearea.min');
    }

    wp_register_script('plugins', theme_assets . '/scripts/plugins.js', array('jquery'), $theme_ver, true );
    wp_enqueue_script('plugins');

    wp_register_script('app', theme_assets . '/scripts/app.min.js', array('jquery'), $theme_ver, true );
    wp_enqueue_script('app');

    $currencies_fee = (float)$playOptions->getOption( 'currencies_fee' );
    $currencies_fee = ($currencies_fee) ? $currencies_fee : 0;
    $dollar_price = 0;
    $eur_price = 0;
    
    if( !empty($playOptions->getOption( 'custom_dollar_price' )) ){
        $dollar_price = (float)$playOptions->getOption( 'custom_dollar_price' );
    }else{
        $dollar_price = (float)$playOptions->getOption( 'dollar_price' );
    }

    if( !empty($playOptions->getOption( 'custom_eur_price' )) ){
        $eur_price = (float)$playOptions->getOption( 'custom_eur_price' );
    }else{
        $eur_price = (float)$playOptions->getOption( 'eur_price' );
    }

    $app_config = array(
        'ip' => getUserIpAddr(),
        'site_url' => site_url,
        'admin_ajax' => admin_url( 'admin-ajax.php' ),
        'sprite' => sprite_url,
        'checkout_url' => wc_get_checkout_url(),
        'api_url' => get_rest_url().'play',
        'auth_url' => modal_login_link(true),
        'base_price_USD' => ( $dollar_price ) ? $dollar_price + $currencies_fee : 0,
        'base_price_EUR' => ( $eur_price ) ? $eur_price + $currencies_fee : 0,
        'sale_fee_percent' => ( $playOptions ) ? (float)$playOptions->getOption( 'sale_fee_percent' ) : 0,
    );
    if( $logged_in ){
        $app_config['show_login'] = false;
        $app_config['is_logged_in'] = true;
        $app_config['user_id'] = get_current_user_id();
        $app_config['token'] = wp_get_session_token();
    }else{
        $app_config['is_logged_in'] = false;
        $app_config['show_login'] = false;
        // $app_config['show_login'] = ( isset($_GET['login_modal']) && $_GET['login_modal'] == 'true' ) ? true : false;
    }

    wp_localize_script( 'app', 'app_config', $app_config);

}
add_action( 'wp_enqueue_scripts', 'app_scripts' , 9999 );

function sdt_remove_ver_css_js( $src, $handle ) 
{
    $handles_with_version = [ 'style' ]; // <-- Adjust to your needs!

    $exclude = array(
        'app',
        'screen',
        'plugins',
    );

    if ( !in_array( $handle, $exclude, true ) && strpos( $src, 'ver=' ) && !in_array( $handle, $handles_with_version, true ) ){
        $src = remove_query_arg( 'ver', $src );
    }

    return $src;
}

add_filter( 'style_loader_src',  'sdt_remove_ver_css_js', 9999, 2 );
add_filter( 'script_loader_src', 'sdt_remove_ver_css_js', 9999, 2 );


function disable_embeds_code_init() {

    if( !is_admin() ){

        remove_action('wp_head', 'wp_generator');
        remove_action( 'wp_head', 'wp_shortlink_wp_head');
        remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
        remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
        remove_action( 'wp_head', 'wp_oembed_add_host_js' );
        remove_action('rest_api_init', 'wp_oembed_register_route');
        remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);
        remove_action( 'wp_head', 'feed_links', 2 );
        //Removes emoji CSS and JS in the tag of your theme
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_print_styles', 'print_emoji_styles');

        //Removes emoji CSS and JS when you're logged in to WordPress
        remove_action('admin_print_scripts', 'print_emoji_detection_script');
        remove_action('admin_print_styles', 'print_emoji_styles');

        //Removes emoji translation from feeds
        remove_filter('the_content_feed', 'wp_staticize_emoji');

        //Removes emoji translation from user-submitted comments in feeds
        remove_filter('comment_text_rss', 'wp_staticize_emoji');

        //Removes emoji translation from email messages sent by WordPress
        remove_filter('wp_mail', 'wp_staticize_emoji_for_email');

        // if( !is_page() || !is_page_template( 'page-contact.php' ) ){
        //     add_filter( 'wpcf7_load_js', '__return_false' );
        //     add_filter( 'wpcf7_load_css', '__return_false' );
        // }else{
        //     var_dump( get_page_template_slug(  ) );
        //     die();
        // }

    }
  
}

add_action( 'init', 'disable_embeds_code_init', 9999 );


