<?php
/**
 * This is a built-in template file. If you need to customize it, please,
 * DO NOT modify this file directly. Instead, copy it to your theme's directory
 * and then modify the code. If you modify this file directly, your changes
 * will be overwritten during next update of the plugin.
 */

?>


<!-- new ticket modal -->
<div id="ticketModal" class="modalOverlay">
	<a class="cancel" href="#"></a>
	<div class="modalContent">
		<a class="closeModal" href="#">
			<svg viewBox="0 0 75.29 75.29"><use xlink:href="<?php echo sprite_url; ?>#close"></use></svg>
		</a>

		<?php 
			
		// wpas_get_template( 'partials/ticket-navigation' );
			
		do_action( 'wpas_ticket_submission_form_outside_top' ); 
		?>

		<form class="ticketModalContent" role="form" method="post" action="<?php echo esc_url_raw( wc_get_account_endpoint_url( 'support' ) ); ?>" id="wpas-new-ticket" enctype="multipart/form-data">
			
			<div class="titleBar">
				<span class="icon">+</span>
				<span class="bold">تیکت جدید</span>
				<span>مشکل یا سوال خود را با ما مطرح کنید</span>
			</div>

			<hr class="hr">

			<?php
			/**
			 * The wpas_submission_form_inside_top has to be placed
			 * inside the form, right in between the form opening tag
			 * and the first field being rendered.
			 *
			 * @since  4.4.0
			 */
			do_action( 'wpas_submission_form_inside_top' );

			/**
			 * Filter the subject field arguments
			 *
			 * @since 3.2.0
			 *
			 * Note the use of the The wpas_submission_form_inside_before 
			 * action hook.  It will be placed inside the form, usually
			 * right in between the form opening tag
			 * and the subject field.
			 *
			 * However, the hook can be moved if the subject field is set 
			 * to a different sort order in the custom fields array.
			 *
			 * The wpas_submission_form_inside_after_subject action 
			 * hook is also declared as a post-render hook.
			 */
			$subject_args = apply_filters( 'wpas_subject_field_args', array(
				'name' => 'title',
				'args' => array(
					'required'   => true,
					'field_type' => 'text',
					'label'      => __( 'Subject', 'awesome-support' ),
					'sanitize'   => 'sanitize_text_field',
					'order'		 => '-2',
					'pre_render_action_hook_fe'		=> 'wpas_submission_form_inside_before_subject',
					'post_render_action_hook_fe'	=> 'wpas_submission_form_inside_after_subject',
				)
			) );

			wpas_add_custom_field($subject_args['name'], $subject_args['args']);

			/**
			 * Filter the description field arguments
			 *
			 * @since 3.2.0
			 */
			$body_args = apply_filters( 'wpas_description_field_args', array(
				'name' => 'message',
				'args' => array(
					'required'   => true,
					'field_type' => 'wysiwyg',
					'label'      => __( 'Description', 'awesome-support' ),
					'sanitize'   => 'sanitize_text_field',
					'order'		 => '-1',
					'pre_render_action_hook_fe'		=> 'wpas_submission_form_inside_before_description',
					'post_render_action_hook_fe'	=> 'wpas_submission_form_inside_after_description',
				)
			) );

			wpas_add_custom_field($body_args['name'], $body_args['args']);		
			
			/**
			 * Declare an action hook just before rendering all the fields...
			 */
			do_action( 'wpas_submission_form_pre_render_fields' );
			
			/* All custom fields have been declared so render them all */
			// WPAS()->custom_fields->submission_form_fields();

			?>

			<label class="label">موضوع تیکت</label>
			<div class="inputGroup">
				<input type="text" placeholder="موضوع" name="wpas_title" required="">
				<svg class="icon" viewBox="0 0 256.49 214.55"><use xlink:href="<?php echo sprite_url; ?>#lines"></use></svg>
			</div>

			<div class="row">

				<?php

				// Get orders from people named John that were paid in the year 2016.
				// Get 10 most recent order ids in date descending order.
				$query = new WC_Order_Query( array(
					'limit' => 10,
					'orderby' => 'date',
					'order' => 'DESC',
					'return' => 'ids',
					'customer_id' => get_current_user_id(),
				) );
				$customer_orders = $query->get_orders();
			
				$orders = [];

				foreach ($customer_orders as $order_id) {
				
					// $order = new WC_Order( $order_id );
					$order = wc_get_order( $order_id ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited

					if( $order ){				
						$orders[$order_id] = get_order_title($order);
					}

				}
				?>

				<div class="col-sm-6">
					<label class="label">مربوط به سفارش</label>
					<div class="inputGroup">
						<select id="wpas_order_id" class="wpas-form-control" name="wpas_order_id" required="">
							<option selected readonly disabled>به سفارش خاصی مربوط نمیشود</option>
							<?php
							foreach ($orders as $order_id => $order_name ) {
								echo '<option value="'.$order_id.'">'.$order_name.'</option>';
							}
							?>
						</select>
						<svg class="icon arrow" viewBox="0 0 24 24"><use xlink:href="<?php echo sprite_url; ?>#arrow"></use></svg>
					</div>
				</div>

				<?php
				$departments = get_terms( array(
					'taxonomy'   => 'department',
					'hide_empty' => false,
				) );

				if ( !empty( $departments ) ) { ?>
					<div class="col-sm-6">
						<label class="label">دپارتمان مرتبط</label>
						<div class="inputGroup">
							<select id="wpas_department" name="wpas_department">
								<option value="">لطفا انتخاب کنید</option>
								<?php
								foreach ($departments as $department) {
									echo '<option value="'.$department->term_id.'">'.$department->name.'</option>';
								}
								?>
							</select>
							<svg class="icon arrow" viewBox="0 0 24 24"><use xlink:href="<?php echo sprite_url; ?>#arrow"></use></svg>
						</div>
					</div>
				<?php } ?>
			</div>

			<label class="label">متن پیام</label>
			<textarea id="wpas_message" class="p-4" name="wpas_message" placeholder="پیام خود را بنویسید..." rows="6" required=""></textarea>

			<?php

			/**
			 * Declare an action hook just after rendering all the fields...
			 */
			 do_action( 'wpas_submission_form_post_render_fields' );		
			

			/**
			 * The wpas_submission_form_inside_before hook has to be placed
			 * right before the submission button.
			 *
			 * @since  3.0.0
			 */
			do_action( 'wpas_submission_form_inside_before_submit' );

			wp_nonce_field( 'new_ticket', 'wpas_nonce', true, true );
			// wpas_make_button( __( 'Submit ticket', 'awesome-support' ), array( 'name' => 'wpas-submit' ) );
			

			$ticket_priority = get_terms( 'ticket_priority', array(
				'hide_empty' => false,
			));

			if ( $ticket_priority ) {
				$ticket_priority = array_shift( $ticket_priority );
				$ticket_priority = $ticket_priority->term_id;
			}else{
				$ticket_priority = null;
			}

			?>

			<div class="submitDiv">
				<?php
				if( $ticket_priority ){
					echo '<div class="radioContainer">
						<input type="checkbox" name="wpas_ticket_priority" value="'.$ticket_priority.'">
						<label class="radioLabel">عجله دارم، فوریه!</label>
					</div>';
				}
				?>
				<button name="wpas-submit" type="submit" class="btn btn--red btn--large btn--text btn--text--right submitBtn">
					شروع گفتگو
					<svg viewBox="0 0 12.56 19.46">
						<use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
					</svg>
				</button>
			</div>

			<?php


			/**
			 * The wpas_submission_form_inside_before hook has to be placed
			 * right before the form closing tag.
			 *
			 * @since  3.0.0
			 */
			do_action( 'wpas_submission_form_inside_after' );
			wpas_do_field( 'submit_new_ticket' );
			?>


		</form>
	</div>
</div>