<?php
/**
 * Ticket Details Template.
 * 
 * This is a built-in template file. If you need to customize it, please,
 * DO NOT modify this file directly. Instead, copy it to your theme's directory
 * and then modify the code. If you modify this file directly, your changes
 * will be overwritten during next update of the plugin.
 */

/* Exit if accessed directly */
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @var $post WP_Post
 */
global $post;
$_post = $post;
$post_ID = $_post->ID;

/* Get author meta */
$author = get_user_by( 'id', $_post->post_author );

$show_reply_form = apply_filters('wpas_show_reply_form_front_end',true, $post );

ob_start();
get_ticket_reply_form($post_ID); 
$reply_form = ob_get_contents();
ob_end_clean();

$has_reply_form = ( strpos($reply_form,"<form") ) ? true : false;

?>

<!-- Chat container -->
<div class="ticketChatContainer">

	<div class="chat">

	<?php
		/**
		 * wpas_frontend_ticket_content_before hook
		 *
		 * @since  3.0.0
		 */
		// do_action( 'wpas_frontend_ticket_content_before', $post_ID, $post );

	?>

	<div class="item customer">
		<div class="icon">
			<?php //echo get_avatar( get_userdata( $author->ID )->user_email, 15, get_option( 'avatar_default' ), '', array( 'class' => 'igameLogo' ) ); ?>
			<img class="avatar" src="<?php echo theme_assets; ?>/imgs/user_pic.png" alt="">
		</div>
		<div class="body">
			<div class="text">
				<?php echo get_the_content(null,false,$post_ID); ?>
				<?php //echo apply_filters( 'the_content', get_the_content(null,false,$post_ID) ); ?>
			</div>
			<div class="date">
				<span><?php echo get_the_date( 'j F y', $post_ID ); ?></span>
				<span class="hour"><?php echo get_the_time( 'g:i', $post_ID ); ?></span>
			</div>
		</div>
	</div>

		<?php
		/**
		 * Display the original ticket's content
		 */
		// echo make_clickable( apply_filters( 'the_content', $_post->post_content ) );

		// Set the number of replies
		$replies_per_page  = wpas_get_option( 'replies_per_page', 10 );
		$force_all_replies = WPAS()->session->get( 'force_all_replies' );

		// Check if we need to force displaying all the replies (direct link to a specific reply for instance)
		if ( true === $force_all_replies ) {
			$replies_per_page = - 1;
			WPAS()->session->clean( 'force_all_replies' ); // Clean the session
		}

		$args = array(
			'posts_per_page' => $replies_per_page,
			'no_found_rows'  => false,
		);

		$replies = wpas_get_replies( $post_ID, array( 'read', 'unread' ), $args, 'wp_query' );

		if ( $replies->have_posts() ):

			while ( $replies->have_posts() ):

				$replies->the_post();
				global $post;
				$reply_id = get_the_ID();
				$reply_author_id = get_post_field( 'post_author', $reply_id );
				$user      = get_userdata( $reply_author_id );
				$user_role = $user->roles[0];
				$time_ago  = human_time_diff( get_the_time( 'U', $reply_id ), current_time( 'timestamp' ) );

				if( $author->ID == $reply_author_id ){
					wpas_get_template( 'partials/ticket-origin', array( 'time_ago' => $time_ago, 'user' => $user, 'post' => $post ) );
				}else{
					wpas_get_template( 'partials/ticket-reply', array( 'time_ago' => $time_ago, 'user' => $user, 'post' => $post ) );
				}

			endwhile;

		endif;

		wp_reset_query();

		/**
		 * wpas_frontend_ticket_content_after hook
		 *
		 * @since  3.0.0
		 */
		// do_action( 'wpas_frontend_ticket_content_after', $post_ID, $post );
		?>

	</div>

	<?php //wpas_get_template( 'partials/ticket-navigation' ); ?>

	<?php
	/**
	 * Display the table header containing the tickets details.
	 * By default, the header will contain ticket status, ID, priority, type and tags (if any).
	 */
	// wpas_ticket_header();
	?>

	<?php
	if ( $replies_per_page !== -1 && (int) $replies->found_posts > $replies_per_page ):

		$current = $replies->post_count;
		$total   = (int) $replies->found_posts;
		?>

		<div class="wpas-alert wpas-alert-info wpas-pagi">
			<div class="wpas-pagi-loader"><?php _e( 'Loading...', 'awesome-support' ); ?></div>
			<p class="wpas-pagi-text"><?php echo wp_kses_post( sprintf( _x( 'Showing %s replies of %s.', 'Showing X replies out of a total of X replies', 'awesome-support' ), "<span class='wpas-replies-current'>$current</span>", "<span class='wpas-replies-total'>$total</span>" ) ); ?>
				<?php 
				if ( 'ASC' == wpas_get_option( 'replies_order', 'ASC' ) ) {
					$load_more_msg = __( 'Load newer replies', 'awesome-support' );
				} else {
					$load_more_msg = __( 'Load older replies', 'awesome-support' );
				} ?>
				<?php if ( -1 !== $replies_per_page ): ?><a href="#" class="wpas-pagi-loadmore"><?php echo $load_more_msg; ?></a><?php endif; ?>
			</p>
		</div>

	<?php endif; ?>

	<?php 
	/**
	* Prepare to show the reply form.
	*/
	if ( $show_reply_form && $has_reply_form ) {
		echo $reply_form;
	} ?>

</div>

<?php 
/**
* Prepare to show the reply form.
*/
if ( $show_reply_form && !$has_reply_form ) {
	echo $reply_form;
} ?>