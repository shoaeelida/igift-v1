<?php
/* Get the tickets object */
global $wpas_tickets;

if ( $wpas_tickets->have_posts() ):

	/* Get list of columns to display */
	$columns 		  = wpas_get_tickets_list_columns();
	
	/* Get number of tickets per page */
	$tickets_per_page = wpas_get_option( 'tickets_per_page_front_end' );
	If ( empty($tickets_per_page) ) {
		$tickets_per_page = 5 ; // default number of tickets per page to 5 if no value specified.
	}
	
	?>

	<!-- tickets list -->
	<div class="ticketsList">

		<?php //wpas_get_template( 'partials/ticket-navigation' ); ?>

		<?php
		/*
		<!-- Filters & Search tickets -->
		<div class="wpas-row" id="wpas_ticketlist_filters">
			<div class="wpas-one-third">
				<select class="wpas-form-control wpas-filter-status">
					<option value=""><?php esc_html_e('Any status', 'awesome-support'); ?></option>
				</select>
			</div>
			<div class="wpas-one-third"></div>
			<div class="wpas-one-third" id="wpas_filter_wrap">
				<input class="wpas-form-control" id="wpas_filter" type="text" placeholder="<?php esc_html_e('Search tickets...', 'awesome-support'); ?>">
				<span class="wpas-clear-filter" title="<?php esc_html_e('Clear Filter', 'awesome-support'); ?>"></span>
			</div>
		</div>
		*/
		?>

		<dl class="accordion" data-filter="#wpas_filter" data-filter-text-only="true" data-page-navigation=".wpas_table_pagination" data-page-size=" <?php echo $tickets_per_page ?> ">
			
			<?php
			while( $wpas_tickets->have_posts() ):

				$wpas_tickets->the_post();

				// echo '<tr class="wpas-status-' . wpas_get_ticket_status( $wpas_tickets->post->ID ) . '" id="wpas_ticket_' . $wpas_tickets->post->ID . '">';

				// printf( '<td %s>', $data_attributes );

				// Display the content for this column 
				// support_get_tickets_list_column_content( $column_id, $column );
				

				// If the replies are displayed from the oldest to the newest we want to link directly to the latest reply in case there are multiple reply pages
				if ( 'ASC' === wpas_get_option( 'replies_order', 'ASC' ) ) {
					$last_reply = wpas_get_replies( get_the_ID(), array(
						'read',
						'unread',
					), array( 'posts_per_page' => 1, 'order' => 'DESC' ) );
					$link       = ! empty( $last_reply ) ? get_ticket_reply_link( $last_reply[ 0 ]->ID ) : get_ticket_permalink( get_the_ID() );
				} else {
					$link = get_ticket_permalink( get_the_ID() );
				}
	
				$id    = get_the_ID();
				$title = get_the_title();
				
				/* Maybe add the ticket id to the title string */
				if ( ! boolval( wpas_get_option( 'hide_ticket_id_title_fe', false ) ) ) {
					$title .= " (#$id)";
				}
				
				$offset = wpas_get_offset_html5();
				$order_id = get_post_meta( $id, '_wpas_order_id', true );

				?>

				<dt>
					<a href="<?php echo $link; ?>" class="accordion-title accordionTitle js-accordionTrigger mb-2" id="<?php echo get_the_ID(); ?>">
                        <div class="date-time">
                            <span class="date"><?php echo get_the_date( 'j F y' ); ?></span>
                            <span class="hour"><?php echo get_the_date( 'g:i' ); ?></span>
                        </div>
                        <h2 class="title">
                            <?php echo $title; ?>
                        </h2>

                        <?php
                        if( $order_id ) {
                            echo '<div class="price-status">
                                    <div class="price flex-nowrap d-flex pr-0 justify-content-between">
                                        <span class="semiBold">مربوط به سفارش</span>
                                        <span class="bold">
                                            #' . $order_id . '
                                        </span>
                                    </div>
                                  </div>';
                        } ?>
                        <div class="status">
                            <svg class="icon" viewBox="0 0 128 128"><use xlink:href="<?php echo sprite_url; ?>#success"></use></svg>
                            <?php echo wpas_get_ticket_status( get_the_ID() ); ?>
                        </div>
                        <span class="accItem">...</span>
					</a>
				</dt>

				<?php

			
			endwhile;

			wp_reset_query(); ?>
		</dl>

		<?php
		/*
		<!-- List of tickets -->
		<table id="wpas_ticketlist" class="wpas-table wpas-table-hover" data-filter="#wpas_filter" data-filter-text-only="true" data-page-navigation=".wpas_table_pagination" data-page-size=" <?php echo $tickets_per_page ?> ">
			<thead>
				<tr>
					<?php foreach ( $columns as $column_id => $column ) {

						$data_attributes = '';

						// Add the data attributes if any
						if ( isset( $column['column_attributes']['head'] ) && is_array( $column['column_attributes']['head'] ) ) {
							$data_attributes = wpas_array_to_data_attributes( $column['column_attributes']['head'] );
						}

						printf( '<th id="wpas-ticket-%1$s" %3$s>%2$s</th>', $column_id, $column['title'], $data_attributes );

					} ?>
				</tr>
			</thead>
			<tbody>
				<?php
				while( $wpas_tickets->have_posts() ):

					$wpas_tickets->the_post();

					echo '<tr class="wpas-status-' . wpas_get_ticket_status( $wpas_tickets->post->ID ) . '" id="wpas_ticket_' . $wpas_tickets->post->ID . '">';

					foreach ( $columns as $column_id => $column ) {

						$data_attributes = '';

						// Add the data attributes if any
						if ( isset( $column['column_attributes']['body'] ) && is_array( $column['column_attributes']['body'] ) ) {
							$data_attributes = wpas_array_to_data_attributes( $column['column_attributes']['body'], true );
						}

						printf( '<td %s>', $data_attributes );

						// Display the content for this column 
						wpas_get_tickets_list_column_content( $column_id, $column );

						echo '</td>';

					}

					echo '</tr>';
				
				endwhile;

				wp_reset_query(); ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="<?php echo count($columns); ?>">
						<ul class="wpas_table_pagination"></ul>
					</td>
				</tr>
			</tfoot>
		</table>
		*/
		?>

		<div class="newTicket">
			<div class="d-flex align-items-center flex-md-nowrap flex-wrap">
				<h3 class="title">مشکل یا سوالی داری؟</h3>
				<p>تیکت جدید ثبت کن تا در اسرع وقت کارشناسان بهت جواب بدن</p>
			</div>
			<a href="#ticketModal" class="btn btn--text btn--text--left pr-0 pr-md-5 link">
				ثبت تیکت جدید
			</a>
		</div>

	</div>

<?php else: ?>

    <div class="noData pt-5 pb-5">
        <img class="image" src="<?php echo theme_assets; ?>/imgs/nodata.svg">
        <h3 class="title">هیچ تیکتی ثبت نشده!</h3>
        <h4 class="text">همین حالا می تونی درخواستت رو ثبت کنی!</h4>
    </div>

	<div class="newTicket">
		<div class="d-flex align-items-center flex-md-nowrap flex-wrap">
			<h3 class="title">مشکل یا سوالی داری؟</h3>
			<p>تیکت جدید ثبت کن تا در اسرع وقت کارشناسان بهت جواب بدن</p>
		</div>
		<a href="#ticketModal" class="btn btn--text btn--text--left pr-0 pr-md-5 link">
			ثبت تیکت جدید
		</a>
	</div>

<?php 
// echo wpas_get_notification_markup( 'info', sprintf( __( 'You haven\'t submitted a ticket yet. <a href="%s">Click here to submit your first ticket</a>.', 'awesome-support' ), wpas_get_submission_page_url() ) );
endif; ?>