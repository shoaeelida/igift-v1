<?php
/**
 * This is a built-in template file. If you need to customize it, please,
 * DO NOT modify this file directly. Instead, copy it to your theme's directory
 * and then modify the code. If you modify this file directly, your changes
 * will be overwritten during next update of the plugin.
 */

/* Exit if accessed directly */
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/* IMPORTANT: make the $post var global as it is used in this template */
global $post;

/* Get author meta */
$author = get_user_by( 'id', $post->post_author );

/**
 * wpas_before_original_post hook
 */
do_action( 'wpas_before_original_post' ); ?>

<div class="item customer">
	<div class="icon">
		<?php //echo get_avatar( get_userdata( $author->ID )->user_email, 15, get_option( 'avatar_default' ), '', array( 'class' => 'igameLogo' ) ); ?>
		<img class="avatar" src="<?php echo theme_assets; ?>/imgs/user_pic.png" alt="">
	</div>
	<div class="body">
		<div class="text">
			<?php echo get_the_content(null,false,$post->ID); ?>
		</div>
		<div class="date">
			<span><?php echo get_the_date( 'j F y', $post->ID ); ?></span>
			<span class="hour"><?php echo get_the_time( 'g:i', $post->ID ); ?></span>
		</div>
	</div>
</div>

<?php
/**
 * Hook after the original post table
 */
do_action( 'wpas_after_original_post' );