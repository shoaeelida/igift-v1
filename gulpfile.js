var gulp = require('gulp'),
  plumber = require('gulp-plumber'),
  rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin'),
  cache = require('gulp-cache');
// var minifycss = require('gulp-minify-css');
let cleanCSS = require('gulp-clean-css');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync');
var gcmq = require('gulp-group-css-media-queries');
var purge = require('gulp-css-purge');

gulp.task('browser-sync', function () {
  return browserSync({
    server: {
      baseDir: "./"
    },
    port: 8000
  });
});

gulp.task('bs-reload', function () {
  return browserSync.reload();
});

gulp.task('imgs', function () {
  return gulp.src('src/imgs/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest('dist/imgs/'));
});

gulp.task('styles', function () {
  // gulp.src(['src/styles/**/*.scss'])
  return gulp.src(['src/styles/screen.scss'])
    .pipe(sourcemaps.init())
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
      }
    }))
    .pipe(sass())
    .pipe(autoprefixer('last 30 versions', "ie >= 10"))
    .pipe(gcmq())
    .pipe(purge({
      trim: false,
      shorten: false,
      verbose: true
    }))
    .pipe(gulp.dest('dist/styles/'))
    .pipe(rename({ suffix: '.min' }))
    // .pipe(minifycss())
    // .pipe(cleanCSS({compatibility: 'ie10+'}))
    .pipe(cleanCSS())
    .pipe(gulp.dest('dist/styles/'))
    // .pipe(browserSync.stream());
    .pipe(browserSync.reload({ stream: true }))
});


gulp.task('pluginsStyles', function () {
  return gulp.src(['src/styles/plugins.scss'])
    .pipe(sourcemaps.init())
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
      }
    }))
    .pipe(sass())
    .pipe(autoprefixer('last 30 versions', "ie >= 10"))
    .pipe(gcmq())
    .pipe(purge({
      trim: false,
      shorten: false,
      verbose: true
    }))
    .pipe(gulp.dest('dist/styles/'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(cleanCSS())
    .pipe(gulp.dest('dist/styles/'))
    .pipe(browserSync.reload({ stream: true }))
});


gulp.task('scripts', function () {
  return gulp.src('src/scripts/**/*.js')
    /*
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))*/
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    // .pipe(concat('main.js'))
    .pipe(babel())
    .pipe(gulp.dest('dist/scripts/'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest('dist/scripts/'))
    // .pipe(browsersync.stream());
    .pipe(browserSync.reload({ stream: true }))
});

// gulp.task('default', ['browser-sync'], function(){
//   gulp.watch("src/styles/**/*.scss", ['styles']);
//   gulp.watch("src/scripts/**/*.js", ['scripts']);
//   gulp.watch("src/images/**/*", ['images']);
//   gulp.watch("*.html", ['bs-reload']);
// });


gulp.task("default", gulp.series(["browser-sync"], function () {
  gulp.watch("src/styles/**/*.scss", gulp.parallel(['styles','pluginsStyles']));
  gulp.watch("src/styles/*.scss", gulp.parallel(['styles','pluginsStyles']));
  gulp.watch("src/styles/**/*", gulp.parallel(['styles','pluginsStyles']));
  gulp.watch("src/styles/*", gulp.parallel(['styles','pluginsStyles']));
  gulp.watch("src/scripts/**/*.js", gulp.parallel(['scripts']));
  gulp.watch("src/images/**/*", gulp.parallel(['images']));
  gulp.watch("*.html", gulp.parallel(['bs-reload']));
}));


gulp.task('watch', gulp.parallel(["browser-sync"], () => {
  gulp.watch('src/styles/*')
    .on('change', gulp.parallel('styles','pluginsStyles'))
    .on('change', gulp.parallel('bs-reload'));
  gulp.watch('src/styles/**/*')
    .on('change', gulp.parallel('styles','pluginsStyles'))
    .on('change', gulp.parallel('bs-reload'));
  gulp.watch('src/scripts/**/*.js')
    .on('change', gulp.parallel('scripts'))
    .on('change', gulp.parallel('bs-reload'));
  gulp.watch('*.html')
    .on('change', gulp.parallel('bs-reload'));
  // gulp.watch('src/imgs/**/*')
  //   .on('change', gulp.parallel('imgs'))
  //   .on('change', gulp.parallel('bs-reload'));
}));