<?php

if (!defined('ABSPATH')) exit();

/**
 * Template Name: Home page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();

?>

<div class="home-content">
    <?php 
    if (($html = get_transient('home_bestselling_products')) == false) {
        $html = '';
        ob_start();
        get_template_part('template-parts/home/bestselling_products');
        $html .= ob_get_contents();
        ob_end_clean();
        set_transient('home_bestselling_products', $html, HOUR_IN_SECONDS);
    }
    echo $html;
    ?>
    <section class="advertise-holder">
        <div class="container">
            <div class="advertise-wrap">
            <div class="advertisement red" data-platform="NETFLIX">
                <h2 class="advertise-txt">
                <span>خـرید گیفت کـارت</span>
                نتفلیکس
                </h2>
                <a href="#" class="btn btn--dark btn--text--right fs-15">
                خرید
                <svg viewBox="0 0 12.56 19.47">
                    <use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
                </svg>
                </a>
                <img src="<?php echo theme_assets; ?>/imgs/adv2.jpg" alt="" />
            </div>
            <div class="advertisement green" data-platform="SPOTIFY">
                <h2 class="advertise-txt">
                <span>خـرید گیفت کـارت</span>
                اسپاتیفای
                </h2>
                <a href="#" class="btn btn--dark btn--text--right fs-15">
                خرید
                <svg viewBox="0 0 12.56 19.47">
                    <use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
                </svg>
                </a>
                <img src="<?php echo theme_assets; ?>/imgs/adv1.jpg" alt="" />
            </div>
            </div>
        </div>
    </section>
    <?php 
    // if (($html = get_transient('home_new_products')) == false) {
        $html = '';
        // ob_start();
        get_template_part('template-parts/home/new_products');
        // $html .= ob_get_contents();
        // ob_end_clean();
        // set_transient('home_new_products', $html, HOUR_IN_SECONDS);
    // }
    echo $html;
    ?>
    <section class="introduce-section">
        <div class="container">
            <div class="introduce-wrap">
            <div class="introduce-desc">
                <h2 class="title">خرید انواع گیفت‌کارت بصورت آنی</h2>
                <p>
                انواع گیفت‌ کارت‌های معتبر کمپانی‌های مختلف را با قیمتی پایین‌تر نسبت به سایر فروشگاه‌ها از سایت آی گیفت کارت خریداری کنید.
                </p>
                <div class="properties-holder">
                <div class="property">
                    <span>تنوع</span>
                    بسیار بالای محصولات 
                </div>
                <div class="property">
                    <span>فعالسازی</span>
                    آنی محصول همراه با آموزش
                </div>
                <div class="property">
                    <span>پرداخت </span>
                    آسان و تحویل فوری گیفت کارت
                </div>
                <div class="property">
                    <span>پشتیبانی</span>
                    آنلاین و 24 ساعته حتی در روزهای تعطیل
                </div>
                </div>
            </div>
            <a href="<?php echo get_post_type_archive_link( 'giftcard' ); ?>" class="btn btn--red btn--text--right fs-15 bold">
                مشاهده محصولات
                <svg viewBox="0 0 12.56 19.47">
                <use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
                </svg>
            </a>
            </div>
        </div>
    </section>
    <section class="guid-wrap">
        <div class="container">
            <div class="guid-holder">
            <div class="card-guid">
                <div class="icon" data-num="1">
                <svg viewBox="0 0 512 512">
                    <use xlink:href="<?php echo sprite_url; ?>#register"></use>
                </svg>
                </div>
                <h3 class="title">ثبت نام در آی‌گیفت</h3>
                <p>
                اولین مرحله خرید، ساخت اکانت کاربری در آی گیفت می باشد. شما به
                راحتی می توانید با شماره موبایل خود در سی ثانیه اکانت خود را
                بسازید.
                </p>
            </div>
            <div class="card-guid">
                <div class="icon" data-num="2">
                <svg viewBox="0 0 18.01 18.01">
                    <use xlink:href="<?php echo sprite_url; ?>#search"></use>
                </svg>
                </div>
                <h3 class="title">جستجوی محصول</h3>
                <p>
                از طریق جستجو در سایت یا بررسی آرشیو گیفت کارت ها، می توانید
                محصول مورد نظر خود را انتخاب و برای خرید آن اقدام کنید.
                </p>
            </div>
            <div class="card-guid">
                <div class="icon" data-num="4">
                <svg viewBox="0 0 28 28">
                    <use xlink:href="<?php echo sprite_url; ?>#wallet"></use>
                </svg>
                </div>
                <h3 class="title">پرداخت آنلاین</h3>
                <p>
                بعد از انتخاب گیفت کارت، از طریق درگاه امن مبلغ سفارش خود را با
                اطمینان خاطر پرداخت کنید و از پرداخت آنلاین و سریع خود لذت
                ببرید.
                </p>
            </div>
            <div class="card-guid">
                <div class="icon" data-num="3">
                <svg viewBox="0 0 512 512">
                    <use xlink:href="<?php echo sprite_url; ?>#delivery"></use>
                </svg>
                </div>
                <h3 class="title">دریافت محصول</h3>
                <p>
                در صورتی که پرداخت شما با موفقیت انجام شده باشد، اطلاعات گیفت
                کارت خریداری شده در اکانت شما قابل مشاهده خواهد بود.
                </p>
            </div>
            </div>
        </div>
    </section>
</div>

<?php
get_footer();
