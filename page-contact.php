<?php

/**
    * Template Name: Contact page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();

if( have_posts() ){

    the_post();

    global $playOptions;

    $email = 0;
    $phone = 0;
    $instagram = 0;
    $youtube = 0;
    $aparat = 0;
    $twitter = 0;

    if( $playOptions ){
        $email = $playOptions->getOption( 'email' );
        $phone = $playOptions->getOption( 'phone' );
        $instagram = $playOptions->getOption( 'instagram' );
        $youtube = $playOptions->getOption( 'youtube' );
        $aparat = $playOptions->getOption( 'aparat' );
        $twitter = $playOptions->getOption( 'twitter' );
    }

    ?>

    <div class="mainHeader">
        <div class="container">
            <h1 class="title"><?php the_title(); ?></h1>
        </div>
    </div>



    <div class="contactUs">
        <div class="container">
            <div class="box">
                <div class="side">
                    <div class="logo-holder">
                        <img class="logo" src="<?php echo theme_assets; ?>/imgs/logo2.svg" alt="<?php echo site_title; ?>">
                    </div>

                    <span class="largeTitle">با ما تماس بگیرید
                        <p >به کمک نیاز دارید؟</p>
                    </span>

                    <div class="phone-email">
                        <?php if( $phone && !empty($phone) ){ ?>
                            <div class="contactRow">
                                <span class="bold number"><?php echo str_replace('021','',$phone); ?></span>
                                <span class="light number">021</span>
                                <svg class="icon" viewBox="0 0 301.81 301.81"><use xlink:href="<?php echo sprite_url; ?>#phone"></use></svg>
                            </div>
                        <?php } ?>
                        <?php if( $email && !empty($email) ){ ?>
                            <div class="contactRow">
                                <span class="bold email"><?php echo $email; ?></span>
                                <svg class="icon" viewBox="0 0 306.78 233.73"><use xlink:href="<?php echo sprite_url; ?>#email"></use></svg>
                            </div>
                        <?php } ?>
                    </div>


                    <span class="second-title largeTitle blue">ما را دنبال کنید
                        <p >از جدیدترین سرویس ها با خبر شوید</p>
                    </span>

                    <ul class="socials">
                        <?php echo ($instagram) ? '<li><a href="'.$instagram.'" target="_blank"><svg class="icon" viewBox="0 0 551.03 551.03"><use xlink:href="'.sprite_url.'#instagram"></use></svg></a></li>' : ''; ?>
                        <?php echo ($twitter) ? '<li><a href="'.$twitter.'" target="_blank"><svg class="icon" viewBox="0 0 638.44 518.87"><use xlink:href="'.sprite_url.'#twitter"></use></svg></a></li>' : ''; ?>
                        <?php echo ($youtube) ? '<li><a href="'.$youtube.'" target="_blank"><svg class="icon" viewBox="5.368 13.434 53.9 37.855"><use xlink:href="'.sprite_url.'#youtube"></use></svg></a></li>' : ''; ?>
                        <?php echo ($aparat) ? '<li><a href="'.$aparat.'" target="_blank"><svg class="icon" viewBox="0 0 48 48"><use xlink:href="'.sprite_url.'#aparat"></use></svg></a></li>' : ''; ?>
                    </ul>
                </div>

                <div class="contactForm">
                    <?php the_content() ?>
                </div>
            </div>
        </div>
    </div>


<?php

}

get_footer();