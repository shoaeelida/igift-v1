<?php

if ( ! defined('ABSPATH')) exit();

if (!session_id()) {
    session_start();
}

// Global vals
define( "theme_directory", trailingslashit( get_template_directory() ) );
define( "site_url", home_url('/') );
define( "site_title", get_bloginfo( 'name' ) );
define( "theme_src", get_template_directory_uri() . '/src' );

// add_filter( 'pre_option_upload_path', function( $upload_path ) {
//     return ABSPATH.'cdn';
// });
// add_filter( 'pre_option_upload_url_path', function( $upload_url_path ) {
//     return site_url . 'cdn';
// });

if( strpos(site_url,'localhost') == false ){
	// define( "theme_cdn", str_replace('://','://cdn.',site_url) . 'theme' );
	define( "theme_cdn", 'https://igift.ir/cdn/theme' );
	define( "theme_assets", theme_cdn . '/dist' );
	define( "sprite_url", theme_assets . '/imgs/sprite.svg' );
}else{
	define( "theme_cdn", get_template_directory_uri() );
	define( "theme_assets", theme_cdn . '/dist' );
	define( "sprite_url", theme_assets . '/imgs/sprite.svg' );
}

if( class_exists('TitanFramework') ){
	$playOptions = TitanFramework::getInstance( 'play' );
}else{
	$playOptions = null;
}

require_once('plugins/favorites.php');
require_once('plugins/otp.php');

/**
 * Twenty Nineteen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

if ( ! function_exists( 'igame_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function igame_setup() {
		
		add_theme_support( 'yoast-seo-breadcrumbs' );
		add_theme_support( 'woocommerce' );

		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Twenty Nineteen, use a find and replace
		 * to change 'twentynineteen' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'igame', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1568, 9999 );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'top' => 'Top Menu',
				'footer' => 'Footer Menu',
				'footer_landing' => 'Footer landing menu',
				'footer_buyGame' => 'Footer buy Game',
				'footer_buyGiftcard' => 'Footer buy Giftcard',
				'footer_link' => 'Footer link',
				'mobile' => 'Mobile Menu',
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 190,
				'width'       => 190,
				'flex-width'  => false,
				'flex-height' => false,
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );

		// Add support for editor styles.
		// add_theme_support( 'editor-styles' );

		// Enqueue editor styles.
		// add_editor_style( 'style-editor.css' );

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );

	}
endif;
add_action( 'after_setup_theme', 'igame_setup' );


/**
 * Enqueue scripts and styles.
 */
require theme_directory . 'inc/scripts.php';



global $current_url;
global $related_game;
global $related_game_loop;

function pre_archive_query($query) {

	if ( is_admin() || !$query->is_main_query() ) {
		return;
	}

	// Check if on frontend and main query is modified
	if( get_query_var( 'relatedGame' ) ) {

		set_query_var('dlc', false);
		set_query_var('name', false);

		$query->is_page = false;
		$query->is_single = false;
		$query->is_singular = false;
		$query->is_post_type_archive = true;
		$query->is_preview = false;
		
		$query->is_404 = false;
		$query->is_archive = true;
		$query->queried_object = null;
		$query->queried_object_id = null;
		$query->current_post = null;
		$query->in_the_loop = true;
		
		unset($query->query_vars['name']);
		unset($query->query_vars['dlc']);

		unset($query->query['name']);
		unset($query->query['dlc']);

		$query->query_vars['pagename'] = null;
		$query->query_vars['page_id'] = null;
		$query->query_vars['error'] = '';	

		$query->set('posts_per_page', -1);
		$query->set('post_type', ['dlc']);
		$query->set('post_status', 'publish');

		$GLOBALS['related_game_loop'] = $query;
		// $results_count = $GLOBALS['related_game_loop']->found_posts;
		// $query->set('found_posts',$GLOBALS['related_game_loop']->found_posts);

		global $wp;
		$GLOBALS['current_url'] = home_url( add_query_arg( array(), $wp->request ) );
		$GLOBALS['related_game'] = substr($GLOBALS['current_url'], strrpos($GLOBALS['current_url'], '/' )+1)."\n";
			
		if( isset($_GET['debug']) ){
			var_dump( $query );
			var_dump( 'is_archive() >> '  );
			var_dump( is_archive() );
			var_dump( '$query->is_main_query >> '  );
			var_dump(  $query->is_main_query() );
			die();
		}
		
		return;

	}
	
    if ( !is_archive() ) {
		return;
	}

	// if( isset($_GET['debug']) ){
		// var_dump( $query );
		// var_dump( 'is_archive() >> ' . is_archive() );
		// var_dump( 'is_admin() >> ' . is_admin() );
		// var_dump( '$query->is_main_query >> ' . $query->is_main_query() );

		// var_dump( $query->is_main_query() );
	// }

	$is_post_type = ( is_post_type_archive('game') || is_post_type_archive('giftcard') || is_post_type_archive('dlc') ) ? true : false;

	if( 
		$is_post_type ||
		is_tax('game_platform') ||
		is_tax('dlc_platform') ||
		is_tax('regions') ||
		is_tag() ||
		is_tax('genres') 
	){

		// if( isset( $_GET['debug'] ) ){
		// 	// $query->set('posts_per_page', 50);
		// 	$query->set('posts_per_page', 9);
		// 	$query->set('posts_per_archive_page', 9);
		// }else{
		// 	$query->set('posts_per_page', 9);
		// 	$query->set('posts_per_archive_page', 9);
		// }

		$query->set('posts_per_page', 15);
		$query->set('posts_per_archive_page', 15);
		$query->set('post_status', 'publish');

        // search
        if (isset($_GET['search'])) {
            $query->set('s', $_GET['search'] );
        }
		// order

		$order = 'desc';
		$available_orders = ['asc','desc'];
		if( isset($_GET['order']) && in_array($_GET['order'],$available_orders)) {
			$order = $_GET['order'];
		}

		$order_by = 'date';
		$available_order_bys = ['date','popularity','price'];
		if( isset($_GET['order_by']) && in_array($_GET['order_by'],$available_order_bys)) {
			$order_by = $_GET['order_by'];
		}
		
		$query->set( 'order', $order ); 

		switch ($order_by) {
			case 'popularity':
				$query->set('meta_key', 'sell_counts');
				$query->set('meta_type', 'NUMERIC');
				$query->set( 'orderby', 'meta_value_num' ); 
				$query->set( 'order', 'desc' ); 
				break;
			case 'price':
				$query->set('meta_key', 'products_minprice');
				$query->set('meta_type', 'NUMERIC');
				$query->set( 'orderby', 'meta_value_num' ); 
				$query->set( 'order', 'asc' ); 
				break;
			default:
				$query->set( 'order_by', $order_by ); 
				break;
		}
	
		if( $is_post_type ) {
		}elseif( is_tag() ){
			$query->set( 'post_type', array( 'game' , 'giftcard' , 'dlc' ) ); 
		}elseif( is_tax('game_platform') ){
			$query->set( 'post_type', array( 'game' ) ); 
		}elseif( is_tax('dlc_platform') ){
			$query->set( 'post_type', array( 'dlc' ) ); 
		}elseif( is_tax('regions') ) {
			$query->set( 'post_type', array( 'game' , 'giftcard', 'dlc' ) ); 
		}elseif( is_tax('genres') ) {
			$query->set( 'post_type', array( 'game' , 'giftcard' , 'dlc' ) ); 
		}
		// elseif( $is_post_type ) {
		// }
		else{
			$query->set( 'post_type', array( 'game' , 'giftcard', 'dlc' ) ); 
		}
	
		// tax query
		$tax_query_obj = clone $query->tax_query;
		// $tax_query = array('relation' => 'OR');
		$tax_query = array('relation' => 'AND');

		foreach ( $tax_query_obj->queries as $q ) {
			$tax_query[] = $q;
		}
		$query->set( 'tax_query', $tax_query ); 

		// var_dump($query->tax_query);
		// die();

		// if( is_tag() ){
		// 	var_dump($query->tax_query);
		// 	die();
		// }

		$min_price = null;
		$max_price = null;

		// price
		if( isset($_GET['min_price']) ) {
			$min_price = (int)$_GET['min_price'];
		}

		if( isset($_GET['max_price']) ) {
			$max_price = (int)$_GET['max_price'];
		}

		if( $min_price || $max_price ){

			$meta_query_args = array(
				'relation' => 'AND', // Optional, defaults to "AND"
			);

			if( $min_price ){
				$meta_query_args[] = array(
					'key'     => 'products_minprice',
					'value'   => $min_price,
					'type' => 'numeric',
					'compare' => '>='
				);
			}

			if( $max_price ){
				$meta_query_args[] = array(
					'key'     => 'products_maxprice',
					'value'   => $max_price,
					'type' => 'numeric',
					'compare' => '<='
				);
			}

			$query->set( 'meta_query', $meta_query_args ); 

		}

		// global $wpdb;
		// var_dump($wpdb->last_query);
		// var_dump($query);
		// die();

		// var_dump($query);
		// die();

		// if( isset( $_GET['debug'] ) ){
		// 	var_dump($query);
		// 	die();
		// }

	}

	// return $query;
	
}

add_action( 'pre_get_posts', 'pre_archive_query' ); 




// function filter_yoast_breadcrumb_links( $links ) {
// 	return $links;
// }
// add_filter( 'wpseo_breadcrumb_links', 'filter_yoast_breadcrumb_links', 10, 1 );


add_filter( 'wpseo_breadcrumb_output_wrapper', function($wrapper){return 'ul';}, 10, 1 );
add_filter( 'wpseo_breadcrumb_single_link_wrapper', function($element){return 'li';}, 10, 1 );
add_filter( 'wpseo_breadcrumb_separator', function($breadcrumbs_sep){return '';}, 10, 1 );

// function fsssssss($contetnt){
	
// 	// The Query
// 	$the_query = new WP_Query( array(
// 		'post_type' => 'game',
// 		'nopaging'               => true,
// 		'posts_per_page'         => '100',
// 		'post_status' => array( 'draft' ),
// 	) );
	
// 	// The Loop
// 	if ( $the_query->have_posts() ) {
// 		while ( $the_query->have_posts() ) {
// 			$the_query->the_post();
// 			$id = get_the_ID();

// 			// if( $id == 4042 ) {

// 				$content = get_the_content( null, false ,$id );
			
// 				//--Remove all inline styles--
// 				$content = preg_replace('/ style=("|\')(.*?)("|\')/','',$content);
// 				$gameplay = get_post_meta( $id, 'gameplay', true );

// 				if($gameplay){
// 					$gameplay = preg_replace('/ style=("|\')(.*?)("|\')/','',$gameplay);
// 					update_post_meta( $id, 'gameplay', $gameplay );
// 				}
			
// 				$my_post = array(
// 					'ID'           => $id,
// 					'post_content' => $content,
// 				);
// 				wp_update_post( $my_post );
			
// 				var_dump('updated >> ' . $id);

// 			// }

// 		}
// 	}
// 	/* Restore original Post Data */
// 	wp_reset_postdata();

// 	var_dump('done');
// 	die();

// }
// add_action( 'init', 'fsssssss' );



if( function_exists('acf_add_options_page') ) {
	$args = array(
		/* (string) The title displayed on the options page. Required. */
		'page_title' => 'تنظیمات پیشرفته',
		/* (string) The title displayed in the wp-admin sidebar. Defaults to page_title */
		'menu_title' => 'تنظیمات پیشرفته',
		/* (string) The URL slug used to uniquely identify this options page. 
		Defaults to a url friendly version of menu_title */
		'menu_slug' => 'advanced_options',
		/* (string) The capability required for this menu to be displayed to the user. Defaults to edit_posts.
		Read more about capability here: http://codex.wordpress.org/Roles_and_Capabilities */
		'capability' => 'edit_posts',
		/* (int|string) The position in the menu order this menu should appear. 
		WARNING: if two menu items use the same position attribute, one of the items may be overwritten so that only one item displays!
		Risk of conflict can be reduced by using decimal instead of integer values, e.g. '63.3' instead of 63 (must use quotes).
		Defaults to bottom of utility menu items */
		'position' => 0,
		/* (string) The slug of another WP admin page. if set, this will become a child page. */
		'parent_slug' => 'themes.php',
		/* (string) The icon class for this menu. Defaults to default WordPress gear.
		Read more about dashicons here: https://developer.wordpress.org/resource/dashicons/ */
		'icon_url' => false,
		/* (boolean) If set to true, this options page will redirect to the first child page (if a child page exists). 
		If set to false, this parent page will appear alongside any child pages. Defaults to true */
		'redirect' => true,
		/* (int|string) The '$post_id' to save/load data to/from. Can be set to a numeric post ID (123), or a string ('user_2'). 
		Defaults to 'options'. Added in v5.2.7 */
		'post_id' => 'options',
		/* (boolean)  Whether to load the option (values saved from this options page) when WordPress starts up. 
		Defaults to false. Added in v5.2.8. */
		'autoload' => false,
		/* (string) The update button text. Added in v5.3.7. */
		'update_button'		=> 'بروزرسانی',
		/* (string) The message shown above the form on submit. Added in v5.6.0. */
		'updated_message'	=> 'تنظیمات بروزرسانی شدند',
	);

	acf_add_options_page( $args );
	
}




/**
 * Load more ajax
 **/
add_action('wp_ajax_data_fetch_load' , 'data_fetch_load');
add_action('wp_ajax_nopriv_data_fetch_load','data_fetch_load');
function data_fetch_load(){
    $data = get_sub_field('data');
    $postType = $_POST['postType'];
    $count = $_POST['count'];

    $args = array(
        'post_type' => array($postType),
        'post_status' => array('publish'),
        'post__in' => $data['lists'],
        'posts_per_page' => $count
    );
    $query = new WP_Query($args);
    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            echo get_template_part('template-parts/content/content', 'product-card');
        }
    } else {
        echo 'موردی پیدا نشد!';
    }
    wp_reset_postdata();
}




/**
 * Quote message text shortcode
 */
add_shortcode( 'quote', 'quote_func' );
function quote_func( $atts ) {
    $atts = shortcode_atts( array(
        'title' => '',
        'text' => ''
    ), $atts, 'quote' );

    return "
        <div class='highlight'>
           <strong class='title'>{$atts['title']}</strong>
           <p>{$atts['text']}</p>
        </div>
    ";
}


//
//
//
//
//function prefix_dlc_rewrite_rule() {
//    add_rewrite_rule( 'dlc/relatedGame/([^/]+)', 'index.php?dlc=$matches[1]&relatedGame=yes', 'top' );
//}
//add_action( 'init', 'prefix_dlc_rewrite_rule' );
//
//
//function prefix_register_query_var( $vars ) {
//    $vars[] = 'relatedGame';
//
//    return $vars;
//}
//add_filter( 'query_vars', 'prefix_register_query_var' );
//
//
//function prefix_url_rewrite_templates() {
//
//    if ( get_query_var( 'relatedGame' ) ) {
//        add_filter( 'template_include', function() {
//            return get_template_directory() . '/archive-dlc.php';
//        });
//    }
//}
//add_action( 'template_redirect', 'prefix_url_rewrite_templates' );
//
//
//
//
//
//global $current_url;
//global $related_game;
//global $related_game_loop;
//add_action( 'pre_get_posts', 'get_dlc_game' );
//function get_dlc_game( $query ) {
//
//    // Check if on frontend and main query is modified
//    if( ! is_admin() && $query->is_main_query() && get_query_var( 'relatedGame' ) ) {
//        $query->is_page = false;
//        $query->is_single = false;
//        $query->is_singular = false;
//        $query->is_post_type_archive = true;
//        $query->is_preview = false;
//
//        $query->is_404 = false;
//        $query->is_archive = true;
//        $query->queried_object = null;
//        $query->queried_object_id = null;
//        $query->current_post = null;
//        $query->in_the_loop = true;
//
//        unset($query->query_vars['name']);
//        unset($query->query_vars['dlc']);
//
//        unset($query->query['name']);
//        unset($query->query['dlc']);
//
//        $query->query_vars['pagename'] = null;
//        $query->query_vars['page_id'] = null;
//        $query->query_vars['error'] = '';
//
//        $query->set('posts_per_page', -1);
//        $query->set('post_type', ['dlc']);
//        $query->set('post_status', 'publish');
//
//            $GLOBALS['related_game_loop'] = $query;
//            $results_count = $GLOBALS['related_game_loop']->found_posts;
//
//            global $wp;
//            $GLOBALS['current_url'] = home_url( add_query_arg( array(), $wp->request ) );
//            $GLOBALS['related_game'] = substr($GLOBALS['current_url'], strrpos($GLOBALS['current_url'], '/' )+1)."\n";
//
//
//    }
//
//}










































function prefix_dlc_rewrite_rule() {
    add_rewrite_rule(
        'dlc/relatedGame/([^/]+)',
        'index.php?dlc=$matches[1]&relatedGame=yes',
        'top'
    );
}
add_action( 'init', 'prefix_dlc_rewrite_rule' );


function prefix_register_query_var( $vars ) {
    $vars[] = 'relatedGame';

    return $vars;
}
add_filter( 'query_vars', 'prefix_register_query_var' );


add_filter('template_include', 'template_suggestions');
function template_suggestions($template) {

    if( get_query_var( 'relatedGame' ) ){

        $template = locate_template(['archive-dlc.php']);

        // Prevent 404.
        status_header(200);

    }

	return $template;
	
}





/**
 * is_home() will return true on this page, and so might any other condition. This is annoying
 * as all functions meant for the home page will effect this custom page if you don't also check
 * for the custom query_vars
 *
 * To avoid this, set these conditions in the main query to false. This will avoid any extract
 * code and eliminate any unexpected output
 *
 * Set a new conditional tag, $wp_query->is_custom_archive if either of the two query_vars is present
 * @link http://codex.wordpress.org/Plugin_API/Action_Reference/parse_query
 */
//add_filter( 'parse_query', function () {
//
//    global $wp_query;
//
//    //Set new conditional tag
//    $wp_query->is_custom_archive = false;
//
//    if( get_query_var( 'relatedGame' ) ){
//
//        $wp_query->is_single = false;
//        $wp_query->is_page = false;
//        $wp_query->is_archive = true;
//        $wp_query->is_search = false;
//        $wp_query->is_home = false;
//
//    }
//
//});

