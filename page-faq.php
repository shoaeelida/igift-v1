<?php

/**
    * Template Name: faq page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();

if( have_posts() ){

    the_post();
    $contact_page_url = '';

    global $playOptions;
    if( $playOptions ){
        $contact_page_id = $playOptions->getOption( 'contact_page' );
        $contact_page_url = ($contact_page_id) ? get_permalink( $contact_page_id ) : '';
    }

    $faqs = get_field('faq');
    $faqs_items = '';
    $faqs_ans = '';
    $faqs_cats = [];

    if( $faqs ){
        $count = 0;
        foreach ($faqs as $faq ) {
            $count++;
            $title = (isset($faq['title'])) ? $faq['title'] : '';
            $title_short = (isset($faq['title_short']) && !empty($faq['title_short'])) ? $faq['title_short'] : $title;
            $active = ($count == 1) ? ' active' : '';
            $display = ($count == 1) ? ' style="display:block;"' : '';
            $cat = (isset($faq['cat'])) ? $faq['cat']['value'] : '';
            if( isset($faq['cat']) && !in_array($faq['cat']['value'],$faqs_cats) ){
                $faqs_cats[$faq['cat']['value']] = $faq['cat'];
            }
            $faqs_items .= '<div class="faq" data-tab="'.$cat.'">
                <a class="faq-header'.$active.'" data-question="'.$count.'" href="#q-'.$count.'">
                    <span class="faq-num">'.$count.'</span>
                    <span class="faq-title">'.$title.'</span>
                    <svg class="icon" viewBox="0 0 12.56 19.46"><use xlink:href="'.sprite_url.'#arrow"></use></svg>
                </a>
                <div class="faq-content" id="q-'.$count.'" '.$display.'>'.$faq['content'].'</div>
            </div>';
        }
    }
    
    ?>

    <div class="mainHeader mainHeader-help">
        <div class="container">
            <h1 class="title">سوالات <span class="white">متداول</span></h1>
            <?php
            $content = get_the_content();
            echo ( !empty($content) ) ? '<div class="mainHeader-desc">'.$content.'</div>' : ''; 
            ?>
        </div>
    </div>

    <?php if( $faqs_cats ){ ?>
        <div class="tabs tabs-faq">
            <div class="container">
                <div class="tabsContainer">
                    <div class="tab active" data-tab="all">
                        <svg viewBox="0 0 236.46 200.53"><use xlink:href="<?php echo sprite_url; ?>#menu"></use></svg>
                        همه
                    </div>
                    <?php foreach ($faqs_cats as $cat ) {
                        echo '<div class="tab" data-tab="'.$cat['value'].'">'.$cat['label'].'</div>';
                    } ?>
                </div>
            </div>
        </div>
    <?php } ?>
 
    <div class="faqs">
        <div class="container">
            <?php echo $faqs_items; ?>
        </div>
    </div>

    <?php
    if( $contact_page_url ){
        echo '<div class="helpFooter">
            <div class="container position-relative d-flex justify-content-center justify-content-sm-between align-items-center flex-wrap">
                <span class="qSymbol">؟</span>
                <h3 class="title">جواب سوال خود را نیافتید؟ از ما بپرسید...</h3>
                <a class="btn-start btn btn--red btn--large btn--text btn--text--right register" href="'.$contact_page_url.'">
                    ثبت سوال
                    <svg viewBox="0 0 12.56 19.46"><use xlink:href="'.sprite_url.'#arrow"></use></svg>
                </a>
            </div>
        </div>';
    }
    ?>
    

<?php

}

get_footer();