var $ = jQuery;

var log = function (str) {
    console.log(str);
};

var callback_runner = function(callback,data,element) {

    // if is obj
    if( callback ){

        if( callback.includes('.') ){

            // find object
            var callback_obj = callback.substring(0, callback.indexOf('.')); 
            callback_obj = window[callback_obj];

            if( typeof callback_obj === "object"){

                var callback_func = callback.substring(callback.indexOf(".") + 1);
                callback_func = callback_obj[callback_func];

                if( typeof callback_func === "function"){
                    callback_func(data,element);
                }

            }
        
        }
        // if is function
        else{

            var callback_func = window[successCallback];
            if( typeof callback_func === "function"){
                callback_func(data,element);
            }

        }

    }

};

var fetch = {
    fetch_data : function (method,url,param,successCallback,failCallback,processData,contentType,element) {
        console.log(' --> ' + method + ' : ' + url);
        console.log(' --> param' );
        console.log(param);
        if( method && url ){

            var ajax_data = {
                type: method,
                url: url,
                data: param,
                success: function (response) {
                    if( successCallback ){
                        callback_runner(successCallback,response,element);
                    }
                },
                error: function (response) {
                    if( failCallback ){
                        callback_runner(failCallback,response,element);
                    }
                },
                fail: function (response) {
                    if( failCallback ){
                        callback_runner(failCallback,response,element);
                    }
                },
            };

            if( processData !== null || processData == false || processData == true ){
                ajax_data['processData'] = processData;
            }

            if( contentType !== null || contentType == false || contentType == true ){
                ajax_data['contentType'] = contentType;
            }

            jQuery.ajax(ajax_data);
        }
    },
    update : function (url,param,successCallback,failCallback) {
        this.fetch_data('UPDATE',url,param,successCallback,failCallback,null,null,element);
    },
    post : function (url,param,successCallback,failCallback,element) {
        this.fetch_data('POST',url,param,successCallback,failCallback,null,null,element);
    },
    postForm : function (url,param,successCallback,failCallback,element) {
        this.fetch_data('POST',url,param,successCallback,failCallback,false,false,element);
    },
    get : function (url,param,successCallback,failCallback,element) {
        this.fetch_data('GET',url,param,successCallback,failCallback,null,null,element);
    },
};

var play_app = {
    /**
     * wc vaiables
     */
    checkout_url : null,
    is_logged_in : false,
    user_id : null,
    token : null,
    /**
     * api variables
     */
    api_url : null,
    api_recommend_url : null,
    api_addtocart_url : null,
    api_products_url : null,
    api_make_order_url : null,
    /**
     * save data variabels
     */
    cats : null,
    selected_cat : null,
    selected_child_cat : null,
    product_url : null,
    /**
     * other variables
     */
    cats_items : null,
    childs_el : null,
    childs_items : null,
    step1_btn : null,
    search_by_url_form : null,
    submit_product_form : null,
    /**
     * templates
     */
    product_box_template : '<div class="product-box">'+
        '<div class="product-box__cover">{{#if thumbnail_url}}<img src="{{thumbnail_url}}" />{{/if}}</div>'+
        '<div class="product-box__content">'+
            '<header class="product-box__header">'+
                '<h2 class="product-box__title">{{title}}</h2>'+
                '<div class="product-box__subtitle">{{subtitle}}</div>'+
                '<div class="product-box__orginalPrice">{{orginalPrice}}</div>'+
            '</header>'+
            '<div class="product-box__desc">{{desc}}</div>'+
            '<div class="product-box__features"></div>'+
            '<div class="product-box__actions">'+
                '{{#if can_buy}}'+
                    '<div class="product-box__price">{{price_html}}</div>'+
                    '{{add_to_cart}}'+
                '{{else}}'+
                    '<a href="{{url}}" class="button alt trackit" ev-category="product" ev-action="product box" ev-label="{{title}}" ev-value="{{id}}">Info</a>'+
                '{{/if}}'+
            '</div>'+
        '</div>'+
    '</div>',
    product_item_template : '<div class="recommend-product" data-id="{{id}}">'+
        '<a class="trackit" href="{{url}}" ev-category="product" ev-action="recommend product" ev-label="{{title}}" ev-value="{{id}}">{{#if thumbnail_url}}<img src="{{thumbnail_url}}" />{{/if}}{{title}}</a>'+
    '</div>',
    /**
     * init play_app object
     */
    init : function () {
        
        this.is_logged_in = window.is_logged_in;
        this.user_id = (this.is_logged_in) ? window.user_id : null;
        this.token = (this.is_logged_in) ? window.token : null;

        this.checkout_url = window.checkout_url;

        this.api_url = window.api_url;
        this.api_recommend_url = this.api_url + '/recommend';
        this.api_addtocart_url = this.api_url + '/addtocart';
        this.api_products_url = this.api_url + '/products';
        this.api_make_order_url = this.api_url + '/make_order';

        this.cats = window.app_cats;
        this.cats_items = $('.main-cats a');
        this.childs_el = $('.child-cats');
        this.step1_btn = $('.step-1 button[type="submit"]');
        this.search_by_url_form = $('.search-by-url');
        this.submit_product_form = $('.submit-product');

        console.log( 'api_url >> ' + this.api_url );

        console.log( this.cats );

        this.start_order_init();
        this.init_step1_cats();

        this.init_add_to_cart();

        this.init_submit_product();

        this.init_trackers();

    },
    /**
     * 
     * base methods
     */
    redirect : function (url){
        window.location = url;
    },
    htmlCompiler : function (template,data) {

        // compile it with Template7
        var compiledTemplate = Template7.compile(template);
        
        // Now we may render our compiled template by passing required context
        return compiledTemplate(data);

    },
    /**
     * 
     * @param {*} eventCategory 
     * @param {*} eventAction 
     * @param {*} eventLabel 
     * @param {*} eventValue 
     */
    event_tracker : function (eventCategory,eventAction,eventLabel,eventValue) {
        eventCategory = (!eventCategory || eventCategory == '') ? 'general' : eventCategory;
        eventAction = (!eventAction || eventAction == '') ? 'click' : eventAction;
        eventLabel = (!eventLabel || eventLabel == '') ? 'general' : eventLabel;
        eventValue = (!eventValue || eventValue == '') ? 0 : parseInt(eventValue);
        var opt_noninteraction = '';

        ga('send', 'event', {
            eventCategory: eventCategory, //required
            eventAction: eventAction, //required
            eventLabel: eventLabel,
            eventValue: eventValue,
            hitCallback: function() {
                // console.log('Sent!!');
                //callback function
            },
            hitCallbackFail : function () {
                // console.log("Unable to send Google Analytics data");
                //callback function
            }
        });


    },
    init_trackers : function () {
        
        $('.trackit:not(.enabled)').on('click',function () {

            var _this = $(this),
                eventCategory = _this.attr('ev-category'),
                eventAction = _this.attr('ev-action'),
                eventLabel = _this.attr('ev-label'),
                eventValue = _this.attr('ev-value');

            play_app.event_tracker(eventCategory,eventAction,eventLabel,eventValue);

        }).addClass('enabled');

    },
    /**
     * 
     * get methods
     */
    get_product : function ( get_parameter , get_val,successCallback,failCallback) {
        var params = {};
        params[get_parameter] = get_val;
        fetch.get(play_app.api_products_url,params,successCallback,failCallback);
    },
    get_recommends : function (parent_cat,child_cat,url) {
        var params = {
            'parent_cat' : parent_cat,
            'child_cat' : child_cat,
        };
        play_app.render_recommends();
        fetch.get(play_app.api_recommend_url,params,'play_app.after_get_recommend');
    },
    get_child_cats : function (parent_id) {

        if( !play_app.cats || !parent_id ) return false;

        if ( !play_app.cats.hasOwnProperty(parent_id) ) return false;

        return ( play_app.cats[parent_id]['children'] && play_app.cats[parent_id]['children'] !== null ) ? play_app.cats[parent_id]['children'] : false;

    },
    /**
     * other methods
     */
    reset_selected_cats : function () {
        log(' << reset_selected_cats >> ');
        play_app.selected_cat = null;
        play_app.selected_child_cat = null;
        play_app.childs_el.html('');
    },
    add_to_cart : function ($thisbutton,product_id ,product_sku ,quantity , variation_id) {
        // console.log('add to cart : ' + product_id);
        var data = {
            action: 'woocommerce_ajax_add_to_cart',
            product_id: product_id,
            product_sku: product_sku,
            quantity: quantity,
            variation_id: variation_id,
        };

        $(document.body).trigger('adding_to_cart', [$thisbutton, data]);

        fetch.post( play_app.api_addtocart_url, data, 'play_app.success_added_to_cart','play_app.failed_added_to_cart',$thisbutton);

    },
    success_added_to_cart: function (response,$thisbutton) {
        if( response['status'] == 'successful' ){
            if( response.hasOwnProperty('pay_url') ){
                // log(response['pay_url']);
                play_app.redirect(response['pay_url']);
            }
        }
        $thisbutton.addClass('added').removeClass('loading');
    },
    failed_added_to_cart: function (response,$thisbutton) {
        log('cant add to cart');

        // if (response.error & response.product_url) {
        //     // window.location = response.product_url;
        //     // return;
        // } else {
        //     $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $thisbutton]);
        // }
    },
    init_add_to_cart : function () {
        $('form.cart').submit(function (e) {
            e.preventDefault();
            var $form = $thisbutton.closest('form.cart'),
                $thisbutton = $form.find('button'),
                id = $thisbutton.val(),
                product_qty = $form.find('input[name=quantity]').val() || 1,
                product_id = $form.find('input[name=product_id]').val() || id,
                variation_id = $form.find('input[name=variation_id]').val() || 0;
            
            log('product_id >> ');
            log(product_id);

            if( !$thisbutton.hasClass('added') ){
                play_app.add_to_cart($thisbutton,product_id);
            }
            return false;
        });
        $('.single_add_to_cart_button:not(.enabled)').on('click',function (e) {
            e.preventDefault();
            var $thisbutton = $(this),
                    $form = $thisbutton.closest('form.cart'),
                    id = $thisbutton.val(),
                    product_qty = $form.find('input[name=quantity]').val() || 1,
                    product_id = $form.find('input[name=product_id]').val() || id,
                    variation_id = $form.find('input[name=variation_id]').val() || 0;
            
            if( !$thisbutton.hasClass('added') ){
                play_app.add_to_cart($thisbutton,product_id);
            }

            return false;
        }).addClass('enabled');  
    },
    init_child_cats : function () {
        
        play_app.childs_items = play_app.childs_el.find('a');

        if( play_app.childs_items.lenght <= 0 ) return false;

        play_app.childs_items.on('click',function (e) {

            e.preventDefault();

            var _this = $(this);
            var _id = _this.attr('data-id');

            play_app.selected_child_cat = _id;

            play_app.toogle_step1_btn('active');

        });

    },
    /**
     * steps methods
     */
    start_order_init: function () {
        $('.start-order').on('click',function (e) {
            e.preventDefault();
            if( play_app.is_logged_in ){
                play_app.toggle_step(1,'show');
            }else{
                // jQuery
                // show_digits_login_modal($this);
                $(this).digits_login_modal($(this));
            }
            return false;
        });
    },
    init_step1_cats : function () {

        if( play_app.cats_items.lenght <= 0 ) return false;

        // reset selected cats
        play_app.reset_selected_cats();

        play_app.cats_items.on('click',function (e) {

            e.preventDefault();

            play_app.toogle_step1_btn('deactive');

            var _this = $(this);
            var _id = _this.attr('data-id');
            var _childs = play_app.get_child_cats(_id);

            play_app.selected_cat = _id;
    
            if( _childs ){

                var _html = '';
                for (let key in _childs){
                    if(_childs.hasOwnProperty(key)){
                        _html += '<a class="child-cat trackit" data-id="'+_childs[key].term_id+'" ev-category="cats" ev-action="select child cat" ev-label="'+_childs[key].name+'" ev-value="'+_childs[key].term_id+'">'+_childs[key].name+'</a> ';
                    }
                }

                play_app.childs_el.html( _html );

                if( _html && _html !== '' ){

                    play_app.childs_el.fadeIn('fast');
                    play_app.init_child_cats();

                }else{
                    play_app.childs_el.fadeOut('fast');
                }

            }else{

                play_app.toogle_step1_btn('active');

            }

        });

        play_app.step1_btn.on('click',function (e) {
            e.preventDefault();
            if( $(this).not('[disabled]') ){

                // get and show recommends
                play_app.get_recommends(play_app.selected_cat,play_app.selected_child_cat);

                // init search by url
                play_app.init_search_by_url();

                // show step 1
                play_app.toggle_step(2,'show');

            }
        });

    },
    toogle_step1_btn : function (action) {
        switch (action) {
            case 'active':
                play_app.step1_btn.prop('disabled',false).fadeIn('fast');
                break;
            case 'deactive':
                play_app.step1_btn.prop('disabled',true).fadeOut('fast');
                break;
            default:
                break;
        }
    },
    toggle_step : function (step,action) {
        log('toggle_step step >> ' + step + ' action >> ' + action);
        var steps_count = 4;
        // if( step > 0 && step <= steps_count ){
            switch (action) {
                case 'show':
                    if( step == 1 ){

                        // deactive step 1 button
                        play_app.toogle_step1_btn('deactive');

                        // reset selected cats 
                        play_app.reset_selected_cats();

                        setTimeout(function () {
                            $('.step-'+step).fadeIn('fast');
                        },300);
                    }else{
                        $('.step-'+step).fadeIn('fast');
                    }

                    // hide other steps
                    for (var i = 1; i <= steps_count; i++) {
                        if( i != step ) {
                            $('.step-'+i).fadeOut('fast');
                        }
                    }
                    break;
                case 'hide':
                    $('.step-'+step).fadeOut('fast');
                    break;
                default:
                    break;
            }
            
        // }
    },
    show_product_modal : function (product_id,product_url) {
        if( product_id ){
            play_app.get_product('product_id',product_id,'play_app.after_get_modal_product','play_app.cant_get_modal_product');
        }else if( product_url ){
            play_app.get_product('product_url',product_url,'play_app.after_get_modal_product','play_app.cant_get_modal_product');
        }
    },
    after_get_modal_product : function (product) {
        if( product ){
            console.log(product);
            $('.step-3').html( play_app.htmlCompiler(play_app.product_box_template,product) );
            play_app.toggle_step(3,'show');
            play_app.init_trackers();
            play_app.init_add_to_cart();
        }
        play_app.search_by_url_form.prop('disabled',false);
    },
    cant_get_modal_product : function (params) {
        console.log('cant_get_modal_product >>>>>>>> ');
        play_app.search_by_url_form.prop('disabled',false);
        play_app.toggle_step('4','show');
    },
    init_search_by_url: function () {
        play_app.search_by_url_form.submit(function (e) {

            e.preventDefault();

            var _this = $(this),
                _url = _this.find('input[type="text"]').val();

            play_app.product_url = _url;

            if( !_this.prop('disabled') ){

                _this.prop('disabled',true);

                play_app.show_product_modal(null,play_app.product_url);

            }

            return false;

        });
    },
    init_submit_product: function () {
        play_app.submit_product_form.submit(function (e) {
            
            e.preventDefault();

            var _this = $(this);
                // _url = _this.find('input[type="text"]').val();

            // log( 'form is >> ' +  _this.prop('disabled'));
            var form_data = new FormData();

            _this.find('input,textarea').each(function (e) {
                form_data.append($(this).attr('name'), $(this).val())
            });

            // set selected_cat
            form_data.append('selected_cat', play_app.selected_cat)

            // set selected_child_cat
            form_data.append('selected_child_cat', play_app.selected_child_cat)

            // set product url
            form_data.append('product_url', play_app.product_url)

            if( !_this.prop('disabled') ){

                _this.prop('disabled',true);

                fetch.postForm(play_app.api_make_order_url,form_data,'play_app.successSubmit_product','play_app.failSubmit_product');

            }

            return false;

        });
    },
    successSubmit_product: function(response){
        
        console.log('successSubmit_product >> ');
        console.log(response);

        if( response['status'] == 'successful' ){
            
            if( response.hasOwnProperty('pay_url') ){
                play_app.redirect(response['pay_url']);
            }
            
        }

        play_app.submit_product_form.prop('disabled',false);

    },
    failSubmit_product: function(response){
        console.log('failSubmit_product >> ');
        console.log(response);
        play_app.submit_product_form.prop('disabled',false);
    },
    init_recommend_products: function () {
        $('.recommend-product').on('click',function (e) {

            e.preventDefault();

            var _this = $(this),
                _id = _this.attr('data-id');
            play_app.show_product_modal(_id);

        });
    },
    render_recommends : function (products) {

        var _html = '';
        if( products ){
            products.forEach(function (product) {
                _html += play_app.htmlCompiler(play_app.product_item_template,product);
            });
        }
        return _html;
    },
    after_get_recommend : function (response) {
        $('.search-recommends').html( play_app.render_recommends(response) );
        play_app.init_trackers();
        play_app.init_add_to_cart();
        play_app.init_recommend_products();
    }, 
};


(function ($) {
    'use strict';

    /**
     * Document ready
     */
    $(function () {

        play_app.init();

    });

})
(jQuery);