<?php get_header(); ?>

<div class="mainHeader mainHeader-help">
    <div class="container">
        <div class="title">راهنمای <span class="white">خرید</span></div>
    </div>
</div>

<?php get_template_part( 'template-parts/parts/help', 'nav' ); ?>

<div class="help" id="help-box">
    <div class="container">
            
        <?php
        if( have_posts() ){
            while ( have_posts() ) {
                the_post();
                ?>

                <div class="answer active" style="display:block;">
                    <h1 class="title"><?php the_title(); ?></h1>
                    <div class="simple-content">
                        <?php the_content(); ?>
                    </div>
                </div>

                <?php
            }
        }
        ?>

    </div>
</div>

<?php get_template_part( 'template-parts/parts/help', 'help' ); ?>

<?php get_footer(); ?>