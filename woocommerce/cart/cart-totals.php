<?php
/**
 * Cart totals
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-totals.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 2.3.6
 */

defined( 'ABSPATH' ) || exit;

global $playOptions;
$cart_count = WC()->cart->get_cart_contents_count();
$subtotal = WC()->cart->get_subtotal();
$base_fee = ( $playOptions ) ? (float)$playOptions->getOption( 'sale_fee_percent' ) : 0;
$total_fee = $subtotal * ( $base_fee / 100 );
$subtotal_without_fee = $subtotal - $total_fee;
$coupon = (isset(WC()->cart->applied_coupons[0])) ? WC()->cart->applied_coupons[0] : '';
$discount = WC()->cart->get_coupon_discount_amount($coupon);
$discount_percentage = (100*$discount) / ($subtotal);
?>

<?php do_action( 'woocommerce_before_cart_totals' ); ?>

<div class="subtotal-detail <?php echo ( WC()->customer->has_calculated_shipping() ) ? 'calculated_shipping' : ''; ?>">
	<div class="detail-row">
		<div class="title">قیمت کالاها
			<span class="count"><?php echo $cart_count; ?></span>
		</div>
		<div class="amount">
			<span><?php echo play_price($subtotal_without_fee); ?></span> تومان
		</div>
	</div>
	<?php if( $discount && (float)$discount > 0 ){ ?>
		<div class="detail-row discount">
			<div class="title">تخفیف کالاها
			</div>
			<div class="amount">
				<b class="off-range"><?php echo round($discount_percentage) ?>%</b>
				<span><?php echo play_price($discount) ?></span> تومان
			</div>
		</div>
	<?php } ?>
	<div class="detail-row wage">
		<div class="title">کارمزد آی گیفت
		</div>
		<div class="amount">
			<span><?php echo play_price($total_fee); ?></span> تومان
		</div>
	</div>
</div>
<div class="final-payment">
	<div class="detail-row">
		<div class="title">مبلغ قابل پرداخت
		</div>
		<div class="amount">
			<span>
			<?php
			if ( $discount ) {
				echo play_price(WC()->cart->get_subtotal() - $discount);
			}
			else {
				echo play_price(WC()->cart->get_subtotal());
			}
			?>
			</span> تومان
		</div>
	</div>
	<?php

	if (is_user_logged_in()) {
		do_action( 'woocommerce_proceed_to_checkout' );
	} else {
		echo '<a id="cart-login-link" href="#" class="btn btn--large btn--red btn--text--right">
				انتقال به درگاه و خرید
				<svg viewBox="0 0 12.56 19.46">
					<use xlink:href="'.sprite_url.'#arrow"></use>
				</svg>
				</a>'
		;
	}
	?>
</div>

<?php do_action( 'woocommerce_after_cart_totals' ); ?>