<?php
/**
 * Proceed to checkout button
 *
 * Contains the markup for the proceed to checkout button on the cart.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/proceed-to-checkout-button.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
/*
<a href="<?php echo esc_url( wc_get_checkout_url() ); ?>" class="checkout-button button alt wc-forward">
	<?php esc_html_e( 'Proceed to checkout', 'woocommerce' ); ?>
</a>*/

if( is_cart() ){
	if( !is_user_logged_in() ){ ?>

		<a href="#regModal" class="btn btn--large btn--red btn--text--right">
			ورود و ادامه خرید
			<svg viewBox="0 0 12.56 19.46">
				<use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
			</svg>
		</a>

		<?php
	}else{
		?>
	
		<a href="<?php echo esc_url( wc_get_checkout_url() ); ?>" class="btn btn--large btn--red btn--text--right">
				ادامه فرآیند خرید
			<svg viewBox="0 0 12.56 19.46">
				<use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
			</svg>
		</a>

		<?php
	}
}elseif( is_checkout() ){

	do_action( 'woocommerce_review_order_before_submit' );
	
	$order_button_text = 'انتقال به درگاه و خرید';

	echo apply_filters( 'woocommerce_order_button_html', '<button type="submit" class="btn btn--large btn--red btn--text--right go-to-gateway" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '">' . esc_html( $order_button_text ) . '<svg viewBox="0 0 12.56 19.46"><use xlink:hrvg#arroef="'.theme_assets.'/imgs/sprite.sw"></use></svg>' . '</button>' ); // @codingStandardsIgnoreLine

	do_action( 'woocommerce_review_order_after_submit' );

}