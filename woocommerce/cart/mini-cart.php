<?php
/**
 * Mini-cart
 *
 * Contains the markup for the mini-cart, used by the cart widget.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/mini-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>

<div class="mini-cart">

	<?php do_action( 'woocommerce_before_mini_cart' ); ?>

	<?php if ( ! WC()->cart->is_empty() ) : ?>

			<div class="order-list">
				<div class="list-holder">
					<?php
					do_action( 'woocommerce_before_mini_cart_contents' );

					foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
						$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
						$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
						if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
							$thumbnail         = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
							// $product_price     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
							$product_price     = WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] );
							// $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
							
							$product_obj = get_wc_product_obj($product_id);
							if( $product_obj ){
								$product_name      = $product_obj['title'];
								
								if( $product_obj['product_type'] == 'giftcard' ){
									$product_name = $product_obj['main_title'];
								}

								?>
								<div class="order-item" data-id="<?php echo $product_id; ?>" data-key="<?php echo $cart_item['key']; ?>">
									<a href="<?php echo $product_obj['url'] ?>" title="<?php echo $product_name; ?>">
										<img class="order-img" src="<?php echo $product_obj['thumbnail_url']; ?>" alt="" />
									</a>
									<div class="order-detail">
										<div class="order-name">
											<h2>
												<a href="<?php echo $product_obj['url'] ?>" title="<?php echo $product_name; ?>"><?php echo $product_name; ?></a>
											</h2> 
											<span>  
												<?php echo (isset($product_obj['region']['name'])) ? 'ریجن ' . $product_obj['region']['name'] . ' ' : ''; ?>
												<?php echo (isset($product_obj['edition']) && !empty($product_obj['edition'])) ? ' - ' . $product_obj['edition'] : ''; ?>
											</span>
											<span class="redText">
												<?php 
												$product_perfix = (isset($product_obj['product_type'])) ? product_type_perfix($product_obj['product_type']) : '';
												echo (isset($product_obj['platform']['name'])) ? $product_perfix . ' ' . $product_obj['platform']['name'] : '';
												?>
											</span>
										</div>
										<div class="order-price">
											<div class="price-amount">
												<!-- <div class="count">
													<span><?php echo $cart_item['quantity']; ?></span>
													عدد
												</div> -->
												<div class="price">
													<span><?php echo play_price($cart_item['line_subtotal']); ?></span>
													تومان
												</div>
											</div>
											<?php
											echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
												'woocommerce_cart_item_remove_link',
												sprintf(
													'<a href="%s" class="delete-order remove remove_from_cart_button" aria-label="%s" data-product_id="%s" data-cart_item_key="%s" data-product_sku="%s"><svg viewBox="0 0 75.29 75.29"><use xlink:href="'.sprite_url.'#close"></use></svg> حذف</a>',
													esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
													esc_attr__( 'Remove this item', 'woocommerce' ),
													esc_attr( $product_id ),
													esc_attr( $cart_item_key ),
													esc_attr( $_product->get_sku() )
												),
												$cart_item_key
											);
											?>
										</div>
									</div>
								</div>
								<?php
							}
						}
					}

					do_action( 'woocommerce_mini_cart_contents' );
					?>

				</div>
			</div>
			<div class="sum-total">
				<div class="total-price">
					مجموع
					<span><?php echo play_price(WC()->cart->get_subtotal()); ?></span>
					تومان
				</div>		
				<?php do_action( 'woocommerce_widget_shopping_cart_before_buttons' ); ?>
				<a class="btn btn--red " href="<?php echo wc_get_cart_url(); ?>">ادامه خرید</a>
				<?php do_action( 'woocommerce_widget_shopping_cart_after_buttons' ); ?>
			</div>

	<?php else : ?>

		<div class="noData">

			<?php echo play_render_image(theme_assets.'/imgs/nodata.svg','','image'); ?>
			<span class="title">سبد خرید شما خالی است!</span>
			<span class="text">
				سبد خریدتان را با
				<a class="link" href="<?php echo get_post_type_archive_link( 'game' ); ?>">
					<strong>بازی ها</strong>
				</a>
				و 
				<a class="link" href="<?php echo get_post_type_archive_link( 'giftcard' ); ?>">
					<strong>گیفت کارت های</strong>
				</a>
				متنوع پر کنید!
			</span>
		</div>

	<?php endif; ?>

	<?php do_action( 'woocommerce_after_mini_cart' ); ?>

</div>
