<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;

?>

<div class="cart-content">
    <?php $notices = WC()->session->get('wc_notices'); ?>
        <form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
            <div class="mainContainer">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="cart-item__holder" id="cart-items-holder">

                            <?php do_action( 'woocommerce_before_cart' );
                            $error = isset($notices["error"]) ? $notices["error"] : null;
                            $success = isset($notices["success"]) ? $notices["success"] : null;
                            ?>
                            <?php if ( $error || $success ) { ?>
                            <div class="notification-holder">
                                <div class="notify-item">
                                    <?php
                                    if ($error) {
                                        echo '<svg class="notify-icon" viewBox="0 0 119.53 107.51">
                                        <use xlink:href="' . sprite_url . '#warning"></use></svg>';
                                        echo '<span class="notify-text">' . $error[0]["notice"] . ' </span>';
                                    } elseif ($success) {
                                        echo '<svg class="notify-icon" viewBox="0 0 128 128">
                                        <use xlink:href="' . sprite_url . '#success"></use></svg>';
                                        echo '<span class="notify-text">' . $success[0]["notice"] . ' </span>';
                                    }
                                    ?>
                                    <span class="notify-caption"></span>
                                    <span class="notify-close">
                                        <svg viewBox="0 0 75.29 75.29">
                                            <use xlink:href="<?php echo sprite_url; ?>#close"></use>
                                        </svg>
                                    </span>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="cart-item__list">
                                <div class="cart-item-count">
                                    <span><?php echo WC()->cart->get_cart_contents_count(); ?></span> آیتم در سبد خرید شما وجود دارد
                                </div>
                                <?php
                                /*<div class="cart-item-count">
                                    <span><?php echo WC()->cart->get_cart_contents_count(); ?></span> آیتم در سبد خرید شما وجود دارد
                                </div>*/
                                ?>

                                <?php do_action( 'woocommerce_before_cart_table' ); ?>
                                <?php do_action( 'woocommerce_before_cart_contents' ); ?>
                                <?php
                                foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                                    $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                                    $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

                                    if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                                        $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                                        $product_obj = get_wc_product_obj($product_id);
                                        if( $product_obj ){
                                            ?>
                                            <div class="cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?><?php echo ($product_obj['product_type'] == 'giftcard') ? ' '.'gift' : ''?>">
                                                <a href="<?php echo esc_url( $product_permalink ); ?>" class="product-img">
                                                    <?php
                                                    // $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
                                                    // echo $thumbnail; // PHPCS: XSS ok.
                                                    ?>
                                                    <img src="<?php echo $product_obj['thumbnail_url']; ?>" />
                                                    <div class="cover-img">
                                                        <div class="icon">
                                                            <svg viewBox="0 0 77.34 77.34">
                                                                <use xlink:href="<?php echo sprite_url; ?>#flash"></use>
                                                            </svg>
                                                        </div>
                                                        <div class="caption" <?php echo ($product_obj['product_type'] == 'giftcard') ? 'style="padding-right: 10px;padding-left: 10px;"' : ''; ?>>
                                                            <?php
                                                            $type_name = 'بازی';
                                                            switch ($product_obj['product_type']) {
                                                                case 'giftcard':
                                                                    $type_name = 'گیفت کارت';
                                                                    break;
                                                                default:
                                                                    break;
                                                            }
                                                            echo 'صفحه ' . $type_name;
                                                            ?>
                                                        </div>
                                                    </div>
                                                </a>
                                                    <div class="product-name">
                                                        <h2>
                                                            <?php
                                                            $title = $product_obj['title'];
                                                            if( $product_obj['product_type'] == 'giftcard' ){
                                                                $title = $product_obj['main_title'];
                                                            }
                                                            if ( ! $product_permalink ) {
                                                                echo wp_kses_post( $title . '&nbsp;' );
                                                            } else {
                                                                echo wp_kses_post( sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $title ) );
                                                            }
                                                            ?>
                                                            <span class="name-caption">
                                                                <?php
                                                                echo (isset($product_obj['edition']) && !empty($product_obj['edition'])) ? $product_obj['edition'] : '';
                                                                ?>
                                                            </span>
                                                        </h2>
                                                        <?php
                                                        echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
                                                            'woocommerce_cart_item_remove_link',
                                                            sprintf(
                                                                '<a href="%s" class="delete-product remove_from_cart_button" aria-label="%s" data-product_id="%s" data-product_sku="%s"><svg viewBox="0 0 75.29 75.29"><use xlink:href="'.sprite_url.'#close"></use></svg></a>',
                                                                esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
                                                                esc_html__( 'Remove this item', 'woocommerce' ),
                                                                esc_attr( $product_id ),
                                                                esc_attr( $_product->get_sku() )
                                                            ),
                                                            $cart_item_key
                                                        );
                                                        ?>
                                                    </div>
                                                    <div class="item-bottom">
                                                        <div class="product-platform">
                                                            <?php
                                                            if( $product_obj['product_type'] == 'giftcard' ){
                                                                echo (isset($product_obj['region']['name'])) ? '<span class="platform-name"><b class="rigion">ریجن ' . $product_obj['region']['name'] . '</b></span> ' : '';
                                                            }else{
                                                                if( isset($product_obj['platform']) && ( $product_obj['platform']['name'] || $product_obj['platform']['icon'] ) ) { ?>
                                                                    <span class="platform-icon">
                                                                        <?php echo $product_obj['platform']['name']; ?>
                                                                        <?php echo (isset($product_obj['platform']['icon'])) ?  '<img src="'.$product_obj['platform']['icon'].'" alt="'.$product_obj['platform']['name'].'">' : ''; ?>
                                                                    </span>
                                                                    <span class="platform-name">
                                                                        <?php
                                                                        $product_perfix = (isset($product_obj['product_type'])) ? product_type_perfix($product_obj['product_type']) : '';
                                                                        echo (isset($product_obj['platform']['name'])) ? $product_perfix . ' ' . $product_obj['platform']['name'] : '';
                                                                        $shared_capacity = product_capacity_name($product_obj['shared_capacity']);
                                                                        echo (!empty($shared_capacity)) ? ' ' . $shared_capacity : '';
                                                                        echo (isset($product_obj['region']['name'])) ? '<b class="rigion">ریجن ' . $product_obj['region']['name'] . '</b>' : ''; 
                                                                        ?>
                                                                    </span>
                                                                    <?php
                                                                }
                                                            }

                                                            ?>
                                                        </div>
                                                        <!--
													<div class="product-count">
														<button class="increase">+</button>
														<?php
                                                        if ( $_product->is_sold_individually() ) {
                                                            $product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
                                                        } else {
                                                            $product_quantity = woocommerce_quantity_input(
                                                                array(
                                                                    'input_name'   => "cart[{$cart_item_key}][qty]",
                                                                    'input_value'  => $cart_item['quantity'],
                                                                    'max_value'    => $_product->get_max_purchase_quantity(),
                                                                    'min_value'    => '0',
                                                                    'product_name' => $product_obj['title'],
                                                                ),
                                                                $_product,
                                                                false
                                                            );
                                                        }
                                                        echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
                                                        ?>
														<button class="decrease">-</button>
													</div>
													-->
                                                        <div class="product-price">
                                                            <!-- <span class="off-range"><b>75000</b> تومان تخفیف</span> -->
                                                            <!-- <div class="sale-price"> -->
                                                            <!-- <span class="price">780,000</span> تومان -->
                                                            <!-- <div class="price-increase">
																<svg viewBox="0 0 119.53 107.51">
																	<use xlink:href="<?php echo sprite_url; ?>#warning"></use>
																</svg>
																<span class="msg">
																		<b>85000</b> تومان افزایش قیمت داشته است
																	</span>
															</div> -->
                                                            <!-- </div> -->
                                                            <?php
                                                            echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
                                                            ?>
                                                        </div>
                                                    </div>
                                            </div>
                                            <?php
                                        }

                                    }
                                }
                                ?>
                                <?php do_action( 'woocommerce_cart_contents' ); ?>
                                <?php do_action( 'woocommerce_after_cart_contents' ); ?>
                                <?php do_action( 'woocommerce_after_cart_table' ); ?>

                                <?php if ( wc_coupons_enabled() ) { ?>
                                    <div class="discount-code">
                                        <strong class="title">
                                            کد تخفیف دارید؟
                                            <span>آنرا در کادر روبرو وارد کنید</span>
                                        </strong>
                                        <div class="input-holder">
                                            <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="<?php echo (isset(WC()->cart->applied_coupons[0])) ? WC()->cart->applied_coupons[0] : ''; ?>" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" />
                                            <?php do_action( 'woocommerce_cart_coupon' ); ?>
                                            <svg viewBox="0 0 234.64 241.32">
                                                <use xlink:href="<?php echo sprite_url ?>#percentage"></use>
                                            </svg>
                                        </div>
                                        <button type="submit" class="btn btn--small btn-silver" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?></button>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="cart-total">
                                <a class="btn btn--small btn--transparent" href="<?php echo site_url(); ?>?clear-cart=true" >
                                    لغو سفارش ها
                                </a>
<!--                                <button type="submit" class="btn btn--small btn--transparent" name="update_cart" value="<?php /*esc_attr_e( 'Update cart', 'woocommerce' ); */?>"><?php /*esc_html_e( 'Update cart', 'woocommerce' ); */?></button>
-->
                                <?php do_action( 'woocommerce_cart_actions' ); ?>

                                <?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>

                                <div class="total-pay">
                                    مجموع
                                    <span class="price"><?php
                                        echo play_price(WC()->cart->get_subtotal());
                                        ?></span>
                                    <span class="currency">تومان</span>
                                </div>
                            </div>
                            <?php do_action( 'woocommerce_after_cart' ); ?>

                        </div>

                        <?php
                        if( !is_user_logged_in() ) { ?>
                            <div class="instant-registration mt-0" id="cart-login-form" style="display: none">
                                <div class="mainContainer">
                                    <div class="registration-wrapper">
                                        <div class="container registration-step active" id="login-step1">
                                            <div class="registration-step1">
                                                <span class="input-lbl">شماره تلفن همراه</span>
                                                <div class="input-holder">
                                                    <input type="text" placeholder="مانند : 09379998877" id="login-phone-input"/>
                                                    <svg viewBox="0 0 301.81 301.81">
                                                        <use xlink:href="<?php echo sprite_url ?>#phone"></use>
                                                    </svg>
                                                </div>
                                                <div class="btn btn--red btn--small mt-2" id="send-otp-error" style="display:none;width: 100%"></div>
                                                <div class="condition-tip">
                                                    <div class="tip-icon">
                                                        <svg viewBox="0 0 359.33 359.33">
                                                            <use xlink:href="<?php echo sprite_url ?>#question"></use>
                                                        </svg>
                                                    </div>
                                                    پس از وارد کردن شماره تلفن همراه ، روی دکمه “تایید شماره
                                                    همراه” کلیک کنید.
                                                </div>
                                                <button type="button" id="login-phone-button" class="btn btn--dark btn--small step-btn">
                                                    تایید شماره همراه
                                                </button>
                                            </div>
                                        </div>
                                        <div class="container registration-step" id="login-step2" style="display: none">
                                            <div class="registration-step2 loginVerifyCtd" data-time="01:00">
                                                <div class="condition-icon">
                                                    <svg viewBox="0 0 306.78 320.04">
                                                        <use xlink:href="<?php echo sprite_url ?>#msg"></use>
                                                    </svg>
                                                </div>
                                                <div class="condition-description">
                                                    آی گیفت پیامکی برای شما ارسال کرده است
                                                    <span>کد تاییدیه 6 رقمی ذکر شده را در کادر زیر وارد کنید</span>
                                                </div>
                                                <div class="input-holder">
                                                    <input type="text" placeholder="" id="verify-phone-input"/>
                                                    <svg viewBox="0 0 246.35 314.83">
                                                        <use xlink:href="<?php echo sprite_url ?>#lock"></use>
                                                    </svg>
                                                </div>
                                                <div class="btn btn--red btn--small mt-2" id="verify-otp-error" style="display:none;"></div>
                                                <div class="btn-holder">
                                                    <button type="button" id="login-step-1" class="change-number btn btn--transparent btn--small step-btn" data-target="registration-step1">
                                                        تغییر شماره
                                                    </button>
                                                    <button type="button" id="verify-phone-button" class="btn btn--small btn--red btn--text--right step-btn" data-target="registration-step3">
                                                        تایید
                                                        <svg viewBox="0 0 12.56 19.46">
                                                            <use xlink:href="<?php echo sprite_url ?>#arrow"></use>
                                                        </svg>
                                                    </button>
                                                </div>
                                                <div class="resend verify-resend" style="display: none">
                                                    پیامکی را دریافت نکردید ؟
                                                    <button type="button" class="verify-resend-btn" data-phone="">ارسال مجدد</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container registration-step" id="login-step3" style="display:none;">
                                            <div class="registration-step3">
                                                <span class="input-lbl">شماره تلفن همراه</span>
                                                <div class="input-holder confirmed">
                                                    <div class="confirmed-tag">
                                                        <svg viewBox="0 0 63.23 42.44">
                                                            <use xlink:href="<?php echo sprite_url ?>#checkmark"></use>
                                                        </svg>
                                                        تایید شده
                                                    </div>
                                                    <input type="text" id="confirmed-phone" value="" disabled />
                                                    <svg viewBox="0 0 301.81 301.81">
                                                        <use xlink:href="<?php echo sprite_url ?>#phone"></use>
                                                    </svg>
                                                </div>
                                                <div class="complete-msg">
                                                    به آی گیفت خوش آمدید! شما با این شماره در آی گیفت ثبت نام شدید
                                                    <span>اکنون میتوانید خرید خود را تکمیل کنید</span><br>
                                                    <span>در حال انتقال به صفحه پرداخت ...</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                    </div>

                    <div class="col-lg-3 ">

                        <?php do_action( 'woocommerce_before_cart_collaterals' ); ?>

                        <div class="subtotal">
                            <?php
                            /**
                             * Cart collaterals hook.
                             *
                             * @hooked woocommerce_cross_sell_display
                             * @hooked woocommerce_cart_totals - 10
                             */
                            do_action( 'woocommerce_cart_collaterals' );
                            ?>
                        </div>

                    </div>

                </div>
            </div>
        </form>
</div>