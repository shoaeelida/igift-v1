<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<?php
do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
	return;
}

$cart_products = cart_products();
$platforms = cart_platforms();
$ip_info = ip_info();
$direct_buy = false;
if ( $cart_products ) {
	foreach ($cart_products as $product_obj) {
		if( $product_obj['product_type'] == 'direct' ){
			if( !$direct_buy ){
				$direct_buy = array();
			}
			$direct_buy[] = $product_obj;
		}
	}
}
?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<div class="cart-content">
		<div class="mainContainer checkoutContainer">
			<div class="row">
				<div class="col-lg-9">
					<div class="activation-content">
						<div class="activation-title">
							<h2>شیوه فعالسازی رو انتخاب کنید</h2>
							<!-- <div class="guid">
								<svg class="guid-icon" viewBox="0 0 359.33 359.33">
									<use xlink:href="<?php echo sprite_url; ?>#question"></use>
								</svg>
								<div class="guid-text">کدوم بهتره؟</div>
							</div> -->
						</div>
						<div class="activation-method__holder">
							<?php if( $platforms && !empty($platforms) ){ ?>
								<div class="direct-method box-part">
									<div class="activation-platform">
										<?php foreach ($platforms as $platform) { 
											$user_name = $platform['slug'] . '_user';
											$pass_name = $platform['slug'] . '_pass';
											$has_submit_errors = (isset($_GET['fill-platforms'])) ? true : false;
											?>
											<div class="platform-item">
												<div class="platform-icon">
													<?php echo (isset($platform['icon']) && !empty($platform['icon'])) ? '<img src="'.$platform['icon'].'" alt="'.$platform['name'].'">' : ''; ?>
													<span class="platform-name"><?php echo $platform['name']; ?></span>
												</div>
												<div class="input-row">
													<div class="input-by-title">
														<span class="input-label">نام کاربری <?php echo $platform['name']; ?></span>
														<input type="text" name="<?php echo $user_name; ?>" placeholder="نام کاربری <?php echo $platform['name']; ?> را وارد کنید" value="<?php echo ( isset($_GET[$user_name]) && $_GET[$user_name] !== 'error' ) ? $_GET[$user_name] : ''; ?>" <?php echo ( $has_submit_errors && ( !isset($_GET[$user_name]) || $_GET[$user_name] == 'error' ) ) ? 'class="error"' : ''; ?> required autocomplete="off" disabled/>
														<svg class="input-icon" viewBox="0 0 209.81 238.81">
																<use xlink:href="<?php echo sprite_url; ?>#user"></use>
														</svg>
													</div>
													<div class="input-by-title">
														<span class="input-label">رمز عبور <?php echo $platform['name']; ?></span>
														<input type="text" name="<?php echo $pass_name; ?>" placeholder="رمز عبور <?php echo $platform['name']; ?> را وارد کنید" value="<?php echo ( isset($_GET[$pass_name]) && $_GET[$pass_name] !== 'error' ) ? $_GET[$pass_name] : ''; ?>" <?php echo ( $has_submit_errors && (!isset($_GET[$pass_name]) || $_GET[$pass_name] == 'error') ) ? 'class="error"' : ''; ?> required autocomplete="off" disabled/>
														<svg class="input-icon" viewBox="0 0 246.35 314.83">
																<use xlink:href="<?php echo sprite_url; ?>#lock"></use>
														</svg>
													</div>
													<?php if ( $has_submit_errors ){
														if ( !isset($_GET[$user_name]) || $_GET[$user_name] == 'error' ){ ?>
															<div class="info error">
																<div class="icon">!</div>
																لطفا نام کاربری  <?php echo $platform['name']; ?> را وارد کنید
															</div>
														<?php }
														if ( !isset($_GET[$pass_name]) || $_GET[$pass_name] == 'error' ){ ?>
															<div class="info error">
																<div class="icon">!</div>
																لطفا رمز عبور <?php echo $platform['name']; ?> را وارد کنید
															</div>
														<?php }
													}
													else{ ?>
														<div class="info">
															<div class="icon">؟</div>
															رمز عبور را بدون “فاصله” وارد کنید. 
															<a href="#">حساب اوریجین ندارم چیکار کنم؟</a> 
														</div>
													<?php } ?>
												</div>
											</div>
										<?php } ?>
									</div>
									<div class="warning-holder">
										<p>
											<svg viewBox="0 0 119.53 107.51">
												<use xlink:href="<?php echo sprite_url; ?>#warning"></use>
											</svg>
											فرآیند خرید بین 1 تا 24 ساعت طول میکشه!
										</p>
										<p>
											<svg viewBox="0 0 119.53 107.51">
												<use xlink:href="<?php echo sprite_url; ?>#warning"></use>
											</svg>
											لطفا تنظیمات امنیتی اکانت خود را غیر فعال کن.
										</p>
										<p>
											<svg viewBox="0 0 119.53 107.51">
												<use xlink:href="<?php echo sprite_url; ?>#warning"></use>
											</svg>
											اگر امکان تغییر ریجن در اکانت شما وجود نداشته باشد برای
											شما اکانت جدید با یک ریجن دیگه ساخته میشه..!
										</p>
									</div>
								</div>
							<?php }
							if ( $direct_buy && !empty($direct_buy) ) {
								$printed_platforms = [];
								?>
								<div class="directbuy-method box-part">
									<div class="quick-active">
										<input class="method-option" type="radio" name="activeMethod" id="mDirectbuy" value="directbuy" checked>
										<label class="method-lbl" for="mDirectbuy" >
											<div class="checkbox">
											<svg viewBox="0 0 63.23 42.44">
												<use xlink:href="<?php echo sprite_url; ?>#checkmark"></use>
											</svg>
											</div>
											<img src="<?php echo theme_assets; ?>/imgs/quickActive.svg">
											<strong class="method-text">
												خرید مستقیم
												<span>آی گیفت مستقیما به اکانت شما متصل میشه و بازی رو براتون میخره</span>
											</strong>
											<span class="tag">
												رایگان
											</span>
										</label>
									</div>
									<div class="activation-platform" style="display: block;">
										<?php
										foreach ($direct_buy as $product_obj) {
											$platform = $product_obj['platform'];
											if( !in_array($platform['slug'],$printed_platforms) ){
												$printed_platforms[] = $platform['slug'];
												$user_name = $platform['slug'] . '_user';
												$pass_name = $platform['slug'] . '_pass';
												$has_submit_errors = (isset($_GET['fill-platforms'])) ? true : false;
												?>
												<div class="platform-item">
													<div class="platform-icon">
														<?php echo (isset($platform['icon']) && !empty($platform['icon'])) ? '<img width="20px" src="'.$platform['icon'].'" alt="'.$platform['name'].'">' : ''; ?>
														<span class="platform-name"><?php echo $platform['name']; ?></span>
													</div>
													<div class="input-row">
														<div class="input-by-title">
															<span class="input-label">نام کاربری <?php echo $platform['name']; ?></span>
															<input type="text" name="<?php echo $user_name; ?>" placeholder="نام کاربری <?php echo $platform['name']; ?> را وارد کنید" value="<?php echo ( isset($_GET[$user_name]) && $_GET[$user_name] !== 'error' ) ? $_GET[$user_name] : ''; ?>" <?php echo ( $has_submit_errors && ( !isset($_GET[$user_name]) || $_GET[$user_name] == 'error' ) ) ? 'class="error"' : ''; ?> required autocomplete="off"/>
															<svg class="input-icon" viewBox="0 0 209.81 238.81">
																	<use xlink:href="<?php echo sprite_url; ?>#user"></use>
															</svg>
														</div>
														<div class="input-by-title">
															<span class="input-label">رمز عبور <?php echo $platform['name']; ?></span>
															<input type="text" name="<?php echo $pass_name; ?>" placeholder="رمز عبور <?php echo $platform['name']; ?> را وارد کنید" value="<?php echo ( isset($_GET[$pass_name]) && $_GET[$pass_name] !== 'error' ) ? $_GET[$pass_name] : ''; ?>" <?php echo ( $has_submit_errors && (!isset($_GET[$pass_name]) || $_GET[$pass_name] == 'error') ) ? 'class="error"' : ''; ?> required autocomplete="off"/>
															<svg class="input-icon" viewBox="0 0 246.35 314.83">
																	<use xlink:href="<?php echo sprite_url; ?>#lock"></use>
															</svg>
														</div>
														<?php if ( $has_submit_errors ){
															if ( !isset($_GET[$user_name]) || $_GET[$user_name] == 'error' ){ ?>
																<div class="info error">
																	<div class="icon">!</div>
																	لطفا نام کاربری  <?php echo $platform['name']; ?> را وارد کنید
																</div>
															<?php }
															if ( !isset($_GET[$pass_name]) || $_GET[$pass_name] == 'error' ){ ?>
																<div class="info error">
																	<div class="icon">!</div>
																	لطفا رمز عبور <?php echo $platform['name']; ?> را وارد کنید
																</div>
															<?php }
														}
														else{ ?>
															<div class="info">
																<div class="icon">؟</div>
																رمز عبور را بدون “فاصله” وارد کنید. 
																<a href="#">حساب اوریجین ندارم چیکار کنم؟</a> 
															</div>
														<?php } ?>
													</div>
												</div>
												<?php
											}
										}
										?>
									</div>
								</div>
								<?php
							}else{ ?>
								<div class="send-method box-part">
									<input class="method-option" type="radio" name="activeMethod" value="undirect" id="m2" checked>
									<label class="method-lbl" for="m2" >
										<div class="checkbox">
										<svg viewBox="0 0 63.23 42.44">
											<use xlink:href="<?php echo sprite_url; ?>#checkmark"></use>
										</svg>
										</div>
										<img src="<?php echo theme_assets; ?>/imgs/CDkey.svg">
										<strong class="method-text">
											دریافت سی دی کی
											<span>سی دی کی برای شما ارسال میشه و زحمت فعالسازیش رو خودتون میکشین</span>
										</strong>
										
									</label>
								</div>
							<?php } ?>
						

							<?php remove_action( 'woocommerce_checkout_order_review', 'woocommerce_order_review', 10 ); ?>
						
							<?php do_action( 'woocommerce_checkout_order_review' ); ?>

						</div>

					</div>
				</div>
					<div class="col-lg-3">
					
						<?php do_action( 'woocommerce_before_cart_collaterals' ); ?>

						<div class="subtotal subtotal--checkout">
							<?php
								/**
								 * Cart collaterals hook.
								 *
								 * @hooked woocommerce_cross_sell_display
								 * @hooked woocommerce_cart_totals - 10
								 */
								wc_get_template( 'cart/cart-totals.php' );
							?>
						</div>

					</div>
			</div>
		</div>
	</div>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>

<div class="container">
	<div class="gateway-content" style="display: none;">
		<img class="waiting-portal" src="<?php echo theme_assets; ?>/imgs/waitingPortal.svg">
		<h2>
			لطفا کمـی صبـر کنیـد
		</h2>
		<span class="description">درحال انتقال به درگاه بانکی...</span>
	</div>
</div>