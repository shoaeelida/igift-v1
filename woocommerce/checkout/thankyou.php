<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

if( !$order ){ ?>

	<div class="mainContainer">
		<div class="afterpayment-content">

			<div class="alert-box alert-box-failed">

				<div class="icon">
					<svg viewBox="0 0 128 128">
						<use xlink:href="<?php echo sprite_url; ?>#error"></use>
					</svg>
				</div>
				<h2>مشکلی در خرید شما پیش آمد</h2>
				<span>در صفحه سفارشات من میتوانید سفارش خود را ببنید</span>
				<div class="buttons">
					<a class="btn btn--transparent btn--large btn--text btn--text--left ml-3 revert" href="<?php echo site_url; ?>">
						<svg viewBox="0 0 12.56 19.46">
							<use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
						</svg>	
						صفحه اصلی
					</a>
					<a class="btn btn--red btn--large btn--text btn--text--right ml-3" href="<?php echo wc_get_account_endpoint_url( get_option( 'woocommerce_myaccount_orders_endpoint', 'edit-account' ) ); ?>">
						مشاهده سفارشات من
						<svg viewBox="0 0 12.56 19.46">
							<use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
						</svg>
					</a>
				</div>
			</div>

		</div>
	</div>

	<?php

	return;

}

$order_id = $order->get_id();

$active_method = get_post_meta( $order_id, 'product_order_active_method', true );
$is_custom_order = get_post_meta( $order_id, 'is_custom_order', true );
$is_product_order = get_post_meta( $order_id, 'is_product_order', true );

$date = $order->get_date_created();
$strtotime = strtotime($date);

do_action( 'woocommerce_before_thankyou', $order_id ); ?>

<div class="mainContainer">
	<div class="afterpayment-content">

		<?php
		if( $is_product_order ){ 
			
			$is_direct_method = ( $active_method && $active_method == 'direct' ) ? true : false;

			?>

			<div class="alert-box">

				<?php if ( $order->has_status( 'failed' ) ) { ?>

					<div class="icon">
						<svg viewBox="0 0 63.23 42.44">
							<use xlink:href="<?php echo sprite_url; ?>#checkmark"></use>
						</svg>
					</div>
					<h2>مشکلی در خرید شما پیش آمد</h2>

				<?php }else{ ?>

					<div class="icon">
						<svg viewBox="0 0 63.23 42.44">
							<use xlink:href="<?php echo sprite_url; ?>#checkmark"></use>
						</svg>
					</div>
					<h2>خرید شما با موفقیت انجام شد</h2>
					<span>با تشکر از خرید شما</span>

				<?php } ?>

				<?php if( $is_direct_method ){
					echo '<span>برای فعالسازی بازیهای خود رو دکمه فعالسازی کلیک نمایید</span>';
				} ?>

			</div>

			<div class="purchased-wrap">
				<div class="purchased-top">
					<div class="info">
						<span><?php echo wp_date( 'j F y', $strtotime ); ?></span>
						<span><?php echo wp_date( 'g:i', $strtotime ); ?></span>
					</div>
					<div class="info">
						<span><b>کد سفارش</b></span>
						<span><?php echo $order->get_order_number(); ?></span>
					</div>
				</div>
				<?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
				<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>
			</div>

		<?php }else{ ?>

			<div class="alert-box">

				<?php if ( $order->has_status( 'failed' ) ) { ?>

					<div class="icon">
						<svg viewBox="0 0 63.23 42.44">
							<use xlink:href="<?php echo sprite_url; ?>#checkmark"></use>
						</svg>
					</div>
					<h2>مشکلی در خرید شما پیش آمد</h2>

				<?php }else{ ?>

					<div class="icon">
						<svg viewBox="0 0 63.23 42.44">
							<use xlink:href="<?php echo sprite_url; ?>#checkmark"></use>
						</svg>
					</div>
					<h2>پرداخت شما با موفقیت انجام شد</h2>

					<p>سفارش شما با شماره <strong><?php echo $order->get_order_number(); ?></strong> با موفقیت ثبت شد و در کمترین زمان انجام خواهد شد.</p>

				<?php } ?>

			</div>

		<?php } ?>

	</div>
</div>