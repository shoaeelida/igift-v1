<?php

/**
* Get an array of User Favorites
* @param $user_id int, defaults to current user
* @param $site_id int, defaults to current blog/site
* @param $filters array of post types/taxonomies
* @return array
*/
$favorites = get_user_favorites( get_current_user_id() );

if( $favorites /*&& !isset($favorites['posts']) && !empty($favorites['posts'])*/ ){ ?>

    <div class="owl-carousel favCarousel" id="favCarousel">
        <?php
        foreach ($favorites as $product_id) {
            set_query_var( 'product_id', $product_id );
            get_template_part( 'template-parts/content/content', 'product-item-dashboard-favorite' );
        }
        ?>
    </div>

<?php
} else{

    // No Favorites
    ?>

    <div class="noData">
        <img class="image" src="<?php echo theme_assets; ?>/imgs/nodata.svg">
        <h3 class="title">لیست علاقه مندی های شما خالیست!</h3>
        <h4 class="text">
			<a class="link" href="<?php echo get_post_type_archive_link( 'game' ); ?>">
				<strong>بازی ها</strong>
			</a>
			یا 
			<a class="link" href="<?php echo get_post_type_archive_link( 'giftcard' ); ?>">
				<strong>گیفت کارت های</strong>
			</a> 
			مورد علاقه خود را انتخاب کنید‌
		</h4>
    </div>

    <?php

}