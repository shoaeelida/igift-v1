<?php

// $user_notifictions = get_user_notifictions( get_current_user_id() );

// if( $user_notifictions ){
// 	foreach ($user_notifictions as $notifiction ) {
// 		// success
// 		echo '<div class="notif " data-id="'.$notifiction['id'].'" data-key="'.$notifiction['key'].'" data-userid="'.$notifiction['user_id'].'" data-seen="'.$notifiction['seen'].'">';
// 			echo '<span class="bold">'.$notifiction['title'].'</span>';
// 			echo '<span class="light">'.$notifiction['content'].'</span>';
// 			echo '<button class="close">
// 				<svg viewBox="0 0 75.29 75.29"><use xlink:href="'.sprite_url.'#close"></use></svg>
// 			</button>';
// 			// <a class="link">
// 			// 	<svg viewBox="0 0 11.15 14.87"><use xlink:href="'.sprite_url.'#editPen"></use></svg>
// 			// 	ویرایش اطلاعات
// 			// </a>
// 			echo '<div class="clearfix"></div>';
// 		echo '</div>';
// 	}
// }

/*
$user_confirm_status = user_confirm_status( get_current_user_id() );
if( !$user_confirm_status || $user_confirm_status['code'] == 0 ){ ?>
    <div class="notif error">
        <svg class="icon" viewBox="0 0 128 128"><use xlink:href="<?php echo sprite_url; ?>#error"></use></svg>
        <span class="bold">اطلاعات کاربری شما ناقص است!</span>
        <span class="light">اطلاعات کاربری را تکمیل نمایید.</span>
        <button class="close">
            <svg viewBox="0 0 75.29 75.29"><use xlink:href="<?php echo sprite_url; ?>#close"></use></svg>
        </button>
        <a class="link" href="<?php echo wc_get_account_endpoint_url( get_option( 'woocommerce_myaccount_edit_account_endpoint', 'edit-account' ) ); ?>">
            <svg viewBox="0 0 11.15 14.87"><use xlink:href="<?php echo sprite_url; ?>#editPen"></use></svg>
            ویرایش اطلاعات
        </a>
        <div class="clearfix"></div>
    </div>
    <?php
}else{ ?>

    <div class="notif success">
        <svg class="icon" viewBox="0 0 128 128"><use xlink:href="<?php echo sprite_url; ?>#success"></use></svg>
        <span class="bold">حساب کاربری شما با موفقیت فعال شد.</span>
        <span class="light">حساب کاربری شما با موفقیت فعال شد.</span>
        <button class="close">
            <svg viewBox="0 0 75.29 75.29"><use xlink:href="<?php echo sprite_url; ?>#close"></use></svg>
        </button>
        <a class="link" href="<?php echo wc_get_account_endpoint_url( get_option( 'woocommerce_myaccount_edit_account_endpoint', 'edit-account' ) ); ?>">
            <svg viewBox="0 0 11.15 14.87"><use xlink:href="<?php echo sprite_url; ?>#editPen"></use></svg>
            ویرایش اطلاعات
        </a>
        <div class="clearfix"></div>
    </div>

<?php
} 

<div class="notif dark waiting">
    <div class="icon">
        <img src="<?php echo theme_assets; ?>/dist/imgs/timer.svg" alt="" />
    </div>
    <span class="bold">شما یک بازی خریداری کرده اید</span>
    <span class="light">درحال خرید بازی برای شما هستیم ...</span>

    <div class="waiting-time">
        زمان انتظار
        <div class="time-holder">
        28
        <b>:</b>
        10
        </div>
    </div>
    <button class="close">
        <svg viewBox="0 0 75.29 75.29">
        <use xlink:href="<?php echo sprite_url; ?>#close"></use>
        </svg>
    </button>

    <div class="clearfix"></div>
</div>

*/

