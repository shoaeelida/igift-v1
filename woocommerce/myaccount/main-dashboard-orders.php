<?php
/**
 * My Orders - Deprecated
 *
 * @deprecated 2.6.0 this template file is no longer used. My Account shortcode uses orders.php.
 * @package WooCommerce/Templates
 */

defined( 'ABSPATH' ) || exit;

$customer_orders = get_posts(
	apply_filters(
		'woocommerce_my_account_my_orders_query',
		array(
			'numberposts' => 10,
			'meta_key'    => '_customer_user',
			'meta_value'  => get_current_user_id(),
			'post_type'   => wc_get_order_types( 'view-orders' ),
			'post_status' => array_keys( wc_get_order_statuses() ),
		)
	)
);

if ( $customer_orders ) {
	$woocommerce_myaccount_orders_endpoint = get_option( 'woocommerce_myaccount_orders_endpoint', 'edit-account' );
	foreach ( $customer_orders as $customer_order ) {

		$order      = wc_get_order( $customer_order ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited
		
		$items = $order->get_items();
		$order_id = $order->get_id();
		$is_custom_order = get_post_meta( $order_id, 'is_custom_order', true );
		$products_array = [];

		if( !empty($items) && !$is_custom_order ){ 

			// it's product order
			$order_base_id = 'order-' . $order->get_order_number();

			foreach ( $items as $item ) {
				$product_id = $item->get_product_id();
				$product = get_wc_product_obj($product_id);
				if( $product ){
					$parent_post = $product['parent_post'];
					if( !isset($products_array[$parent_post]) ){
						$products_array[$parent_post] = '<div class="item">';
							$products_array[$parent_post] .= '<div class="img">';
								$products_array[$parent_post] .= '<img src="'.$product['thumbnail_url'].'" alt="" />';
								$products_array[$parent_post] .= '<div class="hoverLayer"><a class="btn" href="' . wc_get_account_endpoint_url( $woocommerce_myaccount_orders_endpoint ) .'#' . $order_base_id.'_'.$parent_post.'"> ... </a></div>';
							$products_array[$parent_post] .= '</div>';
							$products_array[$parent_post] .= '<div class="product-name"><a class="btn" href="' . wc_get_account_endpoint_url( $woocommerce_myaccount_orders_endpoint ) .'#' . $order_base_id.'_'.$parent_post.'">';
								$products_array[$parent_post] .= $product['title'];
								$products_array[$parent_post] .= '<svg class="status" viewBox="0 0 128 128">
									<use xlink:href="'.sprite_url.'#attention-icon"></use>
								</svg>';
								$products_array[$parent_post] .= '<span class="other-info"> ORIGIN </span>';
							$products_array[$parent_post] .= '</a></div>';
							$products_array[$parent_post] .= '<div class="responsive-info">';
								$products_array[$parent_post] .= '<svg class="status" viewBox="0 0 128 128">
									<use xlink:href="'.sprite_url.'#attention-icon"></use>
									در حال بررسی
								</svg>';
								$products_array[$parent_post] .= '<div class="price"><b>'.play_price($item->get_total()).'</b>تومان</div>';
							$products_array[$parent_post] .= '</div>';
						$products_array[$parent_post] .= '</div>';

					}
				}

			}

			foreach ( $products_array as $html ) {
				echo $html;
			}

		}else{

			echo '<div class="orderRow">
				<span class="name">'.get_order_title($order).'</span>
				<span class="price">'.wc_price( $order->get_total() ).'</span>
				<a class="more" href="'.wc_get_account_endpoint_url( $woocommerce_myaccount_orders_endpoint ).'#order-'.$order->get_order_number().'">...</a>
			</div>';

		}

	}
}else{

    // No Favorites
    ?>

    <div class="noData">
        <img class="image" src="<?php echo theme_assets; ?>/imgs/nodata.svg">
        <h3 class="title">هیچ سفارشی ثبت نشده!</h3>
		<h4 class="text">
			اولین 
			<a class="link" href="<?php echo get_post_type_archive_link( 'game' ); ?>">
				<strong>بازی</strong>
			</a>
			یا 
			<a class="link" href="<?php echo get_post_type_archive_link( 'giftcard' ); ?>">
				<strong>گیفت کارت</strong>
			</a> 
			 خود را سفارش دهید	‌
		</h4>
    </div>

    <?php

}
?>

