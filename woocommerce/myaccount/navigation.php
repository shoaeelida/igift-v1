<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$user_meta = play_user_meta();

// do_action( 'woocommerce_before_account_navigation' );
?>

<!-- <nav class="woocommerce-MyAccount-navigation">
	<ul>
		<?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>
			<li class="<?php echo wc_get_account_menu_item_classes( $endpoint ); ?>">
				<a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>"><?php echo esc_html( $label ); ?></a>
			</li>
		<?php endforeach; ?>
	</ul>
</nav> -->

<!-- <?php do_action( 'woocommerce_after_account_navigation' ); ?> -->


<aside class="dashSide">
	<div class="text-left d-lg-none d-block">
		<button class="dashSideClose">
			<svg viewBox="0 0 75.29 75.29"><use xlink:href="<?php echo sprite_url; ?>#close"></use></svg>
		</button>
	</div>
	<div class="content">
		<figure class="avatar">
			<img src="<?php echo theme_assets; ?>/imgs/user_pic.png" alt="pic">
		</figure>
		<h3 class="userName"><?php echo $user_meta['name']; ?></h3>
		<p class="phone"><?php echo $user_meta['phone']; ?></p>
		
		<!-- <div class="text-center">
			<a class="editBtn" href="<?php echo wc_get_account_endpoint_url( get_option( 'woocommerce_myaccount_edit_account_endpoint', 'edit-account' ) ); ?>">
				<svg viewBox="0 0 11.15 14.87"><use xlink:href="<?php echo sprite_url; ?>#editPen"></use></svg>
				ویرایش اطلاعات
			</a>
		</div> -->

		<div class="navLinks">
			<ul>
				<?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>
					<?php 
					$class = '';
					$link = esc_url( wc_get_account_endpoint_url( $endpoint ) );
					switch ($endpoint) {
						case 'dashboard':
							$icon = '<svg  viewBox="0 0 520 415"><use xlink:href="'.sprite_url.'#dashboard"></use></svg>';
							break;
						case 'confirm':
							$icon = '<svg  viewBox="0 0 18.01 18.01"><use xlink:href="'.sprite_url.'#idCheck"></use></svg>';
							break;
						case 'submit-order':
							$icon = '<svg  viewBox="0 0 465.3 538.24"><use xlink:href="'.sprite_url.'#orderAdd"></use></svg>';
							$class = 'start-order';
							$link = '';
							break;
						case 'orders':
							$icon = '<svg  viewBox="0 0 465.3 538.24"><use xlink:href="'.sprite_url.'#orderList"></use></svg>';
							break;
						case 'مدیریت کارت ها':
							$icon = '<svg  viewBox="0 0 505.11 387.87"><use xlink:href="'.sprite_url.'#cartManage"></use></svg>';
							break;
						case 'support':
							$icon = '<svg  viewBox="0 0 520 519.97"><use xlink:href="'.sprite_url.'#support"></use></svg>';
							break;
						default:
							break;
					} ?>
					<li class="<?php echo wc_get_account_menu_item_classes( $endpoint ); ?>">
						<a href="<?php echo $link; ?>" class="<?php echo $class; ?>">
							<?php echo $icon; ?>
							<?php echo esc_html( $label ); ?>
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
	<div class="sideFooter">
		<a class="logOut" href="<?php echo wp_logout_url(get_home_url()) ?>">
			<svg viewBox="0 0 122.94 129.14"><use xlink:href="<?php echo sprite_url; ?>#logout"></use></svg>
			خروج از حساب کاربری
		</a>
	</div>
</aside>


