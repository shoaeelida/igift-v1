<?php
/**
 * My Orders - Deprecated
 *
 * @deprecated 2.6.0 this template file is no longer used. My Account shortcode uses orders.php.
 * @package WooCommerce/Templates
 */

defined( 'ABSPATH' ) || exit;

?>

<div class="fast-redeem">
    <div class="fast-redeem-header">
        <svg class="icon" viewBox="0 0 13.11 17.34">
            <use xlink:href="<?php echo sprite_url; ?>#redeem"></use>
        </svg>
        ردیم آنی کد بازی های شما
    </div>
    <div class="fast-redeem-body">
        <div class="without-account">
            <span class="caption">
            به زودی فعال می شود
            </span>
        </div> 
        <!-- 
        <span class="drop-title">
            برای ردیم کد یکی از بازیهای خریداری شده خود را انتخاب کنید
        </span>
        <div class="redeem-drop-holder">
            <svg class="arrow" viewBox="0 0 12.56 19.46">
            <use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
            </svg>
            <div class="product-code selected">
            Star Wars Jedi: Fallen Order
            <div class="date-time">
                <span>22 <b>فروردین</b> 98</span>
                <span class="time">22:45</span>
            </div>
            </div>
            <div class="purchased-games-list">
            <input class="rgame" type="radio" name="game" id="g1" />
            <label for="g1" class="product-code">
                Star Wars Jedi: Fallen Order
                <div class="date-time">
                <span>22 <b>فروردین</b> 98</span>
                <span class="time">22:45</span>
                </div>
            </label>
            <input class="rgame" type="radio" name="game" id="g2" />
            <label for="g2" class="product-code">
                Star Wars Jedi: Fallen Order
                <div class="date-time">
                <span>24 <b>فروردین</b> 98</span>
                <span class="time">22:00</span>
                </div>
            </label>
            <input class="rgame" type="radio" name="game" id="g3" />
            <label for="g3" class="product-code">
                Star Wars Jedi: Fallen Order
                <div class="date-time">
                <span>28 <b>فروردین</b> 98</span>
                <span class="time">2:45</span>
                </div>
            </label>
            </div>
        </div>
        <div class="bottom-part">
            <div class="product-info">
            <span class="region"> REGION USA </span>
            <span class="origin"> ORIGIN </span>
            <span class="product-name">
                5647U AUID8 2789K 25652 L0025
            </span>
            </div>
            <a href="#" class="btn btn--red btn--text--right btn--small">
            ردیم <b>بازی</b>
            <svg viewBox="0 0 12.56 19.46">
                <use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
            </svg>
            </a>
        </div>
        -->
    </div>
</div>