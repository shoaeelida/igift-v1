<!-- notifications -->
<?php require_once('notifications.php'); ?>


<?php //<div class="row mt-5 mb-5"> 
?>

<div class="middle-content">
    <div class="orderRG">
        <img class="circle1" src="<?php echo theme_assets; ?>/imgs/circle1.svg" alt="circle 1" />
        <img class="circle2" src="<?php echo theme_assets; ?>/imgs/circle2.svg" alt="circle 2" />
        <img class="circle3" src="<?php echo theme_assets; ?>/imgs/circle1.svg" alt="circle 3" />
        <h3 class="title">محصولی که میخوای توی سایت نیست؟</h3>
        <h4 class="subTitle">
            میتونی هرمحصولی که میخوای رو سفارش بدی تا برات بخریم
        </h4>
        <div class="position-relative">
            <a class="link d-inline-block start-order">
                <svg viewBox="0 0 10.38 10.38">
                    <use xlink:href="<?php echo sprite_url; ?>#add"></use>
                </svg>
                ثبت سفارش
            </a>
            <div class="circ1"></div>
            <div class="circ2"></div>
        </div>
    </div>
    <?php //require_once('main-dashboard-products.php'); ?>
    <?php require_once('main-dashboard-fast-redeem.php'); ?>
</div>


<!-- my orders -->
<div class="myOrders">
    <div class="column">
        <h4 class="title">سفارش های من</h4>
        <a class="link single" href="<?php echo wc_get_account_endpoint_url(get_option('woocommerce_myaccount_orders_endpoint', 'edit-account')); ?>">نمایش همه</a>
    </div>
    <div class="content">
        <div class="orders">
            <div class="owl-carousel orderCarousel" id="favCarousel">
                <?php require_once('main-dashboard-orders.php'); ?>
            </div>
        </div>
    </div>
    <?php /* 
    <div class="myorder-item">
        <div class="column">
            <h4 class="title">
                <svg class="icon" viewBox="0 0 195.2 178.61">
                    <use xlink:href="<?php echo sprite_url; ?>#heart"></use>
                </svg>
                بازی هایی که دوست دارم
            </h4>
            <?php $favorites = get_user_favorites(get_current_user_id());
            if ($favorites) { ?>
                <div class="arrows">
                    <svg class="nextCarousel favNext" viewBox="0 0 24 24">
                        <use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
                    </svg>
                    <svg class="prevCarousel favPrev" viewBox="0 0 24 24">
                        <use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
                    </svg>
                </div>
                <a class="link" href="<?php echo wc_get_account_endpoint_url('favorites'); ?>">نمایش همه</a>
            <?php } ?>
        </div>

        <div class="content fav">
            <?php require_once('main-dashboard-favorites.php'); ?>
        </div>
    </div>
    */ ?>
</div>


<div class="bottom-content">
    <div class="manage-account mainBox">
        <div class="manage-account-header">
        اکانت پلتفرم های من
        <a href="#" class="manage-link">
            <svg viewBox="0 0 10.38 10.38">
            <use xlink:href="<?php echo sprite_url; ?>#plusIcon"></use>
            </svg>
            مدیریت اکانت ها
        </a>
        </div>
        <div class="manage-account-body">
            <div class="without-account">
                <h3>
                شما هنوز اکانتی ندارید!
                <span>از اینجا میتونید اکانت جدید اضافه کنید</span>
                <img class="img" src="<?php echo theme_assets; ?>/imgs/arrow.png" alt="" />
                </h3>
                <span class="caption">
                اگه نمیدونید چجوری اکانت جدید بسازید میتونید از آی گیفت
                بخواید تا براتون بسازه!
                </span>
                <a href="#" class="btn btn--red btn--small">
                درخواست ساخت اکانت
                </a>
            </div> 
             <!--
            <div class="account-list">
                <div class="account-row">
                <div class="icon">
                    <img src="<?php echo theme_assets; ?>/imgs/platform/origin-color.svg" alt="" />
                </div>
                <div class="password">
                    <span class="key">
                    <div class="closet"><b></b><b></b><b></b></div>
                    38699
                    </span>
                </div>
                <div class="username">
                    <span class="name">javadsadeghi</span>
                </div>
                <div class="platform">
                    Origin
                    <div class="more"><b></b><b></b><b></b></div>
                </div>
                </div>
                <div class="account-row">
                <div class="icon">
                    <img src="<?php echo theme_assets; ?>/imgs/platform/origin-color.svg" alt="" />
                </div>
                <div class="password">
                    <span class="key">
                    <div class="closet"><b></b><b></b><b></b></div>
                    38699
                    </span>
                </div>
                <div class="username">
                    <span class="name">javadsadeghi</span>
                </div>
                <div class="platform">
                    Origin
                    <div class="more"><b></b><b></b><b></b></div>
                </div>
                </div>
                <div class="account-row">
                <div class="icon">
                    <img src="<?php echo theme_assets; ?>/imgs/platform/origin-color.svg" alt="" />
                </div>
                <div class="password">
                    <span class="key">
                    <div class="closet"><b></b><b></b><b></b></div>
                    38699
                    </span>
                </div>
                <div class="username">
                    <span class="name">javadsadeghi</span>
                </div>
                <div class="platform">
                    Origin
                    <div class="more"><b></b><b></b><b></b></div>
                </div>
                </div>
                <div class="account-row">
                <div class="icon">
                    <img src="<?php echo theme_assets; ?>/imgs/platform/origin-color.svg" alt="" />
                </div>
                <div class="password">
                    <span class="key">
                    <div class="closet"><b></b><b></b><b></b></div>
                    38699
                    </span>
                </div>
                <div class="username">
                    <span class="name">javadsadeghi</span>
                </div>
                <div class="platform">
                    Origin
                    <div class="more"><b></b><b></b><b></b></div>
                </div>
                </div>
                <div class="account-row">
                <div class="icon">
                    <img src="<?php echo theme_assets; ?>/imgs/platform/origin-color.svg" alt="" />
                </div>
                <div class="password">
                    <span class="key">
                    <div class="closet"><b></b><b></b><b></b></div>
                    38699
                    </span>
                </div>
                <div class="username">
                    <span class="name">javadsadeghi</span>
                </div>
                <div class="platform">
                    Origin
                    <div class="more"><b></b><b></b><b></b></div>
                </div>
                </div>
            </div> -->
        </div>
    </div>
    <div class="help mainBox">
        <div class="icon">
        <img src="<?php echo theme_assets; ?>/imgs/help.png" alt="" />
        </div>
        <h3>کمک میخوام!</h3>
        <span class="caption"> سر در نمیارم چی به چیه </span>
        <a href="<?php echo wc_get_account_endpoint_url( 'support' ); ?>" class="btn">سوالتون رو از ما بپرسین</a>
    </div>
</div>

<?php

/**
 * My Account dashboard.
 *
 * @since 2.6.0
 */
do_action('woocommerce_account_dashboard');

/**
 * Deprecated woocommerce_before_my_account action.
 *
 * @deprecated 2.6.0
 */
do_action('woocommerce_before_my_account');

/**
 * Deprecated woocommerce_after_my_account action.
 *
 * @deprecated 2.6.0
 */
do_action('woocommerce_after_my_account');

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */