<?php $confirm_status = user_phone_confirm_status(); 

var_dump( $confirm_status );
?>


<!-- page title -->
<div class="pageTitleBar">
   <div class="titleDetails">
       <svg class="icon" viewBox="0 0 18.01 18.01"><use xlink:href="<?php echo sprite_url; ?>#idCheck"></use></svg>
       <h1 class="title">احراز هویت</h1>
       <h2 class="subTitle">اطلاعات حقیقی خود را وارد کنید</h2>
   </div>
   <?php breadcrumbs(true); ?>
</div>


<!-- notifications -->
<?php require_once( 'notifications.php'); ?>

<div class="mainBox authForm">
    <form id="identityForm">
        <div class="row">
            <div class="col-md-7">
                <div class="row">
                    <div class="col-xl-6">
                        <label class="inputLabel">نام</label>
                        <div class="inputGroup">
                            <input type="text" name="your-name" placeholder="نام">
                            <svg viewBox="0 0 209.81 238.81"><use xlink:href="<?php echo sprite_url; ?>#user"></use></svg>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <label class="inputLabel">نام خانوادگی</label>
                        <div class="inputGroup">
                            <input type="text" name="your-family" placeholder="نام خانوادگی">
                            <svg viewBox="0 0 209.81 238.81"><use xlink:href="<?php echo sprite_url; ?>#user"></use></svg>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <label class="inputLabel">ایمیل</label>
                        <div class="inputGroup">
                            <input type="email" name="your-email" placeholder="ایمیل">
                            <svg viewBox="0 0 306.78 233.73"><use xlink:href="<?php echo sprite_url; ?>#email"></use></svg>
                        </div>
                    </div>
                </div>
                <div class="row phoneDiv <?php if( $confirm_status ){ echo 'verified' ;} ?>">

                
                    <?php

                    // if( $confirm_status ){

                    //     echo '<input type="text" class="" value="'.user_phone_number().'" readonly>';
                    //     echo 'تایید شد';

                    // }else{

                    //     echo '<input type="text" class="confirm_phone_code" placeholder="code">
                    //         <input type="submit" class="btn confirm_phone" value="ارسال کد">
                    //         <input type="submit" class="btn verify_phone_code" value="تایید">
                    //         <input type="submit" class="btn confirm_phone" value="تماس مجدد">';

                    // } ?>
                    <div class="col-xl-8">
                        <label class="inputLabel">شماره تلفن ثابت</label>
                        <div class="inputGroup">
                            <svg class="verifiedIcon" viewBox="0 0 128 128"><use xlink:href="<?php echo sprite_url; ?>#success"></use></svg>
                            <?php if( $confirm_status ){ 
                                echo '<input type="text" placeholder="تلفن" value="'.user_phone_number().'" readonly disabled>';
                            } else{
                                echo '<input type="text" class="confirm_phone_num" placeholder="تلفن" required>';
                            }?>
                            <svg viewBox="0 0 301.81 301.81"><use xlink:href="<?php echo sprite_url; ?>#phone"></use></svg>
                        </div>
                    </div>
                    <div class="col-xl-4 mb-xl-0 mb-3">
                        <label class="inputLabel hidden d-xl-block d-none">.</label>
                        <?php
                        if( $confirm_status ){
                            echo '<a class="submitBtn width_100 d-block phoneSubmit">تایید شد</a>';
                        }else{
                            echo '<a class="submitBtn width_100 d-block phoneSubmit confirm_phone" href="#vrfModal">تایید شماره ثابت</a>';
                        } ?>
                    </div>
                    <?php if( !$confirm_status ){ ?>
                        <div class="col-12">
                            <div class="validation" id="sendCodeNotic">
                                <span class="icon">?</span>
                                پس از وارد کردن شماره تلفن ثابت ، روی دکمه “تایید شماره ثابت” کلیک کنید.
                            </div>
                            <div class="validation error" id="sendCodeError" style="display:none;">
                                <span class="icon">!</span>
                                مشکلی در تماس با شماره شما پیش آمد، شماره خود را بررسی و مجددا امتحان کنید.
                            </div>
                            <div class="validation error" id="emptyCodeError" style="display:none;">
                                <span class="icon">!</span>
                                لطفا شماره خود را در فیلد بالا وارد کنید.
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>


            <div class="col-md-5">
                <label class="inputLabel">تصویر کارت ملی</label>
                <div class="upload idCardDiv verified">
                    <input type="file" id="idCard" name="idCard">
                    <div class="assets">
                        <svg class="icon" viewBox="0 0 678.43 466"><use xlink:href="<?php echo sprite_url; ?>#idCard"></use></svg>
                        <h4>
                            تصویر کارت ملی خود را<br>
                            در اینجا بکشید و بیندازید
                        </h4>
                        یا <br>
                        <button class="submitBtn white">فایل خود را انتخاب کنید</button>
                        <div>تصویر کارت ملی خود را
                            در اینجا بکشید و بیندازید</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row d-flex flex-row-reverse pr-3 pl-3 submitRow">
            <button class="submitBtn submitForm red" disabled>
                ثبت احراز هویت
                <svg viewBox="0 0 12.56 19.46">
                    <use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
                </svg>
            </button>
        </div>
    </form>
</div>


<?php if( !$confirm_status ){ ?>
    <!-- verify modal -->
    <div id="vrfModal" class="modalOverlay">
        <a class="cancel" href="#"></a>
        <div class="modalContent">
            <div class="vrfModalContent">
                <div class="time">09:48</div>
                <div class="iconHolder">
                    <div class="icon">
                        <svg viewBox="0 0 406.23 301.81"><use xlink:href="<?php echo sprite_url; ?>#ringing"></use></svg>
                    </div>
                </div>
                <p>آی گیفت در حال تماس با شماره شماست.</p>
                <p class="bold">کد تاییدیه 5 رقمی ذکر شده را در کادر زیر وارد کنید</p>
                <div class="inputGroup">
                    <input type="text" class="confirm_phone_code" maxlength="5" minlength="1" placeholder="- - - - -">
                    <svg viewBox="0 0 301.81 301.81"><use xlink:href="<?php echo sprite_url; ?>#phone"></use></svg>
                </div>
                <div class="submitDiv">
                    <a class="btn btn--white btn--small change_phone" href="#">تغییر شماره</a>
                    <button class="btn btn--red btn--text--right btn--small verify_phone_code">
                        تایید
                        <svg viewBox="0 0 12.56 19.46">
                            <use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
                        </svg>
                    </button>
                </div>
                <p class="mt-3 ">تماس را دریافت نکردید؟ <a href="#vrfModal" class="confirm_phone recall_confirm_phone"> تماس مجدد</a></p>
            </div>
        </div>
    </div>
<?php } ?>
