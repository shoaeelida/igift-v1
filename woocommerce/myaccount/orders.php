<?php
/**
 * Orders
 *
 * Shows orders on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/orders.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_account_orders', $has_orders ); ?>

 <!-- page title -->
 <div class="pageTitleBar">
	<div class="titleDetails">
		<svg class="icon" viewBox="0 0 18.01 18.01"><use xlink:href="<?php echo sprite_url; ?>#idCheck"></use></svg>
		<h1 class="title">سفارشات</h1>
		<h2 class="subTitle"></h2>
	</div>
	<?php breadcrumbs(true); ?>
</div>

<!-- notifications -->
<?php require_once( 'notifications.php'); ?>


<?php if ( $has_orders ) : ?>

	<dl class="accordion accordion-order">
	<!-- <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table"> -->
		<!-- <thead>
			<tr>
				<?php foreach ( wc_get_account_orders_columns() as $column_id => $column_name ) : ?>
					<th class="woocommerce-orders-table__header woocommerce-orders-table__header-<?php echo esc_attr( $column_id ); ?>"><span class="nobr"><?php echo esc_html( $column_name ); ?></span></th>
				<?php endforeach; ?>
			</tr>
		</thead> -->

		<!-- <tbody> -->
			<?php


			foreach ( $customer_orders->orders as $customer_order ) {
				$order      = wc_get_order( $customer_order ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited
				$order_id      = $order->get_id(); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited
				$item_count = $order->get_item_count() - $order->get_item_count_refunded();
				// $item_count = $order->get_item_count();
				$items = $order->get_items();
				$fees = $order->get_fees();
				$count = 0;
				// $date = $order->order_date;
				$date = $order->get_date_created();
				$is_custom_order = get_post_meta( $order_id, 'is_custom_order', true );
				$order_status = $order->get_status();
				$custom_order_cdkey = get_post_meta($order_id,'custom_order_cdkey',true);
				$active_method = get_post_meta( $order_id, 'product_order_active_method', true );

				// 'wc-pending'    => _x( 'Pending payment', 'Order status', 'woocommerce' ),
				// 'wc-processing' => _x( 'Processing', 'Order status', 'woocommerce' ),
				// 'wc-on-hold'    => _x( 'On hold', 'Order status', 'woocommerce' ),
				// 'wc-completed'  => _x( 'Completed', 'Order status', 'woocommerce' ),
				// 'wc-cancelled'  => _x( 'Cancelled', 'Order status', 'woocommerce' ),
				// 'wc-refunded'   => _x( 'Refunded', 'Order status', 'woocommerce' ),
				// 'wc-failed'     => _x( 'Failed', 'Order status', 'woocommerce' ),
				$status_icon = '<svg class="icon" viewBox="0 0 128 128"><use xlink:href="'.sprite_url.'#error"></use></svg>';
				if( $custom_order_cdkey ){
					$status_label = 'خریداری شده';
				}else{
					$status_label = 'لغو شده';
					switch ($order_status) {
						case 'completed':
							$status_icon = '<svg class="icon" viewBox="0 0 128 128"><use xlink:href="'.sprite_url.'#pending"></use></svg>';
							$status_label = 'در انتظار خرید ...';
							break;
						case 'on-hold':
							$status_icon = '<svg class="icon" viewBox="0 0 128 128"><use xlink:href="'.sprite_url.'#pending"></use></svg>';
							$status_label = 'در انتظار خرید ...';
							break;
						case 'processing':
							$status_icon = '<svg class="icon" viewBox="0 0 128 128"><use xlink:href="'.sprite_url.'#pending"></use></svg>';
							$status_label = 'در انتظار خرید ...';
							break;
						case 'pending':
							$status_icon = '<svg class="icon" viewBox="0 0 128 128"><use xlink:href="'.sprite_url.'#pending"></use></svg>';
							$status_label = 'در انتظار خرید ...';
							break;
						default:
							break;
					}
				}


				if( !empty($items) && !$is_custom_order ){ 

					// it's product order
					$order_base_id = 'order-' . $order->get_order_number();
					$products_array = [];

					$count = 0;

					foreach ( $items as $item ) {
						$count++;
						$product_id = $item->get_product_id();
						$product_variation_id = $item->get_variation_id();
						$product = get_wc_product_obj($product_id);
						if( $product ){

							$parent_post = $product['parent_post'];
							$base_meta_key = $product_id . '_';
							
							// var_dump( 'isset '.$parent_post.' >> ' );
							// var_dump( isset($products_array[$parent_post]) );

							if( !isset($products_array[$parent_post]) ){
								$count = 1;
								$products_array[$parent_post] = array(
									'id' => $parent_post,
									'title' => $product['title'],
									'edition' => $product['edition'],
									'product_type' => $product['product_type'],
									'arrows' => [],
									'total' => $item->get_total(),
								);
							}else{
								$products_array[$parent_post]['total'] += $item->get_total();
							}
							

							$html = '';

							// if( $product['product_type'] !== 'giftcard' ){
								$html .= '<div class="detailRow">';
									$html .= '<span class="num">'.$count.'.</span>';
									$html .= '<div class="tags">';
										if( $product['product_type'] !== 'giftcard' ){
											if (isset($product['platform']) && !empty($product['platform']) ) {
												$html .= ' <span class="tag">';
													$html .= (isset($product['platform']['icon'])) ?  '<img src="'.$product['platform']['icon'].'" alt="'.$product['platform']['name'].'">' : '';
													$html .= $product['platform']['name'];
												$html .= '</span>';
											}
										}else{
											switch ($product['giftcard_type']) {
												case 'default':
													if( isset($product['giftcard_price']) && !empty($product['giftcard_price']) ){
														$html .= '<span class="tag">';
															$html .= $product['giftcard_price'] . ' ' . $product['giftcard_currency']['label'];
														$html .= '</span>';
													}
													break;
												case 'subscribe':
													if( isset($product['subscribe_duration']) && !empty($product['subscribe_duration']) ){
														$html .= '<span class="tag">';
															$html .= $product['subscribe_duration'] . ' ' . $product['subscribe_type']['label'];
														$html .= '</span>';
													}
													break;
												default:
													break;
											}
										}
										if (isset($product['region']) && !empty($product['region']) && !empty($product['region']['name'])) {
											$html .= '<span class="tag">';
												$html .= $product['region']['name'];
											$html .= '</span>';
										}
										if (isset($product['edition']) && !empty($product['edition'])) {
											$html .= '<span class="tag">';
												$edition = preg_replace('/[^\00-\255]+/u', '', $product['edition']);
												$edition = str_replace('- ','',$edition);
												$html .= $edition;
											$html .= '</span>';
										}
										if( in_array($product['product_type'],['shared_account']) ){

											$shared_capacity = product_capacity_name($product['shared_capacity']);

											$html .= '<span class="tag">';
												if(empty($shared_capacity)){
													$html .= "اکانت اشتراکی";
												}else{
													$html .= "اکانت اشتراکی - $shared_capacity";
												}
											$html .= '</span>';

										}

									$html .= '</div>';

									if( in_array($product['product_type'],['cdkey','giftcard']) ){
										
										$g2a_order_key = get_post_meta( $order_id, $base_meta_key.'g2a_order_key', true );

										if( $g2a_order_key ){
											if(  $product['product_type'] == 'giftcard' ){
												$html .= '<div class="cdKey">
													<span>کد گیفت کارت</span>
													<svg class="icon" viewBox="0 0 128 128"><use xlink:href="'.sprite_url.'#success"></use></svg>
													<span class="key">'.$g2a_order_key.'</span>
													<input type="text" value="'.$g2a_order_key.'" readonly>
													<button class="copy">کپی</button>
												</div>';
											}else{
												$html .= '<div class="cdKey">
													<span>سی دی کی</span>
													<svg class="icon" viewBox="0 0 128 128"><use xlink:href="'.sprite_url.'#success"></use></svg>
													<span class="key">'.$g2a_order_key.'</span>
													<input type="text" value="'.$g2a_order_key.'" readonly>
													<button class="copy">کپی</button>
												</div>';
											}
										}else{
											if( $order_status == 'pending' || $order_status == 'on-hold' ){
												$mes = 'در انتظار پرداخت';
											}elseif( $order_status == 'failed' ){
												$mes = 'لغو شده';
											}else{
												if( $active_method == 'direct' ){
													$mes = 'در انتظار فعالسازی آنی';
												}else{
													$mes = 'در انتظار خرید';
												}
											}
											$html .= '<div class="cdKey">
												<span>'.$mes.'</span>
												<svg class="icon" viewBox="0 0 128 128"><use xlink:href="'.sprite_url.'#time"></use></svg>
											</div>';
										}

									}elseif( in_array($product['product_type'],['direct']) ){
										
										$buyed = get_post_meta( $order_id, $base_meta_key.'direct_buyed', true );
										$buyed = ( !$buyed || $buyed !== '1' ) ? false : true;

										if( $buyed ){
											$html .= '<div class="cdKey">
												<span>خریداری شد</span>
												<svg class="icon" viewBox="0 0 128 128"><use xlink:href="'.sprite_url.'#success"></use></svg>
											</div>';
										}else{
											if( $order_status == 'pending' || $order_status == 'on-hold' ){
												$mes = 'در انتظار پرداخت';
											}elseif( $order_status == 'failed' ){
												$mes = 'لغو شده';
											}else{
												$mes = 'در انتظار خرید';
											}
											$html .= '<div class="cdKey">
												<span>'.$mes.'</span>
												<svg class="icon" viewBox="0 0 128 128"><use xlink:href="'.sprite_url.'#time"></use></svg>
											</div>';
										}

									}elseif( in_array($product['product_type'],['account','shared_account']) ){

										$username_meta_key = $base_meta_key.'username';
										$password_meta_key = $base_meta_key.'password';
										
										$username =  get_post_meta( $order_id, $username_meta_key, true );
										$password =  get_post_meta( $order_id, $password_meta_key, true );
								
										if( $username && !empty($username) && $password && !empty($password) ){ 

											if( isset($product['shared_capacity']) && in_array($product['shared_capacity'],['one_capacity']) ){
											
												// dont show user & pass for one_capacity
										
											}else{

												$html .= '<div class="cdKey">';
													if( $username && !empty($username) ){
														$html .= '<span class="cdKey-user">
															<span>نام کاربری : </span>
															<span class="key">'.$username.'</span>
														</span>';
													}
													if( $password && !empty($password) ){
														$html .= '<span class="cdKey-pass">
															<span>رمز عبور : </span>
															<span class="key">'.$password.'</span>
														</span>';
													}
												$html .= '</div>';

											}

										}else{
											if( $order_status == 'pending' || $order_status == 'on-hold' ){
												$mes = 'در انتظار پرداخت';
											}elseif( $order_status == 'failed' ){
												$mes = 'لغو شده';
											}else{
												if( $active_method == 'direct' ){
													$mes = 'در انتظار فعالسازی آنی';
												}else{
													$mes = 'در انتظار خرید';
												}
											}
											$html .= '<div class="cdKey">
												<span>'.$mes.'</span>
												<svg class="icon" viewBox="0 0 128 128"><use xlink:href="'.sprite_url.'#time"></use></svg>
											</div>';
										}


									}

									$html .= '<div class="fee">
										<span>فی</span>
										'.wc_price($item->get_total()).'
									</div>';

								$html .= '</div>';
							// }
							
							$products_array[$parent_post]['arrows'][] = $html;

						}

					}

					// var_dump($products_array);
					// die();

					foreach ($products_array as $pid => $p ) {
						echo '<dt id="'.$order_base_id.'_'.$pid.'">';
						?>
						<a href="#<?php echo $order_base_id.'_'.$pid; ?>" class="accordion-title accordionTitle js-accordionTrigger">
                            <div class="date-time">
                                <span class="date"><?php echo wp_date( 'j F y', strtotime($date->format ('j F y') ) ); ?></span>
                                <span class="hour"><?php echo wp_date( 'g:i', strtotime($date->format ('g:i') ) ); ?></span>
                            </div>

							<h2 class="title">
								<?php echo $p['title']; ?>
							</h2>
                            <div class="price-status">
                                <div class="price">
                                    <!-- <span class="dollarPrice">50$</span> -->
                                    <?php //echo wc_price($item->get_total()); ?>
                                    <?php echo wc_price($p['total']); ?>
                                </div>
                                <div class="status">
                                    <?php echo get_order_status_icon( $order->get_status() ); ?>
                                    <?php echo esc_html( wc_get_order_status_name( $order->get_status() ) ); ?>
                                </div>
                            </div>
							<span class="accItem">...</span>
						</a>
						<?php
						echo '</dt>';
						echo '<dd class="accordion-content accordionItem" >';
							echo '<div class="content">';
								foreach ($p['arrows'] as $html) {
									echo $html;
								}
								?>
								<div class="accFooter">
									<?php
									/*
									<div class="right">
										<a class="download" disabled>
											<svg viewBox="0 0 77.34 77.34"><use xlink:href="<?php echo sprite_url; ?>#flash"></use></svg>
											دانلود فاکتور (PDF)
										</a>
										<a disabled>گزارش مشکل</a>
									</div>
									*/
									?>
									<div class="left">
										<?php
										$actions = wc_get_account_orders_actions( $order );
										$orders_endpoint = esc_url( wc_get_endpoint_url( 'orders' ) );
										if ( ! empty( $actions ) ) {
											foreach ( $actions as $key => $action ) { 
												// phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited
												switch ($key) {
													case 'cancel':
														// echo '<a class="cancel '.sanitize_html_class( $key ).'" href="'.esc_url( $action['url'] ).'">
														echo '<a class="cancel" href="'.esc_url( $action['url'] ).'&redirect='.$orders_endpoint.'">
															<svg stroke="red" viewBox="0 0 75.29 75.29"><use xlink:href="'.sprite_url.'#close"></use></svg>
															'.esc_html( $action['name'] ).'
														</a>';
														break;
													case 'pay':
														// echo '<a class="cancel '.sanitize_html_class( $key ).'" href="'.esc_url( $action['url'] ).'">
														echo '<a class="" href="'.esc_url( $order->get_checkout_payment_url( true ) ).'">
															<svg stroke="red" viewBox="0 0 77.34 77.34"><use xlink:href="'.sprite_url.'#flash"></use></svg>
															'.esc_html( $action['name'] ).'
														</a>';
														break;
													default:
														break;
												}
												// echo '<a href="' . esc_url( $action['url'] ) . '" class="woocommerce-button button ' . sanitize_html_class( $key ) . '">' . esc_html( $action['name'] ) . '</a>';
											}
										}
										?>
										<a href="<?php echo (isset($p['id'])) ? get_the_permalink( $p['id'] ) : ''; ?>">
											<svg viewBox="0 0 77.34 77.34"><use xlink:href="<?php echo sprite_url; ?>#flash"></use></svg>
											رفتن به صفحه <?php echo ($p['product_type'] == 'giftcard') ? 'گیفت کارت' : 'بازی'; ?>
										</a>
									</div>
								</div>
								<?php
							echo '</div>';
						echo '</dd>';
					}
					?>

				<?php }
				elseif( $is_custom_order ){

					// it's custom order
					$custom_order_currency = get_post_meta($order_id,'custom_order_currency',true);
					$custom_order_url = get_post_meta($order_id,'custom_order_url',true);
					$custom_order_device = get_post_meta($order_id,'custom_order_device',true);
					$custom_order_Platform = get_post_meta($order_id,'custom_order_Platform',true);
					$custom_order_platform_username = get_post_meta($order_id,'custom_order_platform_username',true);
					$custom_order_platform_password = get_post_meta($order_id,'custom_order_platform_password',true);
					$custom_order_price = get_post_meta($order_id,'custom_order_price',true);
					$custom_order_converted_price = get_post_meta($order_id,'custom_order_converted_price',true);
					$custom_order_desc = get_post_meta($order_id,'custom_order_desc',true);
					$custom_order_cdkey = get_post_meta($order_id,'custom_order_cdkey',true);
				
					$platform = get_order_platform_info($custom_order_Platform);
					$currency = '';
					if( $custom_order_currency ){
						switch ($custom_order_currency) {
							case 'USD':
								$currency = '$';
								break;
							case 'EUR':
								$currency = '€';
								break;
							default:
								$currency = $custom_order_currency;
								break;
						}
					}

					?>

					<dt id="order-<?php echo $order->get_order_number(); ?>">
						<a href="#<?php echo $order->get_order_number(); ?>" class="accordion-title accordionTitle js-accordionTrigger">
                            <div class="date-time">
								<span class="date"><?php echo wp_date( 'j F y', strtotime($date->format ('j F y') ) ); ?></span>
                                <span class="hour"><?php echo wp_date( 'g:i', strtotime($date->format ('g:i') ) ); ?></span>
                            
							</div>
                            <h2 class="title">
                                <?php echo get_order_title($order); ?>
                                <!-- <span>_ نسخه گلوبال</span> -->
                            </h2>
                            <div class="price-status">
                                <div class="price">
                                    <span class="dollarPrice"><?php echo $custom_order_price; echo $currency; ?></span>
                                    <span class="tomanPrice">
                                        <?php echo play_price($order->get_total()); ?>
                                        <span class="unit">تومان</span>
                                    </span>
                                </div>
                                <div class="status">
                                    <?php echo get_order_status_icon( $order->get_status() ); ?>
                                    <?php echo esc_html( wc_get_order_status_name( $order->get_status() ) ); ?>
                                </div>
                            </div>
                            <span class="accItem">...</span>
                        </a>
                    </dt>
                    <dd class="accordion-content accordionItem" >
                        <div class="content">
                            <div class="mn">
                                <div class="mn_r">
									<?php if( $custom_order_url ){
										echo '<div class="rowItem">
											<span class="title">لینک</span>
											<span class="text">'.$custom_order_url.'</span>
										</div>';
									} ?>
									<?php if( $custom_order_desc ){
										echo '<div class="rowItem">
											<span class="title">توضیحات</span>
											<span class="text">'.$custom_order_desc.'</span>
										</div>';
									} ?>
                                    <div class="rowItem">
                                        <span class="title">پلتفرم</span>
                                        <span class="text">
												<?php if( $platform ){
													echo '<span class="item">
														<span class="tag">'.$platform['name'].' '.$platform['icon'].'</span>
													</span>';
												} ?>
												<?php if( $custom_order_platform_username ){
													echo '<div class="item">
														<span class="title">نام کاربری</span>
														<span class="text">'.$custom_order_platform_username.'</span>
													</div>';
												} ?>
												<?php if( $custom_order_platform_password ){
													echo '<div class="item">
														<span class="title">کلمه عبور</span>
														<span class="text">'.$custom_order_platform_password.'</span>
													</div>';
												} ?>
                                            </span>
                                    </div>
                                </div>
                                <div class="mn_l">
                                    <h4 class="title">
                                        <?php echo $status_icon; ?>
                                        <?php echo $status_label; ?>
                                    </h4>
                                    <div class="text">
										<?php if( $custom_order_cdkey ){
											echo '<p><strong>CDkey : </strong> '.$custom_order_cdkey.'</p>';
										}else{
											echo '<p>پس از انجام خرید میتوانید سی دی کی بازی خود را در این قسمت مشاهده کنید</p>';
										} ?>
                                    </div>
                                </div>
                            </div>

                            <div class="accFooter">
								<?php
								/*
								<div class="right">
									<a class="download" disabled>
										<svg viewBox="0 0 77.34 77.34"><use xlink:href="<?php echo sprite_url; ?>#flash"></use></svg>
										دانلود فاکتور (PDF)
									</a>
									<a disabled>گزارش مشکل</a>
								</div>
								*/
								?>
                                <div class="left">
								<?php
									$actions = wc_get_account_orders_actions( $order );
									$orders_endpoint = esc_url( wc_get_endpoint_url( 'orders' ) );
									if ( ! empty( $actions ) ) {
										foreach ( $actions as $key => $action ) { 
											// phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited
											switch ($key) {
												case 'cancel':
													// echo '<a class="cancel '.sanitize_html_class( $key ).'" href="'.esc_url( $action['url'] ).'">
													echo '<a class="cancel" href="'.esc_url( $action['url'] ).'&redirect='.$orders_endpoint.'">
														<svg stroke="red" viewBox="0 0 75.29 75.29"><use xlink:href="'.sprite_url.'#close"></use></svg>
														'.esc_html( $action['name'] ).'
													</a>';
													break;
												case 'pay':
													// echo '<a class="cancel '.sanitize_html_class( $key ).'" href="'.esc_url( $action['url'] ).'">
													echo '<a class="" href="'.esc_url( $action['url'] ).'">
														<svg stroke="red" viewBox="0 0 77.34 77.34"><use xlink:href="'.sprite_url.'#flash"></use></svg>
														'.esc_html( $action['name'] ).'
													</a>';
													break;
												default:
													break;
											}
											// echo '<a href="' . esc_url( $action['url'] ) . '" class="woocommerce-button button ' . sanitize_html_class( $key ) . '">' . esc_html( $action['name'] ) . '</a>';
										}
									}
									?>
                                </div>
                            </div>
                        </div>
                    </dd>

					<?php

				} 
			}
			?>
		<!-- </tbody> -->
	<!-- </table> -->
	</dl>

	<?php do_action( 'woocommerce_before_account_orders_pagination' ); ?>

	<?php if ( 1 < $customer_orders->max_num_pages ) :

		$prev = null;
		if ( 1 !== $current_page ){
			$prev = '<a href="'.esc_url( wc_get_endpoint_url( 'orders', $current_page - 1 ) ).'">'.__( 'Previous', 'woocommerce' ).'</a>';
		}

		$next = null;
		if ( intval( $customer_orders->max_num_pages ) !== $current_page ){
			$next = '<a href="'.esc_url( wc_get_endpoint_url( 'orders', $current_page + 1 ) ).'">'.__( 'Next', 'woocommerce' ).'</a>';
		}

		$get_pagenum_link = preg_replace('/[0-9]+/', '', get_pagenum_link(999999999));
		$get_pagenum_link = str_replace('//', '/', $get_pagenum_link);
		$pagination_bar = pagination_bar(
			$customer_orders,
			array(
				'base'         => $get_pagenum_link . '%#%',
				'current' => $current_page,
				'total' => intval( $customer_orders->max_num_pages ),
				'prev_next'          => false,
				'type'               => 'list',
			),
			$prev,
			$next,
			false
		);
		$pagination_bar = str_replace(':/','://',$pagination_bar);
		$pagination_bar = str_replace(':///','://',$pagination_bar);
		$pagination_bar = str_replace('/page','',$pagination_bar);
		echo $pagination_bar;
		?>
	
	<?php endif; ?>

<?php else : ?>

	<?php
	/*
	<div class="notif error">
        <svg class="icon" viewBox="0 0 128 128"><use xlink:href="https://igame.ir/wp-content/themes/igame/<?php echo sprite_url; ?>#error"></use></svg>
        <span class="bold"><?php esc_html_e( 'No order has been made yet.', 'woocommerce' ); ?></span>
        <span class="light"></span>
		<a class="link" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
			نمایش محصولات
		</a>
		<div class="clearfix"></div>
	</div>
	*/
	?>
	<div class="noData">
        <img class="image" src="<?php echo theme_assets; ?>/imgs/nodata.svg">
        <h3 class="title">هیچ سفارشی ثبت نشده!</h3>
        <h4 class="text">
			اولین 
			<a class="link" href="<?php echo get_post_type_archive_link( 'game' ); ?>">
				<strong>بازی</strong>
			</a>
			یا 
			<a class="link" href="<?php echo get_post_type_archive_link( 'giftcard' ); ?>">
				<strong>گیفت کارت</strong>
			</a> 
			 خود را سفارش دهید	‌
		</h4>
    </div>


<?php endif; ?>

<?php do_action( 'woocommerce_after_account_orders', $has_orders ); ?>