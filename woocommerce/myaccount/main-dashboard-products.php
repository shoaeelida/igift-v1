<?php
/**
 * My Orders - Deprecated
 *
 * @deprecated 2.6.0 this template file is no longer used. My Account shortcode uses orders.php.
 * @package WooCommerce/Templates
 */

defined( 'ABSPATH' ) || exit;

$my_orders_columns = apply_filters(
	'woocommerce_my_account_my_orders_columns',
	array(
		'order-number'  => esc_html__( 'Order', 'woocommerce' ),
		'order-date'    => esc_html__( 'Date', 'woocommerce' ),
		'order-status'  => esc_html__( 'Status', 'woocommerce' ),
	)
);

$products = get_posts(
    array(
        'posts_per_page' => 20,
        'post_type' => 'game',
        'fields' => 'ids',
    )
);



if ( $products ) : ?>

    <div class="newGames">
        <div class="header">
            <h4 class="title">جدیدترین بازیهای سایت</h4>
            <div class="arrows">
                <svg class="nextCarousel" id="newGamesNext" viewBox="0 0 24 24"><use xlink:href="<?php echo sprite_url; ?>#arrow"></use></svg>
                <svg class="prevCarousel" id="newGamesPrev" viewBox="0 0 24 24"><use xlink:href="<?php echo sprite_url; ?>#arrow"></use></svg>
            </div>
        </div>
        <div class="newGames__content">
            <div class="owl-carousel" id="newGamesCarousel">
                <?php
                foreach ( $products as $product_id ) :
                    set_query_var( 'product_id', $product_id );
                    get_template_part( 'template-parts/content/content', 'product-item-dashboard' );
                endforeach;
                ?>
            </div>
        </div>
    </div>


<?php
endif;