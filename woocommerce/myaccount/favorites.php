<?php
                        
$favorites = get_user_favorites( get_current_user_id() );
?>

<?php if( $favorites /*&& !isset($favorites['posts']) && !empty($favorites['posts'])*/ ){ ?>
    <div id="favorite-content">
        <!-- page title -->
        <div class="pageTitleBar">
            <div class="titleDetails">
                <svg class="icon" viewBox="0 0 465.3 538.24"><use xlink:href="<?php echo sprite_url; ?>#favorite"></use></svg>
                <h1 class="title">لیست علاقه مندی ها</h1>
            </div>
        </div>

        <div class="archive-content pb-5">
            <div class="archive-result favResult">
                <div class="product-result mt-0 card-list-holder">
                    <?php

                    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    $current_page = (int)uf_getLastDir($actual_link);
                    $current_page = ( !$current_page || $current_page <= 0 ) ? 1 : $current_page; // If you want to include pagination
                    $favorites_query = new WP_Query(array(
                        'post_type' => ['game','giftcard','dlc'], // If you have multiple post types, pass an array
                        'posts_per_page' => 12,
                        'post__in' => $favorites,
                        'paged' => $current_page // If you want to include pagination, and have a specific posts_per_page set
                    ));

                    if ( $favorites_query->have_posts() ) {

                        while ( $favorites_query->have_posts() ){
                            $favorites_query->the_post();
                            $product_id = get_the_ID();
                            get_template_part( 'template-parts/content/content', 'product-item-favorites' );
                        }

                    }

                    wp_reset_postdata();
                    ?>
                </div>
                <?php if ( 1 < $favorites_query->max_num_pages ) :
                    $prev = null;
                    if ( 1 !== $current_page ){
                        $prev = '<a href="'.esc_url( wc_get_endpoint_url( 'favorites', $current_page - 1 ) ).'">'.__( 'Previous', 'woocommerce' ).'</a>';
                    }

                    $next = null;
                    if ( intval( $favorites_query->max_num_pages ) !== $current_page ){
                        $next = '<a href="'.esc_url( wc_get_endpoint_url( 'favorites', $current_page + 1 ) ).'">'.__( 'Next', 'woocommerce' ).'</a>';
                    }
                    $pagination_bar = pagination_bar(
                        $favorites_query,
                        array(
                            'current' => $current_page,
                            'total' => intval( $favorites_query->max_num_pages ),
                            'prev_next'          => false,
                            'type'               => 'list',
                        ),
                        $prev,
                        $next,
                        false
                    );
                    echo $pagination_bar;
                    ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

<?php } else{

    // No Favorites
    ?>

    <div class="noData mt-5 pt-3">
        <img class="image" src="<?php echo theme_assets; ?>/imgs/nodata.svg">
        <h3 class="title">لیست علاقه مندی های شما خالیست!</h3>
        <h4 class="text">
			<a class="link" href="<?php echo get_post_type_archive_link( 'game' ); ?>">
				<strong>بازی ها</strong>
			</a>
			یا 
			<a class="link" href="<?php echo get_post_type_archive_link( 'giftcard' ); ?>">
				<strong>گیفت کارت های</strong>
			</a> 
			مورد علاقه خود را انتخاب کنید‌
		</h4>
    </div>

    <?php

}

?>