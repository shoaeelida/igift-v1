<?php
/**
 * My Orders - Deprecated
 *
 * @deprecated 2.6.0 this template file is no longer used. My Account shortcode uses orders.php.
 * @package WooCommerce/Templates
 */

defined( 'ABSPATH' ) || exit;

$is_ticket_page = false;

$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$post_id = uf_getLastDir($actual_link);

if( $post_id && get_post($post_id) ) {
    $is_ticket_page = true;
    if( ( $_post = get_post($post_id) ) !== null ) {
        $is_ticket_page = true;
        global $post;
        $post = $_post;
    }
}

if( $is_ticket_page ){

    // wpas_clean_notification('reply_added_closed',);
    wpas_clean_notification('reply_added','notifications');
    
    $order_id = get_post_meta( $post_id, '_wpas_order_id', true );
    
    ?>

   <!-- page title -->
   <div class="pageTitleBar align-items-baseline">
        <div class="titleDetails">
            <a href="<?php echo esc_url_raw( wc_get_account_endpoint_url( 'support' ) ); ?>" class="btn btn--small btn--white btn--text--left pr-5 return">
                <svg viewBox="0 0 24 24"><use xlink:href="<?php echo sprite_url; ?>#arrow"></use></svg>
                بازگشت
            </a>
        </div>
        <div class="leftPart">
            <span class="orderDetails">
                <span class="date"><?php echo get_the_date('j F y',$post_id); ?></span>
                <span class="hour"><?php echo get_the_time('g:i',$post_id); ?></span>
                <span class="bold"><?php echo $post->post_title; ?></span>
            </span>
            <?php
            if( $order_id ){
                echo '<span class="orderId">
                    <span>مربوط به سفارش</span>
                    <span class="bold">#'.$order_id.'</span>
                </span>';
            } ?>
           </div>
    </div>

    <?php 
    remove_action( 'wpas_before_template', 'wpas_display_errors', 10 );
    wpas_get_template( 'details' ); ?>

<?php } else{ ?>

    <!-- page title -->
    <div class="pageTitleBar">
        <div class="titleDetails">
            <svg class="icon" viewBox="0 0 520 519.97"><use xlink:href="<?php echo sprite_url; ?>#support"></use></svg>
            <h1 class="title">پشتیبانی</h1>
            <!-- <div class="dashBreadcrumb mb-sm-0 mb-3">
                <a href="#">صفحه اصلی</a>
                &#8250;
                <a href="#">ناحیه کاربری</a>
                &#8250;
                <a href="#">پشتیبانی</a>
            </div> -->
            <?php breadcrumbs(true); ?>
        </div>
        <a href="#ticketModal" class="btn btn--large btn--text btn--text--left btn--red m-md-0 m-auto">
            <svg viewBox="0 0 10.38 10.38"><use xlink:href="<?php echo sprite_url; ?>#add"></use></svg>
            ثبت تیکت جدید
        </a>
    </div>

    <?php

    echo do_shortcode( '[tickets]' );

    echo do_shortcode( '[ticket-submit]' );
}