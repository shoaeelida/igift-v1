<?php
/**
 * Show messages
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/notices/notice.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! $messages ) {
	return;
}

return;

?>

<?php foreach ( $messages as $message ) :
	if( strpos($message,'undo_item') !== false ){
		return;
	}
	if( strpos($message,'دیگری') !== false ){
		return;
	}
	?>
	<div class="woocommerce-info">
		<?php
			echo wc_kses_notice( $message );
		?>
	</div>
	<div class="notif error">
        <svg class="icon" viewBox="0 0 128 128"><use xlink:href="<?php echo sprite_url ?>#error"></use></svg>
        <span class="bold"><?php esc_html_e( 'No order has been made yet.', 'woocommerce' ); ?></span>
        <span class="light"></span>
		<a class="link" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
			نمایش محصولات
		</a>
        <div class="clearfix"></div>
    </div>
<?php endforeach; ?>
