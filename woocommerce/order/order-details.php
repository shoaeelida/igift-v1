<?php
/**
 * Order details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

$order = wc_get_order( $order_id ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited

if ( ! $order ) {
	return;
}

if( isset($_POST['buy_g2a']) ){
	add_to_g2a_buy_queue($order_id);
}

// $order_items           = $order->get_items( apply_filters( 'woocommerce_purchase_order_item_types', 'line_item' ) );
$order_items           = $order->get_items();
$show_purchase_note    = $order->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array( 'completed', 'processing' ) ) );
$show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();
$downloads             = $order->get_downloadable_items();
$show_downloads        = $order->has_downloadable_item() && $order->is_download_permitted();

$active_method = get_post_meta( $order_id, 'product_order_active_method', true );
$has_instant_active = ($active_method && $active_method == 'direct' ) ? true : false;

?>

<div class="purchased-list">
	<?php

	foreach ( $order_items as $item_id => $item ) {
		$product = $item->get_product();

		wc_get_template(
			'order/order-details-item.php',
			array(
				'order'              => $order,
				'item_id'            => $item_id,
				'item'               => $item,
				'show_purchase_note' => $show_purchase_note,
				'purchase_note'      => $product ? $product->get_purchase_note() : '',
				'product'            => $product,
			)
		);
	}

	?>
</div>

<div class="purchased-bottom">
	<div class="total-price">
		<span class="amount-paid">مبلغ پرداخت شده <b><?php echo play_price($order->get_total()); ?> </b>تومان</span>
	</div>
	<div class="btn-holder">
		<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="btn btn--transparent btn--small btn-account">
			ورود به ناحیه کاربری من
		</a>
		<?php /* if( $has_instant_active ){
			 ?>
			<button class="btn btn--red btn--small btn--text--right btn-activate instantly-active" disablad>
				فعالسازی آنی بازی ها
				<svg viewBox="0 0 12.56 19.46">
					<use xlink:href="<?php echo sprite_url; ?>#arrow"></use>
				</svg>
			</button>
		<?php  }
		if ( $order->has_status( 'completed' ) ) { ?>
			<a href="<?php echo wp_nonce_url( add_query_arg( 'order_again', $order->id ) , 'woocommerce-order_again' ); ?>" class="btn btn--transparent btn--small btn-account">
				سفارش دوباره	
			</a>
		} */
		?>
	</div>
</div>

<?php
if ( $show_customer_details ) {
	wc_get_template( 'order/order-details-customer.php', array( 'order' => $order ) );
}