<?php
/**
 * Order Item Details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details-item.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
	return;
}

if(!$item){
	return;
}

$product_id = $item->get_product_id();

if(!$product_id){
	return;
}

$order_id = $order->get_id();

$active_method = get_post_meta( $order_id, 'product_order_active_method', true );

$product_obj = get_wc_product_obj($product_id);

if( !$product_obj ){
	return;
}

$g2a_order_key = get_post_meta( $order_id, $product_id . '_g2a_order_key', true );


?>

<div class="purchased-item">
	<div class="product-info">
		<a class="product-img" href="<?php echo $product_obj['url'] ?>">
			<img src="<?php echo $product_obj['thumbnail_url']; ?>">
		</a>
		<div class="product-detail">
			<h2 class="product-name">
				<?php
				$product_title = $product_obj['title'];
				if( $product_obj['product_type'] == 'giftcard' ){
					$product_title = $product_obj['main_title'];
				}
				echo $product_title; ?>
				<?php echo (isset($product_obj['edition']) && !empty($product_obj['edition'])) ? '<span>' . $product_obj['edition'] . '</span>' : ''; ?>
			</h2>
			<div class="product-platform__holder">
				<?php 
				if( $product_obj['product_type'] == 'giftcard' ){
					echo (isset($product_obj['region']['name'])) ? '<span class="platform-info"><b class="rigion">ریجن ' . $product_obj['region']['name'] . '</b></span>' : '';
				}else{
					if( isset($product_obj['platform']) && ( $product_obj['platform']['name'] || $product_obj['platform']['icon'] ) ) { ?>
						<span class="platform-icon">
							<?php echo $product_obj['platform']['name']; ?>
							<?php echo (isset($product_obj['platform']['icon'])) ?  '<img src="'.$product_obj['platform']['icon'].'" alt="'.$product_obj['platform']['name'].'">' : ''; ?>
						</span>
						<span class="platform-info">
							<?php 
							$product_perfix = (isset($product_obj['product_type'])) ? product_type_perfix($product_obj['product_type']) : '';
							echo '<span class="platform">'; 
								echo (isset($product_obj['platform']['name'])) ? $product_perfix . ' ' . $product_obj['platform']['name'] : ''; 
								$shared_capacity = product_capacity_name($product_obj['shared_capacity']);
								echo (!empty($shared_capacity)) ? ' ' . $shared_capacity : '';
							echo '</span>'; 
							echo (isset($product_obj['region']['name'])) ? '<span class="region"> ریجن ' . $product_obj['region']['name'] . '</span>' : ''; 
							?>
						</span> 
					<?php 
					}
				}
				?>
				<div class="product-price">
					<span><?php echo play_price($product_obj['price']); ?></span>
					تومان
				</div>
			</div>
		</div>
	</div>
	<?php if ( $product_obj['product_type'] == 'cdkey' ) { ?>

		<?php if( $g2a_order_key ){ ?>
			<div class="cdkey-wrap activation-state">
				<div class="cdkey-holder">
					<?php if( isset($product_obj['platform']) && ( $product_obj['platform']['name'] ) ) { ?>
						<span>سی دی کی <?php echo $product_obj['platform']['name']; ?></span>
					<?php } ?>
					<div class="cdkey">
						<div class="icon">
							<svg viewBox="0 0 63.23 42.44">
								<use xlink:href="<?php echo sprite_url; ?>#checkmark"></use>
							</svg>
						</div>
						<input type="text" value="<?php echo $g2a_order_key; ?>" readonly>
						<button class="copy-btn copy">کپی</button>
						<!-- <div class="guid">؟
							<div class="guid-hover">
								راهنمای استفاده از سی دی کی یوپلی
							</div>
						</div> -->
					</div>
				</div>
			</div>
		<?php }else{ ?>
			<div class="waiting-activation">
				<svg viewBox="0 0 128 128">
					<use xlink:href="<?php echo sprite_url; ?>#time"></use>
				</svg>
				<?php if( $active_method == 'direct' ){
					echo 'در انتظار فعالسازی آنی';
				}else{
					echo 'در انتظار خرید';
				}
				?>
			</div>
		<?php } ?>

	<?php }elseif ( $product_obj['product_type'] == 'direct' ) {

		$buyed = get_post_meta( $order_id, $product_id . '_direct_buyed', true );
		$buyed = ( !$buyed || $buyed !== '1' ) ? false : true;
		
		if( $buyed ){
			$mes = 'خریداری شد';
			$icon = '<div class="icon icon-checkmark"><svg viewBox="0 0 63.23 42.44"><use xlink:href="'.sprite_url.'#checkmark"></use></svg></div>';
		}else{
			$mes = 'در انتظار خرید';
			$icon = '<svg viewBox="0 0 128 128"><use xlink:href="'.sprite_url.'#time"></use></svg>';
		}

		?>

		<div class="waiting-activation">
			<?php echo $icon; ?>
			<?php echo $mes; ?>
		</div>

	<?php }elseif ( in_array($product_obj['product_type'],['account','shared_account']) ) {

		$base_meta_key = $product_id . '_';
		$username_meta_key = $base_meta_key.'username';
		$password_meta_key = $base_meta_key.'password';

		$username =  get_post_meta( $order_id, $username_meta_key, true );
		$password =  get_post_meta( $order_id, $password_meta_key, true );

		if( $username && !empty($username) && $password && !empty($password) ){ ?>
			<div class="cdkey-wrap activation-state">
				<div class="cdkey-holder">
					<span>نام کاربری</span>
					<div class="cdkey">
						<div class="icon">
							<svg viewBox="0 0 63.23 42.44">
								<use xlink:href="<?php echo sprite_url; ?>#checkmark"></use>
							</svg>
						</div>
						<input type="text" value="<?php echo $username; ?>" readonly>
						<button class="copy-btn copy">کپی</button>
					</div>
					<span>رمز عبور</span>
					<div class="cdkey">
						<div class="icon">
							<svg viewBox="0 0 63.23 42.44">
								<use xlink:href="<?php echo sprite_url; ?>#checkmark"></use>
							</svg>
						</div>
						<input type="text" value="<?php echo $password; ?>" readonly>
						<button class="copy-btn copy">کپی</button>
					</div>
				</div>
			</div>
		<?php }else{ ?>
			<div class="waiting-activation activation-state">
				<svg viewBox="0 0 128 128">
					<use xlink:href="<?php echo sprite_url; ?>#time"></use>
				</svg>
				در انتظار خرید
			</div>
		<?php } ?>

	<?php }elseif( $product_obj['product_type'] == 'giftcard' ){

		$mes = '';
		$icon = '';

		if( $g2a_order_key ){ ?>
			<div class="cdkey-wrap activation-state">
				<div class="cdkey-holder">
					<?php if( isset($product_obj['platform']) && ( $product_obj['platform']['name'] ) ) { ?>
						<span>کد گیفت کارت <?php echo $product_obj['platform']['name']; ?></span>
					<?php }else{ ?>
						<span>کد گیفت کارت <?php echo $product_obj['platform']['name']; ?></span>
					<?php } ?>
					<div class="cdkey">
						<div class="icon">
							<svg viewBox="0 0 63.23 42.44">
								<use xlink:href="<?php echo sprite_url; ?>#checkmark"></use>
							</svg>
						</div>
						<input type="text" value="<?php echo $g2a_order_key; ?>" readonly>
						<button class="copy-btn copy">کپی</button>
					</div>
				</div>
			</div>
			<?php
		}else{
			$mes = 'در انتظار خرید';
			$icon = '<svg viewBox="0 0 128 128"><use xlink:href="'.sprite_url.'#time"></use></svg>';
		}

		?>

		<div class="waiting-activation activation-state">
			<?php echo $icon; ?>
			<?php echo $mes; ?>
		</div>
	
	<?php } ?>
</div>