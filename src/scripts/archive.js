$(document).ready(function () {
  $(".toggle-title").click(function () {
    var title = $(this),
      section = $(this).parent(".expandable"),
      content = section.find(".toggle-content");

    if (section.hasClass("open")) {
      section.removeClass("open");
      content.slideUp(300);
    } else {
      section.addClass("open");
      content.slideDown(300);
    }
  });

  var range = document.getElementById("range");
  noUiSlider.create(range, {
    start: [0, 500],
    connect: true,
    direction: "rtl",
    range: {
      min: 0,
      max: 1000,
    },
  });

  $(".open-filter").click(function () {
    $(".filter-section").addClass("responsive");
  });

  $(".close-filter").click(function () {
    $(".filter-section").removeClass("responsive");
  });
});
