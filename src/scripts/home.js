$(document).ready(function () {
  $("#platform-carousel").owlCarousel({
    margin: 30,
    autoWidth: true,
    rtl: true,
    loop: true,
    autoplay: true,
    center: true,
    autoplay: true,
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 1,
      },
      500: {
        items: 2,
      },
      800: {
        items: 3,
      },
      900: {
        items: 4,
      },
    },
  });

  if ($(window).width() > 600) {
    if (!$("#bestselling-carousel").hasClass("owl-carousel")) {
      $("#bestselling-carousel").addClass("owl-carousel");
    }

    $("#bestselling-carousel").removeClass("cascade-list");

    $("#bestselling-carousel").owlCarousel({
      margin: 10,
      autoWidth: true,
      autoplay: true,
      items: 7,
      rtl: true,
      loop: true,
      nav: true,
      navText: [
        '<svg viewBox="0 0 12.56 19.47"><use xlink: href = "../dist/imgs/sprite.svg#arrow"></use></svg>',
        '<svg viewBox="0 0 12.56 19.47"><use xlink: href = "../dist/imgs/sprite.svg#arrow"></use></svg>',
      ],
      responsive: {
        0: {
          items: 1,
        },
        400: {
          items: 1,
        },
        500: {
          items: 2,
        },
        800: {
          items: 3,
        },
        900: {
          items: 6,
        },
      },
    });
  } else {
    $("#bestselling-carousel").removeClass("owl-carousel");
    $("#bestselling-carousel").addClass("cascade-list");
  }

  $(".search-icon").click(function () {
    if (!$(".input-search").hasClass("active")) {
      $(".input-search").addClass("active");
    }
  });
});
