// !function(a,b){"function"==typeof define&&define.amd?define([],function(){return a.svg4everybody=b()}):"object"==typeof module&&module.exports?module.exports=b():a.svg4everybody=b()}(this,function(){function a(a,b,c){if(c){var d=document.createDocumentFragment(),e=!b.hasAttribute("viewBox")&&c.getAttribute("viewBox");e&&b.setAttribute("viewBox",e);for(var f=c.cloneNode(!0);f.childNodes.length;)d.appendChild(f.firstChild);a.appendChild(d)}}function b(b){b.onreadystatechange=function(){if(4===b.readyState){var c=b._cachedDocument;c||(c=b._cachedDocument=document.implementation.createHTMLDocument(""),c.body.innerHTML=b.responseText,b._cachedTarget={}),b._embeds.splice(0).map(function(d){var e=b._cachedTarget[d.id];e||(e=b._cachedTarget[d.id]=c.getElementById(d.id)),a(d.parent,d.svg,e)})}},b.onreadystatechange()}function c(c){function e(){for(var c=0;c<o.length;){var h=o[c],i=h.parentNode,j=d(i),k=h.getAttribute("xlink:href")||h.getAttribute("href");if(!k&&g.attributeName&&(k=h.getAttribute(g.attributeName)),j&&k){if(f)if(!g.validate||g.validate(k,j,h)){i.removeChild(h);var l=k.split("#"),q=l.shift(),r=l.join("#");if(q.length){var s=m[q];s||(s=m[q]=new XMLHttpRequest,s.open("GET",q),s.send(),s._embeds=[]),s._embeds.push({parent:i,svg:j,id:r}),b(s)}else a(i,j,document.getElementById(r))}else++c,++p}else++c}(!o.length||o.length-p>0)&&n(e,67)}var f,g=Object(c),h=/\bTrident\/[567]\b|\bMSIE (?:9|10)\.0\b/,i=/\bAppleWebKit\/(\d+)\b/,j=/\bEdge\/12\.(\d+)\b/,k=/\bEdge\/.(\d+)\b/,l=window.top!==window.self;f="polyfill"in g?g.polyfill:h.test(navigator.userAgent)||(navigator.userAgent.match(j)||[])[1]<10547||(navigator.userAgent.match(i)||[])[1]<537||k.test(navigator.userAgent)&&l;var m={},n=window.requestAnimationFrame||setTimeout,o=document.getElementsByTagName("use"),p=0;f&&e()}function d(a){for(var b=a;"svg"!==b.nodeName.toLowerCase()&&(b=b.parentNode););return b}return c});

// svg4everybody({
// 	nosvg: false, // shiv <svg> and <use> elements and use image fallbacks
// 	polyfill: true // polyfill <use> elements for External Content
// });
$.fn.outerHTML = function(s) {
    return s
        ? this.before(s).remove()
        : $("<p>").append(this.eq(0).clone()).html();
};

$.fn.isInViewport = function() {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();
    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();
    return elementBottom > viewportTop && elementTop < viewportBottom;
};

var unCommentHtml = function(s) {
    s = s.replace('<!--','');
    s = s.replace('-->','');
    return s;
};

$.fn.valid = function(s) {
    var is_valid = true;
    this.find('input[required]:not([disabled])').each(function(){
        if( !$(this).val() ){
            $(this).addClass('error');
            is_valid = false;
        }else{
            $(this).removeClass('error');
        }
    });
    if( is_valid ) {
        $(this).removeClass('error');
    }else{
        $(this).addClass('error');
    }
    return is_valid;
};

//
// $('#element').donetyping(callback[, timeout=1000])
// Fires callback when a user has finished typing. This is determined by the time elapsed
// since the last keystroke and timeout parameter or the blur event--whichever comes first.
//   @callback: function to be called when even triggers
//   @timeout:  (default=1000) timeout, in ms, to to wait before triggering event if not
//              caused by blur.
// Requires jQuery 1.7+
//
;(function($){
    $.fn.extend({
        donetyping: function(callback,timeout){
            timeout = timeout || 1e3; // 1 second default timeout
            var timeoutReference,
                doneTyping = function(el){
                    if (!timeoutReference) return;
                    timeoutReference = null;
                    callback.call(el);
                };
            return this.each(function(i,el){
                var $el = $(el);
                // Chrome Fix (Use keyup over keypress to detect backspace)
                // thank you @palerdot
                $el.is(':input') && $el.on('keyup keypress paste',function(e){
                    // This catches the backspace button in chrome, but also prevents
                    // the event from triggering too preemptively. Without this line,
                    // using tab/shift+tab will make the focused element fire the callback.
                    if (e.type=='keyup' && e.keyCode!=8) return;

                    // Check if timeout has been set. If it has, "reset" the clock and
                    // start over again.
                    if (timeoutReference) clearTimeout(timeoutReference);
                    timeoutReference = setTimeout(function(){
                        // if we made it here, our timeout has elapsed. Fire the
                        // callback
                        doneTyping(el);
                    }, timeout);
                }).on('blur',function(){
                    // If we can, fire the event since we're leaving the field
                    doneTyping(el);
                });
            });
        }
    });
})($);

//
// elementInViewport('#element')
// https://stackoverflow.com/questions/123999/how-can-i-tell-if-a-dom-element-is-visible-in-the-current-viewport
//
function elementInViewport(el) {
    var top = el.offsetTop;
    var left = el.offsetLeft;
    var width = el.offsetWidth;
    var height = el.offsetHeight;

    while(el.offsetParent) {
        el = el.offsetParent;
        top += el.offsetTop;
        left += el.offsetLeft;
    }

    return (
        top < (window.pageYOffset + window.innerHeight) &&
        left < (window.pageXOffset + window.innerWidth) &&
        (top + height) > window.pageYOffset &&
        (left + width) > window.pageXOffset
    );
}

var log = function (str) {
    console.log(str);
};

var _price = wNumb({
    prefix: 'تومان ',
    decimals: 0,
    thousand: '،'
});

var _priceNoCurrency = wNumb({
    prefix: '',
    decimals: 0,
    thousand: '،'
});

var callback_runner = function(callback,data,element) {

    var callback_func;

    // if is obj
    if( callback ){

        if( callback.includes('.') ){

            // find object
            var callback_obj = callback.substring(0, callback.indexOf('.'));
            callback_obj = window[callback_obj];

            if( typeof callback_obj === "object"){

                callback_func = callback.substring(callback.indexOf(".") + 1);
                callback_func = callback_obj[callback_func];

                if( typeof callback_func === "function"){
                    callback_func(data,element);
                }

            }

        }
        // if is function
        else{

            callback_func = window[successCallback];
            if( typeof callback_func === "function"){
                callback_func(data,element);
            }

        }

    }

};

var fetch = {
    fetch_data : function (method,url,param,successCallback,failCallback,processData,contentType,element) {
        // console.log(' --> ' + method + ' : ' + url);
        // console.log(' --> param' );
        // console.log(param);
        if( method && url ){

            var ajax_data = {
                type: method,
                url: url,
                data: param,
                cache: true,
                success: function (response) {
                    if( successCallback ){
                        callback_runner(successCallback,response,element);
                    }
                },
                error: function (response) {
                    if( failCallback ){
                        callback_runner(failCallback,response,element);
                    }
                },
                fail: function (response) {
                    if( failCallback ){
                        callback_runner(failCallback,response,element);
                    }
                },
            };

            if( processData !== null || processData == false || processData == true ){
                ajax_data.processData = processData;
            }

            if( contentType !== null || contentType == false || contentType == true ){
                ajax_data.contentType = contentType;
            }

            $.ajax(ajax_data);
        }
    },
    update : function (url,param,successCallback,failCallback) {
        this.fetch_data('UPDATE',url,param,successCallback,failCallback,null,null,element);
    },
    post : function (url,param,successCallback,failCallback,element) {
        this.fetch_data('POST',url,param,successCallback,failCallback,null,null,element);
    },
    postForm : function (url,param,successCallback,failCallback,element) {
        this.fetch_data('POST',url,param,successCallback,failCallback,false,false,element);
    },
    get : function (url,param,successCallback,failCallback,element) {
        this.fetch_data('GET',url,param,successCallback,failCallback,null,null,element);
    },
};
function send_sms(phone) {
    $.ajax({
        type: "POST",
        url: app_config.site_url + "/api/otp/v2/phone",
        data: { "phone" : phone},
        headers: {"x-auth-token": ""},
        dataType: "json",
        success: function(result) { console.log(result) }
    });
}
function redirect_to_cart () {
    if (window.innerWidth < 768 ) window.location.href = app_config.site_url + "/cart";
}
var play_app = {
    /**
     * wc vaiables
     */
    checkout_url : null,
    is_logged_in : false,
    user_id : null,
    token : null,
    /**
     * api variables
     */
    api_url : null,
    api_recommend_url : null,
    api_addtocart_url : null,
    api_subscribe_url : null,
    api_products_url : null,
    api_search_url : null,
    api_make_order_url : null,
    api_confirm_phone_url : null,
    api_identity_url : null,
    /**
     * save data variabels
     */
    cats : null,
    selected_cat : null,
    selected_child_cat : null,
    product_url : null,
    custom_order_data : {},
    search_type : '',
    /**
     * other variables
     */
    lazy : null,
    page_content : null,
    // cats_items : null,
    // childs_el : null,
    // childs_items : null,
    step1_btn : null,
    step2_btn : null,
    step3_btn : null,
    search_by_url_form : null,
    search_by_url_form_input : null,
    search_by_url_result : null,
    search_by_url_active : false,
    submit_product_form : null,
    products_list : null,
    chat_btn : null,
    _crispBtn : null,
    _crispFinder : null,
    /**
     * templates
     */
    product_box_template : '<div class="product-box">' +
        '<div class="product-box__cover">{{#if thumbnail_url}}<img src="{{thumbnail_url}}" />{{/if}}</div>'+
        '<div class="product-box__content">'+
        '<header class="product-box__header">'+
        '<h2 class="product-box__title">{{title}}</h2>'+
        '<div class="product-box__subtitle">{{subtitle}}</div>'+
        '<div class="product-box__orginalPrice">{{orginalPrice}}</div>'+
        '</header>'+
        '<div class="product-box__desc">{{desc}}</div>'+
        '<div class="product-box__features"></div>'+
        '<div class="product-box__actions">'+
        '{{#if can_buy}}'+
        '<div class="product-box__price">{{price_html}}</div>'+
        '<a href="{{url}}" class="button alt trackit" ev-category="product" ev-action="product box" ev-label="{{title}}" ev-value="{{id}}">Buy</a>'+
        // '{{add_to_cart}}'+
        '{{else}}'+
        '<a href="{{url}}" class="button alt trackit" ev-category="product" ev-action="product box" ev-label="{{title}}" ev-value="{{id}}">Info</a>'+
        '{{/if}}'+
        '</div>'+
        '</div>'+
        '</div>',
    product_item_template : '<div class="recommend-product" data-id="{{id}}">'+
        '<a class="trackit" href="{{url}}" ev-category="product" ev-action="recommend product" ev-label="{{title}}" ev-value="{{id}}">{{#if thumbnail_url}}<img src="{{thumbnail_url}}" />{{/if}}{{title}}</a>'+
        '</div>',
    searchResult_item_template : '<a class="item" href="{{link}}">'+
        '<span class="name">{{post_title}} <span href="{{link}}">{{action_desc}}</span></span>'+
        '<span class="buy">{{action_title}}</span>'+
        '</a>',
    /**
     * init play_app object
     */
    init : function () {

        this.is_logged_in = app_config.is_logged_in;
        this.user_id = (this.is_logged_in) ? app_config.user_id : null;
        this.token = (this.is_logged_in) ? app_config.token : null;

        this.checkout_url = app_config.checkout_url;

        this.api_url = app_config.api_url;
        this.api_recommend_url = this.api_url + '/recommend';
        this.api_addtocart_url = this.api_url + '/addtocart';
        this.api_subscribe_url = this.api_url + '/subscribe';
        this.api_products_url = this.api_url + '/products';
        this.api_search_url = this.api_url + '/search';
        this.api_make_order_url = this.api_url + '/make_order';
        this.api_confirm_phone_url = this.api_url + '/confirm_phone';
        this.api_identity_url = this.api_url + '/identity';

        // this.cats = JSON.parse(app_config.app_cats);
        this.page_content = $('.page-content');
        // this.cats_items = $('.main-cats a');
        // this.childs_el = $('.child-cats');
        this.step1_btn = $('#order-step-1 .submitBtn');
        this.step2_btn = $('#order-step-2 .submitBtn');
        this.step3_btn = $('#order-step-3 .submitBtn');
        this.search_by_url_form = $('.search-by-url');
        this.search_by_url_form_input = this.search_by_url_form.find('input[type="search"]');
        this.search_by_url_result = $('.search-by-url .searchResult');
        this.submit_product_form = $('.submit-product');
        this.products_list = $('#products-list');
        this.chat_btn = $('.chat-btn');

        this.lazy_init();

        this.page_init();

        this.product_page_init();

        this.home_init();

        this.init_search_by_url();

        this.start_order_init();

        // this.init_step1_cats();
        if( play_app.is_logged_in ){
            this.init_dashboard();
        }

        this.init_steps();

        this.init_add_to_cart();

        this.init_submit_product();

        // this.init_trackers();

        this.init_product_page();

        this.init_fav();

        this.init_confirm_phone();

        // init identity
        this.init_identity();

        // init archive page
        this.init_archive();

        // init cart page
        this.init_cart();

        // init cart page
        this.init_buy_instantly();

        // igame login
        this.igame_otp();

        // $('.searchbar .input-search input').focus(function () {
        //     $(this).removeAttr('readonly')
        // })
        // $('.searchbar .input-search input').focusout(function () {
        //     $(this).attr('readonly','readonly');
        //     $(this).val(' ');
        // });
        $('a[href*=\\#]').on('click', function(e){
            e.preventDefault();
            if($(this.hash).length > 0){
                $('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
            }
        });

        this.buying_process_init();
        
    },
    /**
     *
     * base methods
     */
    getParameterByName : function (name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    },
    copyToClipboard : function(elem) {
        // create hidden text element, if it doesn't already exist
        var targetId = "_hiddenCopyText_";
        var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
        var origSelectionStart, origSelectionEnd;
        if (isInput) {
            // can just use the original source element for the selection and copy
            target = elem;
            origSelectionStart = elem.selectionStart;
            origSelectionEnd = elem.selectionEnd;
        } else {
            // must use a temporary form element for the selection and copy
            target = document.getElementById(targetId);
            if (!target) {
                var target = document.createElement("textarea");
                target.style.position = "absolute";
                target.style.left = "-9999px";
                target.style.top = "0";
                target.id = targetId;
                document.body.appendChild(target);
            }
            target.textContent = elem.textContent;
        }
        // select the content
        var currentFocus = document.activeElement;
        target.focus();
        target.setSelectionRange(0, target.value.length);

        // copy the selection
        var succeed;
        try {
            succeed = document.execCommand("copy");
        } catch(e) {
            succeed = false;
        }
        // restore original focus
        if (currentFocus && typeof currentFocus.focus === "function") {
            currentFocus.focus();
        }

        if (isInput) {
            // restore prior selection
            elem.setSelectionRange(origSelectionStart, origSelectionEnd);
        } else {
            // clear temporary content
            target.textContent = "";
        }
        return succeed;
    },
    validateEmail : function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    },
    redirect : function (url){
        // console.log('redirect to '+url+' ... ' );
        window.location = url;
    },
    reload : function (){
        // console.log('reload ...');
        // console.log(window.location);
        // location.reload();
        window.location = window.location.origin + '' + window.location.pathname;
    },
    getUrlVars : function() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    },
    htmlCompiler : function (template,data) {

        // compile it with Template7
        var compiledTemplate = Template7.compile(template);

        // Now we may render our compiled template by passing required context
        return compiledTemplate(data);

    },
    /**
     *
     * @param {*} eventCategory
     * @param {*} eventAction
     * @param {*} eventLabel
     * @param {*} eventValue
     */
    event_tracker : function (eventCategory,eventAction,eventLabel,eventValue) {
        eventCategory = (!eventCategory || eventCategory == '') ? 'general' : eventCategory;
        eventAction = (!eventAction || eventAction == '') ? 'click' : eventAction;
        eventLabel = (!eventLabel || eventLabel == '') ? 'general' : eventLabel;
        eventValue = (!eventValue || eventValue == '') ? 0 : parseInt(eventValue);
        var opt_noninteraction = '';

        ga('send', 'event', {
            eventCategory: eventCategory, //required
            eventAction: eventAction, //required
            eventLabel: eventLabel,
            eventValue: eventValue,
            hitCallback: function() {
                // console.log('Sent!!');
                //callback function
            },
            hitCallbackFail : function () {
                // console.log("Unable to send Google Analytics data");
                //callback function
            }
        });


    },
    init_trackers : function () {

        $('.trackit:not(.enabled)').on('click',function () {

            var _this = $(this),
                eventCategory = _this.attr('ev-category'),
                eventAction = _this.attr('ev-action'),
                eventLabel = _this.attr('ev-label'),
                eventValue = _this.attr('ev-value');

            play_app.event_tracker(eventCategory,eventAction,eventLabel,eventValue);

        }).addClass('enabled');

    },
    makeNotic : function ( title, text, type, closabled , autoHide, autoHideTimeOut ) {

        $('.mainAlert').not('.closabled').hide().remove();

        closabled = ( closabled && closabled !== false ) ? true : false;
        autoHide = ( !autoHide || autoHide == true ) ? true : false;
        autoHideTimeOut = ( autoHideTimeOut ) ? autoHideTimeOut : 6000;
        var _html = '';
        var _class = 'error';
        var _icon = 'error';

        if( type ){
            switch (type) {
                case 'error':
                    _class = 'error';
                    _icon = 'error';
                    break;
                case 'success':
                    _class = 'success';
                    _icon = 'success';
                    break;
                case 'warning':
                    _class = 'warning';
                    _icon = 'warning';
                    break;
                default:
                    break;
            }
        }

        if( closabled ){
            _class += ' closabled';
        }

        _html += '<div class="mainAlert notif '+_class+'" style="display:none;">';

        _html += '<svg class="icon" viewBox="0 0 128 128"><use xlink:href="'+app_config.sprite+'#'+_icon+'"></use></svg>';

        if( title ){
            _html += '<span class="bold">'+title+'</span> ';
        }

        if( text ){
            _html += '<span class="light">'+text+'</span>';
        }

        if( closabled ){
            _html += '<button class="close"><svg viewBox="0 0 75.29 75.29"><use xlink:href="'+app_config.sprite+'#close"></use></svg></button>';
        }

        _html += '<div class="clearfix"></div>';

        _html += '</div>';

        var new_notic = $(_html).appendTo( $('body') );

        new_notic
            .fadeIn('fast',function () {

                if( autoHide ){
                    setTimeout(function () {
                        new_notic.fadeOut('fast',function () {
                            $(this).remove();
                        });
                    }, autoHideTimeOut);
                }

            })
            .on('click',function () {
                $(this).fadeOut('fast',function () {
                    $(this).remove();
                });
            });

    },
    /**
     *
     * get methods
     */
    get_product : function ( get_parameter , get_val , successCallback , failCallback) {
        var params = {};
        params[get_parameter] = get_val;
        // fetch.get(play_app.api_products_url,params,successCallback,failCallback);
        fetch.get(play_app.api_search_url,params,successCallback,failCallback);
    },
    // get_recommends : function (parent_cat,child_cat,url) {
    //     var params = {
    //         'parent_cat' : parent_cat,
    //         'child_cat' : child_cat,
    //     };
    //     play_app.render_recommends();
    //     fetch.get(play_app.api_recommend_url,params,'play_app.after_get_recommend');
    // },
    // get_child_cats : function (parent_id) {

    //     log( 'play_app.cats' );
    //     log( play_app.cats );
    //     log( 'parent_id >> ' + parent_id );

    //     if( !play_app.cats || !parent_id ) return false;

    //     if ( !play_app.cats.hasOwnProperty(parent_id) ) return false;

    //     return ( play_app.cats[parent_id]['children'] && play_app.cats[parent_id]['children'] !== null ) ? play_app.cats[parent_id]['children'] : false;

    // },
    /**
     * other methods
     */
    lazy_init : function () {
        lazySizes.init()
    },
    page_init : function () {

        if( app_config.show_login ){
            $(".register-wrap").click();
        }

        $('.namads-enamad').click(function (e) {
            e.preventDefault();
            window.open($(this).attr('data-url'));
        });

        // $('.searchbar .input-search,.searchbar .input-search *').on('click',function (e) {
        //     e.preventDefault();
        //     play_app.toggle_step(0,'show');
        // });

        $('.modalOverlay').addClass('enabled');

        $('.dig-cont-close').on('click',function () {
            play_app.page_content.removeClass('blur');
            $('html').css('overflow','auto');
        });

        $('#dig-ucr-container').on('click',function (e) {
            if( !$(e.target).hasClass('dig-content') && $(e.target).parents('.dig-content').length <= 0 ){
                play_app.page_content.removeClass('blur');
                $('html').css('overflow','auto');
            }
        });

        $('.login-logout .digits-login-modal').on('click',function(){
            $(".responsive-menu").removeClass("active");
        });

        $(".mini-menu").click(function () {
            $(".responsive-menu").addClass("active");
            play_app.page_content.addClass('blur');
            $('html').css('overflow','hidden');
        });

        $(".menu-close").click(function () {
            $(".responsive-menu").removeClass("active");
            play_app.page_content.removeClass('blur');
            $('html').css('overflow','auto');
        });

        $('.hero-content .add-to-cart,.description-content .more,.description-content .more-btn').on('click',function (e) {
            e.preventDefault();
            var speed = 1000;
            var _top = $( $(this).attr('href') ).offset().top;
            $([document.documentElement, document.body]).animate({
                scrollTop: _top
            }, speed);
        });

        /**
         * smooth scroll on anchors
         */
        // $('a[href^="#"]').on('click',function (e) {
        //     e.preventDefault();
        //     var _target = $($(this).attr('href'));
        //     if( _target.length > 0 ){
        //         $($(this).attr('href')).scrollIntoView({
        //             behavior: 'smooth'
        //         });
        //     }
        // });

        // play_app['product_main_content'] = $('.product-main-content');

        // if( play_app['product_main_content'].length > 0 ){
        //     play_app['require-system'] = play_app['product_main_content'].find('#required');
        //     play_app['product_about'] = play_app['product_main_content'].find('#about');
        //     play_app['product_game_properties'] = play_app['product_main_content'].find('#properties');
        //     play_app['product_game_guide'] = play_app['product_main_content'].find('#guide');
        //     $(window).scroll(function () {
        //         play_app.highlight_product_content();
        //     });
        // }

        $('.activation-method__holder .method-option').change(function () {
            if( $(this).val() == 'direct' ){
                $('.direct-method .activation-platform')
                    .slideDown('fast')
                    .find('input').prop('disabled',false);
            }else if( $(this).val() == 'directbuy' ){
                $('.directbuy-method .activation-platform')
                    .slideDown('fast')
                    .find('input').prop('disabled',false);
            }else{
                $('.activation-platform')
                    .slideUp('fast')
                    .find('input').prop('disabled',true);
            }
        });

        var search_bar = $('.searchbar.can_sticky');
        if( search_bar.length > 0 ){
            var num = 30; //number of pixels before modifying styles
            $(window).bind('scroll', function () {
                if ($(window).scrollTop() > num) {
                    search_bar.addClass('sticky');
                } else {
                    search_bar.removeClass('sticky');
                }
            });
        }

        $(".wide-carousel").owlCarousel({
            margin: 15,
            autoWidth: true,
            autoplay: true,
            items: 7,
            rtl: true,
            loop: true,
            center: true,
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 1
                },
                500: {
                    items: 2
                },
                800: {
                    items: 3
                },
                900: {
                    items: 6
                }
            }
        });

        $('.copy, .cdkey input').on('click',function(e){
            e.preventDefault();
            e.stopPropagation();
            var input = $(this).parent().find('input');
            if( input.length > 0 ){
                if( play_app.copyToClipboard(input[0]) ){
                    play_app.makeNotic('تبریک میگم', 'با موفقیت کپی شد','success');
                }
            }
        });

        $('.open-chat').on('click',function(e){
            e.preventDefault();
            play_app.open_chat();
        });

        // auto redirect
        var autoRedirect = $('.auto-redirect');
        if( autoRedirect.length > 0 ){

            var autoRedirect_counter = 10; // 10 seconds
            var autoRedirect_url = autoRedirect.attr('data-url'); // url
            var autoRedirect_seconds = autoRedirect.find('.redirect-seconds'); // url
            var autoRedirect_setInterval; // url

            if( autoRedirect_url ){
                autoRedirect_setInterval = setInterval(function(){
                    if( autoRedirect_seconds.length > 0 ){
                        autoRedirect_seconds.text(autoRedirect_counter);
                    }
                    if( autoRedirect_counter <= 0 ){
                        clearInterval(autoRedirect_setInterval);
                        play_app.redirect(autoRedirect_url);
                    }
                    autoRedirect_counter--;
                },1000);
            }

        }

        /**
         * help
         */
        play_app.helpQuestion();

        /**
         * Faqs
         */
        play_app.faqInit();

        $('.subscribe-form').submit(function (e) {
            e.preventDefault();
            var _this = $(this);
            var _email_input = _this.find('input[type="email"]');
            var $thisbutton = _this.find('button');
            var _email = _email_input.val();
            var data = {
                email: _email,
            };
            if( !$thisbutton.hasClass('loading') ){
                $thisbutton.addClass('loading');
                fetch.post( play_app.api_subscribe_url, data, 'play_app.success_subscribe','play_app.failed_subscribe',$thisbutton);
            }
            return false;
        });

        play_app.chat_btn.on('click',function (e) {
            e.preventDefault();
            play_app.chat_btn.addClass('loading');
            play_app.open_chat();
        });

    },
    buying_process_init : function () {

        var buying_counter = $('#buying-counter-holder');
        if( buying_counter.length > 0 ){

            // Set the date we're counting down to
            var add_minutes = 10;
            var _url = window.location.href;

            var last_buy_id = localStorage['last_buy_id'] || null;
            
            var _current_buy_id = _url.match(/(?<=order-received\/).*(?=\/)/s);
            var current_buy_id = _current_buy_id && _current_buy_id.length > 0 ? _current_buy_id[0] : '';
            
            var countDownDate = new Date(new Date().getTime() + add_minutes * 60000 ).getTime();

            if( current_buy_id == last_buy_id ){
                countDownDate = localStorage['buy_countDownDate'];
            }else{
                localStorage['last_buy_id'] = current_buy_id;
                localStorage['buy_countDownDate'] = countDownDate;
                last_buy_id = localStorage['last_buy_id'];
            }

            // Update the count down every 1 second
            var x = setInterval(function() {

                // Get today's date and time
                var now = new Date().getTime();

                // Find the distance between now and the count down date
                var distance = countDownDate - now;

                // Time calculations for days, hours, minutes and seconds
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // make two number
                days = days.toString();
                days = days.length == 1 ? '0' + days : days;

                hours = hours.toString();
                hours = hours.length == 1 ? '0' + hours : hours;

                minutes = minutes.toString();
                minutes = minutes.length == 0 ? '00' : minutes;
                minutes = minutes.length == 1 ? '0' + minutes : minutes;

                seconds = seconds.toString();
                seconds = seconds.length == 0 ? '00' : seconds;
                seconds = seconds.length == 1 ? '0' + seconds : seconds;

                // Display the result in the element with id="demo"
                buying_counter.find('.seconds').text(seconds);
                buying_counter.find('.minutes').text(minutes);

                // If the count down is finished, write some text
                if (distance < 0) {
                    buying_counter.find('.seconds').text('00');
                    buying_counter.find('.minutes').text('00');
                    clearInterval(x);
                }

                buying_counter.fadeIn();

            }, 1000);

            var buying_counter_interval_counter = 0;

            var buying_counter_interval = setInterval(function () {

                buying_counter_interval_counter++;
                var _url = window.location.href;

                $.ajax({
                    url: _url,
                    type: 'GET',
                    success: function (data) {

                        if( $($.parseHTML(data)).find("#buying-counter-holder").length <= 0 ){
                            window.location.href = _url;
                        }

                    },
                    failed: function () {
                        clearInterval(buying_counter_interval);
                    },
                    error: function () {
                        clearInterval(buying_counter_interval);
                    }
                });

                if( buying_counter_interval_counter > 40 ){
                    clearInterval(buying_counter_interval);
                }

            },40000);

            
        }

    },
    add_crisp : function () {

        // add crisp script
        window.$crisp=[];
        window.CRISP_WEBSITE_ID="d39d909e-a6ec-41ca-b562-ba686569071a";
        d=document;s=d.createElement("script");
        s.src="https://client.crisp.chat/l.js";
        s.async=1;
        d.getElementsByTagName("head")[0].appendChild(s);

    },
    _open_chat : function () {
        if( play_app._crispBtn && play_app._crispBtn.length > 0 ){
            // open chat box
            $crisp.push(['do', 'chat:open']);
        }else{
            play_app._crispFinder = setInterval(function () {
                play_app._crispBtn = $('.crisp-client a');
                if( play_app._crispBtn && play_app._crispBtn.length > 0 ){
                    // open chat box
                    $('.crisp-kquevr').click().trigger('click');
                    $crisp.push(['do', 'chat:open']);
                    clearInterval(play_app._crispFinder);
                }
            },200);
        }
    },
    open_chat : function (_crispBtn) {

        play_app.app_crispBtn = $('.crisp-client a');

        if(typeof $crisp == 'undefined'){

            play_app.add_crisp();
            var chat_btn_finder = null;

            // wait to init chat script
            play_app._crispFinder = setInterval(function () {
                if(typeof $crisp !== 'undefined'){
                    clearInterval(play_app._crispFinder);
                    play_app._open_chat();

                    chat_btn_finder = setInterval(function () {
                        if( $('.crisp-client a').length > 0 ){
                            // hide fake chat btn
                            play_app.chat_btn.removeClass('loading').fadeOut('fast');
                            clearInterval(chat_btn_finder);
                        }
                    },100);

                }
            },200);

        }else{

            play_app._open_chat();

        }

    },
    product_page_init : function () {
        //**********Tab description */
        $(".tab-holder li").click(function () {
            if (!$(this).hasClass("active")) {
            $(".tab-holder li.active").removeClass("active");
            $(this).addClass("active");
            }
            var id = $(this).attr("id");
            $(".tab-content.active").removeClass("active");
            $("#" + id + "-content").addClass("active");
        });
    },
    success_subscribe: function (response,$thisbutton) {
        play_app.makeNotic(response.message,'','success');
        $thisbutton.parent().find('input').val('');
        $thisbutton.removeClass('loading');
    },
    failed_subscribe: function (response,$thisbutton) {
        if( response && response.responseJSON.message ){
            play_app.makeNotic(response.responseJSON.message,'','error');
        }else{
            play_app.makeNotic('مشکلی در عضویت در خبرنامه پیش آمد.','','error');
        }
        $thisbutton.removeClass('loading');
    },
    faqInit : function () {

        var faqEl = $('.faq');

        if (faqEl.length <= 0) {
            return;
        }

        // get hash value
        var hash = window.location.hash;
        // now scroll to element with that id
        if( hash && hash.includes('q-') ){

            // close ope faqs
            $('.faq-header.active').removeClass('active')
                .siblings('.faq-content').slideUp('fast')

            // open target faq
            var target_hash = $(hash);
            var target_faq = target_hash.parents('.faq');
            target_faq.find('.faq-header').addClass('active');
            target_hash.slideDown('fast',function () {
                $('html, body').animate({ scrollTop: target_faq.offset().top },300);
            });

            //console.log(target_faq.attr('data-tab'));
            $('.tabsContainer [data-tab='+ target_faq.attr('data-tab') +']').addClass('active')

        }

        var faqHeader = faqEl.find('.faq-header');
        var faqContents = faqEl.find('.faq-content');
        var faqTabs = $('.tabs-faq');
        var faqTab = faqTabs.find('.tab');

        faqHeader.on('click',function(e){
            e.preventDefault();

            var _this = $(this),
                _target = $('#q-'+_this.attr('data-question'));

            if( _target.length <= 0 ){
                return;
            }

            faqHeader.not(_this).removeClass('active');
            faqContents.not(_target).fadeOut('fast');

            _this.toggleClass('active');
            _target.fadeToggle('fast',function () {

                if( faqHeader.hasClass('active') ){
                    var _top = _this.offset().top - 30;
                    $([document.documentElement, document.body]).animate({
                        scrollTop: _top
                    }, 300);
                }

            });

        });

        faqTab.on('click', function (e) {
            e.preventDefault();

            var _this = $(this);
            var tab_id = _this.attr('data-tab');

            faqTab.removeClass('active');
            _this.addClass('active');

            var _top = faqTabs.offset().top;
            $([document.documentElement, document.body]).animate({
                scrollTop: _top
            }, 300);

            var num = 1;
            faqEl.each(function () {
                var t = $(this);
                var ttab = t.attr('data-tab');

                if( tab_id == 'all' ){
                    t.slideDown('fast')
                        .find('.faq-num').text(num);
                    num++;
                }else{
                    if( ttab == tab_id ){
                        t.slideDown('fast')
                            .find('.faq-num').text(num);
                        num++;
                    }else{
                        t.slideUp('fast');
                    }
                }

            });


        });

    },
    helpQuestion : function(){

        var helpEl = $('#help-box');

        if (helpEl.length <= 0) {
            return;
        }

        $('.tabs-help .tab:not(.enabled)').on('click', function (e) {
            e.preventDefault();

            var _this = $(this);

            var href = _this.attr('href');

            if (href) {
                helpEl.addClass('loading');
                $.ajax({
                    url: href,
                    type: 'GET',
                    success: function (data) {

                        history.replaceState({}, document.title, href);
                        
                        helpEl.html($($.parseHTML(data)).filter("#help-box").html());
                        helpEl.removeClass('loading');
                        $('.tabs .tab').removeClass('active');

                        _this.addClass('active');

                        play_app.helpQuestion();
                        var _top = $('.tabs').offset().top - 30;
                        $([document.documentElement, document.body]).animate({
                            scrollTop: _top
                        }, 300);
                    },
                    failed: function () {
                        helpEl.removeClass('loading');
                    },
                    error: function () {
                        helpEl.removeClass('loading');
                    }
                });
            }
        }).addClass('enabled');

        $('.questions .item:not(.enabled)').click(function (e) {
            e.preventDefault();

            var _this = $(this);

            var href = _this.attr('href');

            if (href) {
                helpEl.addClass('loading');
                $.ajax({
                    url: href,
                    type: 'GET',
                    success: function (data) {
                        history.replaceState({}, document.title, href);
                        helpEl.html($($.parseHTML(data)).filter("#help-box").html());
                        helpEl.removeClass('loading');
                        play_app.helpQuestion();
                        var _top = helpEl.offset().top;
                        $([document.documentElement, document.body]).animate({
                            scrollTop: _top
                        }, 300);
                    },
                    failed: function () {
                        helpEl.removeClass('loading');
                    },
                    error: function () {
                        helpEl.removeClass('loading');
                    }
                });
            }
        }).addClass('enabled');

        $('#help-box .pagination a:not(.enabled)').click(function (e) {
            e.preventDefault();

            var _this = $(this);

            var href = _this.attr('href');

            if (href) {
                helpEl.addClass('loading');
                $.ajax({
                    url: href,
                    type: 'GET',
                    success: function (data) {
                        history.replaceState({}, document.title, href);
                        helpEl.html($($.parseHTML(data)).filter(".help").html());
                        helpEl.removeClass('loading');
                        play_app.helpQuestion();
                        var _top = helpEl.offset().top;
                        $([document.documentElement, document.body]).animate({
                            scrollTop: _top
                        }, 300);
                    },
                    failed: function () {
                        helpEl.removeClass('loading');
                    },
                    error: function () {
                        helpEl.removeClass('loading');
                    }
                });
            }
        }).addClass('enabled');

    },
    // highlight_product_content : function () {
    //     if($('.about-section').length && $('.about-section').offset().top <= $(window).scrollTop()+490 ){
    //         play_app['product_game_properties'].removeClass('active');
    //         play_app['require-system'].removeClass('active');
    //         play_app['product_game_guide'].removeClass('active');
    //         play_app['product_about'].addClass('active');
    //     } else {
    //         play_app['product_about'].removeClass('active');
    //     }
    //     if( $('.properties-section').length && $('.properties-section').offset().top <= $(window).scrollTop()+490 ){
    //         play_app['product_about'].removeClass('active');
    //         play_app['require-system'].removeClass('active');
    //         play_app['product_game_guide'].removeClass('active');
    //         play_app['product_game_properties'].addClass('active');
    //     } else {
    //         play_app['product_game_properties'].removeClass('active');
    //     }
    //     if( $('.required-section').length && $('.required-section').offset().top <= $(window).scrollTop()+490 ){
    //         play_app['product_game_properties'].removeClass('active');
    //         play_app['product_about'].removeClass('active');
    //         play_app['product_game_guide'].removeClass('active');
    //         play_app['require-system'].addClass('active');
    //     } else {
    //         play_app['require-system'].removeClass('active');
    //     }
    //     if( $('.guide-section').length && $('.guide-section').offset().top <= $(window).scrollTop()+490 ){
    //         play_app['product_ga' +
    //         'me_properties'].removeClass('active');
    //         play_app['product_about'].removeClass('active');
    //         play_app['require-system'].removeClass('active');
    //         play_app['product_game_guide'].addClass('active');
    //     } else {
    //         play_app['product_game_guide'].removeClass('active');
    //     }
    // },
    home_init : function () {
        var enableResendCode = false;
        function verify_code_ajax(phone,verify_code) {
            $.ajax({
                type: "POST",
                url: app_config.site_url + "/api/otp/v2/verify",
                data: { "phone" : phone , "verify" : verify_code },
                dataType: "json",
                success: function(result) {
                    if (result == '0') {
                        verify_code_ajax(phone,verify_code);
                    } else if (result == '1') {
                        setTimeout(function () {
                            window.location.href = app_config.site_url + "/dashboard";
                        },500);
                    } else {
                        return false;
                    }
                }
            });
        }
        $('.open-login-modal,.digits-login-modal').click(function (e) {
            e.preventDefault();
            $('.otp-modal-wrapper').fadeIn(800)
        });
        $('.otp-close-btn').click(function () {
            $('.otp-modal-wrapper').fadeOut(0)
        });
        $('.modal-send-sms-btn-l').click(function () {
            var phone = $('#phone-number-l').val();
            var regex = new RegExp('^(09)?\\d{9}$');
            var result = regex.test(phone);
            if (result) {
                send_sms(phone)
                $('.otp-login .otp-step-one').fadeOut(0)
                $('.otp-login .otp-step-two').fadeIn(600)
                var count= 60;
                var counter = setInterval(timer, 1000);
                function timer() {
                    count=count-1;
                    if (count <= 0) {
                        $('.otp-countdown span').text('');
                        enableResendCode = true;
                        clearInterval(counter);
                        return;
                    }
                    $('.otp-countdown span').text(' '+ count)
                }
            }});
        $('.modal-verify-sms-btn-l').click(function () {
            var phone = $('#phone-number-l').val();
            var verify_code = $('#verify-code-l').val();
            var regex_vc = new RegExp('^[0-9]{1,5}$');
            var result_vc = regex_vc.test(verify_code);
            if (result_vc) {
                verify_code_ajax(phone,verify_code)
            }
        });
        $('.go-to-register').click(function () {
            $('.otp-login').fadeOut(0)
            $('.otp-register,.otp-register .otp-step-one').fadeIn(600)
        });
        $('.go-to-login').click(function () {
            $('.otp-register').fadeOut(0)
            $('.otp-login').fadeIn(600)
        });
        $('.modal-send-sms-btn-r').click(function () {
            var phone = $('#phone-number-r').val();
            var regex = new RegExp('^(09)?\\d{9}$');
            var result = regex.test(phone);
            if (result) {
                send_sms(phone)
                $('.otp-register .otp-step-one').fadeOut(0)
                $('.otp-register .otp-step-two').fadeIn(600)
                var count= 60;
                var counter = setInterval(timer, 1000);
                function timer() {
                    count=count-1;
                    if (count <= 0) {
                        $('.otp-countdown-r span').text('');
                        enableResendCode = true;
                        clearInterval(counter);
                        return;
                    }
                    $('.otp-countdown-r span').text(' '+ count)
                }
            }});
        $('.modal-verify-sms-btn-r').click(function () {
            var phone = $('#phone-number-r').val();
            var verify_code = $('#verify-code-r').val();
            var regex_vc = new RegExp('^[0-9]{1,5}$');
            var result_vc = regex_vc.test(verify_code);
            if (result_vc) {
                verify_code_ajax(phone,verify_code)
            }
        });
        $('.otp-countdown-l').click(function () {
            if (enableResendCode) {
                var phone = $('#phone-number-l').val();
                send_sms(phone)
            }
        });
        $('.otp-countdown-r').click(function () {
            if (enableResendCode) {
                var phone = $('#phone-number-r').val();
                send_sms(phone)
            }
        });

        // $(".carousel-holder").owlCarousel({
        //     singleItem: true,
        //     autoWidth: false,
        //     //autoplay: true,
        //     items: 1,
        //     rtl: true,
        //     loop: true,
        //     center: true,
        //     nav: true,
        //     navText: [
        //     '<svg viewBox="0 0 12.56 19.46"><use xlink: href="'+app_config.sprite+'#arrow"></use></svg>',
        //     '<svg viewBox="0 0 12.56 19.46"><use xlink: href="'+app_config.sprite+'#arrow"></use></svg>',
        //     ],
        // });
        
        if ($(window).width() > 600) {
            if (!$(".additional-product").hasClass("owl-carousel")) {
            $(".additional-product").addClass("owl-carousel");
            }
            if (!$(".giftcard-product").hasClass("owl-carousel")) {
            $(".giftcard-product").addClass("owl-carousel");
            }
            if (!$("#expected-slider").hasClass("owl-carousel")) {
            $("#expected-slider").addClass("owl-carousel");
            }
        
            $(".additional-product").removeClass("cascade-list");
            $(".giftcard-product").removeClass("cascade-list");
            $("#expected-slider").removeClass("flex");
        
            $(".additional-product").owlCarousel({
            margin: 20,
            autoWidth: true,
            // autoplay: true,
            items: 6,
            rtl: true,
            loop: true,
            });
            $(".giftcard-product").owlCarousel({
            margin: 20,
            autoWidth: true,
            // autoplay: true,
            items: 6,
            rtl: true,
            loop: true,
            });
            $("#expected-slider").owlCarousel({
            margin: 20,
            autoWidth: true,
            autoplay: true,
            items: 5,
            rtl: true,
            center: true,
            loop: true,
            });
        } else {
            $(".additional-product").removeClass("owl-carousel");
            $(".giftcard-product").removeClass("owl-carousel");
            $("#expected-slider").removeClass("owl-carousel");
        
            $(".additional-product").addClass("cascade-list");
            $(".giftcard-product").addClass("cascade-list");
            $("#expected-slider").addClass("flex");
        }
        
        $(".search-icon").click(function () {
            if (!$(".input-search").hasClass("active")) {
            $(".input-search").addClass("active");
            }
        });
        
        if ($(window).width() <= 600) {
            $(".additional-product").removeClass("owl-carousel");
        }


        $("#platform-carousel").owlCarousel({
            margin: 30,
            autoWidth: true,
            rtl: true,
            loop: true,
            autoplay: true,
            center: true,
            autoplay: true,
            responsive: {
              0: {
                items: 1,
              },
              400: {
                items: 1,
              },
              500: {
                items: 2,
              },
              800: {
                items: 3,
              },
              900: {
                items: 4,
              },
            },
          });
        
          if ($(window).width() > 600) {
            if (!$("#bestselling-carousel").hasClass("owl-carousel")) {
              $("#bestselling-carousel").addClass("owl-carousel");
            }
        
            $("#bestselling-carousel").removeClass("cascade-list");
        
            $("#bestselling-carousel").owlCarousel({
              margin: 10,
              autoWidth: true,
              autoplay: true,
              items: 7,
              rtl: true,
              loop: true,
              nav: true,
              navText: [
                '<svg viewBox="0 0 12.56 19.47"><use xlink: href = "'+app_config.sprite+'#arrow"></use></svg>',
                '<svg viewBox="0 0 12.56 19.47"><use xlink: href = "'+app_config.sprite+'#arrow"></use></svg>',
              ],
              responsive: {
                0: {
                  items: 1,
                },
                400: {
                  items: 1,
                },
                500: {
                  items: 2,
                },
                800: {
                  items: 3,
                },
                900: {
                  items: 6,
                },
              },
            });
          } else {
            $("#bestselling-carousel").removeClass("owl-carousel");
            $("#bestselling-carousel").addClass("cascade-list");
          }
        
    },
    // reset_selected_cats : function () {
    //     log(' << reset_selected_cats >> ');
    //     play_app.selected_cat = null;
    //     play_app.selected_child_cat = null;
    //     play_app.childs_el.html('');
    // },
    add_to_cart : function ($thisbutton,product_id ,product_sku ,quantity , variation_id) {
        // console.log('add to cart : ' + product_id);
        var data = {
            action: 'woocommerce_ajax_add_to_cart',
            product_id: product_id,
            product_sku: product_sku,
            quantity: quantity,
            variation_id: variation_id,
        };

        $(document.body).trigger('adding_to_cart', [$thisbutton, data]);

        fetch.post( play_app.api_addtocart_url, data, 'play_app.success_added_to_cart','play_app.failed_added_to_cart',$thisbutton);

    },
    success_added_to_cart: function (response,$thisbutton) {
        if( response['status'] == 'successful' ){
            if( response.hasOwnProperty('pay_url') ){
                // log(response['pay_url']);
                play_app.redirect(response['pay_url']);
            }
        }
        $thisbutton.addClass('added').removeClass('loading');
    },
    failed_added_to_cart: function (response,$thisbutton) {
        log('cant add to cart');

        // if (response.error & response.product_url) {
        //     // window.location = response.product_url;
        //     // return;
        // } else {
        //     $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $thisbutton]);
        // }
    },
    init_add_to_cart : function () {

        $('form.cart').submit(function (e) {
            e.preventDefault();
            var $form = $thisbutton.closest('form.cart'),
                $thisbutton = $form.find('button'),
                id = $thisbutton.val(),
                product_qty = $form.find('input[name=quantity]').val() || 1,
                product_id = $form.find('input[name=product_id]').val() || id,
                variation_id = $form.find('input[name=variation_id]').val() || 0;

            // log('product_id >> ');
            // log(product_id);

            if( !$thisbutton.hasClass('added') ){
                play_app.add_to_cart($thisbutton,product_id);
            }
            return false;
        });

        $('.single_add_to_cart_button:not(.enabled)').on('click',function (e) {
            e.preventDefault();
            var $thisbutton = $(this),
                $form = $thisbutton.closest('form.cart'),
                id = $thisbutton.val(),
                product_qty = $form.find('input[name=quantity]').val() || 1,
                product_id = $form.find('input[name=product_id]').val() || id,
                variation_id = $form.find('input[name=variation_id]').val() || 0;

            if( !$thisbutton.hasClass('added') ){
                play_app.add_to_cart($thisbutton,product_id);
            }

            return false;
        }).addClass('enabled');

    },
    // init_child_cats : function () {

    //     play_app.childs_items = play_app.childs_el.find('a');

    //     if( play_app.childs_items.length <= 0 ) return false;

    //     play_app.childs_items.on('click',function (e) {

    //         e.preventDefault();

    //         var _this = $(this);
    //         var _id = _this.attr('data-id');

    //         play_app.selected_child_cat = _id;

    //         // play_app.toogle_step1_btn('active');

    //     });

    // },
    /**
     * steps methods
     */
    start_order_init: function () {
        // $('.start-order').on('click',function (e) {
        //     e.preventDefault();
        //     if( play_app.is_logged_in ){
        //         play_app.toggle_step(1,'show');
        //     }else{
        //         // $
        //         // show_digits_login_modal($this);
        //         $(this).digits_login_modal($(this));
        //     }
        //     return false;
        // });
        $('.start-order').on('click',function (e) {
            // console.log('start-order');
            e.preventDefault();
            // if( play_app.is_logged_in ){
            $('#order-step-1 .title').fadeOut(0);
            play_app.toggle_step(1,'show');
            // }else{
            //     play_app.show_login_modal();
            // }
            return false;
        });

        $('.closeModal,.closeModalBtn').click(function () {
            $(this).parents('.modalOverlay').removeClass('opened');
            play_app.page_content.removeClass('blur');
            $('body').css('overflow','auto');
        });

    },
    // init_step1_cats : function () {

    //     if( play_app.cats_items.length <= 0 ) return false;

    //     // reset selected cats
    //     play_app.reset_selected_cats();

    //     play_app.cats_items.on('click',function (e) {

    //         e.preventDefault();

    //         // play_app.toogle_step1_btn('deactive');

    //         var _this = $(this);
    //         var _id = _this.attr('data-id');
    //         var _childs = play_app.get_child_cats(_id);

    //         log( '_childs >>>' );
    //         log( _childs );

    //         play_app.selected_cat = _id;

    //         if( _childs ){

    //             var _html = '';
    //             for (var key in _childs){
    //                 if(_childs.hasOwnProperty(key)){
    //                     _html += '<a class="child-cat trackit" data-id="'+_childs[key].term_id+'" ev-category="cats" ev-action="select child cat" ev-label="'+_childs[key].name+'" ev-value="'+_childs[key].term_id+'">'+_childs[key].name+'</a> ';
    //                 }
    //             }

    //             play_app.childs_el.html( _html );

    //             if( _html && _html !== '' ){

    //                 play_app.childs_el.fadeIn('fast');
    //                 play_app.init_child_cats();

    //             }else{
    //                 play_app.childs_el.fadeOut('fast');
    //             }

    //         }else{

    //             // play_app.toogle_step1_btn('active');

    //         }

    //     });

    // },
    // toogle_step1_btn : function (action) {
    //     switch (action) {
    //         case 'active':
    //             play_app.step1_btn.prop('disabled',false).fadeIn('fast');
    //             break;
    //         case 'deactive':
    //             play_app.step1_btn.prop('disabled',true).fadeOut('fast');
    //             break;
    //         default:
    //             break;
    //     }
    // },
    init_dashboard : function () {

        /**
         * dashboard menu
         */
        $('.dashMenuBtn').click(function () {
            $('.dashSide').toggleClass('opened');
        });
        $('.dashSideClose').click(function () {
            $('.dashSide').toggleClass('opened');
        });

        $('.accordion-order > dt > a').click(function(e) {
            var _this = $(this);
            if( _this.attr('href') ){
                // window.location.hash = $(this).attr('href').replace('#','');
                if(history.pushState) {
                    history.pushState(null, null, _this.attr('href'));
                }
                else {
                    location.hash = _this.attr('href');
                }
            }
            e.preventDefault();
            var is_open;
            var next = _this.parent().next();

            if( next.height() > 0 ){
                var is_open = true;
            }else{
                var is_open = false;
            }

            if( is_open ){
                _this.removeClass('is-expanded')
                _this.parent().addClass('not-expanded');
                next.removeClass('active');
            }else{
                _this.addClass('is-expanded');
                _this.parent().removeClass('not-expanded');
                next.addClass('active');
            }

            // _this.toggleClass('is-expanded');
            // if(_this.hasClass('is-expanded')){
            //     _this.removeClass('not-expanded')
            // }else{
            //     _this.addClass('not-expanded')
            // }
            // _this.parent().next().toggleClass('active');
            return false;
        });



        /**
         * notifications
         */
        $('.notif .close').click(function () {
            var notif = $(this).parents('.notif');
            $(notif).fadeOut();
        });
        
        /**
         * orderCarousel 
         */

        if ($(window).width() > 600) {
            if (!$(".orderCarousel").hasClass("owl-carousel")) {
              $(".orderCarousel").addClass("owl-carousel");
            }
            $(".orderCarousel").removeClass("cascade-list");
            $(".orderCarousel").owlCarousel({
              margin: 10,
              autoWidth: true,
              autoplay: true,
              items: 5,
              rtl: true,
              center: false,
              loop: true,
            });
        } else {
            $(".orderCarousel").removeClass("owl-carousel");
            $(".orderCarousel").addClass("cascade-list");
        }

        $('.favNext').click(function () {
            $('#favCarousel').trigger('next.owl.carousel');
        });
        $('.favPrev').click(function () {
            $('#favCarousel').trigger('prev.owl.carousel');
        });


        /**
         * order modal
         */
        $('.openModal').click(function () {
            $('#ordModal').addClass('opened');
        });
        $('.modalOverlay .cancel').click(function () {
            $(this).parents('.modalOverlay').removeClass('opened');
            play_app.page_content.removeClass('blur');
        });


        var ticket_replay = $("#wpas-reply-textarea");
        if( ticket_replay.length > 0 ){

            ticket_replay.emojioneArea({
                pickerPosition: "top",
                tonesStyle: "bullet",
                // events: {
                //     keyup: function (editor, event) {
                //         console.log(editor.html());
                //         console.log(this.getText());
                //     }
                // },
                dir : "rtl",
                attributes: {
                    dir : "rtl",
                }
            });

            $('#emoji-toggle').click(function () {
                $('.emojionearea-button').click()
            })

        }

    },
    init_steps : function () {

        this.init_step_btn();

        play_app.step1_btn.on('click',function (e) {

            e.preventDefault();

            if( !play_app.is_logged_in ){
                play_app.show_login_modal();
                return false;
            }


            if( $(this).not('[disabled]') ){

                // get and show recommends
                // play_app.get_recommends(play_app.selected_cat,play_app.selected_child_cat);

                // init search by url
                // play_app.init_search_by_url();

                var _val = $('#order-step-1 input').val();

                if( !_val || _val == '' ){

                    play_app.makeNotic('لطفا لینک محصول را وارد کنید');

                }else{

                    // show step 1
                    play_app.update_custom_order({
                        'url' : _val,
                    });

                    play_app.toggle_step(2,'show');

                }

            }

        });

        play_app.step2_btn.on('click',function (e) {

            e.preventDefault();

            if( $(this).not('[disabled]') ){

                var _val = $('#order-step-2 input[name="device"]:checked').val();

                if( !_val || _val == '' ){

                    play_app.makeNotic( 'لطفا دستگاه خود را انتخاب کنید' );

                }else{

                    play_app.update_custom_order({
                        'device' : _val,
                    });

                    play_app.toggle_step(3,'show');

                }

            }

        });

        play_app.init_step3();

        $('.digits-login-modal').on('click',function(){
            var dig_verify_mobile_otp_container = $('.dig_verify_mobile_otp_container');
            if( dig_verify_mobile_otp_container.length > 0 && dig_verify_mobile_otp_container.is(':visible') ){
                $('.dig-log-par').hide();
            }
            play_app.close_steps();
        });

    },
    init_step_btn : function () {
        $('.open-step:not(.enabled)').on('click',function (e) {
            e.preventDefault();
            var _this = $(this),
                _step = _this.attr('data-step');
            _action = _this.attr('data-action');
            if( _step ){
                play_app.toggle_step(_step,_action);
            }
        }).addClass('enabled');

        $('#order-step-4 #place_order:not(.enabled)').on('click',function (e) {
            e.preventDefault();
            play_app.toggle_step(6,'show');
            var _this = $(this);
            setTimeout(function () {
                _this.parents('form').submit();
            }, 1000);
        }).addClass('enabled');

    },
    init_step3 : function () {

        var step_3 = $('#order-step-3');

        step_3.find('.product-name').keyup(function () {
            play_app.update_custom_order({
                'name' : $(this).val(),
            });
        });

        step_3.find('.order-desc').keyup(function () {
            play_app.update_custom_order({
                'desc' : $(this).val(),
            });
        });

        step_3.find('.platform_username').keyup(function () {
            play_app.update_custom_order({
                'platform_username' : $(this).val(),
            });
        });

        step_3.find('.platform_password').keyup(function () {
            play_app.update_custom_order({
                'platform_password' : $(this).val(),
            });
        });

        /**
         * platform selection in order modal
         */
        step_3.find('.platform').click(function () {
            $(this).toggleClass('active')
        });
        step_3.find('.platform .select .item').click(function () {

            var id = $(this).attr('data-id');
            var name = $(this).attr('data-name');

            $('.platform .selected .text').html(name);
            play_app.update_custom_order({
                'platform' : $(this).attr('data-id'),
            });

            name = ( name ) ? name : '';
            name = ( id == 'others' ) ? '' : name;

            step_3.find('.platform_username').attr('placeholder','نام کاربری ' + name );
            step_3.find('.platform_password').attr('placeholder','کلمه عبور ' + name );

            $('.platform-data').slideDown('fast');

        });


        /**
         * currency conversion select in order modal
         */
        var _inputPrice = step_3.find('.input-price');
        var _convertedPrice = step_3.find('.converted-price');

        play_app.update_custom_order({
            'currency' : step_3.find('.currSelect').attr('data-type'),
        });

        _inputPrice.keyup(function () {
            var _val = $(this).val();
            if( !_val || _val == '' || _val == ' ' ){
                _val = 0;
            }else{

                if (Number.isNaN(Number.parseFloat(_val))) {
                    _val = 0;
                }
                _val = parseFloat(_val);

                $(this).val( _val );

            }

            play_app.update_custom_order({
                'price' : _val,
            });

            var sale_fee_percent = Number.parseFloat(app_config.sale_fee_percent);
            var fee = (sale_fee_percent && sale_fee_percent > 0) ? ( Number.parseFloat(app_config.sale_fee_percent) / 100 ) * _val : 0;
            fee = fee * Number.parseFloat(app_config.base_price_USD);

            // console.log('sale_fee_percent > ' + sale_fee_percent);
            // console.log('fee > ' + fee);
            // console.log('_val > ' + _val);

            switch (play_app.custom_order_data.currency) {
                case 'USD':
                    // _val = ( _val * Number.parseFloat(app_config.base_price_USD) ) + Number.parseFloat(app_config.base_fee);
                    _val = ( _val * Number.parseFloat(app_config.base_price_USD) ) + fee;
                    break;
                case 'EUR':
                    // _val = ( _val * Number.parseFloat(app_config.base_price_EUR) ) + Number.parseFloat(app_config.base_fee);
                    _val = ( _val * Number.parseFloat(app_config.base_price_EUR) ) + fee;
                    break;
                default:
                    break;
            }

            // console.log('_val new > ' + _val);

            play_app.update_custom_order({
                'converted_price' : _val,
            });

            _convertedPrice.text( _priceNoCurrency.to ( _val ) );
        });

        step_3.find('.currSelect').click(function () {
            $(this).toggleClass('active')
        });
        step_3.find('.currSelect .popUp .item').click(function () {

            var name = $(this).attr('data-name');

            var selected_text = $(this).html();

            step_3.find('.currSelect .selected .text').html(selected_text);

            play_app.update_custom_order({
                'currency' : $(this).attr('data-type'),
            });

            _inputPrice.trigger('keyup');

        });

        step_3.find('.submitBtn').on('click',function (e) {
            e.preventDefault();
            play_app.make_custom_order();
        });

        step_3.find('.existing .closeIcon').click(function () {
            $(this).parents('.existing').css('display', 'none');
        });
        step_3.find('.priceDetailsBox .closeIcon').click(function () {
            $(this).parents('.priceDetailsBox').css('display', 'none');
        });

    },
    update_custom_order : function (data) {
        for (var d in data) {
            if (data.hasOwnProperty(d)) {
                play_app.custom_order_data[d] = data[d];
                switch (d) {
                    case 'device':
                        if( !data[d] ){
                            $('#order-step-2 input[name="device"]').prop('checked',false);
                        }
                        break;
                    default:
                        break;
                }
            }
        }

    },
    close_steps : function () {
        play_app.page_content.removeClass('blur');
        $('body').css('overflow','auto');
        $('.modalOverlay').removeClass('opened');
    },
    toggle_step : function (step,action) {
        // log('toggle_step step >> ' + step + ' action >> ' + action);
        $('#ordModal').addClass('opened');
        $('body').css('overflow','hidden');
        play_app.page_content.addClass('blur');
        var steps_count = 4;
        // if( step > 0 && step <= steps_count ){
        switch (action) {
            case 'show':
                if( step == 0 ){

                    // init search by url
                    play_app.init_search_by_url();

                }else if( step == 1 ){

                    // deactive step 1 button
                    // play_app.toogle_step1_btn('deactive');

                    // reset selected cats
                    // play_app.reset_selected_cats();

                    // setTimeout(function () {
                    //     $('.step-'+step).fadeIn(100);
                    // },300);

                }else if( step == 2 ){

                    play_app.update_custom_order({
                        'device' : null,
                    });

                }else if( step == 3 ){


                }else{
                    // $('.step-'+step).fadeIn(100);
                }

                // hide other steps
                for (var i = 0; i <= steps_count; i++) {
                    if( i != step ) {
                        $('.step-'+i + ':visible').slideUp(100);
                    }
                }

                // setTimeout(function () {
                $('.step-'+step).slideDown(100,function () {

                    if( step == 0 ){
                        $('#order-step-0 input').focus();
                        setTimeout(function () {
                            $('#order-step-0 input').focus();
                        },600);
                    }


                });
                // },300);

                break;
            case 'hide':
                $('.step-'+step).fadeOut(100);
                break;
            default:
                break;
        }

        // }
    },
    show_login_modal : function () {
        play_app.page_content.addClass('blur');
        $('.modalOverlay').removeClass('opened');
        $('#regModal').addClass('opened');
    },
    show_digits_login : function () {
        $('.digits-login-modal').click();
        return false;
    },
    show_product_modal : function (product_id,product_url) {
        if( product_id ){
            play_app.get_product('product_id',product_id,'play_app.after_get_modal_product','play_app.cant_get_modal_product');
        }else if( product_url ){
            // play_app.get_product('product_url',product_url,'play_app.after_get_modal_product','play_app.cant_get_modal_product');
            play_app.get_product('s',product_url,'play_app.after_get_modal_product','play_app.cant_get_modal_product');
        }else{
            play_app.makeNotic('fill search input');
            // play_app.search_by_url_active = false;
        }
    },
    after_get_modal_product : function (products) {
        if( products ){
            // console.log(products);
            // alert('need to redirect to product page');
            // $('.step-3').html( play_app.htmlCompiler(play_app.product_box_template,product) );
            // play_app.toggle_step(3,'show');
            // play_app.init_trackers();
            // play_app.init_add_to_cart();
            var _html = '';
            if( products ){
                for (var d in products) {
                    if (products.hasOwnProperty(d)) {
                        _html += play_app.htmlCompiler(play_app.searchResult_item_template,products[d]);
                    }
                }
            }
            play_app.search_by_url_result.html(_html).slideDown('fast');

        }
        // play_app.search_by_url_active = false;
    },
    cant_get_modal_product : function (params) {

        // if( !play_app.is_logged_in ){
        //     play_app.show_login_modal();
        //     return false;
        // }

        play_app.search_by_url_form.slideUp('fast');

        // // play_app.search_by_url_active = false;
        // $('#order-step-1 input').val( play_app.product_url );
        // play_app.update_custom_order({
        //     'url' : play_app.product_url,
        // });
        // play_app.toggle_step(1,'show');

    },
    search : function (_string) {
        if( !_string || _string == '' || _string.length <= 0 ){
            play_app.search_by_url_result.html('').fadeOut('fast').removeClass('notFound loading');
        }else{
            play_app.search_by_url_result.fadeIn('fast');
            play_app.get_product('s',_string,'play_app.search_by_url_success','play_app.search_by_url_failed');
        }
    },
    init_search_by_url: function () {

        var search_timer = null;

        play_app.search_by_url_form.submit(function (e) {
            e.preventDefault();
            return false;
        });

        play_app.search_by_url_form_input
            .focusout(function(){
                play_app.search_by_url_result.fadeOut('fast').removeClass('loading');
                $('.mainAlert').fadeOut('fast');
            })
            .on('focus',function(){
                if( play_app.search_by_url_result.hasClass('notFound') || play_app.search_by_url_result.find('.item').length > 0 ){
                    play_app.search_by_url_result.fadeIn('fast');
                }
            }) 
            .keyup(function(e){
                play_app.search_by_url_result.html('').removeClass('notFound').addClass('loading').fadeIn('fast');
                clearTimeout(search_timer);
                search_timer = setTimeout(play_app.search.bind(undefined, $(this).val()), 450);
            })
            .on('search', function () {
                clearTimeout(search_timer);
                play_app.search.bind(undefined, $(this).val());
            });

        play_app.search_by_url_form.find('.btn-search').on('click',function (e) {

            e.preventDefault();

            $('.mainAlert').fadeOut('fast');

            var _this = $(this),
                _url = play_app.search_by_url_form.find('input[type="text"]').val();

            _this.addClass('loading');
            // play_app.product_url = _url;

            if( !_url || _url.length < 1 ){

                _this.removeClass('loading');

                play_app.search_by_url_result.slideUp('fast').removeClass('loading notFound');

                play_app.makeNotic('نام بازی یا گیفت کارت را وارد کنید');

            }else{


                play_app.search_by_url_result.removeClass('notFound');

                play_app.search_by_url_result.addClass('loading');
                play_app.search_type = 'keyup';
                play_app.product_url = _url;
                play_app.get_product('s',play_app.product_url,'play_app.search_by_url_success','play_app.search_by_url_failed');

            }

            return false;

        });

    },
    search_by_url_success : function (products) {
        play_app.search_by_url_result.html('').removeClass('notFound');
        if( products ){
            var _html = '';
            if( products ){
                for (var d in products) {
                    if (products.hasOwnProperty(d)) {
                        _html += play_app.htmlCompiler(play_app.searchResult_item_template,products[d]);
                    }
                }
            }
            play_app.search_by_url_result.html(_html).fadeIn('fast');
        }
        play_app.search_by_url_result.removeClass('loading');
    },
    search_by_url_failed : function (params) {
        play_app.search_by_url_result.html('').removeClass('loading').addClass('notFound').fadeIn('fast');
    },
    // init cart page
    init_cart : function () {

        // play_app.lazy.update();

        function verify_code_ajax () {
            var phone = $('#register-phone-input').val();
            var verify_code = $('#verify-phone-input').val();
            $.ajax({
                type: "POST",
                url: app_config.site_url + "/api/otp/v2/verify",
                data: { "phone" : phone , "verify" : verify_code },
                dataType: "json",
                success: function(result) {
                    if (result == '0') {
                        verify_code_ajax();
                        $('.registration-step').removeClass('active');
                        $('#registration-step3').addClass('active');
                        $('#confirmed-phone').val(phone);
                    } else if (result == '1') {
                        setTimeout(function () {
                            location.reload();
                        },500);
                    } else {
                        return false;
                    }
                }
            });
        }

        $('.resend-sms-btn').click(function (e) {
            e.preventDefault();
            var phone = $('#register-phone-input').val();
            send_sms(phone)
        });

        $('#show-instant-registration').click(function (e) {
            e.preventDefault();
            $('.cart-item__holder').fadeOut(0);
            $('.instant-registration').addClass('active');
        });

        $('.step-btn').click(function (e) {
            e.preventDefault();
            var target = '#' + $(this).attr('data-target');

            if (target == '#registration-step1') {
                $('.registration-step').removeClass('active');
                $(target).addClass('active');
            }

            if (target == '#registration-step2') {
                var phone = $('#register-phone-input').val();
                if(phone) {
                    var regex = new RegExp('^(09)?\\d{9}$');
                    var result = regex.test(phone);
                    if (result) {
                        $('.registration-step').removeClass('active');
                        $(target).addClass('active');
                        send_sms(phone)
                    }
                }
            }
            if (target == '#registration-step3') {
                var verify_code = $('#verify-phone-input').val();
                if (verify_code) {
                    var regex_vc = new RegExp('^[0-9]{1,5}$');
                    var result_vc = regex_vc.test(verify_code);
                    if (result_vc) {
                        verify_code_ajax()
                    }
                }
            }

        });
        $(".increase").click(function (e) {
            e.preventDefault();
            // var holder = $(this).parent(".product-count");
            // var input = holder.find("input");
            // input.val(parseInt(input.val()) + 1);
        })
        $(".decrease").click(function (e) {
            e.preventDefault();
            // var holder = $(this).parent(".product-count");
            // var input = holder.find("input");
            // if ((parseInt(input.val()) - 1) >= 0) {
            //     input.val(parseInt(input.val()) - 1);
            // }
        });

        $('.woocommerce-checkout input[required]').on('keyup',function () {
            var _this = $(this);
            if( !_this.val() ){
                _this.addClass('error');
            } else{
                _this.removeClass('error');
            }
        });

        $('.woocommerce-checkout #place_order:not(.enabled)').on('click',function (e) {

            if( $('.woocommerce-checkout').valid() ){
                $('.gateway-content').fadeIn('fast');
                $('form.woocommerce-checkout').fadeOut('fast');
            }else{
                play_app.makeNotic( 'لطفا فیلد های لازم را تکمیل کنید' );
            }

        }).addClass('enabled');

        // var cart_icon = $('.cart-icon');
        // $('.cart-icon > a:not(.enabled)').on('click',function(e){
        //     e.preventDefault();
        //     cart_icon.toggleClass('active');
        // }).addClass('enabled');

        // $('body').on('click',function (e) {
        //     if( !$(e.target).hasClass('cart-icon') && $(e.target).parents('.cart-icon').length <= 0 ){
        //         cart_icon.removeClass('active');
        //     }
        // });

        $('.ajax_add_to_cart:not(.enabled)').on('click',function(e){

            e.preventDefault();
            var _this = $(this),
                _id = _this.attr('data-product_id')

            if( _this.hasClass('remove_from_cart_button') ){
                return;
            }

            _this.addClass('loading');

            $.ajax( {
                type: 'POST',
                url: app_config.site_url + '?wc-ajax=add_to_cart',
                data:{
                    'product_id' : _id,
                    'quantity' : _this.attr('data-quantity'),
                    'product_sku' : _this.attr('data-product_sku'),
                },
                success: function (response) {

                    _this
                        .removeClass('loading enabled add_to_cart_button ajax_add_to_cart')
                        .addClass('remove remove_from_cart_button')
                        .parents('.buy-action')
                        .addClass( 'selected' )
                        .addClass('added');

                    play_app.update_minicart(response);

                    play_app.init_cart();
                    redirect_to_cart()

                },
                error: function (response) {
                    _this.removeClass('loading');
                },
                fail: function (response) {
                    _this.removeClass('loading');
                },
            });

        }).addClass('enabled');

        $('.remove_from_cart_button:not(.enabled)').on('click',function(e){

            e.preventDefault();
            e.stopPropagation();

            var _this = $(this),
                _id = _this.attr('data-product_id');

            if( _this.hasClass('ajax_add_to_cart') ){
                return;
            }

            var cart_form = $('.woocommerce-cart-form');

            // find in cart
            var cart_item = $('.mini-cart .order-item[data-id="'+_id+'"]');
            var in_minCart = ( _this.parents('.cart-icon').length > 0 ) ? true : false;
            var in_cart = ( _this.parents('.cart-content').length > 0 ) ? true : false;
            var in_cart_page = ( cart_form.length > 0 ) ? true : false;
            var in_products_row = ( _this.parents('#products-list').length > 0 ) ? true : false;

            if( in_products_row ){
                _this.addClass('loading');
            }

            if( cart_item.length > 0 ){
                var key = cart_item.attr('data-key');

                if( in_minCart ){
                    _this.parents('.order-item').css({
                        'opacity' : '.8',
                        'cursor' : 'wait'
                    });
                }

                if( in_cart || in_cart_page ){
                    _this.css({
                        'pointer-events' : 'none'
                    });
                    cart_form.css({
                        'opacity' : '.8',
                        'cursor' : 'wait'
                    });
                }


                $.ajax( {
                    type: 'POST',
                    url: app_config.site_url + '?wc-ajax=remove_from_cart',
                    data:{
                        'cart_item_key' : key,
                    },
                    success: function (response) {

                        play_app.update_minicart(response);

                        if( in_cart ){
                            play_app.update_cart();
                        }

                        if( in_products_row ){

                            _this
                                .removeClass('loading enabled remove remove_from_cart_button')
                                .addClass('add_to_cart_button ajax_add_to_cart')
                                .parents('.buy-action')
                                .removeClass( 'selected' )
                                .removeClass('added');

                        }else{

                            var _btn = $('#add-to-cart-' + _id);

                            if( _btn.length > 0 ){

                                _btn
                                    .removeClass('loading enabled remove remove_from_cart_button')
                                    .addClass('add_to_cart_button ajax_add_to_cart')
                                    .parents('.buy-action')
                                    .removeClass( 'selected' )
                                    .removeClass('added');

                            }

                            // if in cart page
                            if( in_cart_page ){
                                play_app.update_cart();
                            }

                        }

                        play_app.init_cart();

                    },
                    error: function () {
                        _this.removeClass('loading');
                        if( in_minCart ){
                            _this.parents('.order-item').css({
                                'opacity' : '1',
                                'cursor' : 'auto'
                            });
                        }

                        if( in_cart || in_cart_page ){
                            _this.css({
                                'pointer-events' : 'auto'
                            });
                            cart_form.css({
                                'opacity' : '1',
                                'cursor' : 'auto'
                            });
                        }
                    },
                    fail: function () {
                        _this.removeClass('loading');
                        if( in_minCart ){
                            _this.parents('.order-item').css({
                                'opacity' : '1',
                                'cursor' : 'auto'
                            });
                        }

                        if( in_cart || in_cart_page ){
                            _this.css({
                                'pointer-events' : 'auto'
                            });
                            cart_form.css({
                                'opacity' : '1',
                                'cursor' : 'auto'
                            });
                        }
                    },
                });
            }else{
                _this.addClass('loading');
            }

        }).addClass('enabled');

    },
    update_cart : function(){

        $.get( window.location , function(data){
            if( data ){
                if( $(data).find('.woocommerce-cart-form').length > 0 ){
                    $('.woocommerce-cart-form').html( $(data).find('.woocommerce-cart-form').html() ).css({
                        'opacity' : '1',
                        'cursor' : 'auto'
                    });
                }else{
                    $('.cart-content').outerHTML( $(data).find('.noData').height('600').outerHTML() ).css({
                        'opacity' : '1',
                        'cursor' : 'auto',
                        'height' : '600px'
                    });
                }
                play_app.init_cart();
            }
        });

    },
    update_minicart : function(response){

        var cart_icon =  $('.cart-icon'),
            minicart =  cart_icon.find('.mini-cart');

        if( response && response.hasOwnProperty('fragments') ){

            if( response.fragments.hasOwnProperty('div.widget_shopping_cart_content') ){

                var widget_shopping_cart = response.fragments['div.widget_shopping_cart_content'];

                minicart.outerHTML( $(widget_shopping_cart).html() );

                minicart =  cart_icon.find('.mini-cart');
                var cart_items = minicart.find('.order-item');

                cart_icon.attr('data-count',cart_items.length);

                if( cart_items.length <= 0 ){
                    cart_icon.addClass('empty');
                }else{
                    cart_icon.removeClass('empty');
                }

            }

        }

        minicart.css('opacity',1);

    },
    // init buy instantly
    init_buy_instantly: function(){
        $('.instantly-active').on('click',function (e) {
            e.preventDefault();
        });
    },
    make_custom_order: function () {

        $('.mainAlert').fadeOut('fast');

        play_app.step3_btn.addClass('disabled loading');

        var form_data = new FormData();
        var has_error = false;

        for (var d in play_app.custom_order_data) {
            if (play_app.custom_order_data.hasOwnProperty(d)) {
                form_data.append(d, play_app.custom_order_data[d]);
            }
        }

        if( form_data ){

            if ( !form_data.has('name') || form_data.get('name') === '' ){
                play_app.makeNotic( 'لطفا نام بازی را وارد کنید' );
                has_error = true;
            }

            if ( !has_error && !form_data.has('platform') || form_data.get('platform') === '' ){
                play_app.makeNotic( 'لطفا پلتفرم را انتخاب کنید' );
                has_error = true;
            }

            if ( !has_error && !form_data.has('platform_username') || form_data.get('platform_username') === '' ){
                play_app.makeNotic( 'لطفا نام کاربری خود را وارد کنید' );
                has_error = true;
            }

            if ( !has_error && !form_data.has('platform_password') || form_data.get('platform_password') === '' ){
                play_app.makeNotic( 'لطفا کلمه عبور خود را وارد کنید' );
                has_error = true;
            }

            if ( !has_error && !form_data.has('price') || form_data.get('price') === '' ){
                play_app.makeNotic( 'لطفا قیمت بازی را وارد کنید' );
                has_error = true;
            }

            if( !has_error ){
                fetch.postForm(play_app.api_make_order_url,form_data,'play_app.successSubmit_product','play_app.failSubmit_product');
            }else{
                play_app.step3_btn.removeClass('disabled loading');
            }

        }else{
            play_app.makeNotic( 'لطفا فیلد های بالا را تکمیل کنید' );
            play_app.step3_btn.removeClass('disabled loading');
        }

    },
    init_submit_product: function () {
        play_app.submit_product_form.submit(function (e) {

            e.preventDefault();

            var _this = $(this);
            // _url = _this.find('input[type="text"]').val();

            // log( 'form is >> ' +  _this.prop('disabled'));
            var form_data = new FormData();

            _this.find('input,textarea').each(function (e) {
                form_data.append($(this).attr('name'), $(this).val());
            });

            // set selected_cat
            // form_data.append('selected_cat', play_app.selected_cat);

            // set selected_child_cat
            // form_data.append('selected_child_cat', play_app.selected_child_cat);

            // set product url
            form_data.append('product_url', play_app.product_url);

            if( !_this.prop('disabled') ){

                _this.prop('disabled',true);

                fetch.postForm(play_app.api_make_order_url,form_data,'play_app.successSubmit_product','play_app.failSubmit_product');

            }

            return false;

        });
    },
    successSubmit_product: function(response){

        if( response['status'] == 'successful' ){

            if( response.hasOwnProperty('pay_form') ){
                // play_app.redirect(response['pay_url']);
                $('#order-step-4').html( response['pay_form'] );
                play_app.update_custom_order({
                    'order_id' : response['id'],
                });
                play_app.init_step_btn();
                play_app.toggle_step(4,'show');
            }

        }

        play_app.step3_btn.removeClass('disabled loading');

    },
    failSubmit_product: function(response){
        var has_message = false;
        if( response.hasOwnProperty( 'responseJSON' ) ){
            if( response.responseJSON.hasOwnProperty('message') ){
                has_message = true;
                play_app.makeNotic(response.responseJSON.message);
            }
        }
        if( !has_message ){
            play_app.makeNotic('خطایی در ثبت سفارش شما پیش آمد، دوباره امتحان کنید');
        }
        play_app.step3_btn.removeClass('disabled loading');
    },
    init_recommend_products: function () {
        $('.recommend-product').on('click',function (e) {

            e.preventDefault();

            var _this = $(this),
                _id = _this.attr('data-id');
            play_app.show_product_modal(_id);

        });
    },
    render_recommends : function (products) {

        var _html = '';
        if( products ){
            products.forEach(function (product) {
                _html += play_app.htmlCompiler(play_app.product_item_template,product);
            });
        }
        return _html;
    },
    after_get_recommend : function (response) {
        $('.search-recommends').html( play_app.render_recommends(response) );
        play_app.init_trackers();
        play_app.init_add_to_cart();
        play_app.init_recommend_products();
    },
    // filter_product_list : function(region,platform,currency,subscribe){

    //     if( play_app.products_list.length <= 0 ) return false;

    //     var items = play_app.products_list.find('.item-product');
    //     var is_giftcard = ( $('.giftcard-content').length > 0 ) ? true : false;
    //     var is_subscribe = false;

    //     if( items.length <= 0 ) return false;

    //     var active_region = ( !region ) ? play_app.products_list.attr('data-region') : region;
    //     if( is_giftcard ){
    //         if( $('#subscribe-all').length > 0 ){
    //             is_subscribe = true;
    //             var active_subscribe = ( !subscribe ) ? play_app.products_list.attr('data-subscribe') : subscribe;
    //         }else{
    //             var active_currency = ( !currency ) ? play_app.products_list.attr('data-currency') : currency;
    //         }
    //     }else{
    //         var active_platform = ( !platform ) ? play_app.products_list.attr('data-platform') : platform;
    //     }

    //     // if( active_region == 'all' ){
    //     //     active_region = 'none';
    //     // }

    //     // if( active_platform == 'all' ){
    //     //     active_platform = 'none';
    //     // }

    //     if( is_giftcard ){
    //         if( is_subscribe ){
    //             var items_subscribe = (!active_subscribe || active_subscribe == 'all') ? items : play_app.products_list.find('.item-product[data-subscribe="'+active_subscribe+'"]');
    //         }else{
    //             var items_currency = (!active_currency || active_currency == 'all') ? items : play_app.products_list.find('.item-product[data-currency="'+active_currency+'"]');
    //         }
    //     }else{
    //         var items_platform = (!active_platform || active_platform == 'all') ? items : play_app.products_list.find('.item-product[data-platform="'+active_platform+'"]');
    //     }
    //     var items_region = (!active_region || active_region == 'all') ? items : play_app.products_list.find('.item-product[data-region="'+active_region+'"]');

    //     if( is_giftcard ){
    //         if( is_subscribe ){
    //             if( active_subscribe && active_subscribe !== 'all' ){
    //                 items_subscribe.slideDown('fast');
    //             }
    //         }else{
    //             if( active_currency && active_currency !== 'all' ){
    //                 items_currency.slideDown('fast');
    //             }
    //         }
    //     }else{
    //         if( items_platform && items_platform !== 'all' ){
    //             items_platform.slideDown('fast');
    //         }
    //     }

    //     if( active_region && active_region !== 'all' ){
    //         items_region.slideDown('fast');
    //     }

    //     // items.slideDown('fast');

    //     if( is_giftcard ){
    //         if( is_subscribe ){
    //             if( !active_subscribe || active_subscribe == 'all' ){
    //                 if( active_region && active_region !== 'all' ){
    //                     items.not(items_region).slideUp('fast');
    //                 }else{
    //                     items.slideDown('fast');
    //                 }
    //                 return;
    //             }
    //         }else{
    //             if( !active_currency || active_currency == 'all' ){
    //                 if( active_region && active_region !== 'all' ){
    //                     items.not(items_region).slideUp('fast');
    //                 }else{
    //                     items.slideDown('fast');
    //                 }
    //                 return;
    //             }
    //         }
    //     }else{
    //         if( !active_platform || active_platform == 'all' ){
    //             if( active_region && active_region !== 'all' ){
    //                 items.not(items_region).slideUp('fast');
    //             }else{
    //                 items.slideDown('fast');
    //             }
    //             return;
    //         }
    //     }


    //     if( !active_region || active_region == 'all' ){
    //         if( is_giftcard ){
    //             if( is_subscribe ){
    //                 if( active_subscribe && active_subscribe !== 'all' ){
    //                     items.not(items_subscribe).slideUp('fast');
    //                 }else{
    //                     items.slideDown('fast');
    //                 }
    //             }else{
    //                 if( active_currency && active_currency !== 'all' ){
    //                     items.not(items_currency).slideUp('fast');
    //                 }else{
    //                     items.slideDown('fast');
    //                 }
    //             }

    //         }else{
    //             if( active_platform && active_platform !== 'all' ){
    //                 items.not(items_platform).slideUp('fast');
    //             }else{
    //                 items.slideDown('fast');
    //             }
    //         }
    //         return;
    //     }


    //     if( is_giftcard ){
    //         if( is_subscribe ){
    //             items
    //                 .not(items_subscribe)
    //                 .not(items_region)
    //                 .slideUp('fast');
    //         }else{
    //             items
    //                 .not(items_currency)
    //                 .not(items_region)
    //                 .slideUp('fast');
    //         }

    //     }else{
    //         items
    //             .not(items_platform)
    //             .not(items_region)
    //             .slideUp('fast');
    //     }

    //     return;

    // },
    init_product_page : function () {
        $('.group-product .load-more').on('click',function (e) {
            e.preventDefault();
            $(this)
                .toggleClass('active')
                .parent().find('[data-ismore="1"]').slideToggle('fast');
        });
        $('.add-review').click(function () {
            $('.review-modal-wrapper').fadeIn(100);
            $('.review-modal-content').fadeIn(300);
        });
        $('.show_shared_account_xbox_rules').click( function () {
            var xbox_capacity_rules = $('#xbox_capacity_rules').html();
            Swal.fire({
                title : 'قوانین اکانت اشتراکی ایکس باکس',
                html : xbox_capacity_rules,
                icon :'',
                confirmButtonText : 'موافقم !'
            })
        });
        $('.show_shared_account_ps4_rules').click( function () {
            var ps4_capacity_rules = $('#ps4_capacity_rules').html();
            Swal.fire({
                title : 'قوانین اکانت اشتراکی پلی استیشن',
                html : ps4_capacity_rules,
                icon :'',
                confirmButtonText : 'موافقم !'
            })
        });
        $('.review-modal-wrapper,.review-modal-content .close-modal').click(function () {
            $('.review-modal-wrapper,.review-modal-content').fadeOut(100);
        });
        $('.review-modal-content .form-submit #submit').click(function (e) {
            if (!$('.review-modal-content .review-rating .rating-on').length > 0 ) {
                e.preventDefault();

                $('.review-modal-content .review-rating').append('<div class="rating-error">لطفا امتیاز خود را وارد کنید.</div>')
            }
        });

        $('.reviews-summary .review-btns .all-reviews').click(function () {
            if ($('.review-list').length == 0 ) return ;
            $('html, body').animate({
                scrollTop: $(".review-list").offset().top - 50
            }, 2000);
        })
        $(window).click(function () {
            $(".select-count").removeClass("active");
        });
        $(".product-detail").mouseover(function () {
            $(this).next(".product-tip").slideDown(200);
        });
        $(".product-detail").mouseleave(function () {
            $(this).next(".product-tip").slideUp(200);
        });
        $(".question-item").click(function () {
            if ($(this).hasClass("active")) {
                $(this).find(".answer").slideUp();
                $(this).removeClass("active");
            } else {
                $(".question-item.active").find(".answer").slideUp();
                $(".question-item.active").removeClass("active");
                $(this).find(".answer").slideDown();
                $(this).addClass("active");
            }
        });
        $(".more-intro,#go-to-about").click(function (e) {
            e.preventDefault()
            $("html, body").animate({
                scrollTop: $("#description-wrap").offset().top - 300
            }, 2000);
        });
        $("#go-to-trailer").click(function (e) {
            e.preventDefault()
            $("html, body").animate({
                scrollTop: $(".trailer-wrapper").offset().top - 300
            }, 2000);
        });
        $(".cat-item").click(function () {
            $(".cat-item.active").removeClass("active");
            $(this).addClass("active");
            var id = $(this).attr("id");
            id = id + "-section";
            $("html, body").animate({
                scrollTop: $("." + id).offset().top - 390
            }, 2000);
        });
        $('#selectedGiftCardHolder,.giftcard-introduction .product-selection .giftcard-options .options-list').hover(function () {
            $('.options-list').fadeIn(0);
        },function () {
            $('.options-list').fadeOut();
        })
        $('.giftcard-introduction .product-selection .giftcard-options .options-list label').click(function () {
            $('.options-list').fadeOut(0);
            $('.buy-action').removeClass('selected');
            $('.add_to_card_giftcard').removeClass('remove remove_from_cart_button');
            $('.add_to_card_giftcard').addClass('add_to_cart_button ajax_add_to_cart')
        })
        if (!$(".wide-carousel").hasClass("owl-carousel")) {
            $(".wide-carousel").addClass("owl-carousel");
        }

        if (!$(".side-product-list").hasClass("owl-carousel")) {
            $(".side-product-list").addClass("owl-carousel");
        }

        $(".wide-carousel").owlCarousel({
            margin: 15,
            autoWidth: true,
            autoplay: true,
            items: 7,
            rtl: true,
            loop: true,
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 1
                },
                500: {
                    items: 2
                },
                800: {
                    items: 3
                },
                900: {
                    items: 6
                }
            }
        });
        $(".side-product-list").owlCarousel({
            margin: 15,
            autoWidth: true,
            autoplay: true,
            items: 7,
            rtl: true,
            loop: true
        });

        // Add To Card Giftcard
        $('.options-list .product-row').click(function () {
            var id = '#'+ $(this).attr('for');
            var product_id = $(id+'+label input').val();
            $('.add_to_cart_button')
                .attr('data-product_id',product_id)
                .attr('id','add-to-cart-'+product_id);
            var current = $(id+'+label').clone().addClass('selected').css({marginBottom:0});
            $('#selectedGiftCardHolder').html(current);
        });

        // trailer scroll
        var playerContainer = $('#about-product iframe');
        if( playerContainer.length > 0 ){
            $('.trailer-btn').on('click',function (e) {
                e.preventDefault();
                var _top = playerContainer.offset().top - $('.searchbar').height();
                $([document.documentElement, document.body]).animate({
                    scrollTop: _top
                }, 300,'swing');
            });
        }else{
            $('.trailer-btn').hide();
        }

    },
    /**
     * Favorites
     */
    remove_fav_item : function () {
        $('#favCarousel .simplefavorite-button.active').on('click',function() {
            $(this).parents('.owl-item').remove();
            $('body').trigger('resize');
        });

    },
    init_fav : function () {
        var _ajax_fav_btn = $('.ajax-fav-btn .simplefavorite-button');
        var _fake_fav_btns = $('.social-item.like,.btn-favorite');
        if( _ajax_fav_btn ){
            var __ajax_fav_btn_class = _ajax_fav_btn.attr('class');
            var _fav_interval;
            _fake_fav_btns.on('click',function (e) {
                e.preventDefault();
                var _this = $(this);
                if( !app_config.is_logged_in ){
                    play_app.show_digits_login();
                    return false;
                }
                if( _this.hasClass('loading') ){
                    return false;
                }
                _ajax_fav_btn.click();
                _fake_fav_btns.addClass('loading');
                _fav_interval = setInterval(function () {
                    if( __ajax_fav_btn_class !== _ajax_fav_btn.attr('class') ){
                        setTimeout(function() {
                            if( _ajax_fav_btn.hasClass('active') ){
                                _fake_fav_btns.addClass('active');
                                if( _this.hasClass('btn-favorite') ){
                                    _this.text('حذف از علاقه مندی های من');
                                }
                                play_app.makeNotic('تبریک میگم','محصول به علاقه مندی های شما افزوده شد','success');
                            }else{
                                _fake_fav_btns.removeClass('active');
                                if( _this.hasClass('btn-favorite') ){
                                    _this.text('افزودن به علاقه مندی ها');
                                }
                                play_app.makeNotic('تبریک میگم','محصول از علاقه مندی های شما حذف شد','success');
                            }
                            __ajax_fav_btn_class = _ajax_fav_btn.attr('class');
                            _fake_fav_btns.removeClass('loading');
                        }, 1000);
                        clearInterval(_fav_interval);
                    }
                },100);
            });
        }
        if( $('#favCarousel').length > 0 ){
            var find_fav_interval;
            find_fav_interval = setInterval(function () {
                if( $('#favCarousel .simplefavorite-button.active').length > 0 ){
                    play_app.remove_fav_item();
                    clearInterval(find_fav_interval);
                }
            },100);
        }
        if( $('.favResult').length > 0 ){
            $('.favResult .simplefavorite-button.active').on('click',function() {
                $(this).parents('.product-item').remove();
            });
        }
    },
    /**
     * Confirm Phone
     */
    init_confirm_phone : function () {

        $('.confirm_phone').on('click',function (e) {

            e.preventDefault();

            var _this = $(this);
            var _phone = $('.confirm_phone_num').val();

            _this
                .addClass('disabled loading')
                .prop('disabled',true);

            if( !_phone || _phone == '' || _phone.length <= 0 ){
                $('#emptyCodeError').fadeIn();
                $('#sendCodeError').fadeOut();
                $('#sendCodeNotic').fadeOut(function () {
                    $('.confirm_phone_num').focus();
                });
                _this
                    .removeClass('disabled loading')
                    .prop('disabled',false);
                return;
            }else{
                $('#emptyCodeError').fadeOut();
                $('#sendCodeError').fadeOut(function () {
                    $('#sendCodeNotic').fadeIn();
                });
            }

            var data = {
                phone: _phone,
            };

            fetch.post( play_app.api_confirm_phone_url, data, 'play_app.success_send_phone_confirm_code','play_app.failed_send_phone_confirm_code',$(this) );

        });

        $('.verify_phone_code').on('click',function (e) {

            e.preventDefault();

            var _this = $(this),
                _code = $('.confirm_phone_code').val();

            var data = {
                code: _code,
            };

            _this.addClass('loading');

            // fetch.post( play_app.api_confirm_phone_url, data, 'play_app.success_added_to_cart','play_app.failed_added_to_cart',$thisbutton);
            fetch.post( play_app.api_confirm_phone_url, data, 'play_app.success_confirm_code','play_app.failed_confirm_code', _this );

        });

        $('.change_phone').on('click',function (e) {

            $('.confirm_phone_num').val('').focus();
            $('.confirm_phone_code').val('');

        });

        $('.recall_confirm_phone').on('click',function (e) {
            $('.confirm_phone_code').val('').focus();
        });

    },
    success_confirm_code: function (response,$thisbutton) {
        play_app.makeNotic('تبریک میگم','شماره تلفن شما با موفقیت تایید شد','success');
        play_app.reload();
        // play_app.redirect(window.location);
    },
    failed_confirm_code: function (response,$thisbutton) {
        if( response && response.hasOwnProperty['message'] ){
            play_app.makeNotic(response['message'],'success');
        }else{
            play_app.makeNotic('','مشکلی در تایید شماره تلفن شما پیش آمد. دوباره امتحان کنید','failed');
        }
        $thisbutton.removeClass('loading');
    },
    success_send_phone_confirm_code: function (response,$thisbutton) {

        $('#emptyCodeError').fadeOut();
        $('#sendCodeError').fadeOut();
        $('#sendCodeNotic').fadeIn();

        play_app.redirect($thisbutton.attr('href'));

        setTimeout(function () {
            $('.confirm_phone_code').focus();
        },100);

        setTimeout(function () {
            $('.confirm_phone_code').focus();
        },500);

        $thisbutton.removeClass('loading').removeClass('disabled');
        $thisbutton.prop('disabled',false);

    },
    failed_send_phone_confirm_code: function (response,$thisbutton) {

        $('#sendCodeNotic').fadeOut();
        $('#emptyCodeError').fadeOut();
        $('#sendCodeError').fadeIn();

        $thisbutton.removeClass('loading').removeClass('disabled');
        $thisbutton.prop('disabled',false);
    },
    /**
     * Identity
     */
    init_identity : function () {

        var identityForm = $('#identityForm');

        if( identityForm.length <= 0 ) return false;

        var identity_inputs = identityForm.find('input[type="email"],input[type="text"]');
        var idCard_input = identityForm.find('#idCard');
        var identity_submitForm = identityForm.find('.submitForm');
        var phoneDiv = identityForm.find('.phoneDiv');
        var idCardDiv = identityForm.find('.idCardDiv');

        identity_inputs.not('.confirm_phone_num').keyup(function () {

            var _this = $(this);
            var pass = true;

            if( _this.val().length > 0 ){

                if( _this.attr('type') == 'email' ){
                    if( !play_app.validateEmail( _this.val() ) ){
                        pass = false;
                    }
                }

                if( pass ){
                    identity_inputs.not('.confirm_phone_num').each(function () {
                        if( $(this).val().length <= 0 ){
                            pass = false;
                        }
                    });
                }

                if( pass ){
                    if( !phoneDiv.hasClass('verified') ){
                        pass = false;
                    }
                }

                if( pass ){
                    if( !idCardDiv.hasClass('verified') ){
                        pass = false;
                    }
                }

            }else{
                pass = false;
            }

            if( pass ){
                identity_submitForm.prop('disabled',false);
            }else{
                identity_submitForm.prop('disabled',true);
            }

        });

        idCard_input.change(function (e) {

            var _this = $(this)[0];

            var myFormData = new FormData();
            myFormData.append('idCard', _this.files[0]);

            $.ajax({
                url: play_app.api_identity_url,
                type: 'POST',
                processData: false, // important
                contentType: false, // important
                dataType : 'json',
                data: myFormData
            });

        });

        identityForm.submit(function (e) {

            e.preventDefault();

            var _this = $(this);
            var _name = _this.find('input[name="your-name"]').val();
            var _family = _this.find('input[name="your-family"]').val();
            var _email = _this.find('input[name="your-email"]').val();

            var data = {
                name: _name,
                family: _family,
                email: _email,
            };

            fetch.post( play_app.api_identity_url, data, 'play_app.success_save_identity','play_app.failed_save_identity',$(this) );

        });

    },
    success_save_identity: function (response,$thisbutton) {

        $('#emptyCodeError').fadeOut();
        $('#sendCodeError').fadeOut();
        $('#sendCodeNotic').fadeIn();

        setTimeout(function () {
            $('.confirm_phone_code').focus();
        },100);

        setTimeout(function () {
            $('.confirm_phone_code').focus();
        },500);

        $thisbutton.removeClass('loading').removeClass('disabled');
        $thisbutton.prop('disabled',false);

    },
    failed_save_identity: function (response,$thisbutton) {
        $('#sendCodeNotic').fadeOut();
        $('#emptyCodeError').fadeOut();
        $('#sendCodeError').fadeIn();

        $thisbutton.removeClass('loading').removeClass('disabled');
        $thisbutton.prop('disabled',false);
    },
    /**
     * Archive
     */
    init_archive : function () {

        var time = 4; // time in seconds
        var $elem,
            isPause,
            tick,
            percentTime;
        $(".archive-carousel").owlCarousel({
            slideSpeed: 500,
            paginationSpeed: 500,
            singleItem: true,
            onInitialized: progressBar,
            onTranslated: moved,
            onDrag: pauseOnDragging,
            loop: true,
            dots: true,
            autoplay: true,
            items: 1,
            rtl: true,
        });

        function progressBar() {
            $elem = $(".carousel-holder");
            start();
        }
        function start() {
            percentTime = 0;
            isPause = false;
            tick = setInterval(interval, 10);
        };
        function interval() {
            if (isPause === false) {
                percentTime += 1 / time;
                $(".progress").css({
                    width: percentTime + "%"
                });
                if (percentTime >= 100) {
                    $elem.trigger('owl.next')
                }
            }
        }
        function pauseOnDragging() {
            isPause = true;
        }
        function moved() {
            clearTimeout(tick);
            start();
        }

        $(".product-carousel").owlCarousel({
            margin: 10,
            autoWidth: true,
            items: 3,
            rtl: true,
            center: true,
            loop: true,
            autoplay: true,
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 1
                },
                500: {
                    items: 2
                },
                800: {
                    items: 3
                },
                900: {
                    items: 6
                }
            }
        });

        $(".toggle-title").click(function () {
            var title = $(this),
                section = $(this).parent(".expandable"),
                content = section.find('.toggle-content');

            if (section.hasClass("open")) {
                section.removeClass("open");
                content.slideUp();
            }
            else {
                section.addClass("open");
                content.slideDown();
            }
        });

        var range = $('#range');
        if( range.length > 0 ){

            if( !range[0].noUiSlider ){

                var min_price = range.attr('data-minprice');
                var min_price_val = range.attr('data-minpriceval');
                var max_price = range.attr('data-maxprice');
                var max_price_val = range.attr('data-maxpriceval');
                var mininput = $('.input-price-holder .mininput');
                var maxinput = $('.input-price-holder .maxinput');

                noUiSlider.create(range[0], {
                    start: [parseInt(min_price_val), parseInt(max_price_val)],
                    connect: true,
                    range: {
                        'min': parseInt(min_price),
                        'max': parseInt(max_price)
                    },
                    step: 1,
                    direction: 'rtl',
                    format: {
                        // 'to' the formatted value. Receives a number.
                        to: function (value) {
                            return value;
                        },
                        // 'from' the formatted value.
                        // Receives a string, should return a number.
                        from: function (value) {
                            return Number(value.replace('', ''));
                        }
                    }
                });

                range[0].noUiSlider.on('change', function (values, handle, unencoded, tap, positions) {
                    var min = _priceNoCurrency.to ( values[0] );
                    var max = _priceNoCurrency.to ( values[1] );
                    play_app.product_result({
                        'min_price' : values[0],
                        'max_price' : values[1],
                    });
                    mininput.attr('placeholder',min);
                    maxinput.attr('placeholder',max);
                });

            }

        }

        $('.sort-holder').on('click',function (e) {
            var _this = $(this),
                _target = $(e.target);
            if( _target.is('label') || _target.parents('.select-item').length > 0 ){
                _this.removeClass('hover');
            }else{
                _this.addClass('hover');
            }
        });
        $(document).on('click',function (e) {
            var _target = $(e.target);
            if( !_target.is('.sort-holder') && _target.parents('.sort-holder').length <= 0 ){
                $('.sort-holder').removeClass('hover');
            }
        });

        $('#orders-filter .sortby input').change(function () {
            var _this = $(this),
                _val = _this.val();

            play_app.product_result({
                'order_by' : _val
            });

        });
        $('#orders-filter .result-search form').submit(function (e) {
            e.preventDefault();
            var _this = $('#orders-filter .result-search input'),
                _val = _this.val();

            play_app.product_result({
                'search' : _val
            });

        });

        $('.filter-box input[type="checkbox"]').change(function () {

            var _this = $(this),
                _name = _this.attr('name');

            var arr = [];

            $('input[name="'+_name+'"]:checked').each(function(){
                arr.push($(this).val());
            });

            var args = {};
            args[_name.replace('[]','')] =  arr.join(',');

            play_app.product_result(args);

        });

        $('#delete-filters').on('click',function (e) {
            e.preventDefault();
            play_app.product_result(false);
            $(".close-filter").click();
        });

        $(".open-filter").click(function () {
            $(".filter-section").addClass("responsive");
            $(".filter-section-bg").addClass("active");
            $('body').css('overflow','hidden');
        });

        $(".close-filter,.filter-section-bg").click(function () {
            $(".filter-section").removeClass("responsive");
            $(".filter-section-bg").removeClass("active");
            $('body').css('overflow','auto');
        });
        var products_interval;
        $('.more-product-btn').click(function (e) {
            e.preventDefault();
            var _this = $(this);
            _this.addClass('loading');
            var next_link = _this.attr('data-nexturl');
            if( next_link && next_link !== '' ){
                play_app.product_result(null,next_link,true);
                products_interval = setInterval(function () {
                    if( !$('#product-result-wrapper').hasClass('loading') ){
                        clearInterval(products_interval);
                    }
                });
            }
        });

    },
    product_result : function (args,custom_url,append) {

        if( $('#product-result-wrapper').hasClass('loading') ){
            return;
        }

        $('#product-result-wrapper').addClass('loading');

        var urlParams = new URLSearchParams(location.search);
        var more_btn = $('.more-product-btn');
        var has_filters = false;
        var url;
        var base_url;

        console.log('custom_url >> ' + custom_url);
        console.log('location.href >> ' + location.href);

        if(!custom_url) {
            var n = location.href.indexOf('?');
            base_url = location.href.substring(0, n != -1 ? n : location.href.length);
            n = base_url.indexOf('page');
            base_url = base_url.substring(0, n != -1 ? n : base_url.length);
        }else{
            var n = custom_url.indexOf('?');
            base_url = custom_url.substring(0, n != -1 ? n : custom_url.length);
            // base_url = custom_url.split('?')[0];
            // base_url = custom_url;
        }

        console.log('base_url >> ' + base_url);

        // console.log('product_result');
        // console.log(args);

        if( !args ){

            var keys = urlParams.keys();
            var keys = play_app.getUrlVars();

            if( keys ){
                for (var key in keys) {
                    if (keys.hasOwnProperty(key)) {
                        if (key == 'order' || key == 'order_by') {} else {
                            urlParams.delete(key);
                        }
                    }
                }
            }

            // new url
            url = base_url + '?' + urlParams.toString();

            var range = $('#range');
            if( range.length > 0 ){
                var min_price =  range.attr('data-minprice');
                var max_price = range.attr('data-maxprice');
                $('.input-price-holder .mininput').attr('placeholder',_priceNoCurrency.to ( parseInt(min_price) ));
                $('.input-price-holder .maxinput').attr('placeholder',_priceNoCurrency.to ( parseInt(max_price) ) );
                range[0].noUiSlider.reset();
            }

        }else{

            // order
            if( !args.hasOwnProperty('order') ){
                if( urlParams.get('order') ){
                    args['order'] = urlParams.get('order');
                }
            }
            if( args.hasOwnProperty('order') ){
                has_order = true;
                urlParams.set("order", args['order']);
            }

            // order_by
            if( !args.hasOwnProperty('order_by') ){
                if( urlParams.get('order_by') ){
                    args['order_by'] = urlParams.get('order_by');
                }
            }
            if( args.hasOwnProperty('order_by') ){
                has_order_by = true;
                urlParams.set("order_by", args['order_by']);
            }

            // min price
            if( !args.hasOwnProperty('min_price') ){
                if( urlParams.get('min_price') ){
                    args['min_price'] = urlParams.get('min_price');
                }
            }
            if( args.hasOwnProperty('min_price') ){
                has_filters = true;
                urlParams.set("min_price", args['min_price']);
            }

            // max price
            if( !args.hasOwnProperty('max_price') ){
                if( urlParams.get('max_price') ){
                    args['max_price'] = urlParams.get('max_price');
                }
            }
            if( args.hasOwnProperty('max_price') ){
                has_filters = true;
                urlParams.set("max_price", args.max_price);
            }

            // platforms
            // if( args.hasOwnProperty('platforms') ){
            //     has_filters = true;
            //     urlParams.set("platforms", args.platforms);
            // }


            if( args.hasOwnProperty('game_platform') ){
                has_filters = true;
                urlParams.set("game_platform", args.game_platform);
            }

            // regions
            if( args.hasOwnProperty('regions') ){
                has_filters = true;
                urlParams.set("regions", args.regions);
            }

            // game_cat
            if( args.hasOwnProperty('game_cat') ){
                has_filters = true;
                urlParams.set("game_cat", args.game_cat);
            }
            if(args.hasOwnProperty('per_page') ){
                urlParams.set("per_page", args.per_page);
            }

            if(args.hasOwnProperty('search') ){
                urlParams.set("search", args.search);
            }

            // new url
            url = base_url + '?' + urlParams.toString();

        }

        console.log('url >> ' + url);

        $.ajax({
            url: url,
            type:'GET',
            success: function(data){

                history.replaceState({}, document.title, url);

                if( append ){
                    $($(data).find('#product-result-wrapper').html()).appendTo($('#product-result-wrapper'));
                }else{
                    $('#product-result-wrapper').html($(data).find('#product-result-wrapper').html());
                    $('html,body').animate({scrollTop:$('.rout-holder').offset().top}, 500);
                }

                var next_url = $(data).find('.more-product-btn').attr('data-nexturl');
                if( next_url && next_url !== '' ){
                    more_btn.attr('data-nexturl',next_url);
                }else{
                    more_btn.fadeOut()
                }

                if( !has_filters ){
                    $('#delete-filters').fadeOut('fast');
                }else{
                    $('#delete-filters').fadeIn('fast');
                }

                play_app.init_archive();

                $('#product-result-wrapper').removeClass('loading');

                more_btn.removeClass('loading');

            },
            failed : function () {
                $('#product-result-wrapper').removeClass('loading');
                more_btn.removeClass('loading');
            },
            error : function () {
                $('#product-result-wrapper').removeClass('loading');
                more_btn.removeClass('loading');
            }
        });

    },

    igame_otp: function () {
        /**
         * Login and register part
         */

        // opening modal
        $('.open-login-modal,.digits-login-modal').click(function (e) {
            e.preventDefault();
            $('#login-modal-wrapper, #login-modal-content').css('display', 'block');
        });
        $('.login-modal-wrapper').click(function () {
            $('.login-modal-wrapper, .login-modal-content').css('display', 'none');
            $('body').css('overflow','auto');
        });

        // cart page login
        $('#cart-login-link').click(function () {
            $('#cart-login-form').css('display', 'block');
            $('#cart-items-holder').css('display', 'none');
        });

        // countdown
        var seconds=59;
        var timer;
        function countdown() {
            if(seconds <= 59) {
                $('.loginVerifyCtd').attr('data-time', '0:'+seconds);
            }
            if (seconds >0 ) {
                seconds--;
            } else {
                clearInterval(timer);
                $.ajax({
                    url: $('#ajax_url').val(),
                    type: 'post',
                    data: {
                        action: 'otp_expire',
                    },
                    success: function() {
                        $('.verify-resend').css('display', 'block');
                    },
                });
            }
        }

        // back to step 1 button
        $('#login-step-1').click(function () {
            $('#login-step2').removeClass('active');
            $('#login-step2').css('display', 'none');
            $('#login-step1').addClass('active');
            $('#login-step1').css('display', 'block');
            $('#send-otp-error, #verify-otp-error').css('display', 'none');
        });

        // send verify code
        $('#login-phone-button').click(function () {
            var phone = $('#login-phone-input').val();
            $.ajax({
                url: $('#ajax_url').val(),
                type: 'post',
                data: {
                    action: 'otp_send',
                    phone: phone,
                },
                beforeSend: function (){
                    $('#login-phone-button').addClass('loading');
                },
                success: function(data) {
                    $('#login-phone-button').removeClass('loading');
                    if( data.status ){
                        $('#login-step1').removeClass('active');
                        $('#login-step1').css('display', 'none');
                        $('#login-step2').addClass('active');
                        $('#login-step2').css('display', 'block');
                        seconds = 59;
                        $('.loginVerifyCtd').attr('data-time', '1:00');
                        $('.verify-resend').css('display', 'none');
                        if(!timer) {
                            timer = window.setInterval(function() {
                                countdown();
                            }, 1000);
                        }
                    }
                    else {
                        $('#send-otp-error').css('display', 'block');
                        $('#send-otp-error').html(data.errors);
                    }
                },
            });
            $('.verify-resend-btn').attr('data-phone', phone);
        });

        // verify phone with received code
        $('#verify-phone-button').click(function () {
            $.ajax({
                url: $('#ajax_url').val(),
                type: 'post',
                data: {
                    action: 'otp_verify',
                    code: $('#verify-phone-input').val(),
                },
                beforeSend: function (){
                    $('#verify-phone-button').addClass('loading');
                },
                success: function(data) {
                    $('#verify-phone-button').removeClass('loading');
                    if (data.response != undefined){
                        if( !data.response.verified ){
                            $('#verify-otp-error').css('display', 'block');
                            $('#verify-otp-error').html(data.errors);
                        }
                    }
                    else {
                        if ( $('#login-step3').length == 0 ) {
                            window.location.href = "" + $('#site_url').val() + "dashboard";
                        }
                        else {
                            $('#login-step1, #login-step2').css('display', 'none');
                            $('#login-step3').css('display', 'block');
                            $('#confirmed-phone').val( $('.verify-resend-btn').attr('data-phone') );
                            setTimeout(function() {
                                window.location.href = "" + $('#site_url').val() + "checkout";
                            }, 3000);
                        }
                    }
                },
            });
        });

        // resend verify code
        $('.verify-resend-btn').click(function () {
            var phone = $(this).attr('data-phone');
            $.ajax({
                url: $('#ajax_url').val(),
                type: 'post',
                data: {
                    action: 'otp_send',
                    phone: phone,
                },
                success: function(data) {
                    if( data.status ){
                        seconds = 59;
                        $('.loginVerifyCtd').attr('data-time', '1:00');
                        $('.verify-resend').css('display', 'none');
                        setInterval(function() {
                            countdown();
                        }, 1000);
                    }
                },
            });
            $('.verify-resend-btn').attr('data-phone', phone);
            $('#verify-otp-error').css('display', 'none');
        });
    }
};


(function ($) {
    'use strict';

    /**
     * Document ready
     */
    $(function () {

        play_app.init();

    });

})
($);
