$(document).ready(function () {
  $(".action-social__item.like").click(function () {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
    } else {
      $(this).addClass("active");
    }
  });
  $(".btn-favorite").click(function () {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
    } else {
      $(this).addClass("active");
    }
  });
  $(".open-add").click(function () {
    var parent = $(this).parent(".buy-action");
    var child = parent.find(".select-count");
    child.addClass("active");
  });
  $(".increase").click(function () {
    var holder = $(this).parent(".product-count");
    var input = holder.find("input");
    input.val(parseInt(input.val()) + 1);
  });
  $(".decrease").click(function () {
    var holder = $(this).parent(".product-count");
    var input = holder.find("input");
    if (parseInt(input.val()) - 1 >= 0) {
      input.val(parseInt(input.val()) - 1);
    }
  });

  $(window).click(function () {
    $(".select-count").removeClass("active");
  });

  $(".buy-action").click(function (event) {
    event.stopPropagation();
  });

  $(".add-product").click(function () {
    var parent = $(this).parents(".buy-action");
    parent.addClass("selected");
    parent.find(".select-count").removeClass("active");
  });

  $(".filter-toggle").click(function () {
    if (!$(".filter-responsive").hasClass("active")) {
      $(".filter-responsive").addClass("active");
    }
  });

  $(".do-filter").click(function () {
    $(".filter-responsive").removeClass("active");
  });
  $(".close-filter").click(function () {
    $(".filter-responsive").removeClass("active");
  });

  $(".item-product").mouseover(function () {
    $(this).find(".product-tip").slideDown();
  });

  $(".item-product").mouseleave(function () {
    $(this).find(".product-tip").slideUp();
  });

  $(".question-item").click(function () {
    if ($(this).hasClass("active")) {
      $(this).find(".answer").slideUp();
      $(this).removeClass("active");
    } else {
      $(".question-item.active").find(".answer").slideUp();
      $(".question-item.active").removeClass("active");
      $(this).find(".answer").slideDown();
      $(this).addClass("active");
    }
  });
  $(".more-intro").click(function () {
    $("html, body").animate(
      {
        scrollTop: $(".description-wrapper").offset().top,
      },
      2000
    );
  });
  $(".cat-item").click(function () {
    $(".cat-item.active").removeClass("active");
    $(this).addClass("active");
    var id = $(this).attr("id");
    id = id + "-section";
    $("html, body").animate(
      {
        scrollTop: $("." + id).offset().top,
      },
      2000
    );
  });
  if ($(window).width() > 600) {
    $.getScript("../../dist/scripts/owl.carousel.min.js", function (
      data,
      textStatus,
      jqxhr
    ) {
      if (!$(".wide-carousel").hasClass("owl-carousel")) {
        $(".wide-carousel").addClass("owl-carousel");
      }
      if (!$(".side-product-list").hasClass("owl-carousel")) {
        $(".side-product-list").addClass("owl-carousel");
      }
      $(".wide-carousel").removeClass("cascade-list");
      $(".side-product-list").removeClass("cascade-list");

      $(".wide-carousel").owlCarousel({
        margin: 15,
        autoWidth: true,
        autoplay: true,
        items: 7,
        rtl: true,
        loop: true,
        responsive: {
          0: {
            items: 1,
          },
          400: {
            items: 1,
          },
          500: {
            items: 2,
          },
          800: {
            items: 3,
          },
          900: {
            items: 6,
          },
        },
      });
      $(".side-product-list").owlCarousel({
        margin: 15,
        autoWidth: true,
        autoplay: true,
        items: 7,
        rtl: true,
        loop: true,
      });
    });
  } else {
    $(".wide-carousel").removeClass("owl-carousel");
    $(".side-product-list").removeClass("owl-carousel");
    $(".wide-carousel").addClass("cascade-list");
    $(".side-product-list").addClass("cascade-list");
  }

  //**********Tab description */
  $(".tab-holder li").click(function () {
    if (!$(this).hasClass("active")) {
      $(".tab-holder li.active").removeClass("active");
      $(this).addClass("active");
    }
    var id = $(this).attr("id");
    loadContent($("#" + id + "-content"));
  });

  function loadContent(conName) {
    $(".tab-content.active").removeClass("active");
    conName.addClass("active");
  }
});
