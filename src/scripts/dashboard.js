$(document).ready(function () {
  /**
   * dashboard menu
   */
  $(".dashMenuBtn").click(function () {
    $(".dashSide").toggleClass("opened");
  });
  $(".dashSideClose").click(function () {
    $(".dashSide").toggleClass("opened");
  });
  $(".responsive-avatar").click(function () {
    if ($(".dashSide").hasClass("opened")) {
      $(".modal-bg").removeClass("active");
      $(".dashSide").removeClass("opened");
      $("html").css({
        overflow: "auto",
      });
    } else {
      $(".modal-bg").addClass("active");
      $(".dashSide").addClass("opened");
      $("html").css({
        overflow: "hidden",
      });
    }
  });
  $(".modal-bg").click(function (e) {
    e.stopPropagation();
    $(".modal-bg").removeClass("active");
    $(".dashSide").removeClass("opened");
    $("html").css({
      overflow: "auto",
    });
  });

  /**
   * notifications
   */
  $(".notif .close").click(function () {
    var notif = $(this).parents(".notif");
    $(notif).fadeOut();
  });
  /**
   * New games carousel
   */

  $("#newGamesNext").click(function () {
    $("#newGamesCarousel").trigger("next.owl.carousel");
  });
  $("#newGamesPrev").click(function () {
    $("#newGamesCarousel").trigger("prev.owl.carousel");
  });
  /**
   * favorite games carousel
   */

  $(".favNext").click(function () {
    $("#favCarousel").trigger("next.owl.carousel");
  });
  $(".favPrev").click(function () {
    $("#favCarousel").trigger("prev.owl.carousel");
  });

  /**
   * order modal
   */
  $(".openModal").click(function () {
    $("#ordModal").addClass("opened");
  });
  $(".modalOverlay .cancel").click(function () {
    $(this).parents(".modalOverlay").removeClass("opened");
  });

  $("#step-1-submit").click(function () {
    $("#order-step-1").css("display", "none");
    $("#order-step-2").css("display", "block");
  });

  /**
   * platform selection in order modal
   */
  $(".platform").click(function () {
    $(this).toggleClass("active");
  });
  $(".platform .select .item").click(function () {
    var selected_text = $(this).children(".persian").html();
    $(".platform .selected .text").html(selected_text);
  });

  /**
   * currency conversion select in order modal
   */
  $(".currSelect").click(function () {
    $(this).toggleClass("active");
  });
  $(".currSelect .popUp .item").click(function () {
    var selected_text = $(this).html();
    $(".currSelect .selected .text").html(selected_text);
  });

  $(".existing .closeIcon").click(function () {
    $(this).parents(".existing").css("display", "none");
  });
  $(".priceDetailsBox .closeIcon").click(function () {
    $(this).parents(".priceDetailsBox").css("display", "none");
  });

  //*********** */
  $("#newGamesCarousel").owlCarousel({
    margin: 5,
    loop: true,
    autoplay: true,
    items: 1,
    rtl: true,
    responsive: {
      0: {
        items: 1,
      },
      768: {
        items: 2,
      },
      992: {
        items: 1,
      },
    },
  });

  $(".rgame").change(function () {
    var ht = $(this).next("label").html();
    $(".product-code.selected").html(ht);
  });

  if ($(window).width() > 600) {
    if (!$(".orderCarousel").hasClass("owl-carousel")) {
      $(".orderCarousel").addClass("owl-carousel");
    }
    $(".orderCarousel").removeClass("cascade-list");
    $(".orderCarousel").owlCarousel({
      margin: 10,
      autoWidth: true,
      autoplay: true,
      items: 5,
      rtl: true,
      center: false,
      loop: true,
    });
  } else {
    $(".orderCarousel").removeClass("owl-carousel");
    $(".orderCarousel").addClass("cascade-list");
  }
});

// $(document).click(function (event) {
//   var $target = $(event.target);
//   if (
//     !$target.closest(".dashSide").length &&
//     $(".dashSider").hasClass("opened")
//   ) {
//     $(".dashSide").removeClass("opened");
//     $("html").css({
//       overflow: "auto",
//     });
//   }
// });
