$(document).ready(function () {
  $(".mini-menu").click(function () {
    $(".responsive-menu").addClass("active");
  });
  $(".menu-close").click(function () {
    $(".responsive-menu").removeClass("active");
  });
  /**
   * smooth scroll on anchors
   */

  $('a[href^="#"]').on("click", function (e) {
    e.preventDefault();
    document.querySelector($(this).attr("href")).scrollIntoView({
      behavior: "smooth"
    });
  });
  /**
   * FAQ
   */

  $(".questions .item").click(function () {
    var idx = $(this).data("question");
    $(".questions .item").removeClass("active");
    $(this).addClass("active");
    $(".answer .item").removeClass("active");
    $("#ans-" + idx + "").addClass("active");
  });
  $(".cart-icon").click(function (e) {
    e.stopPropagation();

    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
    } else {
      $(this).addClass("active");
    }
  });
  $(".top-head").addClass("sticky"); // var num = 30;
  // $(window).bind("scroll", function () {
  //   if ($(window).scrollTop() > num) {
  //     $(".searchbar").addClass("sticky");
  //   } else {
  //     $(".searchbar").removeClass("sticky");
  //   }
  // });

  $(document).mouseup(function (e) {
    var container = $(".input-search");

    if (!container.is(e.target) && container.has(e.target).length === 0) {
      container.removeClass("active");
    }
  });
});